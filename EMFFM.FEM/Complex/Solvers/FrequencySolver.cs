﻿using System;
using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.Common.Math;
using EMFFM.FEM.Helpers;
using EMFFM.Input;
using EMFFM.Input.Enums;

namespace EMFFM.FEM.Complex.Solvers
{
    /// <summary>
    /// Решатель в частотном диапазоне для комплексных чисел
    /// </summary>
    /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
    /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
    public class FrequencySolver <TType, TType2>
    {
        private readonly InputData _data;
        private readonly WaveData _wave;
        private readonly string _outputFilePrefix;

        private readonly ForceVector _vectorB;
        private readonly GlobalMatrix<TType, TType2> _matrix;

        private readonly int[] _boundNums;

        private readonly List<Result> _results;

        /// <summary>
        /// Инициализация решателя
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="matrix">Глобальная матрица</param>
        /// <param name="vector">Объект для работы с вектором воздействий</param>
        /// <param name="boundaryEdges">Номера граничных ребер</param>
        public FrequencySolver(InputData data, WaveData wave, GlobalMatrix<TType, TType2> matrix, ForceVector vector, int[] boundaryEdges)
        {
            LogHelper.Log(LogMessageType.Info, "Инициализация решателя (Frequency - Complex - NonIterate/Itarate)...");

            _data = data;
            _wave = wave;
            _outputFilePrefix = String.Format("{0}{1}", data.Output.Path, data.Output.FilePrefix);
            _results = new List<Result>();

            _matrix = matrix;
            _vectorB = vector;

            _boundNums = boundaryEdges;
        }

        /// <summary>
        /// Список сведений о результатах решения
        /// </summary>
        public List<Result> Results
        {
            get { return _results; }
        }

        /// <summary>
        /// Выполенение решения
        /// </summary>
        /// <returns>Возвращает список результирующих векторов</returns>
        public List<System.Numerics.Complex[]> Solve()
        {
            var results = new List<System.Numerics.Complex[]>();

            if (_data.Options.WithoutIterations)
            {
                // рассматриваем решение в начальный момент времени 
                results.Add(SolveOne(0));
            }
            else
            {
                // NOTE: пока не работает этот метод!!! (его нету)
                SolveIterate();
            }
            LogHelper.Log(LogMessageType.Info, "Решатель по частоте в скалярных значениях завершил работу!");

            return results;
        }

        /// <summary>
        /// Решение в один проход, без итераций, при единожды заданных условиях
        /// </summary>
        /// <param name="currentTime">Текушее время (для волны)</param>
        /// <returns>Возвращает вектор с результатами решения СЛАУ</returns>
        private System.Numerics.Complex[] SolveOne(double currentTime)
        {
            // 1. Вычисление вектора B
            System.Numerics.Complex[] vec = _vectorB.Generate(currentTime);

            // 2. Построение глобальной матрицы
            System.Numerics.Complex[,] mat = _matrix.Generate(_wave);

            // NOTE: применение граничных условий Дирихле
            // TODO: протестировтаь применение граничных условий
            BoundaryBuilder.ApplyDirichletBoundaryCondition2(ref mat, ref vec, _boundNums,
                                                             ComplexArrayHelper.InitValueVector(_boundNums.Length,
                                                                                                _data.Boundaries.Single
                                                                                                    (b =>
                                                                                                     b.Type ==
                                                                                                     BoundaryTypes.
                                                                                                         Dirichlet).
                                                                                                    Value));

            // NOTE: или так записать вместео трех предыдущих строк
            System.Numerics.Complex[] result = MathNetHelper.Solve(mat, vec);

            // 4. Запись результата в файл
            string[] resfiles = PrintResultsToFile(vec, result, 0);

            // 5. Добавление результата в список с результатами
            //AddResultItem(resfiles[0], resfiles[1], _wave, currentTime);
            AddResultItem(resfiles[0], resfiles[1], vec, result, _wave, currentTime);

            return result;
        }

        // итерационный  решатель
        private void SolveIterate()
        {
            // TODO: описать метод для итерационного решения по частоте
        }

        #region Вспомогательные функции

        /// <summary>
        /// Вывод результата в текстовый файл
        /// </summary>
        /// <param name="vector">Вектор с воздействиями</param>
        /// <param name="result">Вектор с результатом</param>
        /// <param name="number">Номер итерации</param>
        /// <returns>Возвращает имена записанных файлов с результатами</returns>
        private string[] PrintResultsToFile(System.Numerics.Complex[] vector, System.Numerics.Complex[] result, int number)
        {
            // формируем имя файла в зависимости от итерации
            string fileNameVector = String.Format("{0}{1}-{2}.txt", _outputFilePrefix, "vector", number);
            string fileNameResult = String.Format("{0}{1}-{2}.txt", _outputFilePrefix, "result", number);

            // вывод файла с результатами вычисления вектора воздействий
            OutputHelper.OutputVectorToFile(vector, fileNameVector);

            // вывод файла с результатом решения СЛАУ
            OutputHelper.OutputVectorToFile(result, fileNameResult);

            return new[] { fileNameVector, fileNameResult };
        }

        /// <summary>
        /// Добавление результата в список с данными о результатах
        /// </summary>
        /// <param name="forceFile">Имя файала с вектором воздействий ({b})</param>
        /// <param name="resultFile">Имя файла с результатами (вектором)</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="time">Значение времени</param>
        private void AddResultItem(string forceFile, string resultFile, WaveData wave, double time)
        {
            Results.Add(new Result()
                            {
                                FileName = resultFile,
                                ForceFileName = forceFile,
                                Wave = wave,
                                Time = time
                            });
        }

        /// <summary>
        /// Добавление результата в список с данными о результатах
        /// </summary>
        /// <param name="forceFile">Имя файала с вектором воздействий ({b})</param>
        /// <param name="resultFile">Имя файла с результатами (вектором)</param>
        /// <param name="force"> </param>
        /// <param name="result"> </param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="time">Значение времени</param>
        private void AddResultItem(string forceFile, string resultFile, System.Numerics.Complex[] force, System.Numerics.Complex[] result, WaveData wave, double time)
        {
            Results.Add(new Result()
                            {
                                FileName = resultFile,
                                ForceFileName = forceFile,
                                Wave = wave,
                                Time = time,
                                Forces = force,
                                Results = result,
                                Frequency = ParametersBuilder.GetFrequencyByOmega(wave.Omega)
                            });
        }

        /// <summary>
        /// Добавление результата в список с данными о результатах
        /// </summary>
        /// <param name="forceFile">Имя файала с вектором воздействий ({b})</param>
        /// <param name="resultFile">Имя файла с результатами (вектором)</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="frequency">Частота работы источника (Hz)</param>
        /// <param name="epsilon">Диэлектрическая проницаемость</param>
        private void AddResultItem(string forceFile, string resultFile, WaveData wave, double frequency, System.Numerics.Complex epsilon)
        {
            Results.Add(new Result()
                            {
                                FileName = resultFile,
                                ForceFileName = forceFile,
                                Wave = wave,
                                Frequency = frequency,
                                Epsilon = epsilon
                            });
        }

        /// <summary>
        /// Добавление результата в список с данными о результатах
        /// </summary>
        /// <param name="forceFile">Имя файала с вектором воздействий ({b})</param>
        /// <param name="resultFile">Имя файла с результатами (вектором)</param>
        /// <param name="force">Вектор с исходными воздействиями</param>
        /// <param name="result">Вектор с результатами решения СЛАУ</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="frequency">Частота работы источника (Hz)</param>
        /// <param name="epsilon">Диэлектрическая проницаемость</param>
        private void AddResultItem(string forceFile, string resultFile, System.Numerics.Complex[] force,
            System.Numerics.Complex[] result, WaveData wave, double frequency, System.Numerics.Complex epsilon)
        {
            Results.Add(new Result()
                            {
                                FileName = resultFile,
                                ForceFileName = forceFile,
                                Wave = wave,
                                Frequency = frequency,
                                Epsilon = epsilon,
                                Forces = force,
                                Results = result
                            });
        }

        #endregion
    }
}
