﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.FEM.Complex.Helpers;
using EMFFM.FEM.Helpers;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM.Complex
{
    /// <summary>
    /// Класс для выполнения анализа комплексного поля
    /// </summary>
    /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
    /// <typeparam name="TType2">Тип поверхностных элементов</typeparam>
    public class FieldAnalysis <TType, TType2>
    {
        private readonly List<Result> _results;
        private readonly TMesh<TType, TType2> _mesh;
        private readonly ElementType _volElementType;

        /// <summary>
        /// Результирующие поля
        /// </summary>
        public List<FieldData> ResultFields;

        /// <summary>
        /// Поля от внешних источников
        /// </summary>
        public List<FieldData> IncidentFields;

        /// <summary>
        /// Инициализация анализатора с параметрами
        /// </summary>
        /// <param name="results">Сведения о результатах</param>
        /// <param name="mesh">Сетка</param>
        /// <param name="elementsType">Тип объемных элементов сетки</param>
        public FieldAnalysis(List<Result> results, TMesh<TType, TType2> mesh, ElementType elementsType)
        {
            ResultFields = new List<FieldData>(results.Count);
            IncidentFields = new List<FieldData>(results.Count);
            _results = results;
            _mesh = mesh;
            _volElementType = elementsType;

            LoadDataFromResults();
        }

        /// <summary>
        /// Загрузка данных из полей с результатами
        /// </summary>
        private void LoadDataFromResults()
        {
            // считываем поля, полученные в результате решения СЛАУ
            foreach (var result in _results)
            {
                var data = new FieldData(FieldHelper.ConvertFromArray(result.Forces), result.Wave);
                ResultFields.Add(data);
            }
            // считываем поля, полученные в результате составления вектора воздействий
            foreach (var result in _results)
            {
                var data = new FieldData(FieldHelper.ConvertFromArray(result.Results), result.Wave);
                IncidentFields.Add(data);
            }
        }

        /// <summary>
        /// Вычисление полей
        /// </summary>
        public void CalculateFields(double coFactor)
        {
            switch (_volElementType)
            {
            	case ElementType.Tetrahedron: // поля для тетраэдра описаны через базисные векторные функции
                    foreach (var field in ResultFields) // для каждого полученного поля
                    {
                        foreach (var element in _mesh.MeshElements) // для каждого элемента ищем его глобальное поле
                        {
                            CalculateLocalElement<Tetrahedron>(element as Tetrahedron, field, coFactor);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Вычисление поля на элементе
        /// </summary>
        /// <typeparam name="TType">Тип элемента</typeparam>
        /// <param name="element">Данные элемента</param>
        /// <param name="field">Данные о локальном поле</param>
        /// <param name="coFactor">Поправочный размерный коэффициет</param>
        private void CalculateLocalElement<TType>(TType element, FieldData field, double coFactor)
        {
            ElementField elf = new ElementField();

            BasisFunctions<TType> wmat = new BasisFunctions<TType>(element, _volElementType);
            // определяем векторные соотношения для точки в центре масс элемента
            double[,] mat = wmat.GetW((element as Element).GetCenterPoint());

            System.Numerics.Complex[] vec = new System.Numerics.Complex[6];
            int k = 0;
            foreach (var edge in (element as Element).Edges)
            {
                vec[k] = new System.Numerics.Complex(edge.GetLenght(coFactor), 0) * field.EdgeFields[edge.Number - 1].TangentialField;
                elf.EdgeFields.Add(field.EdgeFields[edge.Number - 1]);
                k++;
            }

            System.Numerics.Complex[] result = MathNetHelper.Multiply(ComplexHelper.Convert(mat), vec);

            elf.CenterField = new Field(result);
            field.ElementFields.Add(elf);
        }

        /// <summary>
        /// Вычисление разницы между полями (между полученным и падающим)
        /// </summary>
        /// <remarks>В итоге изменяется значение результирующего поля!</remarks>
        public void CalculateDifference()
        {
            for (int i = 0; i < _results.Count; i++)
            {
                ResultFields[i].EdgeFields = FieldHelper.Difference(ResultFields[i].EdgeFields, IncidentFields[i].EdgeFields);
            }
        }

        /// <summary>
        /// Вывод результатов по всем полученным результирующим полям
        /// </summary>
        /// <param name="outputPath">Каталог для вывода результата</param>
        /// <param name="filePrefix">Префикс названия файла</param>
        public void PrintResultField(string outputPath, string filePrefix)
        {
            for (int i = 0; i < _results.Count; i++)
            {
                string fileName = string.Format("{0}\\{1}-result-{2}.txt", outputPath, filePrefix, i);
                FieldHelper.OutputFieldToFile(ResultFields[i].EdgeFields, fileName);
            }
        }

        /// <summary>
        /// Вывод полей по элементам
        /// </summary>
        /// <param name="outputPath">Каталог для вывода результата</param>
        /// <param name="filePrefix">Префикс названия файла</param>
        /// <param name="withEdgeFields">Выводить значения полея на ребрах?</param>
        public void PrintElementsField(string outputPath, string filePrefix, bool withEdgeFields)
        {
            for (int i = 0; i < _results.Count; i++)
            {
                string fileName = string.Format("{0}\\{1}full-{2}.txt", outputPath, filePrefix, i);
                FieldHelper.OutputFieldToFile(ResultFields[i].ElementFields, fileName, withEdgeFields);
            }
        }

        /// <summary>
        /// Вывод полей по элементам
        /// </summary>
        /// <param name="elementNumbers">Номера элементов из области для вывода данных по ним</param>
        /// <param name="outputPath">Каталог для вывода результата</param>
        /// <param name="filePrefix">Префикс названия файла</param>
        /// <param name="withEdgeFields">Выводить значения полея на ребрах?</param>
        /// <param name="domainNumber">Номер области, для которой выводятся сведения</param>
        public void PrintElementsField(int[] elementNumbers, string outputPath, string filePrefix, bool withEdgeFields, int domainNumber)
        {
            for (int i = 0; i < _results.Count; i++)
            {
                string fileName = string.Format("{0}\\{1}full-domain-{2}-{3}.txt", outputPath, filePrefix, domainNumber, i);
                FieldHelper.OutputFieldToFile(ResultFields[i].ElementFields, elementNumbers, fileName, withEdgeFields);
            }
        }
    }
}
