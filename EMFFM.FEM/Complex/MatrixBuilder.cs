﻿using System;
using System.Collections.Generic;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Math;
using EMFFM.Input.Elements;
using EMFFM.Mesh.Elements;
using EMFFM.Mesh.Elements.Abstract;

namespace EMFFM.FEM.Complex
{
    /// <summary>
    /// Класс для построения матриц (для МКЭ)
    /// </summary>
    /// <remarks>Построение комплексных матриц из скалярных чисел!</remarks>
    public static class MatrixBuilder
    {
        private static double _coFactor = 1; // по умолчанию поправочный коэффициент равен 1 (используется для размеров)

        /// <summary>
        /// Построение глобальной матрицы в комплексных числах
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="size">Размерность матрицы</param>
        /// <param name="elements">Элементы сетки</param>
        /// <param name="wave">Сведения о волне от источника</param>
        /// <param name="coFactor">Поправочный коэффициент</param>
        /// <returns>Возвращает матрицу комплексных чисел</returns>
        public static System.Numerics.Complex[,] BuiltGlobalMatrix<TType>(int size, List<TType> elements, WaveData wave, double coFactor)
        {
            _coFactor = coFactor;
            return BuiltGlobalMatrix(size, elements, wave.WaveNumber, wave.Omega);
        }

        /// <summary>
        /// Построение глобальной матрицы для ребер элементов сетки
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="size">Размерность матрицы (количество ребер)</param>
        /// <param name="elements">Список элементов, полученных при дискретизации</param>
        /// <param name="k">Волновое число (K)</param>
        /// <param name="omega">Круговая частота (омега)</param>
        /// <returns>Возвращает построенную матрицу</returns>
        public static System.Numerics.Complex[,] BuiltGlobalMatrix<TType>(int size, List<TType> elements, double k, double omega)
        {
            var mat = ComplexMatrixHelper.ZerosComplexMatrix(size, size);

            // бегаем по элементам сетки
            foreach (var item in elements)
            {
                // генерируем локальную матрицу элемента
                var tmp = BuildLocalMatrix(item, k, omega);

                // бегаем по ребрам элемента (вставка локальной матрицы в глобальную)
                for (int i = 0; i < (item as Element).Edges.Count; i++)
                {
                    for (int j = 0; j < (item as Element).Edges.Count; j++)
                    {
                        // BUG: хитрый баг при сборке матрицы, иначе получаются бесконечные значения
                        if (System.Math.Abs(tmp[i, j].Real) > 1.0E-10)
                        {
                            mat[(item as Element).Edges[i].Number - 1, (item as Element).Edges[j].Number - 1] += tmp[i, j];
                        }
                    }
                }
            }

            // проверка знака в матрице (глобальной)
            /*foreach (var item in elements)
            {
                for (int i = 0; i < (item as Element).Edges.Count; i++)
                {
                    for (int j = 0; j < (item as Element).Edges.Count; j++)
                    {
                        if ((item as Element).Edges[i].Number - 1 > (item as Element).Edges[j].Number - 1)
                        {
                            //mat[(item as Element).Edges[i].Number - 1, (item as Element).Edges[j].Number - 1] *= -1;
                            mat[(item as Element).Edges[i].Number - 1, (item as Element).Edges[j].Number - 1] =
                                ComplexHelper.RealMultiply(-1,
                                                           mat[(item as Element).Edges[i].Number - 1,
                                                               (item as Element).Edges[j].Number - 1]);
                        }
                    }
                }
            }*/

            return mat;
        }

        /// <summary>
        /// Вычисление локальной матрицы элемента
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="element">Элемент</param>
        /// <param name="k">Волновое число (K)</param>
        /// <param name="omega">Круговая частота (омега)</param>
        /// <returns>Возвращает вычисленную локальную матрицу элемента</returns>
        /// <remarks>Производит вычисление матрицы вида: Ek + k*k *Fk (где k - элемент)
        /// Учитывается как волновое число, так и Epsilon (диэлектрическая проводимость)</remarks>
        private static System.Numerics.Complex[,] BuildLocalMatrix<TType>(TType element, double k, double omega)
        {
            // TODO: продумать этот метод для правильного вычисления локальной матрицы (здесь - комплексной)!

            // для тетраэдра выполним подготовку данных для вычисления локальных матриц
            if (element is Tetrahedron)
            {
                (element as Tetrahedron).Prerequsite(_coFactor);
            }

            // вычисляем локальную матрицу E
            System.Numerics.Complex[,] emat = (element as IElement).GetComplexLocalE(); // матрица Eij
            emat = ComplexMatrixHelper.ComplexMultiply(System.Numerics.Complex.One / CalculateMu(element), emat); // умножаем на 1/Mu

            // если волновое число не нулевое, то считаем еще и локальную матрицу F
            if (Math.Abs(k - 0) > 1e-15)
            {
                // NOTE: выполнение пересчета Epsilon(w)
                var eps = CalculateEpsilon(element, omega);

                System.Numerics.Complex[,] fmat = (element as IElement).GetComplexLocalF(k, eps); // матрицы Fij

                // вычисляем разность матриц: 4*(1/mu*[Ek]) - k*k*eps*[Fk] (иначе говоря: 4*E-F)
                emat = ComplexMatrixHelper.ComplexSub(ComplexMatrixHelper.RealMultiply(4, emat), fmat);
            }

            // настройка знака при элементах матрицы
            for (int i = 0; i < (element as Element).Edges.Count; i++)
            {
                for (int j = 0; j < (element as Element).Edges.Count; j++)
                {
                    if ((element as Element).Edges[i].LocalNumber - 1 > (element as Element).Edges[j].LocalNumber - 1)
                    {
                        emat[(element as Element).Edges[i].LocalNumber - 1, (element as Element).Edges[j].LocalNumber - 1] *= -1;
                        //emat[(element as Element).Edges[i].Number - 1, (element as Element).Edges[j].Number - 1] =
                        //    ComplexHelper.RealMultiply(-1,
                        //                               emat[(element as Element).Edges[i].Number - 1,
                        //                                   (element as Element).Edges[j].Number - 1]);
                    }
                }
            }

            return emat;
        }

        /// <summary>
        /// Вычисление значения параметра Mu
        /// </summary>
        /// <typeparam name="TType">Тип объемного элемента сетки</typeparam>
        /// <param name="element">Элемент</param>
        /// <returns>Возвращает значение параметра</returns>
        private static System.Numerics.Complex CalculateMu<TType>(TType element)
        {
            var mu = new System.Numerics.Complex();

            if ((element as Element).Material is ComplexMaterial)
            {
                mu = ((element as Element).Material as ComplexMaterial).Mu;
            }
            else if ((element as Element).Material is Material)
            {
                mu = new System.Numerics.Complex(((element as Element).Material as Material).Mu, 0);
            }

            return mu;
        }

        /// <summary>
        /// Вычисление дисперсного параметра Epsilon(w)
        /// </summary>
        /// <typeparam name="TType">Тип элемента сетки</typeparam>
        /// <param name="element">Элемент</param>
        /// <param name="omega">Круговая частота (рад/с)</param>
        /// <returns>Возвращает значение диэлектрической проводимости</returns>
        private static System.Numerics.Complex CalculateEpsilon<TType>(TType element, double omega)
        {
            var epsilon = new System.Numerics.Complex();

            if ((element as Element).Material is ComplexMaterial)
            {
                epsilon = ((element as Element).Material as ComplexMaterial).Eps;
            }
            else if ((element as Element).Material is Material)
            {
                epsilon = new System.Numerics.Complex(((element as Element).Material as Material).Eps, 0);
            }

            return epsilon;
        }
    }
}
