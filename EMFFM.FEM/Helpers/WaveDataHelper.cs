﻿using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.Math.Elements;
using EMFFM.Input;
using EMFFM.Input.Elements;

namespace EMFFM.FEM.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с параметрами волны
    /// </summary>
    public static class WaveDataHelper
    {
        /// <summary>
        /// Получение параметров волны от источника
        /// (В комплексных числах)
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <returns>Возвращает сведения о волне</returns>
        public static EMFFM.Common.FEM.Complex.Elements.WaveData ComplexWaveData(InputData data)
        {
            var wave = new EMFFM.Common.FEM.Complex.Elements.WaveData();

            // TODO: описать метод для задания параметров волны в комплексных числах

            Source source = data.Sources.Single(s => s.Type == SourceTypes.Simple);

            wave.Point = source.Point;
            wave.Lambda = source.Lambda;
            wave.Amplitude = source.Amplitude;
            wave.Phase = source.Phase;
            wave.Vector = new UnitVector(source.Direction);
            wave.Omega = ParametersBuilder.GetOmegaByLambda(source.Lambda);
            wave.WaveNumber = ParametersBuilder.CalculateWaveNumberByLambda(source.Lambda);

            return wave;
        }

        /// <summary>
        /// Получение параметров волны от источника
        /// (В вещественных (скалярных) числах)
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <returns>Возвращает сведения о волне</returns>
        public static EMFFM.Common.FEM.Scalar.Elements.WaveData ScalarWaveData(InputData data)
        {
            var wave = new EMFFM.Common.FEM.Scalar.Elements.WaveData();

            Source source = data.Sources.Single(s => s.Type == SourceTypes.Simple);

            wave.Point = source.Point;
            wave.Amplitude = source.Amplitude;
            wave.Phase = source.Phase;
            wave.Vector = new UnitVector(source.Direction);
            wave.Omega = ParametersBuilder.GetOmegaByLambda(source.Lambda);
            wave.WaveNumber = ParametersBuilder.CalculateWaveNumberByLambda(source.Lambda);

            return wave;
        }
    }
}
