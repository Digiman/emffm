﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Input.Elements;

namespace EMFFM.FEM.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с источниками
    /// </summary>
    public static class SourcesHelper
    {
        /// <summary>
        /// Получение источника по его типу из списка во входном файле
        /// </summary>
        /// <param name="type">Тип источника</param>
        /// <param name="sources">Список источников из файла</param>
        /// <returns>Возвращает данные об источнике</returns>
        public static Source GetSource(SourceTypes type, IEnumerable<Source> sources)
        {
            return sources.SingleOrDefault(s => s.Type == type);
        }
    }
}
