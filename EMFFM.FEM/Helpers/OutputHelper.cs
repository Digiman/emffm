﻿using System.Collections.Generic;
using System.IO;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM.Helpers
{
    /// <summary>
    /// Вспомогательный класс для функций вывода данных в файлы
    /// </summary>
    /// <remarks>Генерирует текстовые файлы и XML файлы</remarks>
    public static class OutputHelper
    {
        /// <summary>
        /// Печать матрицы в файл
        /// </summary>
        /// <typeparam name="TType">Тип элементов матрицы</typeparam>
        /// <param name="matrix">Матрица с данными</param>
        /// <param name="fname">Путь и название файла для вывода</param>
        public static void OutputMatrixToFile<TType>(TType[,] matrix, string fname)
        {
            // создаем пустой файл для выводв данных
            StreamWriter file = new StreamWriter(fname);

            // вывод матрицы в файл
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                    file.Write("{0}\t", matrix[i, j].ToString());
                file.WriteLine();
            }

            file.Close();
        }

        /// <summary>
        /// Печать вектора (массива) в файл
        /// </summary>
        /// <typeparam name="TType">Тип элементов вектора (массива)</typeparam>
        /// <param name="vector">Вектор (массив)</param>
        /// <param name="fname">Путь и название файла для вывода</param>
        public static void OutputVectorToFile<TType>(TType[] vector, string fname)
        {
            // создаем пустой файл для выводв данных
            StreamWriter file = new StreamWriter(fname);

            // вывод вектора (массива) в файл
            for (int i = 0; i < vector.Length; i++)
            {
                file.WriteLine("{0}", vector[i].ToString());
            }

            file.Close();
        }

        /// <summary>
        /// Печать в файл сведений об элементах полученной в программе сетки
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов сетки</param>
        /// <param name="fname">Путь и название файла для вывода данных</param>
        /// <param name="withEdges">Выводить ли данные о ребрах</param>
        /// <param name="isAppend">Дописывать ли в файл</param>
        public static void OutputMeshDataToFile<TType>(List<TType> elements, string fname, bool withEdges = false, bool isAppend = false)
        {
            StreamWriter file = new StreamWriter(fname, isAppend);
            foreach (var item in elements)
            {
                file.WriteLine("{0}", item.ToString());
                if (withEdges)
                {
                    file.WriteLine("Edges: {0}", (item as Element).EdgesToString());
                }
            }
            file.Close();
        }

        /// <summary>
        /// Вывод диагонали матрицы в файл
        /// </summary>
        /// <param name="mat">Матрица с данными</param>
        /// <param name="fname">Имя файла для вывода</param>
        public static void PrintDiagonalToFile<TType>(TType[,] mat, string fname)
        {
            TType[] diag = new TType[mat.GetLength(0)];
            for (int i = 0; i < mat.GetLength(0); i++)
            {
                diag[i] = mat[i, i];
            }
            OutputVectorToFile<TType>(diag, fname);
        }
    }
}
