﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.Input.Elements;
using EMFFM.FEM.Common;
using EMFFM.Input;
using EMFFM.Input.Elements;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM.Helpers
{
    /// <summary>
    /// Помощник по работе с материалами объектов модели
    /// </summary>
    public static class MaterialsHelper
    {
        /// <summary>
        /// Инициализация параметров материалов
        /// </summary>
        /// <param name="data">Входные данные</param>
        public static void InitMaterials(ref InputData data)
        {
            // TODO: прописать код для метода инициализации материалов и их параметров из входных данных
            
            var ld = new LorenzDrude(SourcesHelper.GetSource(SourceTypes.Simple, data.Sources).Lambda);

            foreach (var material in data.Materials)
            {
                // если материал комплексный, то вычисляем для него по условию Лоренца-Друде значение epsilon
                if (material is ComplexMaterial)
                {
                    if ((material as ComplexMaterial).Model != null && (material as ComplexMaterial).Type != null)
                    {
                        (material as ComplexMaterial).Eps =
                            ld.Calculate((material as ComplexMaterial).Type,
                                         (material as ComplexMaterial).Model);
                    }
                }
            }
        }

        /// <summary>
        /// Назначение метериала по умолчанию для всех элементов
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов</param>
        /// <param name="defaultMaterial">Материал по умолчанию (воздух)</param>
        /// <returns>Возвращает модифицированный список элементов</returns>
        public static List<TType> SetDefaultMaterials<TType>(List<TType> elements, Material defaultMaterial)
        {
            // назначаем материал по умолчанию всем элементам (воздух обычно)
            foreach (var elem in elements)
            {
                var element = elem as Element;
                if (element != null) element.Material = defaultMaterial;
            }

            return elements;
        }

        /// <summary>
        /// Назначение материалов элементам по их расположению в областях
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        /// <param name="mesh">Сетка</param>
        /// <param name="geometry">Геометрия задачи</param>
        /// <param name="materials">Список материалов</param>
        /// <remarks>На основании областей, сгенерированных в Netgen</remarks>
        public static void SetMaterialsByDomain<TType, TType2>(ref TMesh<TType, TType2> mesh, Geometry geometry, List<MaterialBase> materials)
        {
            foreach (var element in mesh.MeshElements)
            {
                var element1 = element as Element;
                if (element1 != null)
                    element1.Material = GetMaterial(geometry.Domains.Single(dom => dom.Code == element1.SubDomain).MaterialName, materials);
            }
        }

        /// <summary>
        /// Получение материала из списка материалов по имени
        /// </summary>
        /// <param name="name">Название материала</param>
        /// <param name="materials">Список материалов</param>
        /// <returns>Возвращает найденный материал</returns>
        /// <remarks>Также ассоциирует данные о плазмоне для материала</remarks>
        private static MaterialBase GetMaterial(string name, IEnumerable<MaterialBase> materials)
        {
            var material = materials.SingleOrDefault(mat => mat.Name == name);

            return material;
        } 
    }
}
