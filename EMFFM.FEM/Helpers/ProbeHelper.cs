﻿using EMFFM.Input;
using EMFFM.Mesh;
using EMFFM.Mesh.Helpers;

namespace EMFFM.FEM.Helpers
{
    /// <summary>
    /// Помощник по анализу проб из результатов решения
    /// </summary>
    public static class ProbeHelper
    {
        /// <summary>
        /// Сбор пробных сведений из заданной рассматриваемой области
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <param name="mesh">Сетка</param>
        /// <param name="domainNumber">Номер области для сбора сведений</param>
        /// <param name="analysis">Объект с анализом полученных результатов (полей)</param>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        public static void GetProbeByDomain<TType, TType2>(InputData data, TMesh<TType, TType2> mesh, int domainNumber,
            FEM.Scalar.FieldAnalysis<TType, TType2> analysis)
        {
            int[] nums = MeshHelper.GetElementsByDomain(mesh, domainNumber);
            analysis.PrintElementsField(nums, data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields,
                                        domainNumber);
        }

        /// <summary>
        /// Сбор пробных сведений из заданной рассматриваемой области
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <param name="mesh">Сетка</param>
        /// <param name="domainNumber">Номер области для сбора сведений</param>
        /// <param name="analysis">Объект с анализом полученных результатов (полей)</param>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        public static void GetProbeByDomain<TType, TType2>(InputData data, TMesh<TType, TType2> mesh, int domainNumber,
            FEM.Complex.FieldAnalysis<TType, TType2> analysis)
        {
            int[] nums = MeshHelper.GetElementsByDomain(mesh, domainNumber);
            analysis.PrintElementsField(nums, data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields,
                                        domainNumber);
        }

        /*/// <summary>
        /// Сбор пробных сведений из заданной рассматриваемой области
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <param name="mesh">Сетка</param>
        /// <param name="domainNumber">Номер области для сбора сведений</param>
        /// <param name="analysis">Объект с анализом полученных результатов (полей)</param>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        public static void GetProbeByDomain<TType, TType2>(InputData data, TMesh<TType, TType2> mesh, int domainNumber,
            FEM.Complex.FieldAnalysis<TType, TType2> analysis)
        {
            int[] nums = MeshHelper.GetElementsByDomain(mesh, domainNumber);
            analysis.PrintElementsField(nums, data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields,
                                        domainNumber);
        }*/
    }
}
