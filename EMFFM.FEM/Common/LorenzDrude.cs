﻿using System;
using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Structs;

namespace EMFFM.FEM.Common
{
    /// <summary>
    /// Класс для вычисления параметров по правилам Лоренца Друде
    /// </summary>
    public class LorenzDrude
    {
        private double _omegalight; // angular frequency of light (rad/s)
        private double _invsqrt2 = 0.707106781186547; // 1/sqrt(2)

        private double _lambda;

        private readonly List<ComplexMaterial> _materials;

        /// <summary>
        /// Иниуиализация объекта
        /// </summary>
        /// <param name="lambda">Длина волны падающего света</param>
        public LorenzDrude(double lambda)
        {
            _lambda = lambda;
            _omegalight = ConstantParameters.Twopic * (Math.Pow(lambda, -1)); // angular frequency of light (rad/s)
            _materials = ComplexMaterialsInitializer.Built();
        }

        /// <summary>
        /// Вычисление магнитной постоянной
        /// </summary>
        /// <param name="material">Материал частицы</param>
        /// <param name="model">Модель рамсчета</param>
        /// <returns>Возвращает значение постоянной (epsilon)</returns>
        public System.Numerics.Complex Calculate(MaterialTypes material, ModelTypes model)
        {
            System.Numerics.Complex epsilon = new System.Numerics.Complex(), epsilon_D, epsilon_L;

            epsilon_D = CalcelateEpsilonD(GetMaterial(material));

            switch (model)
            {
                case ModelTypes.D: // модель Друде
                    epsilon = epsilon_D;
                    break;
                case ModelTypes.LD: // модель лоренца Друде
                    epsilon_L = CalcelateEpsilonL(GetMaterial(material));
                    epsilon = epsilon_D + epsilon_L;
                    break;
            }

            return epsilon;
        }

        /// <summary>
        /// Вычисление диэлектрической приницаемости для условия Друде
        /// </summary>
        /// <param name="material">Материал</param>
        /// <returns>Возвращает значение диэлектрической проницаемости</returns>
        private System.Numerics.Complex CalcelateEpsilonD(ComplexMaterial material)
        {
            var tmp1 = new System.Numerics.Complex(Math.Pow(_omegalight, 2), material.Gamma[0] * _omegalight);
            tmp1 = System.Numerics.Complex.Pow(tmp1, -1);
            var tmp2 = material.F[0]*Math.Pow(material.Omegap, 2);
            return 1 - (tmp2*tmp1);
        }

        /// <summary>
        /// Вычисление диэлектрической проницаемости для условия Лоренца
        /// </summary>
        /// <param name="material">Материал</param>
        /// <returns>Возвращает значение диэлектрической проницаемости</returns>
        private System.Numerics.Complex CalcelateEpsilonL(ComplexMaterial material)
        {
            var result = System.Numerics.Complex.Zero;
            for (int i = 1; i < material.Order; i++)
            {
                var tmp = new System.Numerics.Complex(Math.Pow(material.Omega[i], 2) - Math.Pow(_omegalight, 2),
                                      -material.Gamma[i]*_omegalight);
                tmp = System.Numerics.Complex.Pow(tmp, -1);
                var tmp2 = material.F[i]*Math.Pow(material.Omegap, 2);
                result += tmp*tmp2;
            }

            return result;
        }

        /// <summary>
        /// Расчет комплексного преломления частицы
        /// </summary>
        /// <param name="epsilon">Электрическая постоянная</param>
        /// <returns>Возвращает значение преломления</returns>
        public System.Numerics.Complex CalculateRefractiveIndex(System.Numerics.Complex epsilon)
        {
            var n = Math.Sqrt(Math.Sqrt(Math.Pow(epsilon.Real, 2) + Math.Pow(epsilon.Imaginary, 2)) + epsilon.Real);
            var k = Math.Sqrt(Math.Sqrt(Math.Pow(epsilon.Real, 2) + Math.Pow(epsilon.Imaginary, 2)) - epsilon.Real);
            return new System.Numerics.Complex(n, k)*_invsqrt2;
        }

        /// <summary>
        /// Получение материала по его типу
        /// </summary>
        /// <param name="type">Тип материала</param>
        /// <returns>Возвращает параметры материала</returns>
        private ComplexMaterial GetMaterial(MaterialTypes type)
        {
            return _materials.SingleOrDefault(mat => mat.Type == type);
        }
    }
}
