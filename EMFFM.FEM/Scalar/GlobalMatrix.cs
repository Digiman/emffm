﻿using EMFFM.Common.FEM.Scalar.Elements;
using EMFFM.FEM.Helpers;
using EMFFM.Input;
using EMFFM.Mesh;

namespace EMFFM.FEM.Scalar
{
    /// <summary>
    /// Класс для глобальной матрицы и ее построения и пересчета
    /// </summary>
    public class GlobalMatrix<TType, TType2>
    {
        private InputData _inputData;
        
        private TMesh<TType, TType2> _mesh;

        /// <summary>
        /// Инициализация матрицы и ее данных
        /// </summary>
        /// <param name="data">Входные данные</param>
        /// <param name="mesh">Сгенерированная сетка</param>
        public GlobalMatrix(InputData data, TMesh<TType, TType2> mesh)
        {
            _inputData = data;
            _mesh = mesh;

            Init();
        }

        /// <summary>
        /// Инициализация параметров для построения матрицы
        /// </summary>
        private void Init()
        {
            // инициализация материалов (с учетом условия Лоренца-Друде)
            MaterialsHelper.InitMaterials(ref _inputData);

            // установка параметров материалов для областей в геометрии задачи
            MaterialsHelper.SetMaterialsByDomain(ref _mesh, _inputData.Geomerty, _inputData.Materials);
        }

        /// <summary>
        /// Генерация глобальной матрицы
        /// </summary>
        /// <param name="currentFreq">Текущая частота источника</param>
        /// <returns>Возвращает сгенерированную матрицу</returns>
        public double[,] Generate(double currentFreq)
        {
            // 1. Вычисление волнового числа
            double waveNumber = ParametersBuilder.CalculateWaveNumberByFrequency(currentFreq);

            // 1.1. Вычисление круговой частоты (омега)
            double omega = ParametersBuilder.GetOmegaByFrequency(currentFreq);

            // 2. Вычисление и построение матрицы глобальной
            double[,] result = MatrixBuilder.BuiltGlobalMatrix(_mesh.EdgesCount, _mesh.MeshElements, waveNumber, omega);

            return result;
        }

        /// <summary>
        /// Генерация глобальной матрицы
        /// </summary>
        /// <param name="wave">Сведения о волне</param>
        /// <returns>Возвращает сгенерированную матрицу</returns>
        public double[,] Generate(WaveData wave)
        {
            // 1. Вычисление и построение матрицы глобальной
            double[,] result = MatrixBuilder.BuiltGlobalMatrix(_mesh.EdgesCount, _mesh.MeshElements, wave.WaveNumber, wave.Omega, _inputData.Geomerty.CoFactor);

            return result;
        }
    }
}
