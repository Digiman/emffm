﻿using System;
using System.Collections.Generic;
using EMFFM.Common.FEM.Scalar.Elements;
using EMFFM.Common.Math;
using EMFFM.Common.Math.Elements;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.FEM.Scalar
{
    // TODO: модифицировать класс для построения вектора возмущений 

    /// <summary>
    /// Описание класса для вектора воздействий
    /// </summary>
    /// <remarks>Вектор возмущений для скалярных значений!</remarks>
    public class ForceVector
    {
        private readonly WaveData _wave;
        private readonly Func<double, WaveData, double, double> _function;
        private readonly int _totalEdgesCount;
        private readonly List<Edge> _edges;
        private readonly double _initValue;

        /// <summary>
        /// Инициализация вектора с известными значениями поля
        /// </summary>
        /// <param name="wave">Параметры волны от источника</param>
        /// <param name="function">Функция для вычисления значений поля</param>
        /// <param name="totalEdges">Общее количество ребер (уникальных) в сетке</param>
        /// <param name="edges">Список ребер на границе источника</param>
        public ForceVector(WaveData wave, Func<double, WaveData, double, double> function, int totalEdges, List<Edge> edges)
        {
            _wave = wave;
            _function = function;
            _edges = edges;
            _totalEdgesCount = totalEdges;
        }

        /// <summary>
        /// Инициализация вектора с известными значениями поля
        /// </summary>
        /// <param name="wave">Параметры волны от источника</param>
        /// <param name="function">Функция для вычисления значений поля</param>
        /// <param name="totalEdges">Общее количество ребер (уникальных) в сетке</param>
        /// <param name="edges">Список ребер на границе источника</param>
        /// <param name="initValue">Начальное значение для инициализации</param>
        public ForceVector(WaveData wave, Func<double, WaveData, double, double> function, int totalEdges, List<Edge> edges, double initValue)
        {
            _wave = wave;
            _function = function;
            _edges = edges;
            _totalEdgesCount = totalEdges;
            _initValue = initValue;
        }

        /// <summary>
        /// Генерация вектора
        /// </summary>
        /// <param name="currentValue">Текущее значение (для функции)</param>
        /// <param name="isInitValue">Инициировать ли вектор начальными значениями</param>
        /// <returns>Возвращает заполенный данными вектор</returns>
        /// <remarks>Для условия постоянной частоты волны источника!</remarks>
        public double[] Generate(double currentValue, bool isInitValue)
        {
            double[] result = new double[_totalEdgesCount];

            if (isInitValue)
            {
                // устанавливаем значения для начального значения
                for (int i = 0; i <_edges.Count; i++)
                {
                    result[_edges[i].Number - 1] = _initValue;
                }
            }
            else
            {
                // вычисляем по функции
                for (int i = 0; i < _edges.Count; i++)
                {
                    result[_edges[i].Number - 1] = _function(_initValue, _wave, 0); // для времени t=0
                }
            }

            return result;
        }

        /// <summary>
        /// Генерация вектора воздействий
        /// </summary>
        /// <param name="currentTime">Текущее значение времени</param>
        /// <returns>Возвращает сгенерированный вектор</returns>
        public double[] Generate(double currentTime)
        {
            double[] result = new double[_totalEdgesCount];

            // вычисляем по функции
            for (int i = 0; i < _edges.Count; i++)
            {
                double position = CalculatePosition(_edges[i]);
                result[_edges[i].Number - 1] = _function(position, _wave, currentTime);
            }

            return result;
        }

        /// <summary>
        /// Вычисление скалярного произведения k*n*r
        /// </summary>
        /// <param name="edge">Ребро</param>
        /// <returns>Возвращает скалярное значение</returns>
        private double CalculatePosition(Edge edge)
        {
            Point p = GeometryHelper.GetCenterPoint(edge.Vertex1.Coord, edge.Vertex2.Coord);

            TVector vector = new TVector(_wave.Point, p);
            double result = vector * _wave.Vector;

            return result * _wave.WaveNumber;
        }
    }
}
