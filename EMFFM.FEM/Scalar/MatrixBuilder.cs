﻿using System.Collections.Generic;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Math;
using EMFFM.Input.Elements;
using EMFFM.Mesh.Elements;
using EMFFM.Mesh.Elements.Abstract;

namespace EMFFM.FEM.Scalar
{
    // TODO: внести изменения в параметры методов, сделать их как и везде, универсально, через параметры волны

    /// <summary>
    /// Класс для построения матриц (для МКЭ)
    /// </summary>
    /// <remarks>Построение матрицы из скалярных (вещественных) чисел!</remarks>
    public static class MatrixBuilder
    {
        private static double _coFactor = 1;

        /// <summary>
        /// Построение глобальной матрицы для ребер элементов сетки
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="size">Размерность матрицы (количество ребер)</param>
        /// <param name="elements">Список элементов, полученных при дискретизации</param>
        /// <param name="k">Волновое число (K)</param>
        /// <param name="omega">Круговая частота (омега)</param>
        /// <param name="coFactor">Нормирующий коэффициент для размеров (перевод к метрам)</param>
        /// <returns>Возвращает построенную матрицу</returns>
        public static double[,] BuiltGlobalMatrix<TType>(int size, List<TType> elements, double k, double omega, double coFactor)
        {
            _coFactor = coFactor;
            return BuiltGlobalMatrix(size, elements, k, omega);
        }
        
        /// <summary>
        /// Построение глобальной матрицы для ребер элементов сетки
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="size">Размерность матрицы (количество ребер)</param>
        /// <param name="elements">Список элементов, полученных при дискретизации</param>
        /// <param name="k">Волновое число (K)</param>
        /// <param name="omega">Круговая частота (омега)</param>
        /// <returns>Возвращает построенную матрицу</returns>
        public static double[,] BuiltGlobalMatrix<TType>(int size, List<TType> elements, double k, double omega)
        {
            double[,] mat = MatrixHelper.ZerosMatrix(size, size);

            // бегаем по элементам сетки
            foreach (var item in elements)
            {
                // генерируем локальную матрицу элемента
                double[,] tmp = BuildLocalMatrix(item, k, omega);

                // бегаем по ребрам элемента (вставка локальной матрицы в глобальную)
                for (int i = 0; i < (item as Element).Edges.Count; i++)
                {
                    for (int j = 0; j < (item as Element).Edges.Count; j++)
                    {
                        // BUG: хитрый баг при сборке матрицы, иначе получаются бесконечные значения
                        if (System.Math.Abs(tmp[i, j]) > 1.0E-10)
                        {
                            mat[(item as Element).Edges[i].Number - 1, (item as Element).Edges[j].Number - 1] += tmp[i, j];
                        }
                    }
                }
            }

            // проверка знака в матрице (глобальной)
            foreach (var item in elements)
            {
                for (int i = 0; i < (item as Element).Edges.Count; i++)
                {
                    for (int j = 0; j < (item as Element).Edges.Count; j++)
                    {
                        if ((item as Element).Edges[i].Number - 1 > (item as Element).Edges[j].Number - 1)
                        {
                            mat[(item as Element).Edges[i].Number - 1, (item as Element).Edges[j].Number - 1] *= -1;
                        }
                    }
                }
            }

            return mat;
        }

        /// <summary>
        /// Вычисление локальной матрицы элемента
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="element">Элемент</param>
        /// <param name="k">Волновое число (K)</param>
        /// <param name="omega">Круговая частота (омега)</param>
        /// <returns>Возвращает вычисленную локальную матрицу элемента</returns>
        /// <remarks>Производит вычисление матрицы вида: Ek + k*k *Fk (где k - элемент)
        /// Учитывается как волновое число, так и Epsilon (диэлектрическая проводимость)</remarks>
        private static double[,] BuildLocalMatrix<TType>(TType element, double k, double omega)
        {
            // TODO: продумать этот метод для правильного вычисления локальной матрицы (как комплексной, так и скалярной)

            // для тетраэдра выполним подготовку данных для вычисления локальных матриц
            if (element is Tetrahedron)
            {
                (element as Tetrahedron).Prerequsite(_coFactor);
            }

            // вычисляем локальную матрицу E
            double[,] emat = (element as IElement).GetLocalE(); // матрица Eij
            emat = MatrixHelper.Multiply(1 / CalculateMu(element), emat); // умножаем на 1/Mu

            // если волновое число не нулевое, то считаем еще и локальную матрицу F
            if (k != 0)
            {
                double[,] fmat = (element as IElement).GetLocalF(); // матрицы Fij
                
                // NOTE: выполнение пересчета Epsilon(w)
                double eps = CalculateEpsilon(element, omega);
                fmat = MatrixHelper.Multiply(-eps * k * k, fmat); // сразу сделаем умножение на Eps*K*K и * -1

                // складываем матрицы: E+k^2*F (так как ранее умножили на -1, то будет E-F)
                // вычисляем [Ek] - k*k*eps*[Fk]
                emat = MatrixHelper.Sum(emat, fmat);
            }

            return emat;
        }

        /// <summary>
        /// Вычисление значения параметра Mu
        /// </summary>
        /// <typeparam name="TType">Тип объемного элемента сетки</typeparam>
        /// <param name="element">Элемент</param>
        /// <returns>Возвращает значение параметра</returns>
        private static double CalculateMu<TType>(TType element)
        {
            double mu = 0;

            if ((element as Element).Material is ComplexMaterial)
            {
                // для комплексного материала берем его вещественную часть для скалярного решателя
                mu = ((element as Element).Material as ComplexMaterial).Mu.Real;
            }
            else if ((element as Element).Material is Material)
            {
                mu = ((element as Element).Material as Material).Mu;
            }

            return mu;
        }

        /// <summary>
        /// Вычисление дисперсного параметра Epsilon(w)
        /// </summary>
        /// <typeparam name="TType">Тип объемного элемента сетки</typeparam>
        /// <param name="element">Элемент</param>
        /// <param name="omega">Круговая частота (рад/c)</param>
        /// <returns>Возвращает значение диэлектрической проводимости</returns>
        private static double CalculateEpsilon<TType>(TType element, double omega)
        {
            double epsilon = 0;

            if ((element as Element).Material is ComplexMaterial)
            {
                // для комплексного материала берем его вещественную часть для скалярного решателя
                epsilon = ((element as Element).Material as ComplexMaterial).Eps.Real;
            }
            else if ((element as Element).Material is Material)
            {
                epsilon = ((element as Element).Material as Material).Eps;
            }

            return epsilon;
        }
    }
}
