﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.FEM.Helpers;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM
{
    /// <summary>
    /// Класс для установки параметров материалов для элементов сетки
    /// </summary>
    public static class MaterialsBuilder
    {
        /// <summary>
        /// Установка электромагнитных параметров для элементов сетки 
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов</param>
        /// <param name="materials">Список материалов</param>
        /// <param name="particles">Номера частиц (номера элементов с частицами)</param>
        /// <param name="particlesData">Данные о частицах</param>
        /// <returns>Возвращает модифицированнй список элементов</returns>
        public static List<TType> SetMaterials<TType>(List<TType> elements, List<Material> materials, int[] particles, List<Particle> particlesData)
        {
            // назначаем материал по умолчанию всем элементам (воздух обычно)
            MaterialsHelper.SetDefaultMaterials(elements, materials.Single(mat => mat.IsDefault == true));

            // назначаем материал только для элементов-частиц
            foreach (var particle in particlesData)
            {
                switch (particle.Type)
                {
                    case ParticlesType.Law:
                        for (int i = 0; i < particles.Length; i++)
                        {
                            (elements[particles[i] - 1] as Element).Material = materials.Single(mat => mat.Name == particle.Material);
                        }
                        break;
                    case ParticlesType.Single:
                        (elements[(particle as ParticleS).Number] as Element).Material = materials.Single(mat => mat.Name == particle.Material);
                        break;
                    case ParticlesType.Many:
                        break;
                }
            }

            return elements;
        }
    }
}
