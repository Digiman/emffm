﻿using EMFFM.Common.Enums;

namespace EMFFM.FEM
{
    // TODO: пересмотреть весь класс для задания (расчета) исходной функции

    /// <summary>
    /// Класс для описания функций используемых источников
    /// </summary>
    public static class SourceFunctions
    {
        /// <summary>
        /// Функция для монохроматической волны
        /// E=Amp*cos(w*t)
        /// </summary>
        /// <param name="amplitude">Амплитуда волны</param>
        /// <param name="currentValue">Текущее значение (частота, омега)</param>
        /// <param name="time">Текущее значение времени</param>
        /// <returns>Возвращает значение волны</returns>
        /// <remarks>Для только вещественного значения волны!</remarks>
        public static double MonochromeWaveRe(double amplitude, double currentValue, double time)
        {
            return amplitude * System.Math.Cos(currentValue * time);
        }

        /// <summary>
        /// Вычисление значения функции для монохроматической волны вида E(w)=E0*exp(w*t)
        /// </summary>
        /// <param name="amplitude">Амплитуда волны</param>
        /// <param name="currentValue">Текущее значение (частота, омега)</param>
        /// <param name="time">Значение времени</param>
        /// <returns>Возвращает комплексное число</returns>
        /// <remarks>Полное комплексное значение!</remarks>
        public static System.Numerics.Complex MonochromeWaveFull(double amplitude, double currentValue, double time)
        {
            double real = amplitude * System.Math.Cos(currentValue * time);
            double img = amplitude * System.Math.Sin(currentValue * time);
            return new System.Numerics.Complex(real, img);
        }

        /// <summary>
        /// Вычисление значения функции для монохроматической волны вида E(w)=E0*exp(w*t)
        /// </summary>
        /// <param name="position"> </param>
        /// <param name="wave"> </param>
        /// <param name="time">Значение времени</param>
        /// <returns>Возвращает комплексное число</returns>
        /// <remarks>Полное комплексное значение!</remarks>
        public static System.Numerics.Complex MonochromeWaveFull(double position, EMFFM.Common.FEM.Complex.Elements.WaveData wave, double time)
        {
            double real = wave.Amplitude*System.Math.Cos(wave.Omega*time + wave.WaveNumber*position + wave.Phase);
            double img = wave.Amplitude*System.Math.Sin(wave.Omega*time - wave.WaveNumber*position + wave.Phase);
            return new System.Numerics.Complex(real, img);
        }

        /// <summary>
        /// Вычисление значения функции для монохроматической волны 
        /// </summary>
        /// <param name="position">Координаты плоскости для плоской волны по заданному направлению</param>
        /// <param name="direction">Направление распространения плоской волны</param>
        /// <param name="wave">Параметры волны от истичника ЭМИ</param>
        /// <param name="time">Значение времени</param>
        /// <returns>Возвращает комплексное число</returns>
        public static System.Numerics.Complex MonochromeWaveFull(double position, EMFFM.Common.FEM.Complex.Elements.WaveData wave, Direction direction, double time)
        {
            // рассчитаем волну поляризованную вдоль оси
            double value = 0;
            switch (direction)
            {
                case Direction.X:
                    value = wave.Point.Y * wave.Amplitude *
                            System.Math.Cos(wave.Omega * time + wave.WaveNumber * position + wave.Phase) +
                            wave.Point.Z * wave.Amplitude *
                            System.Math.Cos(wave.Omega * time + wave.WaveNumber * position + wave.Phase);
                    break;
                case Direction.Y:
                    value = wave.Point.X * wave.Amplitude *
                            System.Math.Cos(wave.Omega * time + wave.WaveNumber * position + wave.Phase) +
                            wave.Point.Z * wave.Amplitude *
                            System.Math.Cos(wave.Omega * time + wave.WaveNumber * position + wave.Phase);
                    break;
                case Direction.Z:
                    value = wave.Point.X*wave.Amplitude*
                            System.Math.Cos(wave.Omega*time + wave.WaveNumber*position + wave.Phase) +
                            wave.Point.Y*wave.Amplitude*
                            System.Math.Cos(wave.Omega*time + wave.WaveNumber*position + wave.Phase);
                    break;
            }

            return new System.Numerics.Complex(value, 0);
        }

        /// <summary>
        /// Вычисление значения поля, создаваемого плоской волной
        /// </summary>
        /// <param name="position">Положение (текущие координаты)</param>
        /// <param name="data">Сведения о волне</param>
        /// <param name="time">Время</param>
        /// <returns>Возвращает скалярное значение поля</returns>
        public static double MonoWave(double position, EMFFM.Common.FEM.Scalar.Elements.WaveData data, double time)
        {
            return data.Amplitude*System.Math.Cos(position - data.Omega*time + data.Phase);
        }
    }
}
