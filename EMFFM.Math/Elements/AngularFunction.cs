﻿namespace EMFFM.Math.Elements
{
    /// <summary>
    /// Angular functons
    /// </summary>
    public class AngularFunction
    {
        public double Pi { get; set; }
        public double Tau { get; set; }
    }
}
