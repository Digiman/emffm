﻿using EMFFM.Math.Common;

namespace EMFFM.Math.Elements
{
    /// <summary>
    /// Сферические координаты
    /// </summary>
    public class Spheric
    {
        public double Theta { get; set; }
        public double Phi { get; set; }
        public double R { get; set; }

        /// <summary>
        /// Инициализация пустого объекта.
        /// </summary>
        public Spheric()
        {
        }

        /// <summary>
        /// Инициализация сферических координат
        /// </summary>
        /// <param name="val1">Значение первой координаты.</param>
        /// <param name="val2">Значение второй координаты.</param>
        /// <param name="val3">Значение третьей координаты.</param>
        /// <param name="isDecart">Значения переданы в декартовой системе координат (x,y,z).</param>
        public Spheric(double val1, double val2, double val3, bool isDecart = false)
        {
            if (isDecart)
            {
                // выполняем перевод из декартовой системы в сферическую
                var tmp = CoordinatesTransform.Cart2Sph(val1, val2, val3);
                Theta = tmp.Theta;
                Phi = tmp.Phi;
                R = tmp.R;
            }
            else
            {
                Theta = val1;
                Phi = val2;
                R = val3;
            }
        }

        public override string ToString()
        {
            return string.Format("({0} {1} {2})", Theta, Phi, R);
        }
    }
}
