﻿using System.Numerics;
using EMFFM.Math.Common;

namespace EMFFM.Math
{
    /// <summary>
    /// Класс для реализации функций Бесселя
    /// </summary>
    public class Bessel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="z"></param>
        /// <param name="steps"></param>
        /// <returns></returns>
        public static double Besselj(double v, double z, int steps)
        {
            double X = 0;
            double product = System.Math.Pow(z / 2, v);
            for (int i = 0; i < steps; i++)
            {
                X += System.Math.Pow(-System.Math.Pow(z, 2) / 4, i) / (MathOperations.Factorial(i) * GammaFunctions.Gamma(v + i + 1));
            }
            return X * product;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static double Bessely(double v, double z)
        {
            double Func = (Besselj(v, z, 55) * System.Math.Cos(v * System.Math.PI) - Besselj(v, z, 55)) / (System.Math.Sin(v *System.Math.PI));
            return Func;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex Besselh(double v, double z)
        {
            double real = Besselj(v, z, 55);
            double img = Bessely(v, z);
            var ot = new Complex((float)real, (float)img);
            return ot;
        }
    }
}
