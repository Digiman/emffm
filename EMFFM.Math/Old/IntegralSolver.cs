﻿namespace EMFFM.Math
{
    /// <summary>
    /// Класс для реализации расчета интеграла.
    /// </summary>
    public class IntegralSolver
    {
        private double _a;
        private double _b;
        private double _epsilon;
        private double _n;

        /// <summary>
        /// Инициализация решателя для интегралов со значениями по умолчанию.
        /// </summary>
        public IntegralSolver()
        {
            _a = 0;
            _b = 0;
            _epsilon = 0;
            _n = 200;
        }

        /// <summary>
        /// Инициализация решателя с параметрами.
        /// </summary>
        /// <param name="start">Начало интервала интегрирования.</param>
        /// <param name="end">Конец интервала интегрирования.</param>
        /// <param name="count">Количество интервалов, на которые делится расчетная область.</param>
        /// <param name="epsilon">Точность.</param>
        public IntegralSolver(double start, double end, int count, double epsilon)
        {
            _a = start;
            _b = end;
            _n = count;
            _epsilon = epsilon;
        }

        /// <summary>
        /// Функция интегрировыания.
        /// Функция: (2-sin(x)/(1+cos(x))
        /// </summary>
        /// <param name="x">Значение для функции.</param>
        /// <returns>Возвращает вычисленное значение функции.</returns>
        private double funk(double x)
        {
            double fun;
            fun = (2 - System.Math.Sin(x))/(1 + System.Math.Cos(x));
            return fun;
        }

        /// <summary>
        /// Расчет интеграла методом Симпсона.
        /// </summary>
        /// <returns>Возвращает вычисленное значение интеграла.</returns>
        public double SolveSimpson()
        {
            double h, s, p;
            h = (_b - _a)/_n;
            s = 0;
            p = 4;
            for (int j = 1; j < _n - 1; j++)
            {
                s = s + funk(_a + h*j)*p;
                p = 6 - p;
            }
            s = h/3*(funk(_a) + funk(_b) + s);
            return s;
        }

        /// <summary>
        /// Расчет интеграла методом трапеций.
        /// </summary>
        /// <returns>Возвращает вычисленное значение интеграла.</returns>
        public double SolveTrapeze()
        {
            double h, s;
            h = (_b - _a)/_n;
            s = 0;
            for (int j = 1; j < _n - 1; j++)
            {
                s = s + funk(_a + h*j);
            }
            s = h*((funk(_a) + funk(_b))/2 + s);
            return s;
        }

        /// <summary>
        /// Расчет интеграла методом левых прямоугольников.
        /// </summary>
        /// <returns>Возвращает вычисленное значение интеграла.</returns>
        public double LeftRectangle()
        {
            double h, s;
            h = (_b - _a)/_n;
            s = 0;
            for (int j = 1; j < _n; j++)
            {
                s = s + funk(_a + h*(j));
            }
            s = h*s;
            return s;
        }
    }
}