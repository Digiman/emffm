﻿using System;

namespace EMFFM.Math.Samples
{
    /// <summary>
    ///     Класс для показа примеров использования функций и их тестирования
    /// </summary>
    public class GammaSamples
    {
        /// <summary>
        ///     Тестирование гамма-функций
        /// </summary>
        public static void TestGamma()
        {
            Console.WriteLine("Testing Gamma()");

            double[,] test =
                {
                    {1e-20, 1e+20},
                    {2.19824158876e-16, 4.5490905327e+15},
                    {2.24265050974e-16, 4.45900953205e+15},
                    {0.00099, 1009.52477271},
                    {0.00100, 999.423772485},
                    {0.00101, 989.522792258},
                    {6.1, 142.451944066},
                    {11.999, 39819417.4793},
                    {12, 39916800.0},
                    {12.001, 40014424.1571},
                    {15.2, 149037380723.0}
                };

            double worst_absolute_error = 0.0;
            double worst_relative_error = 0.0;
            int worst_absolute_error_case = 0;
            int worst_relative_error_case = 0;

            int t;

            for (t = 0; t < test.GetUpperBound(0); t++)
            {
                double computed = GammaFunctions.Gamma(test[t, 0]);
                double absolute_error = System.Math.Abs(computed - test[t, 1]);
                double relative_error = absolute_error/test[t, 1];

                if (absolute_error > worst_absolute_error)
                {
                    worst_absolute_error = absolute_error;
                    worst_absolute_error_case = t;
                }

                if (relative_error > worst_relative_error)
                {
                    worst_relative_error = absolute_error;
                    worst_relative_error_case = t;
                }
            }

            t = worst_absolute_error_case;
            double x = test[t, 0];
            double y = test[t, 1];
            Console.WriteLine("Worst absolute error: {0}. Gamma({1}) computed as {2} but exact value is {3}",
                              System.Math.Abs(GammaFunctions.Gamma(x) - y), x, GammaFunctions.Gamma(x), y);

            t = worst_relative_error_case;
            x = test[t, 0];
            y = test[t, 1];
            Console.WriteLine("Worst relative error: {0}. Gamma({1}) computed as {2} but exact value is {3}",
                              System.Math.Abs(GammaFunctions.Gamma(x) - y)/y, x, GammaFunctions.Gamma(x), y);
        }

        /// <summary>
        ///     Тестирование логарифмической гамма-функции
        /// </summary>
        public static void TestLogGamma()
        {
            Console.WriteLine("Testing LogGamma()");

            double[,] test =
                {
                    {1e-12, 27.6310211159},
                    {0.9999, 5.77297915613e-05},
                    {1.0001, -5.77133422205e-05},
                    {3.1, 0.787375083274},
                    {6.3, 5.30734288962},
                    {11.9999, 17.5020635801},
                    {12, 17.5023078459},
                    {12.0001, 17.5025521125},
                    {27.4, 62.5755868211}
                };

            double worst_absolute_error = 0.0;
            double worst_relative_error = 0.0;
            int worst_absolute_error_case = 0;
            int worst_relative_error_case = 0;

            int t;

            for (t = 0; t < test.GetUpperBound(0); t++)
            {
                double computed = GammaFunctions.LogGamma(test[t, 0]);
                double absolute_error = System.Math.Abs(computed - test[t, 1]);
                double relative_error = absolute_error/test[t, 1];

                if (absolute_error > worst_absolute_error)
                {
                    worst_absolute_error = absolute_error;
                    worst_absolute_error_case = t;
                }

                if (relative_error > worst_relative_error)
                {
                    worst_relative_error = absolute_error;
                    worst_relative_error_case = t;
                }
            }

            t = worst_absolute_error_case;
            double x = test[t, 0];
            double y = test[t, 1];
            Console.WriteLine("Worst absolute error: {0}. LogGamma({1}) computed as {2} but exact value is {3}",
                              System.Math.Abs(GammaFunctions.LogGamma(x) - y), x, GammaFunctions.LogGamma(x), y);

            t = worst_relative_error_case;
            x = test[t, 0];
            y = test[t, 1];
            Console.WriteLine("Worst relative error: {0}. LogGamma({1}) computed as {2} but exact value is {3}",
                              System.Math.Abs(GammaFunctions.LogGamma(x) - y)/y, x, GammaFunctions.LogGamma(x), y);
        }
    }
}