﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Math.Elements;

namespace EMFFM.Math.Common
{
    /// <summary>
    /// Набор операция по преобразованию координат из различных систем.
    /// </summary>
    public static class CoordinatesTransform
    {
        /// <summary>
        /// Преобразование координат из декартовых в сферические
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="z">Координата Z</param>
        /// <returns>Возвращает объект со сферическими координатами</returns>
        public static Spheric Cart2Sph(double x, double y, double z)
        {
            return new Spheric()
            {
                Theta = System.Math.Atan(System.Math.Sqrt(x * x + y * y) / z),
                Phi = System.Math.Atan(y / x),
                R = System.Math.Sqrt(x * x + y * y + z * z)
            };
        }

        /// <summary>
        /// Преобразование из декартовой в сферическую систему
        /// </summary>
        /// <param name="points">Список точек в декартовой системе</param>
        /// <returns>Возвращает массив точек в сферической системе</returns>
        public static List<Spheric> Cart2Sph(List<Point> points)
        {
            var result = new List<Spheric>(points.Count);

            result.AddRange(points.Select(point => Cart2Sph(point.X, point.Y, point.Z)));

            return result;
        }

        /// <summary>
        /// Преобразование из сферических координат в декартовые
        /// </summary>
        /// <param name="sph">Сферические координаты</param>
        /// <returns>Возвращает точку с координатами в декартовой системе</returns>
        public static Point Sph2Cart(Spheric sph)
        {
            return new Point()
            {
                X = sph.R * System.Math.Sin(sph.Theta) * System.Math.Cos(sph.Phi),
                Y = sph.R * System.Math.Sin(sph.Theta) * System.Math.Sin(sph.Phi),
                Z = sph.R * System.Math.Cos(sph.Theta)
            };
        }

        /// <summary>
        /// Преобразование из сферических координат в декартовые
        /// </summary>
        /// <param name="sphs">Список точек в сферической системе координат</param>
        /// <returns>Список точек в декартовой системе координат</returns>
        public static List<Point> Sph2Cart(List<Spheric> sphs)
        {
            var result = new List<Point>(sphs.Count);
            result.AddRange(sphs.Select(sph => Sph2Cart(sph)));
            return result;
        }
    }
}
