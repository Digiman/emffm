﻿namespace EMFFM.Math.Common
{
    /// <summary>
    /// Класс для реализация вспомогатеьных математических операций.
    /// </summary>
    public static class MathOperations
    {
        /// <summary>
        /// Вычисление факториала
        /// </summary>
        /// <param name="value">Значение для расчета факториала</param>
        /// <returns>Возвращает значение факториала</returns>
        public static long Factorial(long value)
        {
            if (value <= 1)
                return 1;
            return value * Factorial(value - 1);
        }
    }
}
