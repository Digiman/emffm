﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EMFFM.Scheduler
{
    static class Program
    {
        static void Test1()
        {
            // Create a scheduler that uses two threads. 
            var lcts = new Scheduler(5);
            List<Task> tasks = new List<Task>();

            // Create a TaskFactory and pass it our custom scheduler. 
            TaskFactory factory = new TaskFactory(lcts);
            CancellationTokenSource cts = new CancellationTokenSource();

            // Use our factory to run a set of tasks. 
            Object lockObj = new Object();
            int outputItem = 0;

            for (int tCtr = 0; tCtr <= 4; tCtr++)
            {
                int iteration = tCtr;
                Task t = factory.StartNew(() =>
                {
                    for (int i = 0; i < 1000; i++)
                    {
                        lock (lockObj)
                        {
                            Console.Write("{0} in task t-{1} on thread {2}   ",
                                          i, iteration, Thread.CurrentThread.ManagedThreadId);
                            outputItem++;
                            if (outputItem % 3 == 0)
                                Console.WriteLine();
                        }
                    }
                }, cts.Token);
                tasks.Add(t);
            }
            // Use it to run a second set of tasks.                       
            for (int tCtr = 0; tCtr <= 4; tCtr++)
            {
                int iteration = tCtr;
                Task t1 = factory.StartNew(() =>
                {
                    for (int outer = 0; outer <= 10; outer++)
                    {
                        for (int i = 0x21; i <= 0x7E; i++)
                        {
                            lock (lockObj)
                            {
                                Console.Write("'{0}' in task t1-{1} on thread {2}   ",
                                              Convert.ToChar(i), iteration, Thread.CurrentThread.ManagedThreadId);
                                outputItem++;
                                if (outputItem % 3 == 0)
                                    Console.WriteLine();
                            }
                        }
                    }
                }, cts.Token);
                tasks.Add(t1);
            }

            // Wait for the tasks to complete before displaying a completion message.
            Task.WaitAll(tasks.ToArray());
        }

        static void Test2()
        {
            // Create a scheduler that uses two threads. 
            var scheduler = new Scheduler(10);
            var tasks = new List<Task>();

            // Create a TaskFactory and pass it our custom scheduler. 
            var factory = new TaskFactory(scheduler);
            var cts = new CancellationTokenSource();

            var file = new StreamWriter("test.txt");

            Object lockObj = new Object();
            for (int taskCount = 0; taskCount <= 5; taskCount++)
            {
                Task task = factory.StartNew(() =>
                {
                    file.WriteLine("-----------------");
                    file.WriteLine("Task {0} started...", Thread.CurrentThread.ManagedThreadId);
                    for (int i = 0; i < 1000; i++)
                    {
                        lock (lockObj)
                        {
                            file.WriteLine("Test message number {0} from task {1}.", i,
                                Thread.CurrentThread.ManagedThreadId);
                        }
                    }
                    file.WriteLine("Task {0} ended...", Thread.CurrentThread.ManagedThreadId);
                    file.WriteLine("-----------------");
                }, cts.Token);

                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());

            file.Close();

            /*// Use our factory to run a set of tasks. 
            Object lockObj = new Object();
            int outputItem = 0;

            for (int tCtr = 0; tCtr <= 4; tCtr++)
            {
                int iteration = tCtr;
                Task t = factory.StartNew(() =>
                {
                    for (int i = 0; i < 1000; i++)
                    {
                        lock (lockObj)
                        {
                            Console.Write("{0} in task t-{1} on thread {2}   ",
                                          i, iteration, Thread.CurrentThread.ManagedThreadId);
                            outputItem++;
                            if (outputItem % 3 == 0)
                                Console.WriteLine();
                        }
                    }
                }, cts.Token);
                tasks.Add(t);
            }*/
        }

        static async void Test3()
        {
            Object lockObj = new Object();

            /*var value =
                await
                    ClassForAsync.SumOfValuesAsync(100,
                        new System.Progress<int>(p =>
                        {
                            lock (lockObj)
                            {
                                Console.WriteLine("Progress is {0}...", p);
                            }
                        }));*/
           
            /*var task = Task.Run(() => ClassForAsync.SumOfValuesAsync(100,
                new Progress<int>(p => Console.WriteLine("Progress is {0}...", p))));
            */
            var task2 = Task.Run(() => ClassForAsync.SumOfValuesAsync(100,
                new System.Progress<int>()));

            //Task.WaitAll(task, task2);         

            Console.WriteLine("Value is {0}", task2.Result);
        }

        static void Main()
        {
            //Test2();
            Test3();

            Console.WriteLine("Successful completion.");
            Console.ReadKey();
        }
    }
}
