﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EMFFM.Scheduler
{
    public class ClassForAsync
    {
        public static Task<int> SumOfValuesAsync(int value, IProgress<int> progress)
        {
            int sum = 0;
            for (int i = 0; i < value; i++)
            {
                sum += i;
                progress.Report(sum);
            }
            return Task.FromResult(sum);
        }
    }

    public sealed class Progress<T> : IProgress<T>
    {
        private readonly Action<T> _handler;

        public Progress()
        {
        }

        public Progress(Action<T> handler)
        {
            _handler = handler;
        }

        public event EventHandler<T> ProgressChanged;

        private void OnProgressChanged(T e)
        {
            //EventHandler<T> handler = ProgressChanged;
            if (_handler != null) _handler(e);
            Trace.TraceInformation("Progress changed for thread {0}...", Thread.CurrentThread.ManagedThreadId);
        }

        public void Report(T value)
        {
            Object lockObj = new Object();

            lock (lockObj)
            {
                OnProgressChanged(value);
                Console.WriteLine("Report execute about value {0} ...", value);
            }
        }
    }
}
