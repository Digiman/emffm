filename='mie.jpg';
h = figure('Visible','off');
x=dlmread('x.txt');
y=dlmread('y.txt');
z=dlmread('matrix.txt');
surf(x, y, z);
view(0,90);
print(h,'-dbmp',filename)
close(h)