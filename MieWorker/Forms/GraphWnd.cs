﻿using System;
using System.Drawing;
using System.Windows.Forms;
using EMFFM.Common.Helpers;
using MieTheory.Common;
using MieWorker.Helpers;

namespace MieWorker.Forms
{
    /// <summary>
    /// Окно для отображения результатов решения задачи по теории Ми.
    /// </summary>
    public partial class GraphWnd : Form
    {
        /// <summary>
        /// Результаты решения 
        /// </summary>
        private readonly MieFullResults _results;

        /// <summary>
        /// Инициализация окна с результатами решения.
        /// </summary>
        /// <param name="results">Результаты решения по Теории Ми.</param>
        public GraphWnd(MieFullResults results)
        {
            _results = results;

            InitializeComponent();
        }

        /// <summary>
        /// Инициализация данных в окне по переданным данным в него.
        /// </summary>
        private void InitData()
        {
            numericUpDown1.Minimum = 0;
            numericUpDown1.Maximum = _results.X.Length;

            numericUpDown2.Minimum = 0;
            numericUpDown2.Maximum = _results.Y.Length;
        }

        #region Обработка событий кнопок

        /// <summary>
        /// Обработка события нажатия кнопки "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обработка события нажатия кнопки "Экспорт".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            dlg.Description = "Выберите каталог для экспорта в него результатов решения";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.SelectedPath))
            {
                _results.Export(dlg.SelectedPath);

                MessageBox.Show("Результаты успешно экспортированы!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Обработка события нажатия кнопки "Построить".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildButton_Click(object sender, EventArgs e)
        {
            var dataY = BuildData();

            // построение графика двухмерного
            chart1.Series.Clear();
            GraphHelper.DrawChart(chart1, _results.X, dataY, Color.Blue, Color.DarkRed);
        }

        #endregion

        private double[,] BuildData()
        {
            int sizeX = _results.X.Length;
            int sizeY = _results.Y.Length;

            // построение вектора (массива) для заданного положения по матрице среза
            double[,] dataY = new double[2, sizeX];

            var nx = numericUpDown1.Value != 0 ? (int)numericUpDown1.Value : (int)Math.Floor(sizeX / 2.0);
            var ny = numericUpDown2.Value != 0 ? (int)numericUpDown2.Value : (int)Math.Floor(sizeY / 2.0);

            for (int i = 0; i < sizeY; i++)
            {
                dataY[0, i] = _results.Z[i, nx];
            }
            for (int i = 0; i < sizeX; i++)
            {
                dataY[1, i] = _results.Z[ny, i];
            }

            return dataY;
        }
    }
}
