﻿using System;
using System.Windows.Forms;
using EMFFM.Common.Enums;
using MieTheory;
using MieTheory.Input;
using MieWorker.Helpers;

namespace MieWorker.Forms
{
    /// <summary>
    /// Главная форма (окно) приложения.
    /// </summary>
    public partial class MainWnd : Form
    {
        /// <summary>
        /// Загружен ли файл с задачей?
        /// </summary>
        private bool _fileLoaded;

        /// <summary>
        /// Файл, открытый или сохраненный.
        /// </summary>
        private string _currentFile;

        /// <summary>
        /// Решена ли задача?
        /// </summary>
        private bool _solved;

        /// <summary>
        /// Главный фасад для работы с методами по теории Ми.
        /// </summary>
        private IMieFacade _mieFacade;

        /// <summary>
        /// Инициализация окна и его компонентов.
        /// </summary>
        public MainWnd()
        {
            _mieFacade = new MieFacade();

            InitializeComponent();

            InitWindow();
            InitData();
        }

        /// <summary>
        /// Инициализация окна приложения и его компонентов.
        /// </summary>
        private void InitWindow()
        {
            HideContols();

            ChangeStatusText("Приложение готово к работе!");

            _fileLoaded = false;

            comboBox1.Items.AddRange(Enum.GetNames(typeof (MaterialTypes)));
            comboBox2.Items.AddRange(Enum.GetNames(typeof(ModelTypes)));
        }

        /// <summary>
        /// Инициализация данных в окне.
        /// </summary>
        private void InitData()
        {
            textBox1.Text = _mieFacade.Data.Radius.ToString();
            textBox2.Text = _mieFacade.Data.Lambda.ToString();
            textBox3.Text = _mieFacade.Data.Count.ToString();

            comboBox1.SelectedIndex = (int)_mieFacade.Data.Material;
            comboBox2.SelectedIndex = (int)_mieFacade.Data.MaterialModel;

            textBox5.Text = _mieFacade.Data.OutputPath;
        }

        #region Обработка компонентов формы

        /// <summary>
        /// Сокрытие контролов.
        /// </summary>
        private void HideContols()
        {
            SaveMenuItem.Enabled = false;
            CloseMenuItem.Enabled = false;

            groupBox1.Visible = false;
            ResultsButton.Enabled = false;
        }

        /// <summary>
        /// Показ контролов.
        /// </summary>
        private void ShowControls()
        {
            SaveMenuItem.Enabled = true;
            CloseMenuItem.Enabled = true;

            groupBox1.Visible = true;
            ResultsButton.Enabled = false;
        }

        /// <summary>
        /// Смена текста в панели статуса.
        /// </summary>
        /// <param name="text">Текст для отображенрия статуса.</param>
        private void ChangeStatusText(string text)
        {
            statusStrip1.Items[0].Text = text;
        }

        #endregion

        #region Обработка событий кнопок

        /// <summary>
        /// Обработка нажатия кнопки "ВЫбрать".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseButton_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.SelectedPath))
            {
                textBox5.Text = dlg.SelectedPath;
            }
        }

        /// <summary>
        /// Обработка нажатия кнопки "Решить".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolveButton_Click(object sender, EventArgs e)
        {
            // сбор данных с контролов на форме
            var data =  PrepareDataFromFormContols();

            // помещение их в фасад для работы с ними
            _mieFacade.Data = data;

            Solve();
        }

        /// <summary>
        /// Подготовка данных, которые могут быть изменены в окне.
        /// </summary>
        /// <returns>Возвращает параметры задачи.</returns>
        private InputMieData PrepareDataFromFormContols()
        {
            var data = new InputMieData();

            data.Radius = Convert.ToDouble(textBox1.Text);
            data.Lambda = Convert.ToDouble(textBox2.Text);
            data.Count = Convert.ToInt32(textBox3.Text);
            data.Material = (MaterialTypes) comboBox1.SelectedIndex;
            data.MaterialModel = (ModelTypes) comboBox2.SelectedIndex;

            data.OutputPath = textBox5.Text;

            return data;
        }

        /// <summary>
        /// Обработка нажатия кнопки "Результаты".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultsButton_Click(object sender, EventArgs e)
        {
            var wnd = new GraphWnd(_mieFacade.Results);
            wnd.ShowDialog();
        } 

        #endregion

        #region Обработка событий элементов меню

        /// <summary>
        /// Обработка выбора пункта меню "Открыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        /// <summary>
        /// Обработка выбора пункта меню "Сохранить".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        /// <summary>
        /// Обработка выбора пункта меню "Создать".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateMenuItem_Click(object sender, EventArgs e)
        {
            CreateTask();
        }

        /// <summary>
        /// Обработка выбора пункта меню "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseMenuItem_Click(object sender, EventArgs e)
        {
            CloseFile();
        }

        /// <summary>
        /// Обработка выбора пункта меню "Выход".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обработка выбора пункта меню "О программе".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            var wnd = new AboutWnd();
            wnd.ShowDialog();
        }

        #endregion

        #region Обработка событий формы

        private void MainWnd_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (
                MessageBox.Show("Выйти из приложения?", "Подтверждение выхода", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) ==
                DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion

        #region Основные рабочие методы с логикой

        /// <summary>
        /// Решение задачи с заданными параметрами.
        /// </summary>
        private void Solve()
        {
            _mieFacade.Solve(AppHelper.GetApplicationPath);

            _solved = true;

            ChangeStatusText("Задача успешно решена!");

            ResultsButton.Enabled = true;
        }

        /// <summary>
        /// Открытие файла с данными для работы.
        /// </summary>
        private void OpenFile()
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "Файлы XML|*.xml";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                _mieFacade.Data = _mieFacade.LoadInputData(dlg.FileName);

                _currentFile = dlg.FileName;

                InitData();

                ShowControls();
                _fileLoaded = true;
            }
        }

        /// <summary>
        /// Создание нового файла (открытие новой сессии работы с приложением).
        /// </summary>
        private void CreateTask()
        {
            var data = new InputMieData();

            _mieFacade = new MieFacade(data);
            _solved = false;

            ShowControls();
        }

        /// <summary>
        /// Закрытие текущей сессии работы с приложением.
        /// </summary>
        private void CloseFile()
        {
            HideContols();
            _fileLoaded = false;
            _solved = false;
        }

        /// <summary>
        /// Сохранение параметров задачи в файл.
        /// </summary>
        private void SaveFile()
        {
            if (String.IsNullOrEmpty(_currentFile))
            {
                var dlg = new SaveFileDialog();
                dlg.Filter = "Файлы XML|*.xml";
                dlg.ShowDialog();
                if (!String.IsNullOrEmpty(dlg.FileName))
                {
                    _mieFacade.SaveInputData(dlg.FileName, _mieFacade.Data);

                    _currentFile = dlg.FileName;

                    MessageBox.Show("Файл успешно сохранен!", "Информация", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
            else
            {
                _mieFacade.SaveInputData(_currentFile, _mieFacade.Data);

                MessageBox.Show("Файл успешно сохранен!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        #endregion
    }
}
