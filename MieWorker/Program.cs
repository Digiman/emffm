﻿using System;
using System.Windows.Forms;
using MieWorker.Forms;

namespace MieWorker
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            InitApp();

            Application.Run(new MainWnd());
        }

        /// <summary>
        /// Инициализация приложения с параметрами при старте.
        /// </summary>
        private static void InitApp()
        {
            // TODO: написать код для инициализации приложения
        }
    }
}
