﻿using System.Windows.Forms;

namespace MieWorker.Helpers
{
    public static class AppHelper
    {
        /// <summary>
        /// Получение каталога, в котором размещено приложения (откуда оно запущено).
        /// </summary>
        public static string GetApplicationPath
        {
            get { return Application.StartupPath; }
        }
    }
}
