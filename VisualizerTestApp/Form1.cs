﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ILNumerics;
using ILNumerics.Drawing;

namespace VisualizerTestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ilPanel1_Load(object sender, EventArgs e)
        {
            //ViewOneSphere();
            //ViewWireframeSphere();
            ViewMultipleObjects2();
            //ViewWithRotate();
            //ViewSceneWithReusingParts();
            //ViewPoints();
            //ViewLines();
        }

        private void ViewOneSphere()
        {
            var scene = new ILScene();
            scene.Camera.Add(new ILSphere());
            ilPanel1.Scene = scene;
        }

        private void ViewWireframeSphere()
        {
            var scene = new ILScene();
            scene.Camera.Add(new ILSphere
            {
                Wireframe = {Color = Color.Black},
                Fill = {Color = Color.FromArgb(150, Color.Transparent)}
            });
            ilPanel1.Scene = scene;
        }

        /// <summary>
        /// Построение нескольких объектов на экране.
        /// В примере строится сфера и треугольник плоский. 
        /// </summary>
        private void ViewMultipleObjects()
        {
            var scene = new ILScene();

            var sphere = scene.Camera.Add(
                new ILSphere
                {
                    Wireframe = {Color = Color.Black},
                    Fill = {Color = Color.FromArgb(150, Color.Yellow)}
                });

            // add a triangle shape
            var tri = sphere.Add(Shapes.Triangle);
            // configure its positions (vertices)
            tri.Positions.Update(new float[,]
            {
                {0, 0, 0}, // first vertex
                {1, 0, 0}, // second vertex
                {1, 1, 0}, // third vertex
            });
            // make the triangle red
            tri.Color = Color.Red;
            // turns lighting off for the triangle
            tri.AutoNormals = false;

            ilPanel1.Scene = scene;
        }

        /// <summary>
        /// Построение нескольких объектов на экране.
        /// В примере строится сфера и несколько плоских треугольников. 
        /// </summary>
        private void ViewMultipleObjects2()
        {
            var scene = new ILScene();

            var sphere = scene.Camera.Add(
                new ILSphere
                {
                    Wireframe = { Color = Color.Black },
                    Fill = { Color = Color.FromArgb(150, Color.Yellow) }
                });

            // add a triangle shape
            var tri = sphere.Add(Shapes.Triangle);
            // configure its positions (vertices)
            tri.Positions.Update(new float[,]
            {
                {0, 0, 0}, // first vertex
                {1, 0, 0}, // second vertex
                {0.5f, 1, 0.5f}, // third vertex
            });
            // make the triangle red
            tri.Color = Color.Red;
            // turns lighting off for the triangle
            tri.AutoNormals = false;
            
            //----------------

            // add a triangle shape
            var tri2 = sphere.Add(Shapes.Triangle);
            // configure its positions (vertices)
            tri2.Positions.Update(new float[,]
            {
                {0, 0, 0}, // first vertex
                {0.5f, 1, 0.5f}, // second vertex
                {1, 1, 1}, // third vertex
            });
            // make the triangle red
            tri2.Color = Color.YellowGreen;
            // turns lighting off for the triangle
            tri2.AutoNormals = false;

            //----------------

            // add a triangle shape
            var tri3 = sphere.Add(Shapes.Triangle);
            // configure its positions (vertices)
            tri3.Positions.Update(new float[,]
            {
                {0, 0, 0}, // first vertex
                {-1, -1, -1}, // second vertex
                {-1, 1, 0}, // third vertex
            });
            // make the triangle red
            tri3.Color = Color.Green;
            // turns lighting off for the triangle
            tri3.AutoNormals = false;

            ilPanel1.Scene = scene;
        }

        private void ViewWithRotate()
        {
            var scene = new ILScene();
            var sphere = scene.Camera.Add(
                new ILSphere
                {
                    Wireframe = {Color = Color.Black},
                    Fill = {Color = Color.FromArgb(150, Color.Yellow)}
                });

            // add a triangle shape
            var tri = sphere.Add(Shapes.Triangle);
            // configure its positions (vertices)
            tri.Positions.Update(new float[,]
            {
                {0, 0, 0}, // first vertex
                {1, 0, 0}, // second vertex
                {1, 1, 0}, // third vertex
            });
            // make the triangle red
            tri.Color = Color.Red;
            // turns lighting off for the triangle
            tri.AutoNormals = false;

            // reuse the triangles shape: 
            // add a new group to the sphere
            var group = sphere.Add(
                // the group gets a transform configured
                new ILGroup(rotateAxis: new Vector3(0, 0, 1),
                    angle: Math.PI/2));
            // add the old triangle to the group
            var tri2 = group.Add(tri);
            // give the new triangle a green color
            tri2.Color = Color.Green;

            // reuse the triangles shape: 
            // add a new group to the sphere
            var group2 = sphere.Add(
                // the group gets a transform configured
                new ILGroup(rotateAxis: new Vector3(0, 1, 0),
                    angle: Math.PI));
            // add the old triangle to the group
            var tri3 = group2.Add(tri);
            // give the new triangle a green color
            tri3.Color = Color.Blue;

            ilPanel1.Scene = scene;
        }

        private void ViewSceneWithReusingParts()
        {
            var scene = new ILScene();

            var sphere = scene.Camera.Add(
                new ILSphere
                {
                    Wireframe = {Color = Color.Black},
                    Fill = {Color = Color.FromArgb(150, Color.Yellow)}
                });

            // add a triangle shape
            var tri = sphere.Add(Shapes.Triangle);
            // configure its positions (vertices)
            tri.Positions.Update(new float[,]
            {
                {0, 0, 0}, // first vertex
                {1, 0, 0}, // second vertex
                {1, 1, 0}, // third vertex
            });
            // make the triangle red
            tri.Color = Color.Red;
            // turns lighting off for the triangle
            tri.AutoNormals = false;

            // reuse the triangles shape: 
            // add a new group to the sphere
            var group = sphere.Add(
                // the group gets a transform configured
                new ILGroup(rotateAxis: new Vector3(0, 1, 0),
                    angle: Math.PI));
            // add the old triangle to the group
            var tri2 = group.Add(tri);
            // give the new triangle a green color
            tri2.Color = Color.Green;

            // even more reusing 
            using (ILScope.Enter())
            {
                // make 50 random positions: range ([-10..10],[-10..10],[0..-20])
                ILArray<float> positions = ILMath.tosingle(ILMath.rand(3, 20));
                positions = positions*ILMath.array<float>(20f, 20f, -20f)
                            + ILMath.array<float>(-10f, -10f, 0f);
                // create 50 clones, each at a random position
                for (int i = 0; i < positions.S[1]; i++)
                {
                    var clone = scene.Camera.Add(sphere);
                    clone.Transform = Matrix4.Translation(
                        positions.GetValue(0, i),
                        positions.GetValue(1, i),
                        positions.GetValue(2, i));
                }
            }

            tri.Detach(); 
            tri.Positions.Update(0, 1, new float[,] {{0, 2, 0}});

            ilPanel1.Scene = scene;
        }

        private void ViewPoints()
        {
            var scene = new ILScene
            {
                Camera =
                {
                    new ILPoints
                    {
                        Positions = ILMath.tosingle(ILMath.randn(3, 1000))
                    }
                }
            };

            ilPanel1.Scene = scene;
        }

        private void ViewLines()
        {
            var scene = new ILScene();
            // create a new shape by starting from a predefined shape 
            var line = scene.Camera.Add(Shapes.Line);
            // construct the pentagons positions, start with angles ...
            ILArray<float> angles = ILMath.linspace<float>(0, ILMath.pi * 2f, 6);
            // ... and derive X and Y coordinates
            ILArray<float> pos = ILMath.zeros<float>(3, 5);
            pos["0;:"] = ILMath.sin(angles); pos["1;:"] = ILMath.cos(angles);
            // set the pentagon positions
            line.Positions = pos;
            // set the pentagon indices
            line.Indices = new int[] {0, 1, 1, 2, 2, 3, 3, 4, 4, 0};
            // display the scene
            ilPanel1.Scene = scene;
        }
    }
}
