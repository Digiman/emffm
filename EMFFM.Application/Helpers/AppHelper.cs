﻿using System;
using System.Configuration;
using System.IO;

namespace EMFFM.App.Helpers
{
    /// <summary>
    /// Вспомогательные методы приложения
    /// Используется конфигурационный файл App.config!
    /// </summary>
    public static class AppHelper
    {
        /// <summary>
        /// Каталог с приложением
        /// </summary>
        public static string ApplicationPath
        {
            get { return Environment.CurrentDirectory; }

        }

        /// <summary>
        /// Полный путь к каталогу с исходными файлами
        /// </summary>
        public static string InputPath
        {
            get { return ApplicationPath + ConfigurationManager.AppSettings["InputPath"]; }
        }

        /// <summary>
        /// Полный путь к каталогу с выходными файлами
        /// </summary>
        public static string OutputPath
        {
            get { return ApplicationPath + ConfigurationManager.AppSettings["OutputPath"]; }
        }

        /// <summary>
        /// Полный путь к каталогу с логами
        /// </summary>
        public static string LogsPath
        {
            get { return ApplicationPath + ConfigurationManager.AppSettings["LogsPath"]; }
        }

        /// <summary>
        /// Использовать ли аргументы по умолчанию
        /// </summary>
        public static bool IsDefaultArguments
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["DefaultArguments"]); }
        }

        /// <summary>
        /// Ожидать ли нажатия клавиши перед выходом из приложения
        /// </summary>
        public static bool WaitForExit
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["WaitForExit"]); }
        }

        /// <summary>
        /// Входной файл по умолчанию
        /// </summary>
        public static string InputFile
        {
            get { return ConfigurationManager.AppSettings["InputFile"]; }
        }

        /// <summary>
        /// Файл автоматизации
        /// </summary>
        public static string AutomationFile
        {
            get { return InputPath + "\\" + ConfigurationManager.AppSettings["AutomationFile"]; }
        }

        /// <summary>
        /// Включена ли автоматизированная обработка
        /// </summary>
        public static bool AutomationEnabled
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["AutomationEnabled"]); }
        }

        /// <summary>
        /// Проверка существования каталога
        /// </summary>
        /// <param name="path">Путь к каталогу</param>
        /// <returns>Возвращает true если каталог существует</returns>
        public static bool CheckDirectory(string path)
        {
            return Directory.Exists(path);
        }

        /// <summary>
        /// Проверка существования каталога и его создание в случае отсутствия
        /// </summary>
        /// <param name="path">Путь к каталогу</param>
        public static void CheckAndCreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
