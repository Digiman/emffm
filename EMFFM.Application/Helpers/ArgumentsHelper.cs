﻿using EMFFM.App.Common;

namespace EMFFM.App.Helpers
{
    /// <summary>
    /// Вспомогательные методы для работы с аргументами
    /// </summary>
    public static class ArgumentsHelper
    {
        /// <summary>
        /// Разбора параметров из командной строки
        /// </summary>
        /// <param name="arguments">Экземпляр объкта с аргументами</param>
        /// <param name="args">Список аргументов</param>
        public static void Parse(ref Arguments arguments, string[] args)
        {
            // TODO: описать все ключи и параметры разбора аргументов командной строки

            for (int i = 0; i < args.Length; i += 2)
            {
                switch (args[i])
                {
                    case "-in": // входной файл
                        arguments.InputFile = args[i + 1];
                        break;
                    case "-out": // выходной файл лога
                        arguments.OutputLogFile = args[i + 1];
                        break;
                    case "-a": // файл автоматизации
                        arguments.AutomationFile = args[i + 1];
                        break;
                }
            }
        }
    }
}
