﻿using System;
using System.Collections.Generic;
using EMFFM.App.Controllers;
using EMFFM.Common.Helpers;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.App.Helpers
{
    // TODO: продумать как формировать списки для последовательности решения задачи в зависимости от типа решателя и других параметров

    /// <summary>
    /// Вспомогательный класс для ассоциации последовательности выполняемых функций
    /// в зависимости от типа значений и решателя
    /// </summary>
    static class ActionsHelper
    {   
        /// <summary>
        /// Формирование списка действий по тем, что описаны в InputAPI
        /// </summary>
        /// <returns>Возвращает словарь ассоциаций</returns>
        public static Dictionary<ActionNames, Action<InputData>> GetActions()
        {
            var actions = new Dictionary<ActionNames, Action<InputData>>();

            actions.Add(ActionNames.GenerateMesh, ActionOperations.GenerateMesh);
            actions.Add(ActionNames.Solve, ActionOperations.Solve);

            return actions;
        }
    }
}
