﻿using EMFFM.App.Helpers;

namespace EMFFM.App.Common
{
    /// <summary>
    /// Аргументы командной строки для приложения
    /// </summary>
    public class Arguments
    {
        /// <summary>
        /// Входной файл
        /// </summary>
        public string InputFile { get; set; }
        /// <summary>
        /// Выходной файл с полным логом операций
        /// </summary>
        public string OutputLogFile { get; set; }
        /// <summary>
        /// Файл автоматизации, со списком файлов для обработки
        /// </summary>
        public string AutomationFile { get; set; }

        /// <summary>
        /// Инициализация списка аргументов
        /// </summary>
        /// <param name="isDefault">Установить параметры по умолчанию</param>
        public Arguments(bool isDefault)
        {
            if (isDefault)
            {
                SetDefault();
            }
        }

        /// <summary>
        /// Установка значений по умолчанию для аргументов
        /// </summary>
        private void SetDefault()
        {
            // TODO: описать параметры по умолчанию для аргументов
            InputFile = AppHelper.InputFile;
            OutputLogFile = @"output\CommonLog.txt";
        }
    }
}
