﻿using EMFFM.App.Common;
using EMFFM.App.Helpers;

namespace EMFFM.App.Extensions
{
    /// <summary>
    /// Расширяющие методы для работы с аргументами
    /// </summary>
    public static class ArgumentsExtensions
    {
        /// <summary>
        /// Получает полный путь ко входному файлу
        /// </summary>
        /// <param name="args">Аргументы</param>
        /// <returns>Возвращает полный путь с именем входного файла</returns>
        public static string GetInputFile(this Arguments args)
        {
            return AppHelper.InputPath + "\\" + args.InputFile;
        }
    }
}
