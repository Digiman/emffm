﻿using EMFFM.App.Automation;
using EMFFM.App.Helpers;

namespace EMFFM.App.Extensions
{
    /// <summary>
    /// Расширения для работы с входным файлом
    /// </summary>
    public static class InputFileExtensions
    {
        /// <summary>
        /// Получает полный путь ко входному файлу
        /// </summary>
        /// <param name="file">Входной файл из данных для автоматизации</param>
        /// <returns>Возвращает полный путь с именем входного файла</returns>
        public static string GetFileName(this InputFile file)
        {
            return AppHelper.InputPath + "\\" + file.Name;
        }
    }
}
