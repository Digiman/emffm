﻿using System.Collections.Generic;

namespace EMFFM.App.Automation
{
    /// <summary>
    /// Класс для описания набора автоматизации для автоматизированного
    /// управления программой
    /// </summary>
    public class AutomationData
    {
        /// <summary>
        /// Список входных файлов для автоматизации
        /// </summary>
        public List<InputFile> Files { get; set; }

        /// <summary>
        /// Инициализация объекта со значениями по умолчанию
        /// </summary>
        public AutomationData()
        {
            Files = new List<InputFile>();
        }
    }
}
