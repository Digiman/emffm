﻿using System;
using System.Collections.Generic;
using System.Xml;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;

namespace EMFFM.App.Automation
{
    /// <summary>
    /// Класс для чтения и разбора файла автоматизации
    /// </summary>
    public static class AutomationFileReader
    {
        /// <summary>
        /// Чтение файла с данными для автоматизации
        /// </summary>
        /// <param name="fname">Имя файла для разбора</param>
        public static AutomationData ReadFile(string fname)
        {
            LogHelper.Log(LogMessageType.Info, "Read automation file {0}.", fname);

            try
            {
                // открываем и загружаем документ в переменную doc
                var doc = new XmlDocument();
                doc.Load(fname);

                var data = new AutomationData();

                // чтение секций входного файла и их разбор
                XmlNodeList list = doc.DocumentElement.ChildNodes;

                // вне зависимости от порядка следования элементов (секциий) разбираем их
                foreach (XmlNode item in list)
                {
                    if (item.NodeType == XmlNodeType.Element)
                    {
                        switch (item.Name)
                        {
                            case "files": // секция со списком файлов для автоматизации
                                ReadFiles(item.ChildNodes, ref data);
                                break;
                        }
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                LogHelper.Log(LogMessageType.Error, String.Format("Catch error in reading process of automation file.  Error message: {0}", ex.Message));
                throw new Exception("Возникло исключение при обработке файла автоматизации! " + ex.Message);
            }
        }

        /// <summary>
        /// Чтение списка файлов для автоматизации
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа с данными</param>
        /// <param name="data">Данные для автоматизации</param>
        private static void ReadFiles(XmlNodeList xmlNodeList, ref AutomationData data)
        {
            if (xmlNodeList == null) throw new ArgumentNullException("xmlNodeList");
            
            data.Files = new List<InputFile>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "file":
                        if (node.Attributes != null)
                            data.Files.Add(new InputFile
                                               {
                                                   Name = node.Attributes["name"].Value,
                                                   IsEnabled = Convert.ToBoolean(node.Attributes["enabled"].Value)
                                               });
                        break;
                }
            }
        }
    }
}
