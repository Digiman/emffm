﻿namespace EMFFM.App.Automation
{
    /// <summary>
    /// Входной файл и его параметры
    /// </summary>
    public class InputFile
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Использовать ли файл?
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public InputFile()
        {
            IsEnabled = false;
        }

        /// <summary>
        /// Инициализация входного файла и его параметров
        /// </summary>
        /// <param name="name">Имя файла</param>
        /// <param name="enabled">Обрабатывать ли файл</param>
        public InputFile(string name, bool enabled)
        {
            Name = name;
            IsEnabled = enabled;
        } 
    }
}
