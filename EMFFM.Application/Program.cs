﻿using System;
using EMFFM.App.Automation;
using EMFFM.App.Common;
using EMFFM.App.Controllers;
using EMFFM.App.Helpers;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Input;
using EMFFM.App.Extensions;

namespace EMFFM.App
{
    /// <summary>
    /// Базовый класс главного консольного приложения
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Выполнение предварительных действий по подготовке программы к работе
        /// </summary>
        static void PrerequisuteActions()
        {
            // TODO: описать подготовительные операции (проверка каталогов и их создание и прочее)
            AppHelper.CheckAndCreateDirectory(AppHelper.LogsPath);
            AppHelper.CheckAndCreateDirectory(AppHelper.OutputPath);
            // очистка рабочей папки для вывода файлов результатов
            IoHelper.ClearPath(AppHelper.OutputPath);
        }

        /// <summary>
        /// Действия, выполняемые в завершении выполнения программы
        /// </summary>
        static void FinishActions()
        {
            // TODO: написать код для финального метода, завершающего выполнение программы
        }

        /// <summary>
        /// Чтение и разбор входного файла
        /// </summary>
        /// <param name="fname">Путь и имя файла со входными данными</param>
        static InputData ReadInputFile(string fname)
        {
            return InputFileReader.ReadInputFile(fname);
        }

        /// <summary>
        /// Разбор аргументов командной строки
        /// </summary>
        /// <param name="args">Список аргументов</param>
        static Arguments ParseArguments(string[] args)
        {
            if (args.Length != 0)
            {
                var arguments = new Arguments(false);
                ArgumentsHelper.Parse(ref arguments, args);
                return arguments;
            }
            else
            {
                if (AppHelper.IsDefaultArguments)
                {
                    return new Arguments(AppHelper.IsDefaultArguments);
                }
                else
                {
                    throw new Exception("Входные аргументы программы отсуствуют!");
                }
            }
        }

        /// <summary>
        /// Проверка папок
        /// </summary>
        /// <param name="data">Входные данные из файла</param>
        private static void CheckFolders(InputData data)
        {
            AppHelper.CheckAndCreateDirectory(data.Output.Path);
        }

        /// <summary>
        /// Выполнение основных действий с заданным входным файлом
        /// </summary>
        /// <param name="fileName">Имя входного файла</param>
        static void Do(string fileName)
        {
            // 1. Разбор входного файла
            LogHelper.Log(LogMessageType.Info, "Reading input file...");

            // здесь читаем входной файл (метод здесь же, в текущем проекте и классе)
            LogHelper.Log(LogMessageType.Info, "Input File: {0}", fileName);
            InputData data = ReadInputFile(fileName);

            CheckFolders(data);

            LogHelper.Log(LogMessageType.Info, "Input file reading is finished!");

            // 2. Запуск контроллера автоматизированного выполнения
            LogHelper.Log(LogMessageType.Info, "Start working...");

            // здесь запускаем котроллер и передаем ему считанные данные из входного файла
            var controller = new AppController(data);
            controller.Run();
            controller.Dispose();

            LogHelper.Log(LogMessageType.Info, "End working! All's done!");
        }

        /// <summary>
        /// Главный метод приложения, запускающий его на выполнение
        /// </summary>
        /// <param name="args">Аргументы командной строки</param>
        static void Main(string[] args)
        {
            // выполнение предварительных действий перед запуском приложения
            PrerequisuteActions();

            try
            {
                // Начало работы программы
                LogHelper.Log(LogMessageType.Info, "Starting application...");
                Arguments arguments = ParseArguments(args);

                LogHelper.Log(LogMessageType.Info, "Application is started!");

                // проверка на автоматизацию
                if (AppHelper.AutomationEnabled)
                {
                    LogHelper.Log(LogMessageType.Info, "Application run in auto mode!");

                    // чтение и разбор файла автоматизации
                    var autoData = AutomationFileReader.ReadFile(AppHelper.AutomationFile);
                    foreach (var file in autoData.Files)
                    {
                        if (file.IsEnabled)
                        {
                            Do(file.GetFileName());
                        }
                    }
                }
                else
                {
                    Do(arguments.GetInputFile());
                }

                // Завершение программы
                LogHelper.Log(LogMessageType.Info, "Application is finished!");
            }
            catch (Exception ex)
            {
                LogHelper.Log(LogMessageType.Error, "Exception {0}", ex.Message);
            }
            finally
            {
                // выполнение финальных операций
                FinishActions();
            }

            // выходим из приложения или ждем нажатия клавиши
            if (AppHelper.WaitForExit)
            {
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
        }
    }
}
