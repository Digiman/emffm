﻿using System;
using System.Linq;
using EMFFM.App.Helpers;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.FEM;
using EMFFM.FEM.Helpers;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.Mesh.Helpers;
using EMFFM.Mesh.Old;

namespace EMFFM.App.Controllers
{
    /// <summary>
    /// Класс с функциями, описывающими порядок выполнения действий
    /// </summary>
    static class ActionOperations
    {
        /// <summary>
        /// Генерация сетки с использованием программы 
        /// </summary>
        /// <param name="data">Исходные данные</param>
        public static void GenerateMesh(InputData data)
        {
            string fileName = StringHelper.GetFileNameWithoutExtension(data.Geomerty.GometryFile);
            // используем генератор сеток Netgen
            NetgenMeshHelper.GenerateNetgenMesh(data, fileName, AppHelper.InputPath);
        }

        /// <summary>
        /// Генерация сетки из тетраэдров с помощью Netgen
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <returns>Возвращает данные сетки</returns>
        public static TMesh<Tetrahedron, Triangle> GenerateNetgenMesh(InputData data)
        {
            string fileName = StringHelper.GetFileNameWithoutExtension(data.Geomerty.GometryFile);
            // используем генератор сеток Netgen
            return NetgenMeshHelper.GenerateNetgenMesh(data, fileName, AppHelper.InputPath);
        }

        /// <summary>
        /// Выполнить полное решение
        /// </summary>
        /// <param name="data">Исходные данные</param>
        public static void Solve(InputData data)
        {
            // TODO: описать последовательность методов, необходимых для решения задачи

            // 1. Построение сетки
            TMesh<Tetrahedron, Triangle> mesh = GenerateNetgenMesh(data);
            
            // 2. Выбор нужного решателя и отправка ему данных
            switch (data.Task.SolverType)
            {
                case SolverTypes.FrequencyDomain:
                    switch (data.Task.ValueType)
                    {
                        case ValueTypes.Scalar:
                            SolveFrequencyScalar(data, mesh);
                            break;
                        case ValueTypes.Complex:
                            SolveFrequencyComlex(data, mesh);
                            break;
                    }
                    break;
                case SolverTypes.TimeDomain:
                    // NOTE: пока решение для временной области не нужно!
                    switch (data.Task.ValueType)
                    {
                        case ValueTypes.Scalar:
                            break;
                        case ValueTypes.Complex:
                            break;
                    }
                    break;
            }
        }

        /// <summary>
        /// Решение по частоте с комплексными числами
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <param name="mesh">Сгенерированная сетка</param>
        private static void SolveFrequencyComlex(InputData data, TMesh<Tetrahedron, Triangle> mesh)
        {
            // TODO: описать список функций для проведения решения по частоте для комплексных значений

            // 1. Формируем объект для построения глобальных матриц
            var matrix = new FEM.Complex.GlobalMatrix<Tetrahedron, Triangle>(data, mesh);

            // 2. Формируем объект для построения векторов в воздействиями (значения падающего поля)
            var wave = WaveDataHelper.ComplexWaveData(data); // формируем сведения о волне от источника
            var boundSource = MeshHelper.GetBoundaryEdges(mesh,
                                                          data.Sources.Single(s => s.Type == SourceTypes.Simple).Boundary);
            var edgeSource = MeshHelper.GetEdgesInDomain(mesh,
                                                         data.Sources.Single(s => s.Type == SourceTypes.Simple).Domain);
            var vector = new FEM.Complex.ForceVector(wave, SourceFunctions.MonochromeWaveFull, mesh.EdgesCount,
                                                     edgeSource);

            // 3. Использование решателя для решения
            int[] boundNumsBoundary = MeshHelper.GetNumbersOfDomainEdges(mesh,
                                                                         data.Boundaries.Single(
                                                                             b => b.Type == BoundaryTypes.Dirichlet).
                                                                             DomainNumber);
            var solver = new FEM.Complex.Solvers.FrequencySolver<Tetrahedron, Triangle>(data, wave, matrix, vector, boundNumsBoundary);
            var results = solver.Solve();

            // 3.1. Вывод сведений о полученных результатах
            if (data.Options.IsOutputToFile)
            {
                int count = 0;
                foreach (var item in solver.Results)
                {
                    item.OutputToFile(String.Format("{0}resultdata-{1}.txt", data.Output.Path, count));
                    count++;
                }
            }

            // 4. Выполнение анализа результатов решения
            var analysis = new FEM.Complex.FieldAnalysis<Tetrahedron, Triangle>(solver.Results, mesh, ElementType.Tetrahedron);
            //analysis.CalculateDifference();
            analysis.CalculateFields(data.Geomerty.CoFactor);
            analysis.PrintElementsField(data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields);

            // 5. Оценка результата на основе проб, указанных во входном файле
            if (data.Options.IsProbeOutput)
            {
                foreach (var item in data.ProbePoints)
                {
                    switch (item.Type)
                    {
                        case ProbeType.Domain:
                            // собираем сведения из области
                            ProbeHelper.GetProbeByDomain(data, mesh, item.DomainNumber, analysis);
                            break;
                        case ProbeType.Point:
                            // собираем сведения в точке
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Решение по частоте с целыми (скалярными) значениями
        /// </summary>
        /// <param name="data">Исходные данные</param>
        /// <param name="mesh">Сгенерированная сетка</param>
        private static void SolveFrequencyScalar(InputData data, TMesh<Tetrahedron, Triangle> mesh)
        {
            // 1. Формируем объект для построения глобальных матриц
            var matrix = new FEM.Scalar.GlobalMatrix<Tetrahedron, Triangle>(data, mesh);

            // 2. Формируем объект для построения векторов в воздействиями (значения падающего поля)
            var wave = WaveDataHelper.ScalarWaveData(data); // формируем сведения о волне от источника
            var boundSource = MeshHelper.GetBoundaryEdges(mesh, 
                data.Sources.Single(s => s.Type == SourceTypes.Simple).Boundary);
            var vector = new FEM.Scalar.ForceVector(wave, SourceFunctions.MonoWave, mesh.EdgesCount, boundSource);

            // 3. Использование решателя для решения
            int[] boundNumsBoundary = MeshHelper.GetNumbersOfBoundaryEdges(mesh, 
                data.Boundaries.Single(b => b.Type == BoundaryTypes.Dirichlet).BoundaryNumber);
            var solver = new FEM.Scalar.Solvers.FrequencySolver<Tetrahedron, Triangle>(data, wave, matrix, vector, boundNumsBoundary);
            var results = solver.Solve();

            // 3.1. Вывод сведений о полученных результатах
            if (data.Options.IsOutputToFile)
            {
                int count = 0;
                foreach (var item in solver.Results)
                {
                    item.OutputToFile(String.Format("{0}resultdata-{1}.txt", data.Output.Path, count));
                    count++;
                }
            }

            // 4. Выполнение анализа результатов решения
            var analysis = new FEM.Scalar.FieldAnalysis<Tetrahedron, Triangle>(solver.Results, mesh,
                                                                               ElementType.Tetrahedron, true);
            //analysis.CalculateDifference();
            analysis.CalculateFields(data.Geomerty.CoFactor);
            analysis.PrintElementsField(data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields);

            // 5. Оценка результата на основе проб, указанных во входном файле
            if (data.Options.IsProbeOutput)
            {
                foreach (var item in data.ProbePoints)
                {
                    switch (item.Type)
                    {
                        case ProbeType.Domain:
                            // собираем сведения из области
                            ProbeHelper.GetProbeByDomain(data, mesh, item.DomainNumber, analysis);
                            break;
                        case ProbeType.Point:
                            // собираем сведения в точке
                            break;
                    }
                }
            }
        }

        #region Методы для организации решения во временной области
        
        private static void SolveTimeComlex(InputData data)
        {

        }

        private static void SolveTimeScalar(InputData data)
        {

        } 

        #endregion
    }
}
