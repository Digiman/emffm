﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using EMFFM.App.Helpers;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Input;
using EMFFM.Input.Enums;

namespace EMFFM.App.Controllers
{
    /// <summary>
    /// Главный контроллер, управляющий работой программы
    /// </summary>
    class AppController : IDisposable
    {
        private InputData _data;

        private Dictionary<ActionNames, Action<InputData>> _actions;

        /// <summary>
        /// Инициализация контроллера
        /// </summary>
        /// <param name="inputData">Исходные данные для приложения</param>
        public AppController(InputData inputData)
        {
            _data = inputData;
            InitController();
        }

        /// <summary>
        /// Инициализация параметров, необходимых для работы контроллера
        /// </summary>
        private void InitController()
        {
            _actions = ActionsHelper.GetActions();
        }

        /// <summary>
        /// Переинициализация контроллера новым списком действий
        /// </summary>
        /// <param name="actions">Новый список действий</param>
        public void ReinitController(Dictionary<ActionNames, Action<InputData>> actions)
        {
            _actions = actions;
        }

        /// <summary>
        /// Запуск на выполнения набора действий
        /// </summary>
        /// <returns>Возвращает список значений со временем выполнения каждого действия</returns>
        public List<long> Run()
        {
            var results = new List<long>(_data.Actions.Count);
            
            // выполняем действия в порядке их расположения в списке (по возрастанию кода)
            results.AddRange(_data.Actions.Select(Run));

            return results;
        }

        /// <summary>
        /// Запуск на обработку определенного действия
        /// </summary>
        /// <param name="action">Действие для выполнения</param>
        /// <returns>Возвращает время в миллисекундах, которое выполнялось действие</returns>
        private long Run(Input.Elements.Action action)
        {
            LogHelper.Log(LogMessageType.Info, "Run action {0}", action);
            
            var time = new Stopwatch();
            time.Start();
            
            _actions[action.Value](_data);

            time.Stop();

            LogHelper.Log(LogMessageType.Info, "Ends action {0} for {1} ms", action, time.ElapsedMilliseconds);

            return time.ElapsedMilliseconds;
        }

        public void Dispose()
        {
            _data = null;
            _actions = null;
        }
    }
}
