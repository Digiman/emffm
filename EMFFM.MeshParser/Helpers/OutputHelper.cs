﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace EMFFM.MeshParser.Helpers
{
    /// <summary>
    /// Вспомагательный класс для вывода данных о сетке в файлы.
    /// </summary>
    public static class OutputHelper
    {
        /// <summary>
        /// Вывод данных о сетке в XML файл
        /// </summary>
        /// <param name="data">Данные сетки</param>
        /// <param name="fname">Файл для вывода</param>
        public static void OutputMeshDataToXmlFile(MeshData data, string fname)
        {
            var file = new XmlTextWriter(fname, Encoding.UTF8);

            file.WriteStartDocument();
            file.WriteStartElement("meshdata");

            // 1. Пишем список узлов
            file.WriteStartElement("nodes");
            file.WriteStartAttribute("count");
            file.WriteValue(data.Points.Count);
            int i = 0;
            foreach (var item in data.Points)
            {
                file.WriteStartElement("node");
                file.WriteStartAttribute("number");
                file.WriteValue(i + 1);
                file.WriteStartAttribute("x");
                file.WriteValue(item.X);
                file.WriteStartAttribute("y");
                file.WriteValue(item.Y);
                file.WriteStartAttribute("z");
                file.WriteValue(item.Z);
                file.WriteEndElement();
                i++;
            }
            file.WriteEndElement();

            // 2. Пишем список объемных элементов
            file.WriteStartElement("volelements");
            file.WriteStartAttribute("count");
            file.WriteValue(data.VolumeElements.Count);
            i = 0;
            foreach (var item in data.VolumeElements)
            {
                file.WriteStartElement("element");
                file.WriteStartAttribute("subdomain");
                file.WriteValue(item.SubDomain);
                for (var j = 0; j < item.Nodes.Length; j++)
                {
                    file.WriteStartAttribute(String.Format("node{0}", j + 1));
                    file.WriteValue(item.Nodes[j]);
                }
                file.WriteEndElement();
                i++;
            }
            file.WriteEndElement();

            // 3. Пишем список поверхностных элементов
            file.WriteStartElement("surfelements");
            file.WriteStartAttribute("count");
            file.WriteValue(data.SurfaceElements.Count);
            i = 0;
            foreach (var item in data.SurfaceElements)
            {
                file.WriteStartElement("node");
                if (item.BoundaryCond != 0)
                {
                    file.WriteStartAttribute("boundcond");
                    file.WriteValue(item.BoundaryCond);
                }
                for (var j = 0; j < item.Nodes.Length; j++)
                {
                    file.WriteStartAttribute(String.Format("node{0}", j + 1));
                    file.WriteValue(item.Nodes[j]);
                }
                file.WriteEndElement();
                i++;
            }
            file.WriteEndElement();

            file.WriteEndElement();

            file.Close();
        }

        /// <summary>
        /// Вывод сведений о сетке в текстовый файл.
        /// Для использования в Mathcad и других приложениях.
        /// </summary>
        /// <param name="data">Данные сетки.</param>
        /// <param name="fname">Файл для вывода.</param>
        /// <remarks>Выводит данные для объемных элементов (тетраэдров).</remarks>
        public static void OutputMeshToTextFile(MeshData data, string fname)
        {
            var file = new StreamWriter(fname);

            // запись списка элементов
            foreach (var element in data.VolumeElements)
            {
                // пишем для каждого элемента значения координат его узлов
                foreach (var node in element.Nodes)
                {
                    file.Write("{0} {1} {2} ", data.Points[node - 1].X, data.Points[node - 1].Y, data.Points[node - 1].Z);
                }
                file.WriteLine();
            }

            file.Close();
        }
    }
}
