﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EMFFM.Common.Extensions;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MeshParser.Elements;
using EMFFM.MeshParser.Enums;

namespace EMFFM.MeshParser.Helpers
{
    /// <summary>
    /// Вспомогательные методы для библиотеки
    /// </summary>
    static class CommonHelpers
    {
        /// <summary>
        /// Словарь ассоциаций между типами файлов и их расширениями
        /// </summary>
        static readonly Dictionary<string, FileTypes> Files = new Dictionary<string, FileTypes>
        {
            { "neu", FileTypes.NeutralFormat },
            { "surf", FileTypes.SurfaceTriangulation }
        };

        /// <summary>
        /// Получение расширения.
        /// </summary>
        /// <param name="fname">Имя файла</param>
        /// <returns>Возвращает строку с расширением</returns>
        private static string GetFileExtensions(string fname)
        {
            var fi = new FileInfo(fname);
            return fi.Extension.Trim('.');
        }

        /// <summary>
        /// Получение типа файла (по его расширению).
        /// </summary>
        /// <param name="fname">Имя файла.</param>
        /// <returns>Возвращает тип файла.</returns>
        public static FileTypes GetFileType(string fname)
        {
            string ext = GetFileExtensions(fname);

            return Files[ext];
        }

        /// <summary>
        /// Разбор строки с координатами узла.
        /// </summary>
        /// <param name="line">Строка.</param>
        /// <returns>Возвращает массив координат узла.</returns>
        public static double[] ParseCoordinatesString(string line)
        {
            string[] mas = line.CorrectDoubleStringValue().Split(' ');
            mas = RemoveSpaces(mas);

            double[] result = new double[mas.Length];

            for (int i = 0; i < mas.Length; i++)
            {
                result[i] = Convert.ToDouble(mas[i].Trim());
            }

            return result;
        }

        /// <summary>
        /// Разбор строки для объемного элемента
        /// </summary>
        /// <param name="line">Строка</param>
        /// <returns>Возвращает массив с данными об элементе</returns>
        public static int[] ParseVolumeElementString(string line)
        {
            string[] mas = line.Split(' ');
            mas = RemoveSpaces(mas);

            int[] result = new int[mas.Length];

            for (int i = 0; i < mas.Length; i++)
            {
                result[i] = Convert.ToInt32(mas[i].Trim());
            }

            return result;
        }

        /// <summary>
        /// Разбор строки для поверхностного элемента
        /// </summary>
        /// <param name="line">Строка</param>
        /// <returns>Возвращает массив с данными об элементе</returns>
        public static int[] ParseSurfaceElementString(string line)
        {
            string[] mas = line.Split(' ');
            mas = RemoveSpaces(mas);

            int[] result = new int[mas.Length];

            for (int i = 0; i < mas.Length; i++)
            {
                result[i] = Convert.ToInt32(mas[i].Trim());
            }

            return result;
        }

        /// <summary>
        /// Конвертация массива с координатами в узел
        /// </summary>
        /// <param name="coords">Массив координат</param>
        /// <returns>Возвращает точку (Point)</returns>
        public static Point ConvertArrayToPoint(double[] coords)
        {
            return new Point(coords[0], coords[1], coords[2]);
        }

        /// <summary>
        /// Генерация объемного элемента из массива
        /// </summary>
        /// <param name="nums">Массив с данными об элементе</param>
        /// <returns>Возвращает элемент</returns>
        public static VolumeElement ConvertArrayToVolumeElement(int[] nums)
        {
            var element = new VolumeElement();

            element.SubDomain = nums[0];
            int[] nodes = new int[nums.Length - 1];
            for (int i = 1; i < nums.Length; i++)
            {
                nodes[i - 1] = nums[i];
            }
            element.Nodes = nodes;

            return element;
        }

        /// <summary>
        /// Генерация поверхностного элемента из массива
        /// </summary>
        /// <param name="nums">Массив с данными об элементе</param>
        /// <param name="withBoundaryCond">С учетом описания границы</param>
        /// <returns>Возвращает элемент</returns>
        public static SufraceElement ConvertArrayToSurfaceElement(int[] nums, bool withBoundaryCond = false)
        {
            var element = new SufraceElement();

            int[] nodes;

            if (withBoundaryCond)
            {
                element.BoundaryCond = nums[0];
                nodes = new int[nums.Length - 1];
                for (int i = 1; i < nums.Length; i++)
                {
                    nodes[i - 1] = nums[i];
                }
            }
            else
            {
                nodes = new int[nums.Length];
                for (int i = 0; i < nums.Length; i++)
                {
                    nodes[i] = nums[i];
                }
            }
            element.Nodes = nodes;

            return element;
        }

        /// <summary>
        /// Удаление пустых элементов в массиве
        /// </summary>
        /// <param name="mas">Исходный массив</param>
        /// <returns>Очищенный массив</returns>
        private static string[] RemoveSpaces(string[] mas)
        {
            return mas.Where(t => !String.IsNullOrWhiteSpace(t)).ToArray();
        }
    }
}
