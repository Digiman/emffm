﻿using System;
using System.Collections.Generic;
using System.IO;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MeshParser.Elements;
using EMFFM.MeshParser.Enums;
using EMFFM.MeshParser.Helpers;

namespace EMFFM.MeshParser
{
    /// <summary>
    /// Парсер для файла с данными сетки
    /// </summary>
    /// <remarks>Текстовый файл, генерируемый Netgen при сохранении сетки(*.vol)</remarks>
    public class MeshFileParser
    {
        private readonly string _fileName;

        /// <summary>
        /// Инициализация парсера
        /// </summary>
        /// <param name="fname">Название файла для разбора</param>
        public MeshFileParser(string fname)
        {
            _fileName = fname;
        }

        /// <summary>
        /// Разбор содержимого файла
        /// </summary>
        /// <returns>Возвращает данные о сетке, прочитанной из файла</returns>
        public MeshData Parse()
        {
            FileTypes type = CommonHelpers.GetFileType(_fileName);

            var data = new MeshData();

            switch (type)
            {
                case FileTypes.NeutralFormat: // Neutral Format
                    ReadNeutralFormatFile(ref data);
                    break;
                case FileTypes.SurfaceTriangulation: // Surface triangulation file
                    ReadSurfaceTriangulationFile(ref data);
                    break;
            }

            return data;
        }

        #region Чтение содержимого для разных типов файлов
        
        /// <summary>
        /// Чтение файла в формате Neutral Format
        /// </summary>
        /// <param name="data">Данные о сетке из файла</param>
        private void ReadNeutralFormatFile(ref MeshData data)
        {
            try
            {
                StreamReader file = new StreamReader(_fileName);

                // 1. Чтение списка точек и их кооридинаты (nodes)
                data.Points = ReadPoints(file);

                // 2. Чтение списа объемных элементов (volume elements)
                data.VolumeElements = ReadVolumeElements(file);

                // 3. Чтение списка поверзностных элементов (surface elements)
                data.SurfaceElements = ReadSurfaceElements(file, true);

                file.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при чтении *.neu файла: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Чтение файла формата Surface Triangulation File Format
        /// </summary>
        /// <param name="data">Данные о сетке из файла</param>
        private void ReadSurfaceTriangulationFile(ref MeshData data)
        {
            try
            {
                StreamReader file = new StreamReader(_fileName);

                string header = file.ReadLine();
                if (header != "surfacemesh")
                {
                    throw new Exception("Файл не соответствует структуре Sufrace Triangulation Mesh!");
                }

                // 1. Чтение списка точек и их кооридинаты (nodes)
                data.Points = ReadPoints(file);

                // 2. Чтение списка поверзностных элементов (surface elements)
                data.SurfaceElements = ReadSurfaceElements(file);

                file.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка причтение Surface Trianguation файла: {0}", ex.Message);
            }
        } 

        #endregion

        #region Чтение блоков файла
        
        /// <summary>
        /// Чтение списка точек и их кординат
        /// </summary>
        /// <param name="file">Открытый файл для чтения</param>
        /// <returns>Возвращает список точек</returns>
        private List<Point> ReadPoints(StreamReader file)
        {
            // считываем количество точек
            int count = Convert.ToInt32(file.ReadLine());

            List<Point> points = new List<Point>(count);

            string line;
            for (int i = 0; i < count; i++)
            {
                line = file.ReadLine();
                // разбираем строку с координатами (x y z)
                double[] coords = CommonHelpers.ParseCoordinatesString(line);
                points.Add(CommonHelpers.ConvertArrayToPoint(coords));
            }

            return points;
        }

        /// <summary>
        /// Чтение списка объемных элементов
        /// </summary>
        /// <param name="file">Открытый файл для чтения</param>
        /// <returns>Возвращает список элементов</returns>
        private List<VolumeElement> ReadVolumeElements(StreamReader file)
        {
            // считываем количество элементов
            int count = Convert.ToInt32(file.ReadLine());

            List<VolumeElement> elements = new List<VolumeElement>(count);

            string line;
            for (int i = 0; i < count; i++)
            {
                line = file.ReadLine();
                // разбираем строку с координатами (x y z)
                int[] nums = CommonHelpers.ParseVolumeElementString(line);
                elements.Add(CommonHelpers.ConvertArrayToVolumeElement(nums));
            }

            return elements;
        }

        /// <summary>
        /// Чтение списка поверхностных элементов
        /// </summary>
        /// <param name="file">Открытый файл для чтения</param>
        /// <param name="withBoundaryCond">Читать с учетом граничных условий</param>
        /// <returns>Возвращает список элементов</returns>
        private List<SufraceElement> ReadSurfaceElements(StreamReader file, bool withBoundaryCond = false)
        {
            // считываем количество элементов
            int count = Convert.ToInt32(file.ReadLine());

            List<SufraceElement> elements = new List<SufraceElement>(count);

            string line;
            for (int i = 0; i < count; i++)
            {
                line = file.ReadLine();
                // разбираем строку с координатами (x y z)
                int[] nums = CommonHelpers.ParseSurfaceElementString(line);
                elements.Add(CommonHelpers.ConvertArrayToSurfaceElement(nums, withBoundaryCond));
            }

            return elements;
        } 

        #endregion
    }
}
