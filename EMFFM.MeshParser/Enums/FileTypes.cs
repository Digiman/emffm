﻿namespace EMFFM.MeshParser.Enums
{
    // NOTE: определение перечня поддерживаемых форматов

    /// <summary>
    /// Типы файлов с данными сетки
    /// </summary>
    enum FileTypes
    {
        NeutralFormat,
        SurfaceTriangulation
    }
}