﻿namespace EMFFM.MeshParser.Enums
{
    /// <summary>
    /// Типы сеток
    /// </summary>
    enum MeshType
    {
        Mesh3D,
        Mesh2D
    }
}
