﻿namespace EMFFM.MeshParser.Elements
{
    /// <summary>
    /// Описание элементов объемных
    /// </summary>
    public class VolumeElement
    {
        /// <summary>
        /// Номер области, где расположен элемент 
        /// </summary>
        public int SubDomain;

        /// <summary>
        /// Список номеров точек
        /// </summary>
        public int[] Nodes;

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public VolumeElement()
        {
        }

        /// <summary>
        /// Инициализация элемента
        /// </summary>
        /// <param name="subnum">Номер подобласти (?)</param>
        /// <param name="nodes">Номера узлов элемента (соответствующие массиву узлов)</param>
        public VolumeElement(int subnum, int[] nodes)
        {
            SubDomain = subnum;
            Nodes = nodes;
        }
    }
}
