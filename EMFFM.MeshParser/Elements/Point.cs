﻿using System;
using System.Linq;

namespace EMFFM.MeshParser.Elements
{
    /// <summary>
    /// Описание точки по ее координатам
    /// </summary>
    public class Point
    {
        public double X;
        public double Y;
        public double Z;

        /// <summary>
        /// Инициализация точки в 3D
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="z">Координата Z</param>
        public Point(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return String.Format("( {0}, {1}, {2} )", X, Y, Z);
        }
    }
}
