﻿namespace EMFFM.MeshParser.Elements
{
    /// <summary>
    /// Поверхностный элемент сетки
    /// </summary>
    /// <remarks>Список тетраэдров в указанием граничных условий</remarks>
    public class SufraceElement
    {
        /// <summary>
        /// Номер граничного условия
        /// </summary>
        public int BoundaryCond;

        /// <summary>
        /// Номера узлов поверхностного элемента
        /// </summary>
        public int[] Nodes;
    }
}
