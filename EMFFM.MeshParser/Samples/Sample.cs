﻿using EMFFM.MeshParser.Helpers;

namespace EMFFM.MeshParser.Samples
{
    /// <summary>
    /// Класс с примерами использования модуля для разбора сетки из текстовых файлов специального формата.
    /// </summary>
    public static class Sample
    {
        /// <summary>
        /// Пример разбора сетки из тестого файла:
        ///     - используется класс для чтения и разбора файла с данными о сетке из программы Netgen,
        ///     - прочитанная и разобранная сетка выводится в виде XML-файла.
        /// </summary>
        public static void TestParser()
        {
            var parser = new MeshFileParser("test1.neu");
            MeshData mesh = parser.Parse();

            OutputHelper.OutputMeshDataToXmlFile(mesh, "test1.xml");
        }
    }
}
