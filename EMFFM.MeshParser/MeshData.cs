﻿using System;
using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MeshParser.Elements;
using EMFFM.MeshParser.Helpers;

namespace EMFFM.MeshParser
{
    /// <summary>
    /// Описание сетки, прочитанной из файла Mesh
    /// </summary>
    public class MeshData
    {
        /// <summary>
        /// Список поверхностных элементов сетки.
        /// </summary>
        public List<SufraceElement> SurfaceElements;

        /// <summary>
        /// Список объемных элементов сетки.
        /// </summary>
        public List<VolumeElement> VolumeElements;
        
        /// <summary>
        /// Список точек с координатами узлов сеток.
        /// </summary>
        public List<Point> Points;

        /// <summary>
        /// Инициализация данных сетки.
        /// </summary>
        public MeshData()
        {
            SurfaceElements = new List<SufraceElement>();
            Points = new List<Point>();
            VolumeElements = new List<VolumeElement>();
        }

        /// <summary>
        /// Перегруженный метод для ToString().
        /// </summary>
        /// <returns>Возвращает строку с описанием сетки.</returns>
        public override string ToString()
        {
            return String.Format("Mesh with {0} points", Points.Count);
        }

        /// <summary>
        /// Экпорт сетки в виде файла с заданным расширением и в нужном формате.
        /// </summary>
        /// <param name="filename">Выходной файл (полный путь).</param>
        /// <param name="filetype">Тип файла для экспорта.</param>
        public void ExportMesh(string filename, ExportFileType filetype)
        {
            switch (filetype)
            {
                case ExportFileType.XML: // вывод в XML файл сведений о сетке
                    OutputHelper.OutputMeshDataToXmlFile(this, filename);
                    break;
                case ExportFileType.TXT: // вывод в текстовый файл сетки для Mathcad
                    OutputHelper.OutputMeshToTextFile(this, filename);
                    break;
            }
        }
    }
}
