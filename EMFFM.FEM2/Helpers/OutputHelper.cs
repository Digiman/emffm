﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using EMFFM.FEM2.Elements;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Helpers
{
    /// <summary>
    /// Специльный клсс для реализации функции печати данных из матриц и векторов в текстовые файлы.
    /// </summary>
    /// <remarks>Генерирует текстовые файлы с данными из матриц и векторов.</remarks>
    public static class OutputHelper
    {
        /// <summary>
        /// Печать матрицы в файл.
        /// </summary>
        /// <typeparam name="TType">Тип элементов матрицы.</typeparam>
        /// <param name="matrix">Матрица с данными.</param>
        /// <param name="fname">Путь и название файла для вывода.</param>
        public static void OutputMatrixToFile<TType>(TType[,] matrix, string fname)
        {
            // TODO: придумать правильный формат экспорта матрицы с комплексными числами для чтения ее потом в математических пакетах

            // создаем пустой файл для выводв данных
            var file = new StreamWriter(fname);

            // вывод матрицы в файл
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                    file.Write("{0}\t", matrix[i, j].ToString());
                file.WriteLine();
            }

            file.Close();
        }

        /// <summary>
        /// Печать вектора (массива) в файл.
        /// </summary>
        /// <typeparam name="TType">Тип элементов вектора (массива).</typeparam>
        /// <param name="vector">Вектор (массив).</param>
        /// <param name="fname">Путь и название файла для вывода.</param>
        public static void OutputVectorToTextFile<TType>(TType[] vector, string fname)
        {
            // создаем пустой файл для выводв данных
            var file = new StreamWriter(fname);

            // вывод вектора (массива) в файл
            for (int i = 0; i < vector.Length; i++)
            {
                file.WriteLine("{0}", vector[i]);
            }

            file.Close();
        }

        /// <summary>
        /// Печать в файл сведений об элементах полученной в программе сетки.
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки.</typeparam>
        /// <param name="elements">Список элементов сетки.</param>
        /// <param name="fname">Путь и название файла для вывода данных.</param>
        /// <param name="withEdges">Выводить ли данные о ребрах.</param>
        /// <param name="isAppend">Дописывать ли в файл.</param>
        public static void OutputMeshDataToFile<TType>(List<TType> elements, string fname, bool withEdges = false, bool isAppend = false)
        {
            var file = new StreamWriter(fname, isAppend);
            foreach (var item in elements)
            {
                file.WriteLine("{0}", item.ToString());
                if (withEdges)
                {
                    file.WriteLine("Edges: {0}", (item as Element).EdgesToString());
                }
            }
            file.Close();
        }

        /// <summary>
        /// Вывод диагонали матрицы в файл.
        /// </summary>
        /// <param name="mat">Матрица с данными.</param>
        /// <param name="fname">Имя файла для вывода.</param>
        public static void PrintDiagonalToFile<TType>(TType[,] mat, string fname)
        {
            TType[] diag = new TType[mat.GetLength(0)];
            for (int i = 0; i < mat.GetLength(0); i++)
            {
                diag[i] = mat[i, i];
            }
            OutputVectorToTextFile<TType>(diag, fname);
        }

        /// <summary>
        /// Экспорт данных, полученных после решения СЛАУ в текстовый файл.
        /// </summary>
        /// <typeparam name="TValue">Тип данных, которые имеют значения в полученном решении (векторе).</typeparam>
        /// <param name="filename">Файл, куда сохранять данные.</param>
        /// <param name="results">Результаты решения для сохранения.</param>
        public static void OutputSolverResultToTextFile<TValue>(string filename, SolverResult<TValue> results)
        {
            var file = new StreamWriter(filename);

            file.WriteLine("Solve time: {0} ms", results.SolveTime);

            file.WriteLine("Results");
            foreach (var value in results.ResultVector)
            {
                file.WriteLine(value);
            }

            file.Close();
        }

        /// <summary>
        /// Экспорт данных, полученных после решения СЛАУ в XML файл.
        /// </summary>
        /// <typeparam name="TValue">Тип данных, которые имеют значения в полученном решении (векторе).</typeparam>
        /// <param name="filename">Файл, куда сохранять данные.</param>
        /// <param name="results">Результаты решения для сохранения.</param>
        public static void OutputSolverResultToXmlFile<TValue>(string filename, SolverResult<TValue> results)
        {
            // TODO: протестировать вывод результатов рещения в XML файл

            var file = new XmlTextWriter(filename, Encoding.UTF8);

            file.WriteStartDocument();
            file.WriteStartElement("solverresults"); // </solverresults>

            file.WriteStartElement("solvetime"); // </solvetime>
            file.WriteValue(results.SolveTime);
            file.WriteEndElement(); // </solvetime>

            file.WriteStartElement("values"); // </values>
            foreach (var value in results.ResultVector)
            {
                file.WriteStartElement("value"); // </value>
                file.WriteValue(value.ToString());
                file.WriteEndElement(); // </value>
            }
            file.WriteEndElement(); // </values>

            file.WriteEndElement(); // </solverresults>

            file.Close();
        }

        /// <summary>
        /// Вывод вектора в XML файл.
        /// </summary>
        /// <typeparam name="TValue">Типа данных для значений в векторе.</typeparam>
        /// <param name="vector">Вектор (массив одномерный) с данными.</param>
        /// <param name="filename">Имя файла, куда сохрянять результат.</param>
        public static void OutputVectorToXmlFile<TValue>(TValue[] vector, string filename)
        {
            var file = new XmlTextWriter(filename, Encoding.UTF8);

            file.WriteStartDocument();
            file.WriteStartElement("vector");
            foreach (var value in vector)
            {
                file.WriteStartElement("value");
                file.WriteString(value.ToString());
                file.WriteEndElement();
            } 
            
            file.WriteEndElement();

            file.Close();
        }

        /// <summary>
        /// Печать вектора (массива) в файл.
        /// </summary>
        /// <param name="vector">Вектор (массив).</param>
        /// <param name="fname">Путь и название файла для вывода.</param>
        public static void OutputVectorToFile<TType>(TType[] vector, string fname)
        {
            // создаем пустой файл для выводв данных
            var file = new StreamWriter(fname);

            // вывод вектора (массива) в файл
            for (int i = 0; i < vector.Length; i++)
            {
                file.WriteLine("{0}; {1}", (vector[i] as System.Numerics.Complex?).Value.Real, (vector[i] as System.Numerics.Complex?).Value.Imaginary);
            }

            file.Close();
        }
    }
}
