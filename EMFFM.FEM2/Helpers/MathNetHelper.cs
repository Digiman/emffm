﻿using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Double.Solvers.Iterative;
using MathNet.Numerics.LinearAlgebra.Double.Solvers.StopCriterium;
using MathNet.Numerics.LinearAlgebra.Double.Solvers;

namespace EMFFM.FEM2.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с библиотекой Math.NET Numerics.
    /// </summary>
    public static class MathNetHelper
    {
        /// <summary>
        /// Метод для решения СЛАУ вида Ax=B с настройками метода решения.
        /// </summary>
        /// <param name="matrixA">Матрица A.</param>
        /// <param name="vectorB">Вектор B.</param>
        /// <returns>Возвращает результат решения (вектор x).</returns>
        public static Vector Solve(Matrix matrixA, Vector vectorB)
        {
            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor = new Iterator(new IIterationStopCriterium[] { iterationCountStopCriterium, residualStopCriterium });

            // Create Bi-Conjugate Gradient Stabilized solver
            var solver = new BiCgStab(monitor);

            // solve SLAU
            return solver.Solve(matrixA, vectorB);
        }

        /// <summary>
        /// Метод для решения СЛАУ вида Ax=B с настройками метода решения.
        /// </summary>
        /// <param name="matrixA">Матрица A.</param>
        /// <param name="vectorB">Вектор B.</param>
        /// <returns>Возвращает результат решения (вектор x).</returns>
        public static double[] Solve(double[,] matrixA, double[] vectorB)
        {
            Matrix MatrixA = SparseMatrix.OfArray(matrixA);
            Vector VectorB = SparseVector.OfEnumerable(vectorB);

            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor = new Iterator(new IIterationStopCriterium[] { iterationCountStopCriterium, residualStopCriterium });

            // Create Bi-Conjugate Gradient Stabilized solver
            var solver = new BiCgStab(monitor);

            // solve SLAU
            return solver.Solve(MatrixA, VectorB).ToArray();
        }

        /// <summary>
        /// Метод для решения СЛАУ вида Ax=B с настройками метода решения
        /// (комплексныне числа в матрице и векторе).
        /// </summary>
        /// <param name="matrixA">Матрица A.</param>
        /// <param name="vectorB">Вектор B.</param>
        /// <returns>Возвращает результат решения (вектор x).</returns>
        public static System.Numerics.Complex[] Solve(System.Numerics.Complex[,] matrixA, System.Numerics.Complex[] vectorB)
        {
            var MatrixA = MathNet.Numerics.LinearAlgebra.Complex.SparseMatrix.OfArray(matrixA);
            var VectorB = MathNet.Numerics.LinearAlgebra.Complex.SparseVector.OfEnumerable(vectorB);

            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new MathNet.Numerics.LinearAlgebra.Complex.Solvers.StopCriterium.IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new MathNet.Numerics.LinearAlgebra.Complex.Solvers.StopCriterium.ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor =
                new MathNet.Numerics.LinearAlgebra.Complex.Solvers.Iterator(new MathNet.Numerics.LinearAlgebra.Complex.
                                                                                Solvers.StopCriterium.
                                                                                IIterationStopCriterium[]
                                                                                {
                                                                                    iterationCountStopCriterium,
                                                                                    residualStopCriterium
                                                                                });

            // Create Bi-Conjugate Gradient Stabilized solver
            var solver = new MathNet.Numerics.LinearAlgebra.Complex.Solvers.Iterative.BiCgStab(monitor);

            // solve SLAU
            return solver.Solve(MatrixA, VectorB).ToArray();
        }

        /// <summary>
        /// Перемножение матрицы на вектор.
        /// </summary>
        /// <param name="matrix">Матрица вещественных чисел.</param>
        /// <param name="vector">Вектор вещественных чисел.</param>
        /// <returns>Возвращает результирующий вектор.</returns>
        public static double[] Multiply(double[,] matrix, double[] vector)
        {
            Matrix mat = DenseMatrix.OfArray(matrix);
            Vector vec = DenseVector.OfEnumerable(vector);

            return mat.Multiply(vec).ToArray();
        }

        /// <summary>
        /// Перемножение матрицы на вектор с комплексными значениями.
        /// </summary>
        /// <param name="matrix">Матрица комплесных чисел.</param>
        /// <param name="vector">Вектор комплексныхз чисел.</param>
        /// <returns>Возвращает результирующий вектор комплексных чисел.</returns>
        public static System.Numerics.Complex[] Multiply(System.Numerics.Complex[,] matrix,
            System.Numerics.Complex[] vector)
        {
            MathNet.Numerics.LinearAlgebra.Complex.Matrix mat =
                MathNet.Numerics.LinearAlgebra.Complex.DenseMatrix.OfArray(matrix);
            MathNet.Numerics.LinearAlgebra.Complex.Vector vec =
                new MathNet.Numerics.LinearAlgebra.Complex.DenseVector(vector);

            return mat.Multiply(vec).ToArray();
        }
    }
}
