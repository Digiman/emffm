﻿using MathNet.Numerics.LinearAlgebra.Complex;
using MathNet.Numerics.LinearAlgebra.Complex.Solvers;
using MathNet.Numerics.LinearAlgebra.Complex.Solvers.Iterative;
using MathNet.Numerics.LinearAlgebra.Complex.Solvers.StopCriterium;

namespace EMFFM.FEM2.Helpers
{
    /// <summary>
    ///     Вспомогательный класс для работы с библиотекой Math.NET Numerics.
    /// </summary>
    /// <remarks>Класс для работы с комплексными решателями!</remarks>
    public static class MathNetComplexHelper
    {
        /// <summary>
        ///     Метод для решения СЛАУ вида Ax=B с настройками метода решения.
        /// </summary>
        /// <param name="matrixA">Матрица A.</param>
        /// <param name="vectorB">Вектор B.</param>
        /// <returns>Возвращает результат решения (вектор x).</returns>
        public static Vector Solve(Matrix matrixA, Vector vectorB)
        {
            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor =
                new Iterator(new IIterationStopCriterium[] {iterationCountStopCriterium, residualStopCriterium});

            // Create Bi-Conjugate Gradient Stabilized solver
            var solver = new BiCgStab(monitor);

            // solve SLAU
            return solver.Solve(matrixA, vectorB);
        }

        /// <summary>
        ///     Метод для решения СЛАУ вида Ax=B с настройками метода решения
        ///     (комплексныне числа в матрице и векторе).
        /// </summary>
        /// <param name="matrix">Матрица A.</param>
        /// <param name="vector">Вектор B.</param>
        /// <returns>Возвращает результат решения (вектор x).</returns>
        public static System.Numerics.Complex[] Solve(System.Numerics.Complex[,] matrix, System.Numerics.Complex[] vector)
        {
            var matrixA = SparseMatrix.OfArray(matrix);
            var vectorB = SparseVector.OfEnumerable(vector);
            
            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor = new Iterator(new IIterationStopCriterium[]
            {
                iterationCountStopCriterium,
                residualStopCriterium
            });

            // Create solver
            var solver = new BiCgStab(monitor);

            var resultX = solver.Solve(matrixA, vectorB);

            // solve SLAU
            return resultX.ToArray();
        }

        /// <summary>
        ///     Перемножение матрицы на вектор с комплексными значениями.
        /// </summary>
        /// <param name="matrix">Матрица комплесных чисел.</param>
        /// <param name="vector">Вектор комплексныхз чисел.</param>
        /// <returns>Возвращает результирующий вектор комплексных чисел.</returns>
        public static System.Numerics.Complex[] Multiply(System.Numerics.Complex[,] matrix, System.Numerics.Complex[] vector)
        {
            Matrix mat = DenseMatrix.OfArray(matrix);
            Vector vec = new DenseVector(vector);

            return mat.Multiply(vec).ToArray();
        }
    }
}