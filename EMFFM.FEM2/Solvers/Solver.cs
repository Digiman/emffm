﻿using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Solvers
{
    /// <summary>
    /// Базовый класс решателя.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, в которых производится решение (скалярные, комплексные величины).</typeparam>
    public abstract class Solver<TValue>
    {
        /// <summary>
        /// Сведения о задаче.
        /// </summary>
        protected readonly InputData Data;

        /// <summary>
        /// Сетка (набор элементов).
        /// </summary>
        protected readonly TMesh<Tetrahedron, Triangle> Mesh;

        /// <summary>
        /// Инициализация базового решателя с параметрами.
        /// </summary>
        /// <param name="data">Входные данные с описанием задачи.</param>
        /// <param name="mesh">Сгенерированная сетка.</param>
        protected Solver(InputData data, TMesh<Tetrahedron, Triangle> mesh)
        {
            Data = data;

            // TODO: прикинуть здесь, нудна ли сетка или нет? чтобы не загружить объект лишними данными и память меньше использовать!
            Mesh = mesh;
        }

        /// <summary>
        /// Выполнение решения.
        /// </summary>
        /// <returns>Возвращает результат решения.</returns>
        public abstract SolverResult<TValue> Solve();

        /// <summary>
        /// Вычисление или получение значения волнового числа.
        /// </summary>
        /// <returns>Возвращает найденное значение волнового числа (безразмерная величина).</returns>
        protected double GetWaveNumber()
        {
            // TODO: проверить расчет волнового числа по длине волны!

            var source = Data.Sources.Single(s => s.Type == SourceTypes.Simple);

            return ParametersBuilder.CalculateWaveNumberByLambda(source.Lambda);
        }

        /// <summary>
        /// Вычисление или получение значения круговой частоты
        /// </summary>
        /// <returns>Возвращает найденное значение круговой частоты (рад/с).</returns>
        protected double GetOmega()
        {
            // TODO: проверить расчет круговой частоты работы источника по длине волны!

            var source = Data.Sources.Single(s => s.Type == SourceTypes.Simple);

            return ParametersBuilder.GetOmegaByLambda(source.Lambda);
        }

        /// <summary>
        /// Экспорт данных, используемых в решателе.
        /// </summary>
        /// <param name="matrix">Матрица глобальная.</param>
        /// <param name="vector">Вектор воздействия.</param>
        /// <param name="result">Вектор с результатами решенис СЛАУ.</param>
        protected void ExportCurentSolverData(TValue[,] matrix, TValue[] vector, TValue[] result)
        {
            IoHelper.CheckAndCreateDirectory(Data.Output.Path);

            if (Data.Options.IsOutputGlobalMatrix)
            {
                OutputHelper.OutputMatrixToFile(matrix, Data.Output.GetFullPath(DatafileType.Matrix));
            }

            if (Data.Options.IsOutputForceVector)
            {
                OutputHelper.OutputVectorToFile(vector, Data.Output.GetFullPath(DatafileType.Vector));
            }

            if (Data.Options.IsOutputResultVector)
            {
                OutputHelper.OutputVectorToFile(result, Data.Output.GetFullPath(DatafileType.Result));
            }
        }
    }
}
