﻿using EMFFM.Common.Enums;
using EMFFM.Input;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Solvers
{
    /// <summary>
    /// Класс для реализации выбора решателя для той или иной задачи.
    /// </summary>
    public class SolverFactory
    {
        /// <summary>
        /// Входные данные (описание задачи).
        /// </summary>
        private readonly InputData _data;
        
        /// <summary>
        /// Сетка из элементов, на которые была дискретизирована расчетная область задачи.
        /// </summary>
        private readonly TMesh<Tetrahedron, Triangle> _mesh;

        /// <summary>
        /// Инициализация фабрики для работы с решателями.
        /// </summary>
        /// <param name="data">Входные данные с описанием задачи.</param>
        /// <param name="mesh">Сгенерированная сетка в виде набора элементов с ребрами.</param>
        public SolverFactory(InputData data, TMesh<Tetrahedron, Triangle> mesh)
        {
            _data = data;
            _mesh = mesh;
        }

        /// <summary>
        /// Получение конкретной реализации решателя для текущей активной задачи.
        /// </summary>
        /// <typeparam name="TValue">Тип значений, которые получаются в результате решения (вещественные или комплексные).</typeparam>
        /// <returns>Возвращает обхект решателя, который будет использоваться для решения задачи.</returns>
        public Solver<TValue> GetSolver<TValue>()
        {
            switch (_data.Task.SolverType)
            {
                case SolverTypes.FrequencyDomain:
                    switch (_data.Task.ValueType)
                    {
                        case ValueTypes.Complex:
                            return (Solver<TValue>) (object) (new FrequencyComplexSolver(_data, _mesh));
                    }
                    break;
                    // TODO: пока нет решения во временной области, оно сильно сложное и вероятно будет давать другие результаты (за каждую единицу времени)
                case SolverTypes.TimeDomain:
                    switch (_data.Task.ValueType)
                    {
                        case ValueTypes.Complex:
                            //return new TimeScalarSolver(_data, _mesh);
                            break;
                    }
                    break;
            }

            return null;
        }
    }
}
