﻿using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Math;
using EMFFM.FEM2.Complex;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.Mesh.Helpers;

namespace EMFFM.FEM2.Solvers
{
    // TODO: протестировать реализацию решателя!

    /// <summary>
    /// Класс для выполнения решения задачи в частотном диапазоне для комплексных величин.
    /// </summary>
    public class FrequencyComplexSolver : Solver<System.Numerics.Complex>
    {
        /// <summary>
        /// Глобальная матрица.
        /// </summary>
        private readonly GlobalMatrix _matrix;
        
        /// <summary>
        /// Вектор воздействий (возмущений).
        /// </summary>
        private readonly ForceVector _vector; 

        /// <summary>
        /// Инициализация решателя в частотном диапазоне для комплексных чисел.
        /// </summary>
        /// <param name="data">Входные данные с описанием задачи.</param>
        /// <param name="mesh">Сетка из элементов.</param>
        public FrequencyComplexSolver(InputData data, TMesh<Tetrahedron, Triangle> mesh) : base(data, mesh)
        {
            var source = Data.Sources.Single(s => s.Type == SourceTypes.Simple);
            MaterialsHelper.InitMaterials(source.Lambda, data.Materials);

            _matrix = new GlobalMatrix(mesh, data.Materials, data.Geometry);

            var edgeSource = MeshHelper.GetEdgesInDomain(mesh,
                data.Sources.Single(s => s.Type == SourceTypes.Simple).Domain);

            var waveData = CalculateWaveParams();

            _vector = new ForceVector(mesh, edgeSource, waveData);
        }

        /// <summary>
        /// Формирование объекта с данными о параметрах источника и волны от него.
        /// </summary>
        /// <returns>Возвращает данные с параметрами волны от источника.</returns>
        private WaveData CalculateWaveParams()
        {
            // TODO: этот метод нужно четко помнить, ведь в нем идет расчет параметров источника!

            return WaveDataHelper.ComplexWaveData(Data);
        }

        /// <summary>
        /// Выполнение решения.
        /// </summary>
        /// <returns>Возвращает результат решения СЛАУ.</returns>
        public override SolverResult<System.Numerics.Complex> Solve()
        {
            // TODO: протестировать решатель для комплесных значений при решении в частной области!

            var waveNumber = GetWaveNumber();
            var omega = GetOmega();

            // 1. Постоение глобальной матрицы и вектора воздействий
            var mat = _matrix.Generate(waveNumber, omega, Data.Geometry.CoFactor);
            var vec = _vector.Generate();

            // 2. Применение граничных условий
            ApplyBoundaryCondition(ref mat, ref vec);

            // 3. Выполнение решения СЛАУ
            System.Numerics.Complex[] resultVector = MathNetComplexHelper.Solve(mat, vec);

            // 4. Экспорт результатов (глобальной матрицы, вектора воздействий и результата решения СЛАУ)
            ExportCurentSolverData(mat, vec, resultVector);

            var dif = ComplexArrayHelper.Difference(resultVector, vec);
            //Helpers.OutputHelper.OutputVectorToFile(dif, "difference.txt");
            // NOTE: результат поменят временно на разницу поля от решения СЛАУ и исходного!
            var result = new SolverResult<System.Numerics.Complex>(dif);

            return result;
        }

        /// <summary>
        /// Примение граничных условий для матрицы.
        /// </summary>
        /// <param name="matrix">Матрица глобальная.</param>
        /// <param name="vec">Вектор воздействий.</param>
        private void ApplyBoundaryCondition(ref System.Numerics.Complex[,] matrix, ref System.Numerics.Complex[] vec)
        {
            // TODO: разобраться с правильным применением граничных условий!

            int[] boundNumsBoundary = MeshHelper.GetNumbersOfDomainEdges(Mesh, Data.Boundaries.Single(
                b => b.Type == BoundaryTypes.Dirichlet).
                DomainNumber);

            BoundaryBuilder.ApplyDirichletBoundaryCondition2(ref matrix, ref vec, boundNumsBoundary,
                ComplexArrayHelper.InitValueVector(boundNumsBoundary.Length,
                    Data.Boundaries.Single(b => b.Type == BoundaryTypes.Dirichlet).Value));
        }
    }
}
