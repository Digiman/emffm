﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Mesh.Elements;
using EMFFM.FEM2.Abstract;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using TValue = System.Numerics.Complex;

namespace EMFFM.FEM2.Complex
{
    // TODO: в данном классе придумать как строить вектор воздействия!!

    /// <summary>
    /// Класс для реализации вектора воздействий и его генерации.
    /// </summary>
    public class ForceVector : ForceVectorBase<TValue>
    {
        /// <summary>
        /// Калькулятор значений поля, который будет использовваться для расчета.
        /// </summary>
        private readonly FieldCalculatorBase<TValue> _fieldCalculator;

        /// <summary>
        /// Момент времени, выбираемый для расчета значения поля.
        /// </summary>
        /// <remarks>
        /// Вероятно это не понадобиться, потому что для частотного случая там и так время в нулевой момент или другой, 
        /// для которого есть значение распределения поля по рассматриваемой области.
        /// </remarks>
        private double _time = 0;

        /// <summary>
        /// Инициализация класса для построения вектора воздействий.
        /// </summary>
        /// <param name="mesh">Сетка из элементов.</param>
        /// <param name="edges">Набор ребер, для которых известны значения поля в расчетный момент.</param>
        /// <param name="waveData">Данные о волне (пока так, а потом видимо это измениться).</param>
        public ForceVector(TMesh<Tetrahedron, Triangle> mesh, List<Edge> edges, WaveData waveData) : base(mesh, edges)
        {
            _fieldCalculator = new FieldCalculator(waveData.Amplitude, waveData.WaveNumber, waveData.Omega);
        }

        /// <summary>
        /// Генерация вектора воздействий.
        /// </summary>
        /// <returns>Возвращает сгенерированный вектор с исходными значениями поля на ребрах.</returns>
        public TValue[] Generate()
        {
            var result = new TValue[Mesh.EdgesCount];

            // TODO: здесь необходимо придумать и написать алгоритм расчета значений для вектора для набора ребер!

            foreach (var edge in Edges)
            {
                var position = _fieldCalculator.CalculatePosition(edge, Direction.X);
                result[edge.Number - 1] = _fieldCalculator.Calculate(position, _time);
            }

            // NOTE: нужно ли сохранять в памяти вектор воздействий?
            Vector = result;

            return result;
        }
    }
}
