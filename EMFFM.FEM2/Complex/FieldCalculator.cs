﻿using System;
using EMFFM.FEM2.Abstract;

namespace EMFFM.FEM2.Complex
{
    // TODO: написать правильную реализацию класса для проведения расчета значения поля

    /// <summary>
    /// Класс для реализации расчета поля в комплексных значениях для ветора воздействий.
    /// </summary>
    public class FieldCalculator : FieldCalculatorBase<System.Numerics.Complex>
    {
        /// <summary>
        /// Инициализация расчета полей с параметрами волны (источника).
        /// </summary>
        /// <param name="amplitude">Начальная амплитуда волны.</param>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="omega">Циклическая частота (рад/с).</param>
        public FieldCalculator(double amplitude, double waveNumber,  double omega) : base(amplitude, waveNumber, omega)
        {
        }


        /// <summary>
        /// Вычисление значения поля в указанной точке для источника с заданными параметрами. 
        /// </summary>
        /// <param name="position">Положение (координата на ребре).</param>
        /// <param name="time">Время.</param>
        /// <returns>Возвращает значение поля в указанной точке с заданными параметрами.</returns>
        /// <remarks>Ведется расчет для плоской манохроматической электромагнитной волны.</remarks>
        public override System.Numerics.Complex Calculate(double position, double time)
        {
            // NOTE: собственно здесь ведется расчет значения поля на ребре для вектора воздействий.

            double value = Amplitude*Math.Cos(WaveNumber*position - Omega*time);

            return new System.Numerics.Complex(value, 0);
        }
    }
}
