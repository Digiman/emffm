﻿using System.Collections.Generic;
using EMFFM.Common.Input.Elements;
using EMFFM.FEM2.Abstract;
using EMFFM.FEM2.Helpers;
using EMFFM.Input.Elements;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Complex
{
    /// <summary>
    /// Класс для реализации глобальной матрицы и ее генерации.
    /// </summary>
    /// <remarks>Матрица в вещественных числах (скалярные значения).</remarks>
    public class GlobalMatrix : GlobalMatrixBase<System.Numerics.Complex>
    {
        /// <summary>
        /// Инициализация глобальной матрицы с параметрами для ее построения.
        /// </summary>
        /// <param name="mesh">Сетка из элементов.</param>
        /// <param name="materials">Материалы, которые заданы областям задачи.</param>
        /// <param name="geometry">Геометрия задачи (список областей).</param>
        public GlobalMatrix(TMesh<Tetrahedron, Triangle> mesh, List<MaterialBase> materials, Geometry geometry) : base(mesh, materials, geometry)
        {
            // инициализация параметров материала для каждой из областей задачи
            MaterialsHelper.SetMaterialsByDomain(ref mesh, geometry, materials);
        }

        /// <summary>
        /// Генерация матрицы жесткости.
        /// </summary>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="omega">Круговая частота.</param>
        /// <returns>Возвращает сгенерированную матрицу.</returns>
        public System.Numerics.Complex[,] Generate(double waveNumber, double omega)
        {
            Matrix = MatrixBuilder.BuiltGlobalMatrix(GetSize(), Mesh.MeshElements, waveNumber, omega);
            return Matrix;
        }

        /// <summary>
        /// Генерация матрицы жесткости.
        /// </summary>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="omega">Круговая частота.</param>
        /// <param name="sizeFactor">Размерный фактор, применяемый для нормирования значений.</param>
        /// <returns>Возвращает сгенерированную матрицу.</returns>
        public System.Numerics.Complex[,] Generate(double waveNumber, double omega, double sizeFactor)
        {
            Matrix = MatrixBuilder.BuiltGlobalMatrix(GetSize(), Mesh.MeshElements, waveNumber, omega, sizeFactor);
            return Matrix;
        }
    }
}
