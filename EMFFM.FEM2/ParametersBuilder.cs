﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Input.Elements;

namespace EMFFM.FEM2
{
    // TODO: полностью переписать этот класс, передалать расчет параметров, сделать его единственным местом их расчета

    /// <summary>
    /// Класс для вычисления параметров, необходимых для решения
    /// </summary>
    /// <remarks>Вычисление всех вспомогательных параметров и величин здесь!</remarks>
    public static class ParametersBuilder
    {
        private const double AbsPermeable = 1.25663706144E-06; // магнитная постоянная
        private const double AbsPermitt = 8.85418782E-12; // электрическая постоянная
        static readonly double FreeSpaceVel = 1.0 / System.Math.Sqrt(AbsPermeable * AbsPermitt); // фазовая скорость распространения волны (скорость света)
        private const double LightSpeed = 299792458; // скорость света в вакууме(м/с)

        /// <summary>
        /// Вычисление значения волнового числа K по частоте
        /// </summary>
        /// <param name="freq">Частота работы источника (Hz)</param>
        /// <returns>Возвращает значение волнового числа</returns>
        /// <remarks>За поднобностями смотреть:
        /// http://ru.wikipedia.org/wiki/%D0%A3%D1%80%D0%B0%D0%B2%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F_%D0%9C%D0%B0%D0%BA%D1%81%D0%B2%D0%B5%D0%BB%D0%BB%D0%B0#.D0.9F.D0.BB.D0.BE.D1.81.D0.BA.D0.B8.D0.B5_.D1.8D.D0.BB.D0.B5.D0.BA.D1.82.D1.80.D0.BE.D0.BC.D0.B0.D0.B3.D0.BD.D0.B8.D1.82.D0.BD.D1.8B.D0.B5_.D0.B2.D0.BE.D0.BB.D0.BD.D1.8B
        /// Код заимстован в EMAP3 (и выше).
        /// </remarks>
        public static double CalculateWaveNumberByFrequency(double freq)
        {
            double operateFreq = freq; // частота фунционирования источника

            //WaveLength = FreeSpaceVel / (operateFreq * 1.0E+06);
            double waveLength = FreeSpaceVel/operateFreq; // длина волны (в метрах)
            double waveNumber = 2.0*System.Math.PI/waveLength; // волновое число

            double omega = waveNumber*FreeSpaceVel;

            LogHelper.Log(LogMessageType.Debug, "OperateFrequency = {0} Hz", operateFreq);
            LogHelper.Log(LogMessageType.Debug, "WaveLength = {0} m", waveLength);
            LogHelper.Log(LogMessageType.Debug, "FreeSpaceVelocity = {0} m/s", FreeSpaceVel);
            LogHelper.Log(LogMessageType.Debug, "WaveNumber = {0}", waveNumber);
            LogHelper.Log(LogMessageType.Debug, "Omega = {0} rad/s", omega);

            return waveNumber;
        }

        /// <summary>
        /// Вычисление значения волнового числа по длине волны
        /// </summary>
        /// <param name="lambda">Длина волны (в метрах)</param>
        /// <returns>Возвращает значение волнового числа</returns>
        public static double CalculateWaveNumberByLambda(double lambda)
        {
            double waveNumber = 2.0 * System.Math.PI / lambda; // волновое число

            LogHelper.Log(LogMessageType.Debug, "WaveNumber (k) = {0}", waveNumber);

            return waveNumber;
        }

        /// <summary>
        /// Получение значения круговой частоты из входного файла
        /// </summary>
        /// <param name="sources">Список источников во входном файле</param>
        /// <param name="sourceType">Тип источника</param>
        /// <returns>Возвращает исходное значение частоты</returns>
        public static double GetOmega(IEnumerable<Source> sources, SourceTypes sourceType)
        {
            double lambda = sources.Single(s => s.Type == sourceType).Lambda;

            return GetOmegaByLambda(lambda);
        }

        /// <summary>
        /// Получение значения круговой частоты (рад/с) по длине волны
        /// </summary>
        /// <param name="lambda">Длина волны (метры)</param>
        /// <returns>Возвращает значение Omega</returns>
        public static double GetOmegaByLambda(double lambda)
        {
            double omega = (2*System.Math.PI*LightSpeed)/lambda;

            LogHelper.Log(LogMessageType.Debug, "Omega (w) = {0} Rad/s", omega);

            return omega;
        }

        /// <summary>
        /// Получение значения круговой частоты (рад/с) по частоте
        /// </summary>
        /// <param name="freq">Частота работы источника (Гц)</param>
        /// <returns>Возвращает значение Omega</returns>
        public static double GetOmegaByFrequency(double freq)
        {
            double omega = 2 * System.Math.PI * freq;

            LogHelper.Log(LogMessageType.Debug, "Omega = {0} Rad/s", omega);

            return omega;
        }

        /// <summary>
        /// Получение частоты (в Гц) по круговой частоте (рад/с)
        /// </summary>
        /// <param name="omega">Круговая частота (рад/с)</param>
        /// <returns>Возвращает значение частоты (Hz)</returns>
        public static double GetFrequencyByOmega(double omega)
        {
            double freq = omega/(2*System.Math.PI);

            LogHelper.Log(LogMessageType.Debug, "Frequency = {0} Hz", freq);

            return freq;
        }

        /// <summary>
        /// Получение значения амплитуды для источника
        /// </summary>
        /// <param name="sources">Список источников из входного файла</param>
        /// <param name="simple">Тип источника</param>
        /// <returns>Возвращает значение амплитуды</returns>
        public static double GetAmplitude(IEnumerable<Source> sources, SourceTypes simple)
        {
            double amp = sources.Single(s => s.Type == SourceTypes.Simple).Amplitude;

            LogHelper.Log(LogMessageType.Debug, "Amplitude = {0}", amp);

            return amp;
        }
    }
}
