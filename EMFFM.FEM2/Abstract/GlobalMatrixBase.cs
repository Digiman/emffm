﻿using System.Collections.Generic;
using EMFFM.Common.Input.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Input.Elements;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Abstract
{
    // TODO: посмотреть по ходу дальше, возможно поле с самой матрицей не понадобиться в базовом классе и ее нет смысла хранить

    /// <summary>
    /// Базовый класс для построения глобальной матрицы.
    /// </summary>
    /// <typeparam name="TValue">Тип значений в матрице (вещественные или комплексные).</typeparam>
    public abstract class GlobalMatrixBase <TValue>
    {
        /// <summary>
        /// Сетка из элементов, используемая для построения матрицы.
        /// </summary>
        protected TMesh<Tetrahedron, Triangle> Mesh { get; set; }

        /// <summary>
        /// Собственно сама матрица с данными.
        /// </summary>
        protected TValue[,] Matrix { get; set; }

        /// <summary>
        /// Список материалов, которые используются в задаче.
        /// </summary>
        private List<MaterialBase> Materials { get; set; }

        /// <summary>
        /// Описание геометрии области задачи, которая решается (по сути - список из доменов или геометрических объектов).
        /// </summary>
        private Geometry Geometry { get; set; }

        /// <summary>
        /// Инициализация класса для работы с глобальной матрицей.
        /// </summary>
        /// <param name="mesh">Сетка из элементов рассматриваемой области задачи.</param>
        protected GlobalMatrixBase(TMesh<Tetrahedron, Triangle> mesh)
        {
            Mesh = mesh;
        }

        /// <summary>
        /// Инициализация класса для работы с глобальной матрицей.
        /// </summary>
        /// <param name="mesh">Сетка, состоящая из элементов, на которые разбита рассматриваемая область задачи.</param>
        /// <param name="materials">Материалы, которые заданы областям задачи.</param>
        /// <param name="geometry">Геометрия задачи (список областей).</param>
        protected GlobalMatrixBase(TMesh<Tetrahedron, Triangle> mesh, List<MaterialBase> materials, Geometry geometry)
        {
            Mesh = mesh;
            Materials = materials;
            Geometry = geometry;
        }

        /// <summary>
        /// Экспорт матрицы в текстовый файл.
        /// </summary>
        /// <param name="filename">Имя файла для экспорта (полный путь).</param>
        public void Export(string filename)
        {
            OutputHelper.OutputMatrixToFile(Matrix, filename);
        }

        /// <summary>
        /// Получение размера матрицы (размерность квадратной матрицы Size*Size).
        /// </summary>
        /// <returns>Возвращает размерность матрицы.</returns>
        /// <remarks>Равно количеству ребер в сетке, уникальных.</remarks>
        public int GetSize()
        {
            return Mesh.EdgesCount;
        }
    }
}
