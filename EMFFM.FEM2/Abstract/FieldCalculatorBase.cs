﻿using EMFFM.Common.Enums;
using EMFFM.Common.Math;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.FEM2.Abstract
{
    /// <summary>
    /// Базовый класс для реализации логики расчета полей для вектора воздействий.
    /// </summary>
    public abstract class FieldCalculatorBase <TValue>
    {
        /// <summary>
        /// Начальная амплитуда.
        /// </summary>
        protected readonly double Amplitude;
        
        /// <summary>
        /// Волновое число.
        /// </summary>
        protected readonly double WaveNumber;
        
        /// <summary>
        /// Циклическая частота.
        /// </summary>
        protected readonly double Omega; 

        /// <summary>
        /// Инициализация калькулятора поля с параметрами.
        /// <param name="amplitude">Начальная амплитуда волны.</param>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="omega">Циклическая частота (рад/с).</param>
        /// </summary>
        protected FieldCalculatorBase(double amplitude, double waveNumber, double omega)
        {
            Amplitude = amplitude;
            WaveNumber = waveNumber;
            Omega = omega;
        }

        /// <summary>
        /// Вычисление значения поля в указанной точке для источника с заданными параметрами. 
        /// </summary>
        /// <param name="position">Положение (координата на ребре).</param>
        /// <param name="time">Время.</param>
        /// <returns>Возвращает значение поля в указанной точке с заданными параметрами.</returns>
        /// <remarks>Ведется расчет для плоской манохроматической электромагнитной волны.</remarks>
        public abstract TValue Calculate(double position, double time);

        /// <summary>
        /// Вычисление точки, расположенной в центре ребра.
        /// </summary>
        /// <param name="edge">Ребро с координатами двух его вершин.</param>
        /// <param name="direction">Направление волны от источника.</param>
        /// <returns>Возвращает скалярное значение координаты центра ребра в зависимости от направления.</returns>
        public double CalculatePosition(Edge edge, Direction direction)
        {
            var p = GeometryHelper.GetCenterPoint(edge.Vertex1.Coord, edge.Vertex2.Coord);

            switch (direction)
            {
                case Direction.X:
                    return p.X;
                case Direction.Y:
                    return p.Y;
                case Direction.Z:
                    return p.Z;
                default:
                    return 0;
            }
        }
    }
}
