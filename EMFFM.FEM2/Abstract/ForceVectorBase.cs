﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Mesh.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Abstract
{
    /// <summary>
    /// Базовый класс для построения вектора воздействий.
    /// </summary>
    public class ForceVectorBase <TValue>
    {
        /// <summary>
        /// Сетка из элементов, построенная для рассматриваемой области задачи.
        /// </summary>
        protected readonly TMesh<Tetrahedron, Triangle> Mesh;

        /// <summary>
        /// Список ребер, для которых есть известное поле.
        /// </summary>
        protected readonly List<Edge> Edges;

        /// <summary>
        /// Собственно сам вектор воздействий.
        /// </summary>
        protected TValue[] Vector;

        /// <summary>
        /// Инициализация базового класс для построения вектора воздействий.
        /// </summary>
        /// <param name="mesh">Сетка из элементов.</param>
        /// <param name="edges">Список ребер.</param>
        protected ForceVectorBase(TMesh<Tetrahedron, Triangle> mesh, List<Edge> edges)
        {
            Mesh = mesh;
            Edges = edges;
        }

        /// <summary>
        /// Экспорт вектора в текстовый файл.
        /// </summary>
        /// <param name="filename">Имя файла для экспорта (полный путь).</param>
        /// <param name="fileType">Тип файла, для записи в него данных вектора.</param>
        public void Export(string filename, ExportFileType fileType)
        {
            switch (fileType)
            {
                case ExportFileType.XML:
                    OutputHelper.OutputVectorToXmlFile(Vector, filename);
                    break;
                case ExportFileType.TXT:
                    OutputHelper.OutputVectorToTextFile(Vector, filename);
                    break;
            }
        }

        /// <summary>
        /// Получение размера вектора.
        /// </summary>
        /// <returns>Возвращает размерность матрицы.</returns>
        /// <remarks>Равно количеству кникальных ребер в сетке.</remarks>        
        public int GetSize()
        {
            return Vector.Length;
        }
    }
}
