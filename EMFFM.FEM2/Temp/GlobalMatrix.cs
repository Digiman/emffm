﻿using System.Collections.Generic;
using EMFFM.Common.Input.Elements;
using EMFFM.FEM2.Complex;
using EMFFM.FEM2.Helpers;
using EMFFM.Input.Elements;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.FEM2.Temp
{
    /// <summary>
    /// Класс для реализации глобальной матрицы и ее генерации.
    /// </summary>
    /// <typeparam name="TValue">Тип значений в глобальной матрице (скалярные или вещественные).</typeparam>
    public class GlobalMatrix <TValue>
    {
        private readonly TMesh<Tetrahedron, Triangle> _mesh;
        private List<MaterialBase> _materials;
        private Geometry _geometry;

        /// <summary>
        /// Инициализация глобальной матрицы с параметрами для ее построения.
        /// </summary>
        /// <param name="mesh">Сетка из элементов.</param>
        /// <param name="materials">Материалы, которые заданы областям задачи.</param>
        /// <param name="geometry">Геометрия задачи (список областей).</param>
        public GlobalMatrix(TMesh<Tetrahedron, Triangle> mesh, List<MaterialBase> materials, Geometry geometry)
        {
            _mesh = mesh;
            _materials = materials;
            _geometry = geometry;

            // инициализация параметров материала для каждой из областей задачи
            MaterialsHelper.SetMaterialsByDomain(ref mesh, geometry, materials);
        }

        /// <summary>
        /// Генерация матрицы жесткости.
        /// </summary>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="omega">Круговая частота.</param>
        /// <returns>Возвращает сгенерированную матрицу.</returns>
        public TValue[,] Generate(double waveNumber, double omega)
        {
            // TODO: придумать как универсально строить матрицу здесь
            MatrixBuilder.BuiltGlobalMatrix(GetSize(), _mesh.MeshElements, waveNumber, omega);

            return null;
        }

        /// <summary>
        /// Генерация матрицы жесткости.
        /// </summary>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="omega">Круговая частота.</param>
        /// <param name="sizeFactor">Размерный фактор, применяемый для нормирования значений.</param>
        /// <returns>Возвращает сгенерированную матрицу.</returns>
        public TValue[,] Generate(double waveNumber, double omega, double sizeFactor)
        {
            // TODO: придумать как универсально строить матрицу здесь
            MatrixBuilder.BuiltGlobalMatrix(GetSize(), _mesh.MeshElements, waveNumber, omega, sizeFactor);
            
            return null;
        }

        /// <summary>
        /// Получение размерности матрицы
        /// (равно количеству ребер в сетке, уникальных).
        /// </summary>
        /// <returns>Возвращает размерность матрицы.</returns>
        public int GetSize()
        {
            return _mesh.EdgesCount;
        }
    }
}
