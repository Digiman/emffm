﻿using System.IO;

namespace EMFFM.FEM2.Elements
{
    /// <summary>
    /// Класс для описания содержимого с результатов, выданным решателем после решения.
    /// </summary>
    /// <typeparam name="TValue">Тип значений, в которых представлен результат (скалярные и комплексные).</typeparam>
    public class SolverResult<TValue>
    {
        /// <summary>
        /// Вектор с результами (голыми, без обработки).
        /// </summary>
        private readonly TValue[] _resultVector;

        /// <summary>
        /// Время, за которое было произведено решение задачи.
        /// </summary>
        public double SolveTime { get; set; }

        /// <summary>
        /// Инициализация результата с данными вектора.
        /// </summary>
        /// <param name="vector">Вектора, полученный после решения СЛАУ.</param>
        public SolverResult(TValue[] vector)
        {
            _resultVector = vector;
        }

        /// <summary>
        /// Вектор с результами (голыми, без обработки).
        /// </summary>
        public TValue[] ResultVector
        {
            get { return _resultVector; }
        }
    }
}
