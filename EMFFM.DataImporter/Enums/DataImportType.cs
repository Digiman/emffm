﻿namespace EMFFM.DataImporter.Enums
{
    /// <summary>
    /// Типы файлов с данными, которые можно импортировать.
    /// Определяют по сути формат данных, которые нужно импортировтаь и представить в едином виде для приложения.
    /// </summary>
    public enum DataImportType
    {
        /// <summary>
        /// Данные, полкченные при решении в программе MEEP.
        /// </summary>
        MeepData, 
        /// <summary>
        /// Данные, полкченные при решении по методу FDTD.
        /// </summary>
        FdtdData
    }
}
