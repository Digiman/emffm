﻿using EMFFM.DataImporter.Elements;
using EMFFM.DataImporter.Enums;

namespace EMFFM.DataImporter
{
    // TODO: придумать что нудно будет описать в фасаде для работы с импортером

    /// <summary>
    /// Фасад для работы с библиотекой импортирования данных.
    /// </summary>
    public interface IImporterFacade
    {
        /// <summary>
        /// Импорт данных на основе файла описания параметров импорта (файл XML).
        /// </summary>
        /// <param name="filename">Имя файла XML с описанием данных для импорта.</param>
        /// <returns>Возвращает структурировнный набор данных для построения плоских графиков.</returns>
        GraphData Import(string filename);

        /// <summary>
        /// Импорт данных на основе файла описания параметров импорта (файл XML).
        /// </summary>
        /// <param name="filename">Имя файла XML с описанием данных для импорта.</param>
        /// <param name="type">Тип экспортируемых данных.</param>
        /// <returns>Возвращает структурировнный набор данных для построения плоских графиков.</returns>
        GraphData Import(string filename, DataImportType type);
    }
}
