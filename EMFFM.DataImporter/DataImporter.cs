﻿using EMFFM.DataImporter.Elements;
using EMFFM.DataImporter.Enums;
using EMFFM.DataImporter.Importers;

namespace EMFFM.DataImporter
{
    /// <summary>
    /// Основной класс для реализации импорта данных.
    /// </summary>
    /// <remarks>Фактически класс реализует паттерн проектирвоания "Фабричный метод", который позволяет на основании параметров считать данные в нужном формате не меняя при этом вызов и логику.</remarks>
    public static class DataImporter
    {
        /// <summary>
        /// Импорт данных на основе файла описания параметров импорта (файл XML).
        /// </summary>
        /// <param name="filename">Имя файла XML с описанием данных для импорта.</param>
        /// <returns>Возвращает структурировнный набор данных для построения плоских графиков.</returns>
        public static GraphData Import(string filename)
        {
            var result = new GraphData();

            var worker = new DataFileWorker(filename);

            switch (worker.DataFile.DataType)
            {
                case DataImportType.MeepData:
                    var imp = new MeepDataImporter(worker.DataFile.Series);
                    result = imp.Import();
                    break;
            }

            return result;
        }

        /// <summary>
        /// Импорт данных на основе файла описания параметров импорта (файл XML).
        /// </summary>
        /// <param name="filename">Имя файла XML с описанием данных для импорта.</param>
        /// <param name="type">Тип экспортируемых данных.</param>
        /// <returns>Возвращает структурировнный набор данных для построения плоских графиков.</returns>
        public static GraphData Import(string filename, DataImportType type)
        {
            var result = new GraphData();

            var worker = new DataFileWorker(filename);

            switch (type)
            {
                case DataImportType.MeepData:
                    var imp = new MeepDataImporter(worker.DataFile.Series);
                    result = imp.Import();
                    break;
            }

            return result;
        }
    }
}
