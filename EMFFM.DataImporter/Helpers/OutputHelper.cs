﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using EMFFM.DataImporter.Elements;

namespace EMFFM.DataImporter.Helpers
{
    /// <summary>
    /// Класс ля реализации методов для экспорта данных (вывод их из приложения в файлы).
    /// </summary>
    public static class OutputHelper
    {
        /// <summary>
        /// Сохраненение серий с данными в XML файл.
        /// </summary>
        /// <param name="filename">Имя файла, куда писать данные.</param>
        /// <param name="seriesDatas">Набор серий с данными.</param>
        public static void SaveSeriesToXmlFiles(string filename, List<SeriesData> seriesDatas)
        {
            var file = new XmlTextWriter(filename, Encoding.UTF8);

            file.WriteStartDocument();
            file.WriteStartElement("graphdata"); // <graphdata>

            file.WriteStartElement("series"); // <series>

            foreach (var seriesData in seriesDatas)
            {
                file.WriteStartElement("seriesdata"); // <seriesdata>

                file.WriteStartAttribute("name");
                file.WriteValue(seriesData.Name);
                file.WriteEndAttribute();

                file.WriteStartAttribute("color");
                file.WriteValue(seriesData.Color);
                file.WriteEndAttribute();

                file.WriteStartAttribute("width");
                file.WriteValue(seriesData.Width);
                file.WriteEndAttribute();

                file.WriteStartAttribute("style");
                file.WriteValue(seriesData.Style);
                file.WriteEndAttribute();

                WriteArrayData(file, seriesData.DataX, "DataX");
                WriteArrayData(file, seriesData.DataY, "DataY");

                file.WriteEndElement(); // </seriesdata>
            }

            file.WriteEndElement(); // </series>

            file.WriteEndElement(); // </graphdata>

            file.Close();
        }

        /// <summary>
        /// Запись данных из массива в XML файл.
        /// </summary>
        /// <param name="file">Объект для записи в файл.</param>
        /// <param name="data">Данные (массив со значениями).</param>
        /// <param name="name">Название набора данных.</param>
        private static void WriteArrayData(XmlTextWriter file, double[] data, string name)
        {
            file.WriteStartElement("data"); // <data>

            file.WriteStartAttribute("name");
            file.WriteValue(name);
            file.WriteEndAttribute();

            for (int i = 0; i < data.Length; i++)
            {
                file.WriteStartElement("value"); // <value>
                file.WriteValue(data[i].ToString(CultureInfo.InvariantCulture));
                file.WriteEndElement(); // </value>
            }

            file.WriteEndElement(); // </data>
        }

        /// <summary>
        /// Сохранение данных о сериях в текстовый файл.
        /// </summary>
        /// <param name="filename">Имя файла для сохраненения в него данных.</param>
        /// <param name="seriesDatas">Данные в виде серий.</param>
        public static void SaveSeriesToTextFiles(string filename, List<SeriesData> seriesDatas)
        {
            var file = new StreamWriter(filename);

            foreach (var seriesData in seriesDatas)
            {
                var header = String.Format("Name: {0}, Color: {1}, Width: {2}, Style: {3}", seriesData.Name, seriesData.Color, seriesData.Width,
                    seriesData.Style);
                file.WriteLine(header);

                WriteArrayData(file, seriesData.DataX, "DataX");
                WriteArrayData(file, seriesData.DataY, "DataY");

                file.WriteLine();
            }

            file.Close();
        }

        /// <summary>
        /// Запись массива с данными в текстовый файл.
        /// </summary>
        /// <param name="file">Объект для записи в текстовый файл данных.</param>
        /// <param name="data">Данные для записи в файл (массив со значениями).</param>
        /// <param name="name">Название данных.</param>
        private static void WriteArrayData(StreamWriter file, double[] data, string name)
        {
            file.WriteLine("Data name: {0}", name);

            for (int i = 0; i < data.Length; i++)
            {
                file.WriteLine(data[i].ToString(CultureInfo.InvariantCulture));
            }
        }
    }
}
