﻿using System.Collections.Generic;
using EMFFM.DataImporter.Elements;

namespace EMFFM.DataImporter.Importers
{
    /// <summary>
    /// Базовый класс для импортеров.
    /// </summary>
    public abstract class BaseImporter
    {
        /// <summary>
        /// Список файлов с данными для экспорта.
        /// </summary>
        protected readonly List<Serie> Series;

        /// <summary>
        /// Инициализация ипортера с параметрами.
        /// </summary>
        /// <param name="series">Список серий и файлов к ним.</param>
        protected BaseImporter(List<Serie> series)
        {
            Series = series;
        }

        /// <summary>
        /// Выполнение импорта данных мз файлов в приложение.
        /// </summary>
        /// <returns>Возвращает данные в виде структурированного набора серий, которые будут использоваться дял построения графиков.</returns>
        public abstract GraphData Import();
    }
}
