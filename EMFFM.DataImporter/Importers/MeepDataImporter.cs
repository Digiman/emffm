﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using EMFFM.DataImporter.Elements;

namespace EMFFM.DataImporter.Importers
{
    /// <summary>
    /// Класс для реализации импорта данных, которые производит программа MEEP для графиков.
    /// </summary>
    /// <remarks>Вернее речь идет о том, чтобы получить данные после анализа результатов в Matlab, которые в нее пришли из HDF5 файлов с результатами из MEEP.</remarks>
    public class MeepDataImporter : BaseImporter
    {
        /// <summary>
        /// Инициализация ипортера с параметрами.
        /// </summary>
        /// <param name="series">Список файлов.</param>
        public MeepDataImporter(List<Serie> series) : base(series)
        {
        }

        /// <summary>
        /// Выполнение импорта данных мз файлов в приложение.
        /// </summary>
        /// <returns>Возвращает данные в виде структурированного набора серий, котоыре будут использоваться дял построения графиков.</returns>
        public override GraphData Import()
        {
            var data = new GraphData();

            foreach (var series in Series)
            {
                double[] x = ReadFile(series.DataXFile);
                double[] y = ReadFile(series.DataYFile);

                data.SeriesDatas.Add(new SeriesData
                {
                    DataX = x,
                    DataY = y,
                    Color = series.Color,
                    Width = series.Width,
                    Name = series.Name,
                    Style = series.Style
                });
            }

            return data;
        }

        /// <summary>
        /// Чтение текстового файла с данными.
        /// </summary>
        /// <param name="filename">Имя файла с данными.</param>
        /// <returns>Возвращает массив значений, считанных с файла.</returns>
        private double[] ReadFile(string filename)
        {
            var file = new StreamReader(filename);

            var data = new List<double>();

            while (!file.EndOfStream)
            {
                var line = file.ReadLine();
                data.Add(Convert.ToDouble(line, CultureInfo.InvariantCulture));
            }
            file.Close();

            return data.ToArray();
        }
    }
}
