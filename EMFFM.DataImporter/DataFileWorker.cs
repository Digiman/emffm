﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.DataImporter.Enums;

namespace EMFFM.DataImporter
{
    /// <summary>
    /// Класс для работы с XML файлом с описанием типа и данных для импорта.
    /// </summary>
    public class DataFileWorker
    {
        /// <summary>
        /// Данные из файла описания параметров импорта.
        /// </summary>
        public DataFile DataFile { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public DataFileWorker()
        {
            DataFile = new DataFile();
        }

        /// <summary>
        /// Инициализация объекта с параметрами.
        /// </summary>
        /// <param name="filename">Имя файла с данными и параметрами импорта.</param>
        public DataFileWorker(string filename)
        {
            DataFile = ReadFile(filename);
        }

        #region Чтение файла с параметрами импорта.

        /// <summary>
        /// Чтение даннх из файла.
        /// </summary>
        /// <param name="filename">Имя XML файла с параметрами.</param>
        /// <returns>Возвращает объект с данными о параметрах импорта.</returns>
        private DataFile ReadFile(string filename)
        {
            try
            {
                var data = new DataFile();

                var doc = new XmlDocument();
                doc.Load(filename);

                // чтение секций входного файла и их разбор
                XmlNodeList list = doc.DocumentElement.ChildNodes;

                // вне зависимости от порядка следования элементов (секциий) разбираем их
                foreach (XmlNode item in list)
                {
                    if (item.NodeType == XmlNodeType.Element)
                    {
                        switch (item.Name)
                        {
                            case "datatype":
                                data.DataType = ReadDataTypeSection(item);
                                break;
                            case "series":
                                data.Series = ReadSeriesSection(item.ChildNodes);
                                break;
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                LogHelper.Log(LogMessageType.Error,
                    String.Format("Catch error in import datafile reading process. Error message: {0}", ex.Message));
                throw new Exception("Возникло исключение при обработке файла с описанием параметров импорта! " +
                                    ex.Message);
            }
        }

        #region Методы для чтения секций файла

        /// <summary>
        /// Чтение секции со сведениями о типе данных.
        /// </summary>
        /// <param name="xmlNode">Узел XML докумнета документа для разбора.</param>
        /// <returns>Возвращает тип данных для импорта.</returns>
        private DataImportType ReadDataTypeSection(XmlNode xmlNode)
        {
            return (DataImportType) Enum.Parse(typeof (DataImportType), xmlNode.InnerText);
        }

        /// <summary>
        /// Чтение списка файлов для импорта.
        /// </summary>
        /// <param name="xmlNodes">Узлы XML документа для разбора их.</param>
        /// <returns>Возвращает список файлов, описанных в файле для импорта.</returns>
        private List<Serie> ReadSeriesSection(XmlNodeList xmlNodes)
        {
            var series = new List<Serie>();

            foreach (XmlNode node in xmlNodes)
            {
                switch (node.Name)
                {
                    case "serie":
                        var files = ReadSerieFilesSection(node.ChildNodes);
                        var serie = new Serie
                        {
                            DataXFile = files["DataX"],
                            DataYFile = files["DataY"],
                            Color = Color.FromName(node.Attributes["color"].Value)
                        };
                        if (node.Attributes["width"] != null)
                        {
                            serie.Width = Convert.ToInt32(node.Attributes["width"].Value);
                        }
                        if (node.Attributes["name"] != null)
                        {
                            serie.Name = node.Attributes["name"].Value;
                        }
                        if (node.Attributes["style"] != null)
                        {
                            serie.Style = node.Attributes["style"].Value;
                        }
                        series.Add(serie);
                        break;
                }
            }

            return series;
        }

        /// <summary>
        /// Чтение сведений о файлах ля серии.
        /// </summary>
        /// <param name="xmlNodes">Узлы XML документа для разбора их.</param>
        /// <returns>Возвращает список файлов и их названий.</returns>
        private Dictionary<string, string> ReadSerieFilesSection(XmlNodeList xmlNodes)
        {
            var files = new Dictionary<string, string>();

            foreach (XmlNode node in xmlNodes)
            {
                switch (node.Name)
                {
                    case "file":
                        files.Add(node.Attributes["name"].Value, node.Attributes["value"].Value);
                        break;
                }
            }

            return files;
        }

        #endregion

        #endregion

        #region Запись файла (создание) с параметрами импорта

        /// <summary>
        /// Сохранение данных о параметрах импорта в файл.
        /// </summary>
        /// <param name="filename">Имя XML файла для сохранения данных в него.</param>
        /// <param name="data">Парамтеры импорта.</param>
        public void SaveFile(string filename, DataFile data)
        {
            var writer = new XmlTextWriter(filename, Encoding.UTF8);

            // запись начала документа
            writer.WriteStartDocument();
            writer.WriteStartElement("datafile"); // <datafile>

            // запись данных по секциям

            WriteDataTypeSection(writer, data.DataType);

            WriteSeriesSection(writer, data.Series);

            // запись конца документа
            writer.WriteEndElement(); // </datafile>

            writer.Close();
        }

        #region Методы для записи секций с параметрами импорта

        /// <summary>
        /// Запись секции с описанием типа данных для импорта.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="dataType">Тип данных для импорта.</param>
        private void WriteDataTypeSection(XmlTextWriter writer, DataImportType dataType)
        {
            writer.WriteStartElement("datatype"); // <datatype>
            writer.WriteValue(dataType);
            writer.WriteEndElement(); // </datatype>
        }

        /// <summary>
        /// Запись секции в файл со списком файлов для импорта.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="series">Список серий с файлами с данными.</param>
        private void WriteSeriesSection(XmlTextWriter writer, List<Serie> series)
        {
            writer.WriteStartElement("series"); // <series>

            foreach (var serie in series)
            {
                writer.WriteStartElement("serie"); // <serie>
                
                writer.WriteStartAttribute("color");
                writer.WriteValue(serie.Color);
                writer.WriteEndAttribute();

                writer.WriteStartAttribute("width");
                writer.WriteValue(serie.Width);
                writer.WriteEndAttribute();

                writer.WriteStartAttribute("name");
                writer.WriteValue(serie.Name);
                writer.WriteEndAttribute();

                writer.WriteStartAttribute("style");
                writer.WriteValue(serie.Style);
                writer.WriteEndAttribute();

                writer.WriteStartElement("file"); // <file>
                writer.WriteStartAttribute("name"); // name=""
                writer.WriteValue("DataX");
                writer.WriteEndAttribute();
                writer.WriteStartAttribute("value"); // value=""
                writer.WriteValue(serie.DataXFile);
                writer.WriteEndAttribute();
                writer.WriteEndElement(); // </file>

                writer.WriteStartElement("file"); // <file>
                writer.WriteStartAttribute("name"); // name=""
                writer.WriteValue("DataY");
                writer.WriteEndAttribute();
                writer.WriteStartAttribute("value"); // value=""
                writer.WriteValue(serie.DataYFile);
                writer.WriteEndAttribute();
                writer.WriteEndElement(); // </file>

                writer.WriteEndElement(); // </serie>
            }

            writer.WriteEndElement(); // </series>
        }

        #endregion

        #endregion
    }
}
