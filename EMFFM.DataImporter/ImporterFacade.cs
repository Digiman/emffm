﻿using EMFFM.DataImporter.Elements;
using EMFFM.DataImporter.Enums;

namespace EMFFM.DataImporter
{
    /// <summary>
    /// Фасад для работы с библиотекой импортирования данных.
    /// </summary>
    public class ImporterFacade : IImporterFacade
    {
        /// <summary>
        /// Импорт данных на основе файла описания параметров импорта (файл XML).
        /// </summary>
        /// <param name="filename">Имя файла XML с описанием данных для импорта.</param>
        /// <returns>Возвращает структурировнный набор данных для построения плоских графиков.</returns>
        public GraphData Import(string filename)
        {
            return DataImporter.Import(filename);
        }

        /// <summary>
        /// Импорт данных на основе файла описания параметров импорта (файл XML).
        /// </summary>
        /// <param name="filename">Имя файла XML с описанием данных для импорта.</param>
        /// <param name="type">Тип экспортируемых данных.</param>
        /// <returns>Возвращает структурировнный набор данных для построения плоских графиков.</returns>
        public GraphData Import(string filename, DataImportType type)
        {
            return DataImporter.Import(filename, type);
        }
    }
}
