﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.DataImporter.Helpers;

namespace EMFFM.DataImporter.Elements
{
    /// <summary>
    /// Данные для построения графиков.
    /// </summary>
    public class GraphData
    {
        /// <summary>
        /// Серии (данные для графиков).
        /// </summary>
        public List<SeriesData> SeriesDatas { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public GraphData()
        {
            SeriesDatas = new List<SeriesData>();
        }

        /// <summary>
        /// Экспорт данных для графика.
        /// </summary>
        /// <param name="filename">Имя файла куда экспортировать.</param>
        /// <param name="fileType">Тип файла для экспорта.</param>
        public void Export(string filename, ExportFileType fileType)
        {
            switch (fileType)
            {
                case ExportFileType.TXT:
                    OutputHelper.SaveSeriesToTextFiles(filename, SeriesDatas);
                    break;
                case ExportFileType.XML:
                    OutputHelper.SaveSeriesToXmlFiles(filename, SeriesDatas);
                    break;
            }
        }
    }
}
