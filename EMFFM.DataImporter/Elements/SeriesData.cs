﻿using System.Drawing;

namespace EMFFM.DataImporter.Elements
{
    /// <summary>
    /// Данные для отображения плоского графика.
    /// </summary>
    public class SeriesData
    {
        /// <summary>
        /// Цвет, который будет использоваться для отображения.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Данные по оси OX (координаты).
        /// </summary>
        public double[] DataX { get; set; }
        
        /// <summary>
        /// Данные по оси OY (значения).
        /// </summary>
        public double[] DataY { get; set; }

        /// <summary>
        /// Толщина линии.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Название серии (для легенды).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Стиль линии.
        /// </summary>
        public string Style { get; set; }

        /// <summary>
        /// Инициализация "пустого" объкта.
        /// </summary>
        public SeriesData()
        {
            DataX = new double[0];
            DataY = new double[0];

            Width = 1;
        }
    }
}
