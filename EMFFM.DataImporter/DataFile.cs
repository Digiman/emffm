﻿using System.Collections.Generic;
using System.Drawing;
using EMFFM.DataImporter.Enums;

namespace EMFFM.DataImporter
{
    /// <summary>
    /// Описание содержимого XML файла с описанием данных для экспорта и типа данных.
    /// По сути - параметры импорта.
    /// </summary>
    public class DataFile
    {
        /// <summary>
        /// Тип данных, размещенных в файле.
        /// </summary>
        public DataImportType DataType { get; set; }

        /// <summary>
        /// Список файлов, которые содержат сведения по графикам для экспорта.
        /// </summary>
        public List<Serie> Series { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public DataFile()
        {
            Series = new List<Serie>();
        }
    }

    /// <summary>
    /// Серия - состав из двух файлов для графика.
    /// </summary>
    public class Serie
    {
        public Color Color { get; set; }
        public string DataXFile { get; set; }
        public string DataYFile { get; set; }
        public int Width { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }

        public Serie()
        {
            Width = 1;
        }
    }
}
