﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BesselData
{
    /// <summary>
    /// Класс для реализации чтения и зачистки данных в файлах с данными для функций Бесселя.
    /// </summary>
    public class Cleaner
    {
        /// <summary>
        /// Набор строк из файла.
        /// </summary>
        private List<string> _lines = new List<string>();

        /// <summary>
        /// Имя файла,к отоырй читать и чистить.
        /// </summary>
        private readonly string _inputfile;
        
        /// <summary>
        /// Имя файла, который будет содержать очищенные строки.
        /// </summary>
        private readonly string _outputfile;

        /// <summary>
        /// Инициализация чистильщика с параметрами.
        /// </summary>
        /// <param name="inputfile">Исходный файл.</param>
        /// <param name="outputfile">Выходной файл.</param>
        public Cleaner(string inputfile, string outputfile)
        {
            _inputfile = inputfile;
            _outputfile = outputfile;
        }

        /// <summary>
        /// Выполнение зачистки для файла (полной).
        /// </summary>
        /// <param name="withSave">Сохранять ли строки в зачищенный файл?</param>
        public void Clenup(bool withSave = false)
        {
            ReadBigFile();
            CleanLines();
            RemoveEmptyLines();
            if (withSave) SaveLines();
        }

        /// <summary>
        /// Чтение строк из файла.
        /// </summary>
        /// <returns>Возвращает число прочитаных строк.</returns>
        private int ReadBigFile()
        {
            var file = new StreamReader(_inputfile);
            int i = 0;
            while (!file.EndOfStream)
            {
                _lines.Add(file.ReadLine());
                i++;
            }
            file.Close();

            return i;
        }

        /// <summary>
        /// Чистка каждой строки от ненужных элементов.
        /// </summary>
        private void CleanLines()
        {
            for (int i = 0; i < _lines.Count; i++)
            {
                _lines[i] = _lines[i].Replace("\t", "");
                _lines[i] = _lines[i].Replace("{", "");
                _lines[i] = _lines[i].Replace("},", "");
                _lines[i] = _lines[i].Replace("}", "");
            }
        }

        /// <summary>
        /// Удаление пустых строк из списка.
        /// </summary>
        private void RemoveEmptyLines()
        {
            var newLines = new List<string>();

            foreach (var line in _lines)
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    newLines.Add(line);
                }
            }

            _lines = newLines;
        }

        /// <summary>
        /// Сохранение в файл очищенных строк.
        /// </summary>
        private void SaveLines()
        {
            var file = new StreamWriter(_outputfile);

            foreach (var line in _lines)
            {
                file.WriteLine(line);
            }

            file.Close();
        }

        /// <summary>
        /// Получение самих строк.
        /// </summary>
        /// <returns>Возвращет загруженные и обработанные строки.</returns>
        public List<string> GetLines()
        {
            return _lines;
        }

        /// <summary>
        /// Получение числа строк.
        /// </summary>
        /// <returns>Возвращает число строк из прочитанного и разобранного файла.</returns>
        public long GetSize()
        {
            return _lines.Count;
        }
    }
}
