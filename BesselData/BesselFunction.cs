﻿using System.Numerics;

namespace BesselData
{
    /// <summary>
    /// Класс для инициализации функции Бесселя из данных, читаемых из файла.
    /// </summary>
    public class BesselFunction
    {
        /// <summary>
        /// Парсер и по совместительству хранилище данных из файла (прочитанных и разобранных).
        /// </summary>
        private readonly Parser _parser;

        /// <summary>
        /// Инициализация объекта с данными из файла для определения значения функции Бесселя.
        /// </summary>
        /// <param name="inputfile">Файл, откуда читать данные.</param>
        /// <param name="outputfile">Файл, куда сохранять данные.</param>
        /// <param name="sizes">Размерности (для многомерного массива.</param>
        public BesselFunction(string inputfile, string outputfile, int[] sizes)
        {
            var cleaner = new Cleaner(inputfile, outputfile);
            cleaner.Clenup();

            _parser = new Parser(cleaner.GetLines(), sizes[0], sizes[1], sizes[2]);
            _parser.Parse();
        }

        /// <summary>
        /// Получение значения функции по индексам многомерного массива.
        /// </summary>
        /// <param name="ind1">Индекс 1.</param>
        /// <param name="ind2">Индекс 2.</param>
        /// <param name="ind3">Индекс 3.</param>
        /// <returns>Возвращает комплексное число, соответсвующее значению функцию Бесселя.</returns>
        public Complex this[int ind1, int ind2, int ind3]
        {
            get { return _parser.GetValue(ind1, ind2, ind3); }
        }
    }
}
