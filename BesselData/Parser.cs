﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace BesselData
{
    /// <summary>
    /// Класс для обработки данных в строках и их преобразования в многомерный массив.
    /// </summary>
    public class Parser
    {
        /// <summary>
        /// Строки с данными для обработки.
        /// </summary>
        private readonly List<string> _lines;

        /// <summary>
        /// Полученный многомерный массив.
        /// </summary>
        private double[, ,][] _data;

        /// <summary>
        /// Размерность 1 многомерного массива.
        /// </summary>
        private readonly int _size1;
        
        /// <summary>
        /// Размерность 2 многомерного массива.
        /// </summary>
        private readonly int _size2;
        
        /// <summary>
        /// Размерность 3 многомерного массива.
        /// </summary>
        private readonly int _size3;

        /// <summary>
        /// Инициализация объекта с параметрами.
        /// </summary>
        /// <param name="lines">Строки для разбора.</param>
        /// <param name="size1">Размерность 1 многомерного массива.</param>
        /// <param name="size2">Размерность 2 многомерного массива.</param>
        /// <param name="size3">Размерность 3 многомерного массива.</param>
        public Parser(List<string> lines, int size1, int size2, int size3)
        {
            _lines = lines;
            _size1 = size1;
            _size2 = size2;
            _size3 = size3;
        }

        /// <summary>
        /// Выполнение разбора строк и преобразование их в многомерный массив.
        /// </summary>
        public void Parse()
        {
            _data = new double[_size1, _size2, _size3][];
            Int32 count = 0;
            for (int i = 0; i < _size1; i++)
            {
                for (int j = 0; j < _size2; j++)
                {
                    for (int k = 0; k < _size3; k++)
                    {
                        var line = ParseLine(_lines[count]);
                        _data[i, j, k] = ConvertLine(line);

                        count++;
                    }
                }
            }
        }

        /// <summary>
        /// Разбиение строки на массив строк.
        /// </summary>
        /// <param name="line">Строка для разбиения.</param>
        /// <returns>Возвращает массив, разделенный нужным разделителем.</returns>
        private string[] ParseLine(string line)
        {
            return line.Split(',');
        }

        /// <summary>
        /// Преобразование строк в вещественные значения.
        /// </summary>
        /// <param name="line">Данные для преобразования.</param>
        /// <returns>Возвращает вещественные значения.</returns>
        private double[] ConvertLine(string[] line)
        {
            var var1 = Convert.ToDouble(line[0].Replace('.', ','));
            var var2 = Convert.ToDouble(line[1].Replace('.', ','));
            return new[] { var1, var2 };
        }

        /// <summary>
        /// Получение комплексного числа по его индексам в многомерном массиве.
        /// </summary>
        /// <param name="size1">Значение 1 многомерного массива.</param>
        /// <param name="size2">Значение 2 многомерного массива.</param>
        /// <param name="size3">Значение 3 многомерного массива.</param>
        /// <returns>Возвращает найденное вещественное число.</returns>
        public Complex GetValue(int size1, int size2, int size3)
        {
            var tmp = _data[size1, size2, size3];
            return new Complex(tmp[0], tmp[1]);
        }
    }
}
