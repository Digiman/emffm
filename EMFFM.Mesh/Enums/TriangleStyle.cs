﻿namespace EMFFM.Mesh.Enums
{
    /// <summary>
    /// Типы ортогональных треугольников для сетки.
    /// </summary>
    /// <remarks>Используется для построения своей сетки из ортогональных элемнетов.</remarks>
    public enum TriangleStyle
    {
        /// <summary>
        /// Треугольники с основной гипотенузой.
        /// </summary>
        Left,
        /// <summary>
        /// Треугольники с побочной гипотенузой.
        /// </summary>
        Right
    }
}
