﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Mesh.Helpers;

namespace EMFFM.Mesh
{
    /// <summary>
    /// Данные сетки, используемые при решении.
    /// </summary>
    /// <typeparam name="TValueType">Тип объемных элементов.</typeparam>
    /// <typeparam name="TSurfaceType">Тип поверхностных элементов.</typeparam>
    public class TMesh<TValueType, TSurfaceType>
    {
        /// <summary>
        /// Тип элементов сетки (объемных) - основные рабочие элементы.
        /// </summary>
        public ElementType ElementType { get; set; }

        /// <summary>
        /// Список объемных элементов сетки.
        /// </summary>
        /// <remarks>Объемные элементы (тетраэдры).</remarks>
        public List<TValueType> MeshElements { get; set; }

        /// <summary>
        /// Список поверхностных элементов сетки.
        /// </summary>
        /// <remarks>Поверхностные элементы (треугольники).</remarks>
        public List<TSurfaceType> SurfElements { get; set; }

        /// <summary>
        /// Количество ребер в сетке всего.
        /// </summary>
        public int EdgesCount { get; set; }

        /// <summary>
        /// Количество ребер на гранях.
        /// </summary>
        /// <remarks>Все ребра для всех плоскостей!</remarks>
        public int BoundaryEdgesCount { get; set; }

        /// <summary>
        /// Инициализация хранилища сетки.
        /// </summary>
        /// <param name="type">Тип элементов сетки.</param>
        public TMesh(ElementType type)
        {
            ElementType = type;
        }

        /// <summary>
        /// Инициализация хранилица сетки.
        /// </summary>
        /// <param name="type">Тип элементов сетки.</param>
        /// <param name="elements">Список элементов.</param>
        /// <param name="edgesCount">Количество ребер.</param>
        public TMesh(ElementType type, List<TValueType> elements, int edgesCount)
        {
            ElementType = type;
            MeshElements = elements;
            EdgesCount = edgesCount;
        }

        /// <summary>
        /// Экпорт сетки из элементов (с ребрами) в XML-файл.
        /// </summary>
        /// <param name="filename">Файл для экспорта (полный путь к XML файлу).</param>
        /// <param name="type">Тип файла, в которой экспортировать данные о сетке.</param>
        /// <param name="withEdges">Выводить со сведениями о ребрах или без них?</param>
        public void Export(string filename, ExportFileType type, bool withEdges = false)
        {
            switch (type)
            {
                case ExportFileType.XML:
                    OutputHelper.OutputMeshDataToXmlFile(MeshElements, filename, withEdges);
                    break;
                case ExportFileType.TXT:
                    OutputHelper.OutputMeshDataToTextFile(MeshElements, filename, withEdges);
                    break;
            }
        }
    }
}
