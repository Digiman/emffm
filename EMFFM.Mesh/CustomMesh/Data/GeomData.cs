﻿using System;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input.Elements;

namespace EMFFM.Mesh.CustomMesh.Data
{
    /// <summary>
    /// Описание геометрии для кастомной сетки.
    /// </summary>
    public class GeomData
    {
        /// <summary>
        /// Полные размеры области.
        /// </summary>
        public double A, B, C;
        /// <summary>
        /// Размеры элемента (КЭ).
        /// </summary>
        public double Da, Db, Dc;

        /// <summary>
        /// Координаты узла, для начала нумерации.
        /// </summary>
        public Point StartPoint;

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public GeomData()
        {
            StartPoint = new Point();
        }

        /// <summary>
        /// Инициалилизация объекта с параметрами путем конвертации параметров коробки 
        /// в формат для генерации сеток (для генерации сетки для одной коробки).
        /// </summary>
        /// <param name="box">Коробка (параметры области в виде параллелепипеда).</param>
        /// <returns>Возвращает данные для генератора сеток.</returns>
        public GeomData(Box box)
        {
            // размеры области дискретизации
            A = Math.Abs(box.P2.X - box.P1.X); // x2-x1
            B = Math.Abs(box.P2.Y - box.P1.Y); // y2-y1
            C = Math.Abs(box.P2.Z - box.P1.Z); // z2-z1
            // размеры элемента сетки
            Da = box.Element.SizeX;
            Db = box.Element.SizeY;
            Dc = box.Element.SizeZ;
            // начальная точка
            StartPoint = box.P1;
        }

        /// <summary>
        /// Инициалилизация объекта с параметрами путем конвертации параметров коробки 
        /// в формат для генерации сеток (для генерации сетки для одной коробки).
        /// </summary>
        /// <param name="box">Коробка (параметры области в виде параллелепипеда).</param>
        /// <returns>Возвращает данные для генератора сеток.</returns>
        public GeomData(AnalyzeBox box)
        {
            // размеры области дискретизации
            A = Math.Abs(box.P2.X - box.P1.X); // x2-x1
            B = Math.Abs(box.P2.Y - box.P1.Y); // y2-y1
            C = Math.Abs(box.P2.Z - box.P1.Z); // z2-z1
            // размеры элемента сетки
            Da = box.Element.SizeX;
            Db = box.Element.SizeY;
            Dc = box.Element.SizeZ;
            // начальная точка
            StartPoint = box.P1;
        }
    }
}
