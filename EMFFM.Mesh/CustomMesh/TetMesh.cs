﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Common.Structs;
using EMFFM.Mesh.CustomMesh.Abstract;
using EMFFM.Mesh.CustomMesh.Data;
using EMFFM.Mesh.Elements;

namespace EMFFM.Mesh.CustomMesh
{
    /// <summary>
    /// Класс для генерации сетки из тетраэдров
    /// </summary>
    /// <remarks>Тетраэдры получаем путем деленения параллелепипедов!</remarks>
    public class TetMesh : IMesh<Tetrahedron>
    {
        private readonly GeomData _geomData;

        /// <summary>
        /// Инициализация генератора сетки для тетраэдров
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        public TetMesh(GeomData geomData)
        {
            LogHelper.Log(LogMessageType.Info, "Initializing parallelepiped mesh...");
            
            _geomData = geomData;
        }

        /// <summary>
        /// Генерация сетки
        /// </summary>
        /// <returns>Возвращает список из элементов</returns>
        public List<Tetrahedron> GenerateMesh()
        {
            LogHelper.Log(LogMessageType.Info, "Generate parallelepiped mesh...");
            
            // 1. Получаем список параллелепипедов путем генерации сетки из них
            // делаем это путем использования уже написанного кода, без создания повтором в этом классе!!!
            ParMesh mesh = new ParMesh(null);
            List<Parallelepiped> pars = mesh.GenerateMesh();

            // определяем общее количество тетраэдров
            int size = pars.Count * 5;

            List<Tetrahedron> result = new List<Tetrahedron>(size);

            LogHelper.Log(LogMessageType.Info, "Generate tetrahedrons for each parallelepiped...");

            // 2. Делим каждый параллелепипед на тетраэдры
            int num = 1;
            foreach (var item in pars)
            {
                result.AddRange(GetTetrahedronsForParallelepiped(item, num));
                num += 5;
            }

            LogHelper.Log(LogMessageType.Info, "Ends generating parallelepiped mesh...");

            return result;
        }

        /// <summary>
        /// Разбиение параллелепипеда на тетраэдры
        /// </summary>
        /// <param name="par">Параллелепиппед</param>
        /// <param name="number">Номер элемента</param>
        /// <returns>Возвращает список тетраэдров, полученных из разбиения элемента</returns>
        /// <remarks>Разбивает на 5 тетраэдров!</remarks>
        private List<Tetrahedron> GetTetrahedronsForParallelepiped(Parallelepiped par, int number)
        {
            List<Tetrahedron> tets = new List<Tetrahedron>(5);

            // тетраэдр с узлами: 1-5-6-8
            tets.Add(new Tetrahedron(new List<Node>()
            {
                par.Nodes[0], par.Nodes[4], par.Nodes[5], par.Nodes[7]
            }, number));
            // тетраэдр с узлами: 1-4-3-8
            tets.Add(new Tetrahedron(new List<Node>()
            {
                par.Nodes[0], par.Nodes[3], par.Nodes[2], par.Nodes[7]
            }, number + 1));
            // тетраэдр с узлами: 3-7-6-8
            tets.Add(new Tetrahedron(new List<Node>()
            {
                par.Nodes[2], par.Nodes[6], par.Nodes[5], par.Nodes[7]
            }, number + 2));
            // тетраэдр с узлами: 1-2-6-3
            tets.Add(new Tetrahedron(new List<Node>()
            {
                par.Nodes[0], par.Nodes[1], par.Nodes[5], par.Nodes[2]
            }, number + 3));
            // тетраэдры 1-4 равны!
            // тетраэдр с узлами: 1-6-8-3
            tets.Add(new Tetrahedron(new List<Node>()
            {
                par.Nodes[0], par.Nodes[5], par.Nodes[7], par.Nodes[2]
            }, number + 4));

            // выполнение ассоциации с ребрами 
            foreach(var item in tets)
            {
                item.AssociateEdges();
            }

            return tets;
        }
    }
}
