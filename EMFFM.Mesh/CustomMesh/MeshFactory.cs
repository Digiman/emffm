﻿using EMFFM.Common.Enums;
using EMFFM.Mesh.CustomMesh.Abstract;
using EMFFM.Mesh.CustomMesh.Data;
using EMFFM.Mesh.Enums;

namespace EMFFM.Mesh.CustomMesh
{
    /// <summary>
    /// Фабрика для генераторов сетки
    /// </summary>
    public static class MeshFactory
    {
        /// <summary>
        /// Получение нужного генератора сеток для заданного типа элементов
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elementType">Название типа элементов сетки</param>
        /// <param name="geomData">Данные для геометрии</param>
        /// <returns>Возвращает нужный класс через интерфейс</returns>
        public static IMesh<TType> GetMeshGenerator<TType>(ElementType elementType, GeomData geomData)
        {
            switch (elementType)
            {
                case ElementType.Tetrahedron:
                    return (IMesh<TType>) new TetMesh(geomData);
                case ElementType.Triangle:
                    return (IMesh<TType>) new TriMesh(geomData, TriangleStyle.Left);
                case ElementType.Parallelepiped:
                    //return (IMesh<TType>) new ParMesh(geomData);
                case ElementType.Rectangle:
                    return (IMesh<TType>) new RectMesh(geomData);
                default:
                    return null;
            }
        }
    }
}