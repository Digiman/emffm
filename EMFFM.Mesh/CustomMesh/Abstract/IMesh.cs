﻿using System.Collections.Generic;

namespace EMFFM.Mesh.CustomMesh.Abstract
{
    /// <summary>
    /// Интерфейс для описания классов для генерации сеток
    /// </summary>
    public interface IMesh <TType>
    {
        /// <summary>
        /// Генерация сетки
        /// </summary>
        /// <returns>Возвращает список элементов сетки</returns>
        List<TType> GenerateMesh();
    }
}
