﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Input;
using EMFFM.Mesh.Elements;

namespace EMFFM.Mesh.CustomMesh
{
    // TODO: разобраться, где используется этот класс вообще!

    /// <summary>
    /// Класс для генерации сетки с использованием встроенных средств
    /// </summary>
    public class MeshGenerator
    {
        /// <summary>
        /// Генерация сетки собственным генератором в виде параллелепипедов, а затем разбиванием их на тетраэдры
        /// </summary>
        /// <param name="data">Входные данные</param>
        /// <returns>Возвращает сгенерированную сетку</returns>
        public static TMesh<Parallelepiped, Rectangle> GenerateParallelepipeds(InputData data)
        {
            var mesh = new TMesh<Parallelepiped, Rectangle>(ElementType.Tetrahedron);
            /*List<Parallelepiped> elements = new ParMesh(data.BaseGeometry).GenerateMesh();
            mesh.MeshElements = elements;

            // Нумерация ребер элементов (глобальная нумерация)
            mesh.EdgesCount = EdgeMesh.BuiltEdges(ref elements);
            */
            return mesh;
        }
    }
}
