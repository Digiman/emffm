﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements;

namespace EMFFM.Mesh.Helpers
{
    /// <summary>
    /// Вспомогательный класс для вывода данных в библотеке Mesh.
    /// </summary>
    public static class OutputHelper
    {
        /// <summary>
        /// Вывод списка элементов сетки в XML файл.
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки.</typeparam>
        /// <param name="elements">Список элементов.</param>
        /// <param name="filename">Путь и имя файла для вывода.</param>
        /// <param name="withEdges">Выводить данные о ребрах.</param>
        public static void OutputMeshDataToXmlFile<TType>(List<TType> elements, string filename, bool withEdges = false)
        {
            var file = new XmlTextWriter(filename, Encoding.UTF8);

            file.WriteStartDocument();

            file.WriteStartElement("mesh"); // открываем <mesh>
            // пишем список элементов
            file.WriteStartElement("elements"); // открываем <elements>
            file.WriteStartAttribute("count");
            file.WriteValue(elements.Count);

            // пишем сами элементы
            foreach (var item in elements)
            {
                file.WriteStartElement("element"); // открываем <element>

                file.WriteStartAttribute("number");
                file.WriteValue((item as Element).Number);

                // пишем список узлов элемента
                file.WriteStartElement("nodes"); // открываем <nodes>
                foreach (Node node in (item as Element).Nodes)
                {
                    file.WriteStartElement("node"); // открываем <node>
                    file.WriteStartAttribute("number");
                    file.WriteValue(node.Number);
                    file.WriteStartAttribute("x");
                    file.WriteValue(node.Coord.X);
                    file.WriteStartAttribute("y");
                    file.WriteValue(node.Coord.Y);
                    file.WriteStartAttribute("z");
                    file.WriteValue(node.Coord.Z);
                    file.WriteEndElement(); // закрываем </node>
                }
                file.WriteEndElement(); // закрываем </nodes>

                if (withEdges)
                {
                    // пишем список ребер элемента
                    file.WriteStartElement("edges"); // открываем <nodes>
                    foreach (Edge edge in (item as Element).Edges)
                    {
                        file.WriteStartElement("edge"); // открываем <edge>
                        file.WriteStartAttribute("number");
                        file.WriteValue(edge.Number);
                        file.WriteStartAttribute("vetr1");
                        file.WriteValue(edge.Vertex1.Number);
                        file.WriteStartAttribute("vert2");
                        file.WriteValue(edge.Vertex2.Number);
                        if (edge.IsBoundary)
                        {
                            file.WriteStartAttribute("isBoundary");
                            file.WriteValue(edge.IsBoundary);
                            file.WriteStartAttribute("boundary");
                            file.WriteValue(edge.BoundaryNumber);
                        }
                        file.WriteEndElement(); // закрываем </edge>
                    }
                    file.WriteEndElement(); // закрываем </nodes>
                }

                file.WriteStartElement("center"); // открываем <center>
                Point tmp = (item as Element).GetCenterPoint();
                file.WriteStartAttribute("x");
                file.WriteValue(tmp.X);
                file.WriteStartAttribute("y");
                file.WriteValue(tmp.Y);
                file.WriteStartAttribute("z");
                file.WriteValue(tmp.Z);
                file.WriteEndElement(); // закрываем </center>

                file.WriteEndElement(); // закрываем </element>
            }

            file.WriteEndElement(); // закрываем </elements>

            file.WriteEndElement(); // закрываем </mesh>

            file.Close();
        }

        /// <summary>
        /// Вывод списка элементов сетки в текстовый файл.
        /// </summary>
        /// <typeparam name="TValueType">Тип элементов сетки (объемных или трехмерных).</typeparam>
        /// <param name="elements">Список элементов.</param>
        /// <param name="filename">Путь и имя файла для вывода.</param>
        /// <param name="withEdges">Выводить данные о ребрах.</param>
        public static void OutputMeshDataToTextFile<TValueType>(List<TValueType> elements, string filename, bool withEdges = false)
        {
            var file = new StreamWriter(filename);

            // TODO: проверить код для экспорта сетки в текстовый формат!

            foreach (var element in elements)
            {
                file.Write((element as Element).ToString());
                if (withEdges)
                {
                    file.Write((element as Element).EdgesToString());
                }
                else
                {
                    file.WriteLine();
                }
            }

            file.Close();
        }
    }
}
