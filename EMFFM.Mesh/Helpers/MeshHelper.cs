﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements;

namespace EMFFM.Mesh.Helpers
{
    // TODO: полностью проверить все вызовы методов данного класса и исключить ненужные, выполнить оптимизацию, перенести методы куда необходимо

    /// <summary>
    /// Вспомогательный класс для работы с сеткой
    /// </summary>
    public static class MeshHelper
    {
        /// <summary>
        /// Генерация номеров граничных ребер по данным сетки.
        /// </summary>
        /// <typeparam name="TType">Тиип объемных элементов.</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов.</typeparam>
        /// <param name="mesh">Сетка.</param>
        /// <param name="boundaryNumber">Номер границы.</param>
        /// <returns>Возвращает массив номеров граничных ребер.</returns>
        public static int[] GetNumbersOfBoundaryEdges<TType, TType2>(TMesh<TType, TType2> mesh, int boundaryNumber)
        {
            var numbers = new List<int>();

            foreach (var element in mesh.MeshElements)
            {
                foreach (var edge in (element as Element).Edges)
                {
                    if (edge.IsBoundary && edge.BoundaryNumber == boundaryNumber)
                    {
                        numbers.Add(edge.Number);
                    }
                }
            }
            numbers.Sort();

            return numbers.Distinct().ToArray();
        }

        /// <summary>
        /// Получение номеров ребер в заданной области.
        /// </summary>
        /// <typeparam name="TType">Тип поверхностных элементов сетки.</typeparam>
        /// <typeparam name="TType2">Тип объемных элементов сетки.</typeparam>
        /// <param name="mesh">Сетка.</param>
        /// <param name="domainNumber">Номер области.</param>
        /// <returns>Возвращает массив новеров ребер.</returns>
        public static int[] GetNumbersOfDomainEdges<TType, TType2>(TMesh<TType, TType2> mesh, int domainNumber)
        {
            var edges = GetEdgesInDomain(mesh, domainNumber);

            var numbers = new List<int>(edges.Count);
            numbers.AddRange(edges.Select(edge => edge.Number));

            return numbers.Distinct().ToArray();
        }

        /// <summary>
        /// Генерация списка ребер.
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки.</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки.</typeparam>
        /// <param name="mesh">Сетка.</param>
        /// <param name="withBoundary">Включая граничные ребра.</param>
        /// <returns>Возвращает список ребер.</returns>
        public static List<Edge> GetEdges<TType, TType2>(TMesh<TType, TType2> mesh, bool withBoundary = false)
        {
            var edges = new List<Edge>(withBoundary ? mesh.EdgesCount : mesh.EdgesCount - mesh.BoundaryEdgesCount);

            if (withBoundary) // если с граничными
            {
                foreach (var element in mesh.MeshElements)
                {
                    foreach (var edge in (element as Element).Edges)
                    {
                        edges.Add(edge);
                    }
                }
            }
            else // если без граничных
            {
                foreach (var element in mesh.MeshElements)
                {
                    foreach (var edge in (element as Element).Edges)
                    {
                        if (!edge.IsBoundary)
                        {
                            edges.Add(edge);
                        }
                    }
                }
            }
            edges = edges.AsParallel().Distinct().AsParallel().ToList();
            edges.Sort();

            return edges;
        }

        /// <summary>
        /// Получение списка граничных ребер.
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки.</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки.</typeparam>
        /// <param name="mesh">Сетка.</param>
        /// <param name="boundaryNumber">Номер границы.</param>
        /// <returns>Возвращает список граничных ребер.</returns>
        public static List<Edge> GetBoundaryEdges<TType, TType2>(TMesh<TType, TType2> mesh, int boundaryNumber)
        {
            var edges = new List<Edge>();

            foreach (var element in mesh.MeshElements)
            {
                foreach (var edge in (element as Element).Edges)
                {
                    if (edge.IsBoundary && edge.BoundaryNumber == boundaryNumber)
                    {
                        edges.Add(edge);
                    }
                }
            }
            edges.Sort();

            return edges.AsParallel().Distinct().ToList();
        }

        /// <summary>
        /// Получение списка ребер из заданной области
        /// (ребра элементов, расположенных в заданной области).
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки.</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки.</typeparam>
        /// <param name="mesh">Сетка.</param>
        /// <param name="domainNumber">Номер области.</param>
        /// <returns>Возвращает список ребер в заданной области.</returns>
        public static List<Edge> GetEdgesInDomain<TType, TType2>(TMesh<TType, TType2> mesh, int domainNumber)
        {
            var edges = new List<Edge>();

            foreach (var element in mesh.MeshElements.AsParallel().Where(element => (element as Element).SubDomain == domainNumber))
            {
                edges.AddRange((element as Element).Edges);
            }
            edges.Sort();

            return edges.AsParallel().Distinct().ToList();
        }

        /// <summary>
        /// Получение списка номеров элементов, находящихся в области.
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки.</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки.</typeparam>
        /// <param name="mesh">Сетка.</param>
        /// <param name="domainNumber">Номер области.</param>
        /// <returns>Возвращает массив номеров элементов из заданной области.</returns>
        public static int[] GetElementsByDomain<TType, TType2>(TMesh<TType, TType2> mesh, int domainNumber)
        {
            var result = new List<int>();

            foreach (var element in mesh.MeshElements)
            {
                if ((element as Element).SubDomain == domainNumber)
                {
                    result.Add((element as Element).Number);
                }
            }

            return result.ToArray();
        }
    }
}
