﻿using System.Numerics;

namespace EMFFM.Mesh.Elements.Abstract
{
    /// <summary>
    /// Интерфейс для описания функция расчетов локальных матриц элементов.
    /// </summary>
    public interface IElement
    {
        /// <summary>
        /// Вычисление локальной матрицы Ek для элемента.
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу элемента.</returns>
        double[,] GetLocalE();

        /// <summary>
        /// Вычисление комплексной локальной матрицы Ek элемента.
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу.</returns>
        Complex[,] GetComplexLocalE();

        /// <summary>
        /// Вычисление локальной матрицы Fk для элемента.
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу элемента.</returns>
        double[,] GetLocalF();

        /// <summary>
        /// Вычисление комплексной локальной матрицы Fk для элемента.
        /// </summary>
        /// <param name="waveNumber">Волновое число.</param>
        /// <param name="eps">Электрическая постоянная (Epsilon).</param>
        /// <returns>Возвращает вычисленную матрицу элемента.</returns>
        Complex[,] GetComplexLocalF(double waveNumber, Complex eps);
    }
}
