﻿namespace EMFFM.Mesh.Elements.Abstract
{
    /// <summary>
    /// Интерфейс для описания общих методов и свойств для трехмерных КЭ.
    /// </summary>
    public interface I3DElement
    {
        /// <summary>
        /// Вычисление объема элемента.
        /// </summary>
        /// <returns>Возвращает значение объема.</returns>
        double GetVolume();
    }
}
