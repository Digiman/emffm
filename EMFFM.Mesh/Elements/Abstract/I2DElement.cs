﻿namespace EMFFM.Mesh.Elements.Abstract
{
    /// <summary>
    /// Интерфейс для описания общих методов и свойств для двухмерных КЭ
    /// </summary>
    public interface I2DElement
    {
        /// <summary>
        /// Вычисление площади элемента.
        /// </summary>
        /// <returns>Возвращает значение площади.</returns>
        double GetArea();

        /// <summary>
        /// Вычисление периметра элемента.
        /// </summary>
        /// <returns>Возвращает значение периметра.</returns>
        double GetPerimeter();

        /// <summary>
        /// Ассоциация ребер с элементом (для поверхностного элемента) 
        /// </summary>
        /// <param name="number">Номер граничного условия.</param>
        void AssociateBoundaryEdges(int number);
    }
}
