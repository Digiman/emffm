﻿using System.Collections.Generic;
using System.Numerics;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Math;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements.Abstract;

namespace EMFFM.Mesh.Elements
{
    /// <summary>
    /// Прямоугольный элемент
    /// </summary>
    /// <remarks>Плоский элемент!</remarks>
    public class Rectangle : Element, IElement, I2DElement
    {
        #region Конструкторы
        
        /// <summary>
        /// Инициализация "пустого" элемента
        /// </summary>
        public Rectangle()
        {
            // инициализируем список узлов в размером в 4 узла
            Nodes = new List<Node>(4);
            // инициализируем пустой список ребер
            Edges = new List<Edge>(4);
            Number = -1; // по умолчанию для пустого элемента номер его -1
        }

        /// <summary>
        /// Инициализируем элемент с данными об узлах
        /// </summary>
        /// <param name="nodes">Узлы элемента</param>
        /// <param name="number">Номер элемента</param>
        public Rectangle(List<Node> nodes, int number)
        {
            Nodes = nodes;
            Number = number;
            // инициализируем пустой список ребер
            Edges = new List<Edge>(4);
        }

        /// <summary>
        /// Копирование элемента
        /// </summary>
        /// <param name="rect">Прямоугольный элемент</param>
        public Rectangle(Rectangle rect)
        {
            this.Nodes = rect.Nodes;
            this.Number = rect.Number;
            this.Edges = rect.Edges;
        }

        #endregion

        #region Реализация интерфейса I2DElement
         
        /// <summary>
        /// Вычисление площади прямоугольника
        /// </summary>
        /// <returns>Возвращает значение площади</returns>
        public double GetArea()
        {
            double a = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // сторона A
            double b = GeometryHelper.GetDistance(Nodes[1].Coord, Nodes[2].Coord); // сторона B
            return a * b;
        }

        /// <summary>
        /// Вычисление периметра прямоугольника
        /// </summary>
        /// <returns>Возвращает значение периметра</returns>
        public double GetPerimeter()
        {
            double a = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // сторона A
            double b = GeometryHelper.GetDistance(Nodes[1].Coord, Nodes[2].Coord); // сторона B
            double c = GeometryHelper.GetDistance(Nodes[2].Coord, Nodes[3].Coord); // сторона C
            double d = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[3].Coord); // сторона D
            return a + b + c + d;
        }

        #endregion

        /// <summary>
        /// Ассоциация ребер элемента с его узлами
        /// </summary>
        /// <remarks>На основании заданной локальной нумерации ребер на элементе!</remarks>
        public override void AssociateEdges()
        {
            Edges = new List<Edge>(4);
            Edges.Add(new Edge(Nodes[0], Nodes[3])); // 1-4 x
            Edges.Add(new Edge(Nodes[1], Nodes[2])); // 2-3 x
            Edges.Add(new Edge(Nodes[0], Nodes[1])); // 1-2 y
            Edges.Add(new Edge(Nodes[3], Nodes[2])); // 4-3 y
        }

        public override bool CheckPointInside(Point p)
        {
            // TODO: написать код для определения попадания точки внутрь элемента
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Ассоциация ребер с элементом (для поверхностного элемента) 
        /// </summary>
        /// <param name="number">Номер граничного условия</param>
        public void AssociateBoundaryEdges(int number)
        {
            Edges = new List<Edge>(4);
            Edges.Add(new Edge(Nodes[0], Nodes[3], true, number)); // 1-4 x
            Edges.Add(new Edge(Nodes[1], Nodes[2], true, number)); // 2-3 x
            Edges.Add(new Edge(Nodes[0], Nodes[1], true, number)); // 1-2 y
            Edges.Add(new Edge(Nodes[3], Nodes[2], true, number)); // 4-3 y
        }

        #region Вычисления локальных матриц
         
        /// <summary>
        /// Вычисление локальной матрицы Ek для элемента
        /// </summary>
        /// <returns>Возвращает заполненную локальную матрицу</returns>
        public double[,] GetLocalE()
        {
            double lx = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord);
            double ly = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[3].Coord);
            double lxy = lx / ly;
            double lyx = ly / lx;
            double[,] mat =
            {
                { lxy, -lxy, -1, 1 },
                { -lxy, lxy, 1, -1 },
                { -1, 1, lyx, -lyx },
                { 1, -1, -lyx, lyx }
            };

            return mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Ek элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу</returns>
        public Complex[,] GetComplexLocalE()
        {
            // TODO: найти и описать метод дял генерации комплексной матрицы Ek для прямоугольника
            return null;
        }

        /// <summary>
        /// Вычисление локальной матрицы Fk для элемента
        /// </summary>
        /// <returns>Возвращает заполненную локальную матрицу</returns>
        public double[,] GetLocalF()
        {
            double lx = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord);
            double ly = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[3].Coord);
            double[,] mat =
            {
                { 2, 1, 0, 0 },
                { 1, 2, 0, 0 },
                { 0, 0, 2, 1 },
                { 0, 0, 1, 2 }
            };
            mat = MatrixHelper.Multiply((lx * ly) / 6, mat);

            return mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Fk для элемента
        /// </summary>
        /// <param name="waveNumber">Волновое число</param>
        /// <param name="eps">Электрическая постоянная (Epsilon)</param>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        public Complex[,] GetComplexLocalF(double waveNumber, Complex eps)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
