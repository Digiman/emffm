﻿using System.Collections.Generic;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Mesh.Elements
{
    /// <summary>
    /// Плоскость.
    /// </summary>
    public class Plane
    {
        /// <summary>
        /// Список точек, которые образуют плоскость.
        /// </summary>
        public List<Point> Points { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public Plane()
        {
            Points = new List<Point>();
        }

        /// <summary>
        /// Инициализация плоскости по трем точкам.
        /// </summary>
        /// <param name="p1">Точка 1.</param>
        /// <param name="p2">Точка 2.</param>
        /// <param name="p3">Точка 3.</param>
        public Plane(Point p1, Point p2, Point p3)
        {
            Points = new List<Point>();
            Points.AddRange(new[] {p1, p2, p3});
        }

        public double A
        {
            get
            {
                var x1 = Points[0].X;
                var y1 = Points[0].Y;
                var z1 = Points[0].Z;
                var x2 = Points[1].X;
                var y2 = Points[1].Y;
                var z2 = Points[1].Z;
                var x3 = Points[2].X;
                var y3 = Points[2].Y;
                var z3 = Points[2].Z;
                return y1*(z2 - z3) + y2*(z3 - z1) + y3*(z1 - z2);
            }
        }

        public double B
        {
            get
            {
                var x1 = Points[0].X;
                var y1 = Points[0].Y;
                var z1 = Points[0].Z;
                var x2 = Points[1].X;
                var y2 = Points[1].Y;
                var z2 = Points[1].Z;
                var x3 = Points[2].X;
                var y3 = Points[2].Y;
                var z3 = Points[2].Z;
                return z1*(x2 - x3) + z2*(x3 - x1) + z3*(x1 - x2);
            }
        }

        public double C
        {
            get
            {
                var x1 = Points[0].X;
                var y1 = Points[0].Y;
                var z1 = Points[0].Z;
                var x2 = Points[1].X;
                var y2 = Points[1].Y;
                var z2 = Points[1].Z;
                var x3 = Points[2].X;
                var y3 = Points[2].Y;
                var z3 = Points[2].Z;
                return x1*(y2 - y3) + x2*(y3 - y1) + x3*(y1 - y2);
            }
        }

        public double D
        {
            get
            {
                var x1 = Points[0].X;
                var y1 = Points[0].Y;
                var z1 = Points[0].Z;
                var x2 = Points[1].X;
                var y2 = Points[1].Y;
                var z2 = Points[1].Z;
                var x3 = Points[2].X;
                var y3 = Points[2].Y;
                var z3 = Points[2].Z;
                return -(x1*(y2*z3 - y3*z2) + x2*(y3*z1 - y1*z3) + x3*(y1*z2 - y2*z1));
            }
        }

        /// <summary>
        /// Знак s = Ax + By + Cz + D определяет, с какой стороны по отношению к плоскости находится точка (x,y,z). 
        /// Если s > 0, то точка лежит в той стороне, куда указывает нормальный вектор (A,B,C). 
        /// Если s < 0 - на противаположной стороне, а в случае s = 0 точка принадлежит плоскости.
        /// </summary>
        /// <param name="p">Точка для проверки.</param>
        /// <returns>Возвращает значение S.</returns>
        public double GetS(Point p)
        {
            return A*p.X + B*p.Y + C*p.Z + D;
        }
    }
}
