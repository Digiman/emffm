﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using EMFFM.Common.Helpers;
using EMFFM.Common.Math;
using EMFFM.Common.Math.Elements;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements.Abstract;

namespace EMFFM.Mesh.Elements
{
    /// <summary>
    /// Тетраэдр
    /// </summary>
    public class Tetrahedron : Element, IElement, I3DElement
    {
        #region Конструкторы
        
        /// <summary>
        /// Инициализация "пустого" элемента
        /// </summary>
        public Tetrahedron()
        {
            // инициализируем список узлов в размером в 3 узла
            Nodes = new List<Node>(4);
            Number = -1; // по умолчанию для пустого элемента номер его -1
        }

        /// <summary>
        /// Инициализируем элемент с данными об узлах
        /// </summary>
        /// <param name="nodes">Узлы элемента</param>
        /// <param name="number">Номер элемента</param>
        public Tetrahedron(List<Node> nodes, int number)
        {
            Nodes = nodes;
            Number = number;
        }

        /// <summary>
        /// Инициализируем элемент с данными об узлах
        /// </summary>
        /// <param name="nodes">Узлы элемента</param>
        /// <param name="number">Номер элемента</param>
        /// <param name="subdomain">Область, где расположен элемент</param>
        public Tetrahedron(List<Node> nodes, int number, int subdomain)
        {
            Nodes = nodes;
            Number = number;
            SubDomain = subdomain;
        }

        /// <summary>
        /// Копирование элемента
        /// </summary>
        /// <param name="tr">Тетраэдра для копирования</param>
        public Tetrahedron(Tetrahedron tr)
        {
            Nodes = tr.Nodes;
            Number = tr.Number;
        }

        #endregion

        #region Реализация интерфейса I3DElement

        /// <summary>
        /// Определение объема элемента
        /// </summary>
        /// <returns>Возвращает значение объема</returns>
        /// <remarks>Вычисление выполняется на основе определителя матрицы!(длинная формула)</remarks>
        public double GetVolume()
        {
            // определение векторов с общим узлом в точке 1 тетраэдра
            var v1 = new TVector(Nodes[1].Coord, Nodes[0].Coord); // 2-1
            var v2 = new TVector(Nodes[2].Coord, Nodes[0].Coord); // 3-1
            var v3 = new TVector(Nodes[3].Coord, Nodes[0].Coord); // 4-1

            // вычисление смешанного произведения векторов
            return System.Math.Abs((1.0 / 6.0) * TVector.VectorMult(v1, v2, v3));
        }

        #endregion

        /// <summary>
        /// Ассоциация ребер элемента с его узлами
        /// </summary>
        /// <remarks>На основании заданной локальной нумерации ребер на элементе!
        /// По книге и по проге EMAP3!</remarks>
        public override void AssociateEdges()
        {
            Edges = new List<Edge>(6);
            Edges.Add(new Edge(new Node(Nodes[0], 1), new Node(Nodes[1], 2))); // 1-2
            Edges.Add(new Edge(new Node(Nodes[0], 1), new Node(Nodes[2], 3))); // 1-3
            Edges.Add(new Edge(new Node(Nodes[0], 1), new Node(Nodes[3], 4))); // 1-4
            Edges.Add(new Edge(new Node(Nodes[1], 2), new Node(Nodes[2], 3))); // 2-3
            Edges.Add(new Edge(new Node(Nodes[3], 4), new Node(Nodes[1], 2))); // 4-2
            Edges.Add(new Edge(new Node(Nodes[2], 3), new Node(Nodes[3], 4))); // 3-4
            // добавим локальную нумерацию
            Edges[0].LocalNumber = 1;
            Edges[1].LocalNumber = 2;
            Edges[2].LocalNumber = 3;
            Edges[3].LocalNumber = 4;
            Edges[4].LocalNumber = 5;
            Edges[5].LocalNumber = 6;
        }

        /// <summary>
        /// Проверка вхождения точки в элемент (находится ли она внутри элемента).
        /// </summary>
        /// <param name="p">Точка для поверки.</param>
        /// <returns>Возвращает true, если точка находится внутри элемента, иначе False.</returns>
        /// <remarks>Обязательный метод для алгоритма анализа, когда нужно знать, попадает ли точка в элемент или нет.</remarks>
        public override bool CheckPointInside(Point p)
        {
            // TODO: протестировать код для определения попадания точки в тетраэдр!!!

            var pl1 = new Plane(Nodes.Single(n => n.LocalNumber == 1).Coord, Nodes.Single(n => n.LocalNumber == 2).Coord,
                Nodes.Single(n => n.LocalNumber == 3).Coord);
            var pl2 = new Plane(Nodes.Single(n => n.LocalNumber == 1).Coord, Nodes.Single(n => n.LocalNumber == 2).Coord,
                Nodes.Single(n => n.LocalNumber == 4).Coord);
            var pl3 = new Plane(Nodes.Single(n => n.LocalNumber == 1).Coord, Nodes.Single(n => n.LocalNumber == 3).Coord,
                Nodes.Single(n => n.LocalNumber == 4).Coord);
            var pl4 = new Plane(Nodes.Single(n => n.LocalNumber == 2).Coord, Nodes.Single(n => n.LocalNumber == 3).Coord,
                Nodes.Single(n => n.LocalNumber == 4).Coord);

            if ((pl1.GetS(p) < 0 && pl2.GetS(p) < 0 && pl3.GetS(p) < 0 && pl4.GetS(p) < 0) ||
                (pl1.GetS(p) > 0 && pl2.GetS(p) > 0 && pl3.GetS(p) > 0 && pl4.GetS(p) > 0) ||
                (pl1.GetS(p) == 0 && pl2.GetS(p) == 0 && pl3.GetS(p) == 0 && pl4.GetS(p) == 0))
            {
                return true;
            }

            return false;
        }

        #region Вычисление локальных матриц

        private double[,] _coFactor;
        private double _sizeCoFactor;

        /// <summary>
        /// Вычисление локальной матрицы Ek для элемента 
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        /// <remarks>Взято из EMAP3!</remarks>
        public double[,] GetLocalE()
        {
            var S_mat = new double[6, 6];

            int i, j;
            double Length_i, Length_j, Mult1, Volume;
            {
                Volume = GetVolume() * _sizeCoFactor;
                Mult1 = (1296.0 * Volume * Volume * Volume);
                for (i = 0; i <= 5; i++)
                    for (j = 0; j <= 5; j++)
                    {
                        Length_i = GeometryHelper.GetDistance(Edges[i].Vertex1.Coord, Edges[i].Vertex2.Coord, _sizeCoFactor);
                        Length_j = GeometryHelper.GetDistance(Edges[j].Vertex1.Coord, Edges[j].Vertex2.Coord, _sizeCoFactor);

                        S_mat[i, j] = (_coFactor[2, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[i].Vertex2.LocalNumber - 1] -
                                       _coFactor[3, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[i].Vertex2.LocalNumber - 1]) *
                                      (_coFactor[2, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[j].Vertex2.LocalNumber - 1] -
                                       _coFactor[3, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[j].Vertex2.LocalNumber - 1]) +
                                      (_coFactor[3, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[i].Vertex2.LocalNumber - 1] -
                                       _coFactor[1, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[i].Vertex2.LocalNumber - 1]) *
                                      (_coFactor[3, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[j].Vertex2.LocalNumber - 1] -
                                       _coFactor[1, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[j].Vertex2.LocalNumber - 1]) +
                                      (_coFactor[1, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[i].Vertex2.LocalNumber - 1] -
                                       _coFactor[2, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[i].Vertex2.LocalNumber - 1]) *
                                      (_coFactor[1, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[j].Vertex2.LocalNumber - 1] -
                                       _coFactor[2, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[j].Vertex2.LocalNumber - 1]);

                        S_mat[i, j] = (Length_i * Length_j * S_mat[i, j]) / Mult1;
                        S_mat[i, j] *= 4;
                    }
            }

            return S_mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Ek элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу</returns>
        /// <remarks>Взято из EMAP4!</remarks>
        public Complex[,] GetComplexLocalE()
        {
            var S_mat_s = new double[6, 6];
            var S_Mat_c = new Complex[6, 6];

            double Volume = GetVolume() * _sizeCoFactor;
            double Mult1 = (1296.0 * Volume * Volume * Volume);
            for (int i = 0; i <= 5; i++)
                for (int j = 0; j <= 5; j++)
                {
                    double Length_i = GeometryHelper.GetDistance(Edges[i].Vertex1.Coord, Edges[i].Vertex2.Coord, _sizeCoFactor);
                    double Length_j = GeometryHelper.GetDistance(Edges[j].Vertex1.Coord, Edges[j].Vertex2.Coord, _sizeCoFactor);

                    S_mat_s[i, j] = (_coFactor[2, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[i].Vertex2.LocalNumber - 1] -
                                   _coFactor[3, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[i].Vertex2.LocalNumber - 1]) *
                                  (_coFactor[2, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[j].Vertex2.LocalNumber - 1] -
                                   _coFactor[3, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[j].Vertex2.LocalNumber - 1]) +
                                  (_coFactor[3, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[i].Vertex2.LocalNumber - 1] -
                                   _coFactor[1, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[i].Vertex2.LocalNumber - 1]) *
                                  (_coFactor[3, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[j].Vertex2.LocalNumber - 1] -
                                   _coFactor[1, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[3, Edges[j].Vertex2.LocalNumber - 1]) +
                                  (_coFactor[1, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[i].Vertex2.LocalNumber - 1] -
                                   _coFactor[2, Edges[i].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[i].Vertex2.LocalNumber - 1]) *
                                  (_coFactor[1, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[2, Edges[j].Vertex2.LocalNumber - 1] -
                                   _coFactor[2, Edges[j].Vertex1.LocalNumber - 1] * _coFactor[1, Edges[j].Vertex2.LocalNumber - 1]);

                    S_mat_s[i, j] = (Length_i*Length_j*S_mat_s[i, j])/Mult1;
                    S_Mat_c[i, j] = new Complex(S_mat_s[i, j], 0); // создание комплексного числа
                }

            // NOTE: здесь также нужно указат, если элемент в слое PML, расчет тогда для него идет здесь

            return S_Mat_c;
        }

        /// <summary>
        /// Вычисление локальной матрицы Fk для элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        /// <remarks>Взято из EMAP3!</remarks>
        public double[,] GetLocalF()
        {
            double TetVolume = GetVolume() * _sizeCoFactor; // определяем объем тетраэдра

            int i, j;
            double Length_i, Length_j; // длины ребер элемента

            double[,] T_mat = new double[6, 6];

            double Mult1 = 1.0 / (720.0 * TetVolume);
            // вычисление частей элементов с кофакторами
            T_mat[0, 0] = FF(1, 1) + FF(2, 2) - FF(1, 2);
            T_mat[0, 1] = 2 * FF(2, 3) - FF(2, 1) - FF(1, 3) + FF(1, 1);
            T_mat[0, 2] = 2 * FF(2, 4) - FF(2, 1) - FF(1, 4) + FF(1, 1);
            T_mat[0, 3] = -2 * FF(1, 3) - FF(2, 2) + FF(2, 3) + FF(1, 2);
            T_mat[0, 4] = 2 * FF(1, 4) - FF(2, 4) - FF(1, 2) + FF(2, 2);
            T_mat[0, 5] = FF(2, 4) - FF(2, 3) - FF(1, 4) + FF(1, 3);
            T_mat[1, 1] = FF(1, 1) + FF(3, 3) - FF(1, 3);
            T_mat[1, 2] = 2 * FF(3, 4) - FF(1, 3) - FF(1, 4) + FF(1, 1);
            T_mat[1, 3] = 2 * FF(1, 2) - FF(2, 3) - FF(1, 3) + FF(3, 3);
            T_mat[1, 4] = FF(2, 3) - FF(3, 4) - FF(1, 2) + FF(1, 4);
            T_mat[1, 5] = -2 * FF(1, 4) - FF(3, 3) + FF(1, 3) + FF(3, 4);
            T_mat[2, 2] = FF(1, 1) + FF(4, 4) - FF(1, 4);
            T_mat[2, 3] = FF(3, 4) - FF(2, 4) - FF(1, 3) + FF(1, 2);
            T_mat[2, 4] = -2 * FF(1, 2) - FF(4, 4) + FF(1, 4) + FF(2, 4);
            T_mat[2, 5] = 2 * FF(1, 3) - FF(3, 4) - FF(1, 4) + FF(4, 4);
            T_mat[3, 3] = FF(3, 3) + FF(2, 2) - FF(2, 3);
            T_mat[3, 4] = -2 * FF(3, 4) - FF(2, 2) + FF(2, 3) + FF(2, 4);
            T_mat[3, 5] = -2 * FF(2, 4) - FF(3, 3) + FF(2, 3) + FF(3, 4);
            T_mat[4, 4] = FF(2, 2) + FF(4, 4) - FF(2, 4);
            T_mat[4, 5] = -2 * FF(2, 3) - FF(4, 4) + FF(2, 4) + FF(3, 4);
            T_mat[5, 5] = FF(3, 3) + FF(4, 4) - FF(3, 4);
            // копирование симметричной части матрицы
            for (i = 0; i <= 5; i++)
                for (j = 0; j <= i - 1; j++)
                {
                    T_mat[i, j] = T_mat[j, i];
                }

            for (i = 0; i <= 5; i++)
                for (j = 0; j <= 5; j++)
                {
                    Length_i = GeometryHelper.GetDistance(Edges[i].Vertex1.Coord, Edges[i].Vertex2.Coord, _sizeCoFactor);
                    Length_j = GeometryHelper.GetDistance(Edges[j].Vertex1.Coord, Edges[j].Vertex2.Coord, _sizeCoFactor);
                     
                    if (i == j) // диагональные элементы , чтобы получилось 360 в знаменателе, а не 720
                        T_mat[i, j] = 2 * Length_i * Length_j * Mult1 * T_mat[i, j];
                    else
                        T_mat[i, j] = Length_i * Length_j * Mult1 * T_mat[i, j];
                    
                    // домножение на Epsilon осуществляется в методе сборке локальной матрицы
                    // T_mat[i, j] = RELPerm[HexNum,0] * T_mat[i,j];
                    // T_mat[i, j] = WaveNumber * WaveNumber * T_mat[i][j];
                    // домножение на волновое число осуществляется в общем методе сборки локальной матрицы
                }

            return T_mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Fk для элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        /// <remarks>Взято из EMAP4!</remarks>
        public Complex[,] GetComplexLocalF(double waveNumber, Complex eps)
        {
            double[,] mat = GetLocalF();

            var T_Mat = new Complex[6,6];
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    mat[i, j] = waveNumber*waveNumber*mat[i, j];
                    //T_Mat[i, j] = mat[i, j]*eps;
                    T_Mat[i, j] = ComplexHelper.RealMultiply(mat[i, j], eps);
                }
            }

            return T_Mat;
        }

        /// <summary>
        /// Подготовительные операции для вычисления локальных матриц
        /// </summary>
        /// <param name="coFactor">Размерный кофактор для нормирования</param>
        public void Prerequsite(double coFactor)
        {
            _sizeCoFactor = coFactor;
            // вычисление кофакторов для элемента
            _coFactor = ComputeTetraCoFactor();
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Вычисление кофакторов для тетраэдра
        /// </summary>
        /// <returns>Возвращает матрицу кофакторов [4x4]</returns>
        /// <remarks>Код взят из EMAP3(4)!</remarks>
        private double[,] ComputeTetraCoFactor()
        {
            /* This calculates the a's, b's, c's and d's in reference [9] (EMAP4) */
            
            int i, j, RowNum, ColNum, Row, Col = 0, Count = 0;
            double[,] CoF = new double[4, 4];
            double[,] Buff = new double[4, 4];
            double[,] CoFs = new double[4, 4];

            /* This just involves some basic matrix simulations */
            for (i = 0; i <= 3; i++)
            {
                for (j = 0; j <= 3; j++)
                {
                    if (j == 3)
                        Buff[i, j] = 1.0;
                    else
                        Buff[i, j] = Nodes[i].Coord[j]; //Buff[i,j] = NodeCord[TetGlobalNodeNum[t][i] - 1][j];
                }
            }
            /* The main matrix is made up of a column of 1's and the other columns are made of the node coordinates */

            /* The co-factor matrices are formed from the main matrix by taking part of the matrix */
            for (RowNum = 0; RowNum <= 3; RowNum++)
            {
                for (ColNum = 0; ColNum <= 3; ColNum++)
                {
                    /* The next few steps calculate the co-factors by taking the determinant of the sub-matrix */
                    Row = 0;
                    Col = 0;
                    Count = 0;
                    for (i = 0; i <= 3; i++)
                        for (j = 0; j <= 3; j++)
                        {
                            if ((i != RowNum) && (j != ColNum))
                            {
                                Count++;
                                if (Count < 4)
                                    Row = 0;
                                if ((Count > 3) && (Count < 7))
                                    Row = 1;
                                else if (Count > 6)
                                    Row = 2;
                                CoF[Row, Col] = Buff[i, j];
                                Col++;
                                Col = Col % 3;
                            }
                        }
                    Row = 0;
                    Col = 0;
                    Count = 0;
                    CoFs[RowNum, ColNum] = CoF[0, 0] * ((CoF[1, 1] * CoF[2, 2]) - (CoF[1, 2] * CoF[2, 1])) -
                                           CoF[0, 1] * ((CoF[1, 0] * CoF[2, 2]) - (CoF[2, 0] * CoF[1, 2])) +
                                           CoF[0, 2] * ((CoF[1, 0] * CoF[2, 1]) - (CoF[2, 0] * CoF[1, 1]));
                    if ((RowNum + ColNum) % 2 != 0)
                        CoFs[RowNum, ColNum] *= -1.0;
                }
            }
            double[,] CoFactor = new double[4, 4];
            for (RowNum = 0; RowNum <= 3; RowNum++)
            {
                if (RowNum == 0)
                    Col = 3;
                else if (RowNum == 1)
                    Col = 0;
                else if (RowNum == 2)
                    Col = 1;
                else if (RowNum == 3)
                    Col = 2;
                for (ColNum = 0; ColNum <= 3; ColNum++)
                {
                    CoFactor[RowNum, ColNum] = -CoFs[ColNum, Col];
                }
            }
            return CoFactor;
        }

        /// <summary>
        /// Вычисление Fij для элемента
        /// </summary>
        /// <param name="i">Ребро i</param>
        /// <param name="j">Ребро j</param>
        /// <returns>Возвращает значение Fij</returns>
        /// <remarks>Взято из EMAP3(4)!</remarks>
        private double FF(int i, int j)
        {
            if (_coFactor != null)
            {
                return _coFactor[1, i - 1]*_coFactor[1, j - 1] + _coFactor[2, i - 1]*_coFactor[2, j - 1] +
                       _coFactor[3, i - 1]*_coFactor[3, j - 1];
            }
            return 0;
        }

        #endregion
    }
}