﻿using System.Collections.Generic;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Math;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements.Abstract;

namespace EMFFM.Mesh.Elements
{
    /// <summary>
    /// Треугольный элемент
    /// </summary>
    public class Triangle : Element, IElement, I2DElement
    {
        #region Конструкторы
        
        /// <summary>
        /// Инициализация "пустого" элемента
        /// </summary>
        public Triangle()
        {
            // инициализируем список узлов в размером в 3 узла
            Nodes = new List<Node>(3);
            Number = -1; // по умолчанию для пустого элемента номер его -1
        }

        /// <summary>
        /// Инициализируем элемент с данными об узлах
        /// </summary>
        /// <param name="nodes">Узлы элемента</param>
        /// <param name="number">Номер элемента</param>
        public Triangle(List<Node> nodes, int number)
        {
            Nodes = nodes;
            Number = number;
        }

        /// <summary>
        /// Инициализируем элемент с данными об узлах
        /// </summary>
        /// <param name="nodes">Узлы элемента</param>
        /// <param name="number">Номер элемента</param>
        /// <param name="boundaryCond">Номер граничных условий</param>
        public Triangle(List<Node> nodes, int number, int boundaryCond)
        {
            Nodes = nodes;
            Number = number;
            BoundaryCond = boundaryCond;
        }

        /// <summary>
        /// Копирование элементов
        /// </summary>
        /// <param name="tr">Треугольник для копирования</param>
        public Triangle(Triangle tr)
        {
            this.Nodes = tr.Nodes;
            this.Number = tr.Number;
            this.BoundaryCond = tr.BoundaryCond;
        }

        #endregion

        #region Реализация интерфейса I2DElement

        /*/// <summary>
        /// Вычисление площади треугольника
        /// </summary>
        /// <returns>Возвращает значение пощади</returns>
        /// <remarks>Площадь находим по формуле Герона</remarks>
        public double GetArea()
        {
        double a = Geometry.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // сторона A
        double b = Geometry.GetDistance(Nodes[1].Coord, Nodes[2].Coord); // сторона B
        double c = Geometry.GetDistance(Nodes[0].Coord, Nodes[2].Coord); // сторона C
        double p = (a + b + c) / 2; // полупериметр
        return System.Math.Sqrt(p * (p - a) * (p - b) * (p - c)); // формула Герона
        }*/

        /// <summary>
        /// Вычисление площади треугольника
        /// </summary>
        /// <returns>Возвращает значение пощади</returns>
        /// <remarks>Площадь определяется по определителю матрицы!</remarks>
        public double GetArea()
        {
            // по формуле: (x1(y2-y3) + x2(y3-y1) + x3(y1-y2))/-2
            return System.Math.Abs(Nodes[0].Coord.X * (Nodes[1].Coord.Y - Nodes[2].Coord.Y) +
                                   Nodes[1].Coord.X * (Nodes[2].Coord.Y - Nodes[0].Coord.Y) +
                                   Nodes[2].Coord.X * (Nodes[0].Coord.Y - Nodes[1].Coord.Y)) / 2;
        }

        /// <summary>
        /// Вычисление периметра треугольника
        /// </summary>
        /// <returns>Возвращает значение периметра</returns>
        public double GetPerimeter()
        {
            double a = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // сторона A
            double b = GeometryHelper.GetDistance(Nodes[1].Coord, Nodes[2].Coord); // сторона B
            double c = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[2].Coord); // сторона C
            return a + b + c;
        }

        #endregion

        /// <summary>
        /// Ассоциация ребер с элементом
        /// </summary>
        /// <remarks>Обход против часовой стрелки!</remarks>
        public override void AssociateEdges()
        {
            Edges = new List<Edge>(3);
            Edges.Add(new Edge(Nodes[0], Nodes[1])); // 1-2
            Edges.Add(new Edge(Nodes[1], Nodes[2])); // 2-3
            Edges.Add(new Edge(Nodes[2], Nodes[0])); // 3-1
        }

        /// <summary>
        /// Определение принадлежности точки треугольнику.
        /// </summary>
        /// <param name="p">Точка для проверки.</param>
        /// <returns>Возвращает True, если точка принадлежит треугольнику, иначе - False.</returns>
        public override bool CheckPointInside(Point p)
        {
            var a = Nodes[0].Coord;
            var b = Nodes[1].Coord;
            var c = Nodes[2].Coord;
            return ((p.Classify(a, b) != Orientations.LEFT) &&
                    (p.Classify(b, c) != Orientations.LEFT) &&
                    (p.Classify(c, a) != Orientations.LEFT));
        }

        /// <summary>
        /// Ассоциация ребер с элементом (для поверхностного элемента) 
        /// </summary>
        /// <param name="number">Номер граничного условия</param>
        /// <remarks>Обход против часовой стрелки!</remarks>
        public void AssociateBoundaryEdges(int number)
        {
            Edges = new List<Edge>(3);
            Edges.Add(new Edge(Nodes[0], Nodes[1], true, number)); // 1-2
            Edges.Add(new Edge(Nodes[1], Nodes[2], true, number)); // 2-3
            Edges.Add(new Edge(Nodes[2], Nodes[0], true, number)); // 3-1
        }

        #region Вычисление локальных матриц

        /// <summary>
        /// Вычисление локальной матрицы Ek элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу</returns>
        public double[,] GetLocalE()
        {
            double l1 = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // 1-2
            double l2 = GeometryHelper.GetDistance(Nodes[1].Coord, Nodes[2].Coord); // 2-3
            double l3 = GeometryHelper.GetDistance(Nodes[2].Coord, Nodes[1].Coord); // 3-1

            double[,] mat = new double[,]
            {
                { l1 * l1, l1 * l2, l1 * l3 },
                { l1 * l2, l2 * l2, l2 * l3 },
                { l1 * l3, l2 * l3, l3 * l3 }
            };
            mat = MatrixHelper.Multiply(1 / GetArea(), mat);

            return mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Ek элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу</returns>
        /// <remarks>Метод не определен для треугольника!</remarks>
        public Complex[,] GetComplexLocalE()
        {
            // TODO: найти и описать метод дял генерации комплексной матрицы Ek для треугольника
            return null;
        }

        /// <summary>
        /// Вычислеление локальной матрицы Fk элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        public double[,] GetLocalF()
        {
            double l1 = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // 1-2
            double l2 = GeometryHelper.GetDistance(Nodes[1].Coord, Nodes[2].Coord); // 2-3
            double l3 = GeometryHelper.GetDistance(Nodes[2].Coord, Nodes[1].Coord); // 3-1

            double area = GetArea();

            double F11 = ((l1 * l1) / (24 * area)) * (GetF(1, 1) - GetF(1, 2) + GetF(2, 2));
            double F22 = ((l2 * l2) / (24 * area)) * (GetF(2, 2) - GetF(2, 3) + GetF(3, 3));
            double F33 = ((l3 * l3) / (24 * area)) * (GetF(3, 3) - GetF(3, 1) + GetF(1, 1));
            double F12 = ((l1 * l2) / (48 * area)) * (GetF(2, 3) - GetF(2, 2) - 2 * GetF(1, 3) + GetF(1, 2));
            double F13 = ((l1 * l3) / (48 * area)) * (GetF(2, 1) - 2 * GetF(2, 3) - GetF(1, 1) + GetF(1, 3));
            double F23 = ((l2 * l3) / (48 * area)) * (GetF(3, 1) - GetF(3, 3) - 2 * GetF(2, 1) + GetF(2, 3));

            double[,] mat = new double[,]
            {
                { F11, F12, F13 },
                { F12, F22, F23 },
                { F13, F23, F33 }
            };

            return mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Fk для элемента
        /// </summary>
        /// <param name="waveNumber">Волновое число</param>
        /// <param name="eps">Электрическая постоянная (Epsilon)</param>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        /// <remarks>Метод не определен для треугольника!</remarks>
        public Complex[,] GetComplexLocalF(double waveNumber, Complex eps)
        {
            // TODO: найти и описать метод дял генерации комплексной матрицы Fk для треугольника
            return null;
        }

        #endregion

        #region Вспомогательные функции

        /// <summary>
        /// Вычисление коэффициентов для расчета матрицы элемента
        /// </summary>
        /// <param name="i">Узел 1</param>
        /// <param name="j">Узел 2</param>
        /// <returns>Возвращает значение коэффициента</returns>
        private double GetF(int i, int j)
        {
            var b = new double[3];
            var c = new double[3];

            b[0] = Nodes[1].Coord.Y - Nodes[2].Coord.Y;
            b[0] = Nodes[1].Coord.Y - Nodes[2].Coord.Y;
            b[0] = Nodes[0].Coord.Y - Nodes[1].Coord.Y;

            c[0] = Nodes[2].Coord.X - Nodes[1].Coord.X;
            c[0] = Nodes[2].Coord.X - Nodes[0].Coord.X;
            c[0] = Nodes[1].Coord.X - Nodes[0].Coord.X;

            return b[i - 1] * b[j - 1] + c[i - 1] * c[j - 1];
        }

        #endregion
    }
}
