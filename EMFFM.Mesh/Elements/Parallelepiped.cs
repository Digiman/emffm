﻿using System.Collections.Generic;
using System.Numerics;
using EMFFM.Common.Math;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements.Abstract;

namespace EMFFM.Mesh.Elements
{
    /// <summary>
    /// Параллелепипед
    /// </summary>
    public class Parallelepiped : Element, IElement, I3DElement
    {
        #region Конструкторы
        
        /// <summary>
        /// Инициализация "пустого" элемента
        /// </summary>
        public Parallelepiped()
        {
            // инициализируем список узлов в размером в 3 узла
            Nodes = new List<Node>(8);
            Number = -1; // по умолчанию для пустого элемента номер его -1
        }

        /// <summary>
        /// Инициализируем элемент с данными об узлах
        /// </summary>
        /// <param name="nodes">Узлы элемента</param>
        /// <param name="number">Номер элемента</param>
        public Parallelepiped(List<Node> nodes, int number)
        {
            Nodes = nodes;
            Number = number;
        }

        /// <summary>
        /// Копирование элемента
        /// </summary>
        /// <param name="pr">Параллелепипед для копирования</param>
        public Parallelepiped(Parallelepiped pr)
        {
            Nodes = pr.Nodes;
            Number = pr.Number;
        }

        #endregion

        #region Реализация интерфейса I3DElement

        /// <summary>
        /// Определение объема элемента
        /// </summary>
        /// <returns>Возвращает значение объема</returns>
        public double GetVolume()
        {
            double a = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // сторона A (длина)
            double b = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[3].Coord); // сторона B (ширина)
            double c = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[4].Coord); // сторона C (высота)
            return a * b * c;
        }

        #endregion

        /// <summary>
        /// Ассоциация ребер элемента с его узлами
        /// </summary>
        /// <remarks>На основании заданной локальной нумерации ребер на элементе!</remarks>
        public override void AssociateEdges()
        {
            Edges = new List<Edge>(12);
            // по X
            Edges.Add(new Edge(Nodes[0], Nodes[1])); // 1-2
            Edges.Add(new Edge(Nodes[3], Nodes[2])); // 4-3
            Edges.Add(new Edge(Nodes[4], Nodes[5])); // 5-6
            Edges.Add(new Edge(Nodes[7], Nodes[6])); // 8-7
            // по Y
            Edges.Add(new Edge(Nodes[0], Nodes[3])); // 1-4
            Edges.Add(new Edge(Nodes[4], Nodes[7])); // 5-8
            Edges.Add(new Edge(Nodes[1], Nodes[2])); // 2-3
            Edges.Add(new Edge(Nodes[5], Nodes[6])); // 6-7
            // по Z
            Edges.Add(new Edge(Nodes[0], Nodes[4])); // 1-5
            Edges.Add(new Edge(Nodes[1], Nodes[5])); // 2-6
            Edges.Add(new Edge(Nodes[3], Nodes[7])); // 4-8
            Edges.Add(new Edge(Nodes[2], Nodes[6])); // 3-7
        }

        public override bool CheckPointInside(Point p)
        {
            // TODO: написать код для определения попадания точки внутрь элемента
            throw new System.NotImplementedException();
        }

        #region Вычисление локальных матриц
         
        /// <summary>
        /// Вычисление локальной матрицы Ek элемента
        /// </summary>
        /// <returns>Возвращает вычисленную локальную матрицу</returns>
        public double[,] GetLocalE()
        {
            double[,] mat = new double[12, 12];

            double lx = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // 1-2
            double ly = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[3].Coord); // 1-4
            double lz = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[4].Coord); // 1-5

            double[,] K1 = GetK1();
            double[,] K2 = GetK2();
            double[,] K3 = GetK3();

            double[,] Exx = MatrixHelper.Sum(MatrixHelper.Multiply((lx * lz) / (6 * ly), K1), MatrixHelper.Multiply((lx * ly) / (6 * lz), K2));
            double[,] Eyy = MatrixHelper.Sum(MatrixHelper.Multiply((lx * ly) / (6 * lz), K1), MatrixHelper.Multiply((ly * lz) / (6 * lx), K2));
            double[,] Ezz = MatrixHelper.Sum(MatrixHelper.Multiply((ly * lz) / (6 * lx), K1), MatrixHelper.Multiply((lx * lz) / (6 * ly), K2));

            double[,] Exy = MatrixHelper.Multiply(-lz / 6, K3);
            double[,] Eyx = MatrixHelper.Transpose(Exy);
            double[,] Ezx = MatrixHelper.Multiply(-ly / 6, K3);
            double[,] Exz = MatrixHelper.Transpose(Ezx);
            double[,] Eyz = MatrixHelper.Multiply(-lx / 6, K3);
            double[,] Ezy = MatrixHelper.Transpose(Eyz);

            // сборка итоговой матрицы
            MatrixHelper.BuiltMatrix(ref mat, Exx, 1, 4, 1, 4);
            MatrixHelper.BuiltMatrix(ref mat, Exy, 1, 4, 5, 8);
            MatrixHelper.BuiltMatrix(ref mat, Exz, 1, 4, 9, 12);
            MatrixHelper.BuiltMatrix(ref mat, Eyx, 5, 8, 1, 4);
            MatrixHelper.BuiltMatrix(ref mat, Eyy, 5, 8, 5, 8);
            MatrixHelper.BuiltMatrix(ref mat, Eyz, 5, 8, 9, 12);
            MatrixHelper.BuiltMatrix(ref mat, Ezx, 9, 12, 1, 4);
            MatrixHelper.BuiltMatrix(ref mat, Ezy, 9, 12, 5, 8);
            MatrixHelper.BuiltMatrix(ref mat, Ezz, 9, 12, 9, 12);

            return mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Ek элемента
        /// </summary>
        /// <returns>Возвращает вычисленную матрицу</returns>
        public Complex[,] GetComplexLocalE()
        {
            // TODO: найти и описать метод дял генерации комплексной матрицы Ek для параллелепипеда
            return null;
        }

        /// <summary>
        /// Вычисление локальной матрицы Fk элемента
        /// </summary>
        /// <returns>Возвращает вычисленную локальную матрицу</returns>
        public double[,] GetLocalF()
        {
            double[,] mat = MatrixHelper.ZerosMatrix(12, 12);

            double[,] Fp = GetFp();

            int k = 0;
            for (int l = 0; l < 3; l++)
            {
                for (int i = 0; i < 4; i++)
                {
                    int m = l * 4;
                    for (int j = 0; j < 4; j++)
                    {
                        mat[k, m] = Fp[i, j];
                        m++;
                    }
                    k++;
                }
            }

            return mat;
        }

        /// <summary>
        /// Вычисление комплексной локальной матрицы Fk для элемента
        /// </summary>
        /// <param name="waveNumber">Волновое число</param>
        /// <param name="eps">Электрическая постоянная (Epsilon)</param>
        /// <returns>Возвращает вычисленную матрицу элемента</returns>
        public Complex[,] GetComplexLocalF(double waveNumber, Complex eps)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Вспомогательные методы

        private double[,] GetFp()
        {
            double lx = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[1].Coord); // 1-2
            double ly = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[3].Coord); // 1-4
            double lz = GeometryHelper.GetDistance(Nodes[0].Coord, Nodes[4].Coord); // 1-5

            double[,] mat =
            {
                { 4, 2, 2, 1 },
                { 2, 4, 1, 2 },
                { 2, 1, 4, 2 },
                { 1, 2, 2, 4 }
            };
            mat = MatrixHelper.Multiply((lx * ly * lz) / 36, mat);

            return mat;
        }

        private double[,] GetK1()
        {
            return new double[,]
            {
                { 2, 1, -2, -1 },
                { 1, 2, -1, -2 },
                { -2, -1, 2, 1 },
                { -1, -2, 1, 2 }
            };
        }

        private double[,] GetK2()
        {
            return new double[,]
            {
                { 2, -2, 1, -1 },
                { -2, 2, -1, 1 },
                { 1, -1, 2, -2 },
                { -1, 1, -2, 2 }
            };
        }

        private double[,] GetK3()
        {
            return new double[,]
            {
                { 2, 1, -2, -1 },
                { -2, -1, 2, 1 },
                { 1, 2, -1, -2 },
                { -1, -2, 1, 2 }
            };
        }

        #endregion
    }
}
