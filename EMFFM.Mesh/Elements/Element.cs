﻿using System.Collections.Generic;
using System.Text;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Math;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Mesh.Elements
{
    /// <summary>
    /// Базовый класс для всех элементов, которые используются для дискретизации области здачи.
    /// </summary>
    public abstract class Element
    {
        /// <summary>
        /// Узлы элемента.
        /// </summary>
        public List<Node> Nodes { get; set; }

        /// <summary>
        /// Ребра элемента.
        /// </summary>
        /// <remarks>Используется для реализации векторного МКЭ, где для работы с элементами нужно знать не узлы, а ребра.</remarks>
        public List<Edge> Edges { get; set; }

        /// <summary>
        /// Материал элемента.
        /// </summary>
        /// <remarks>Электромагнитные параметры области, занимаемой элементом.</remarks>
        public MaterialBase Material { get; set; }

        /// <summary>
        /// Номер элемента.
        /// </summary>
        /// <remarks>Номер может быть только одим - глобальным, в рамках всей сетки для рассматриваемой области.</remarks>
        public int Number { get; set; }

        /// <summary>
        /// Номер области в описываемой задаче.
        /// </summary>
        /// <remarks>Область из генерации сетки с помощью Netgen.</remarks>
        public int SubDomain { get; set; }

        /// <summary>
        /// Номер граничного условия.
        /// </summary>
        /// <remarks>Для поверхностных плоских элементов!</remarks>
        public int BoundaryCond { get; set; }

        /// <summary>
        /// Вывод форматированной строки с номером элемента и номерами его узлов.
        /// </summary>
        /// <returns>Возвращает форматированную строку с данными об элементе.</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0} [", Number); // номер элемента
            // записываем список узлов элемента
            for (int i = 0; i < Nodes.Count; i++)
            {
                if (i != Nodes.Count - 1)
                {
                    sb.AppendFormat("{0}, ", Nodes[i].ToString());
                }
                else
                {
                    sb.AppendFormat("{0}", Nodes[i].ToString());
                }
            }
            sb.Append("]");

            return sb.ToString();
        }

        /// <summary>
        /// Генерация строки с данными о ребрах элемента.
        /// </summary>
        /// <returns>Возвращает строку со сведениями о ребрах для элемента.</returns>
        public string EdgesToString()
        {
            var sb = new StringBuilder();

            sb.AppendFormat("[ ");
            for (int i = 0; i < Edges.Count; i++)
            {
                if (i != Edges.Count - 1)
                {
                    sb.AppendFormat("{0}, ", Edges[i].ToString());
                }
                else
                {
                    sb.AppendFormat("{0}", Edges[i].ToString());
                }
            }
            sb.Append("]");

            return sb.ToString();
        }

        /// <summary>
        /// Определение центра элемента.
        /// </summary>
        /// <returns>Возвращает точку, расположенную в центре элемента.</returns>
        /// <remarks>По сути речь идет о центре тяжести. Формула одинакова для всех типов элементов.</remarks>
        public Point GetCenterPoint()
        {
            int count = Nodes.Count; // число вершин

            double x = MathHelper.SumOfCooodinates(Nodes, 0) / count;
            double y = MathHelper.SumOfCooodinates(Nodes, 1) / count;
            double z = MathHelper.SumOfCooodinates(Nodes, 2) / count;

            return new Point(x, y, z);
        }

        #region Абстрактные методы.

        /// <summary>
        /// Ассоциация узлов с ребрами для элемента.
        /// </summary>
        public abstract void AssociateEdges();

        /// <summary>
        /// Проверка вхождения точки в элемент (находится ли она внутри элемента).
        /// </summary>
        /// <param name="p">Точка для поверки.</param>
        /// <returns>Возвращает true, если точка находится внутри элемента, иначе False.</returns>
        /// <remarks>Обязательный метод для алгоритма анализа, когда нужно знать, попадает ли точка в элемент или нет.</remarks>
        public abstract bool CheckPointInside(Point p);

        #endregion
    }
}
