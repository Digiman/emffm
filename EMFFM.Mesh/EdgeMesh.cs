﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements;

namespace EMFFM.Mesh
{
    /// <summary>
    /// Класс для векторизации узловых элементов (нумерация ребер).
    /// Производит нумерацию ребер с учетом того, что они могут принадлежать нескольким элементам.
    /// </summary>
    public static class EdgeMesh
    {
        /// <summary>
        /// Нумерация ребер для сетки.
        /// </summary>
        /// <typeparam name="TType">Тип элементов.</typeparam>
        /// <param name="elements">Список элементов.</param>
        /// <returns>Возвращает количество уникальных ребер, которые пронумерованы.</returns>
        public static int BuiltEdges<TType>(ref List<TType> elements)
        {
            // 1. Сборка всех ребер элементов в список
            var element = elements[0] as Element;
            if (element != null)
            {
                int elementEdgesCount = element.Edges.Count;
                var edges = new List<Edge>(elements.Count * elementEdgesCount);
                foreach (var item in elements)
                {
                    edges.AddRange((item as Element).Edges);
                }

                // 2. Выполнение уникальной нумерации узлов
                var numbered = NumerateEdges(edges);

                // 3. Объединение уникальных номеров с номерами ребер в элементах
                foreach (var item in elements.AsParallel())
                {
                    foreach (var edge in (item as Element).Edges)
                    {
                        foreach (var edgenum in numbered.AsParallel())
                        {
                            if (edge == edgenum)
                            {
                                edge.Number = edgenum.Number;
                            }
                        }
                    }
                }

                return numbered.Count;
            }

            return 0;
        }

        /// <summary>
        /// Выполнение уникальной нумерации ребер (без повторов).
        /// </summary>
        /// <param name="edges">Список ребер (всех).</param>
        /// <returns>Возвращает список пронумерованных уникально ребер элементов сетки.</returns>
        /// <remarks>Вход - список всех ребер сетки, выход - нумерованные ребра без повторов (уникальные).</remarks>
        private static List<Edge> NumerateEdges(List<Edge> edges)
        {
            var nonduplicates = edges.AsParallel().Distinct().ToList();

            int curnum = 1; // начальный номер для текущего ребра
            foreach (var item in nonduplicates)
            {
                item.Number = curnum;
                curnum++;
            }
            return nonduplicates;
        }
    }
}
