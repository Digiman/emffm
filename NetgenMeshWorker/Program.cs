﻿using System;
using NetgenMeshWorker.Common;
using NetgenMeshWorker.Helpers;

namespace NetgenMeshWorker
{
    /// <summary>
    /// Главный рабочий класс приложения
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Разбор аргументов командной строки
        /// </summary>
        /// <param name="args">Список аргументов</param>
        private static InputArgs ParseArgumemts(string[] args)
        {
            var result = new InputArgs();
            SetDefaultParams(ref result);

            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "-xml":
                        result.GeomFile = args[1];
                        result.IsGenerateGeomFile = true;
                        break;
                    case "-geo":
                        result.GeomFile = args[1];
                        result.IsGenerateGeomFile = false;
                        break;
                }
                result.OutputFile = args[2];
            }

            return result;
        }

        /// <summary>
        /// Установка параметров окружения по умолчанию
        /// </summary>
        /// <param name="data">Входные данные</param>
        private static void SetDefaultParams(ref InputArgs data)
        {
            data.NetgenParams = "input\\Params.xml";
            data.GeomFile = "geom1";
            data.IsGenerateGeomFile = true;
            data.IsOutputToFile = true;
            data.OutputFile = "geom1Output";
        }

        /// <summary>
        /// Главная стартовая функциия программы
        /// </summary>
        /// <param name="args">Аргументы командной строки</param>
        private static void Main(string[] args)
        {
            Console.WriteLine("Starting...");

            InputArgs inputArgs = ParseArgumemts(args);

            Console.WriteLine("Generating Mesh...");

            NetgenHelper.GenerateMesh(inputArgs);

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}