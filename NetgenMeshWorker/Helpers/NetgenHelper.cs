﻿using System;
using EMFFM.MeshParser;
using EMFFM.MeshParser.Helpers;
using EMFFM.NetgenUtils;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.GeomFiles;
using EMFFM.NetgenUtils.Helpers;
using EMFFM.NetgenUtils.Preprocessing;
using NetgenMeshWorker.Common;

namespace NetgenMeshWorker.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с Netgen (с модулями для него)
    /// </summary>
    public static class NetgenHelper
    {
        /// <summary>
        /// Генерация сетки и вывод в файл
        /// </summary>
        /// <param name="data">Входные данные из аргументов командной строки</param>
        public static void GenerateMesh(InputArgs data)
        {
            // 1. Использование библиотеки NetgenUtils
            var init = new Initializer(data.NetgenParams);
            if (!init.IsApplicationExist)
            {
                throw new Exception("Приложение Netgen не может быть инициализировано! Неверно указан путь к нему!");
            }

            var meshGen = new MeshGenerator(init.Vars);
            string geoFile = String.Format("input\\{0}.geo", data.GeomFile);

            if (data.IsGenerateGeomFile)
            {
                // 1.1. Генерация файла геометрии
                /* старый вариант генерации файла геометрии
                var reader = new The3DGeometryFileReader(String.Format("input\\{0}.xml", data.GeomFile));
                GeometryData geomDataFile = reader.GetGemetryData;

                GeomFileGenerator.Generate(geoFile, GeometryTypes.The3D, geomDataFile);*/

                // создание читателя для файла определения и чтение его
                var reader = FactoryHelper.GetFileReader(GeometryTypes.The3D, String.Format("input\\{0}.xml", data.GeomFile));
                reader.ReadFile();

                // создание генератора и генерация файла геометрии
                var generator = FactoryHelper.GetFileGenerator(GeometryTypes.The3D, geoFile, (reader as The3DGeometryFileReader).GetGemetryData);
                generator.CreateFile();
            }

            // 1.2. Генерация файла сетки
            meshGen.Generate(geoFile, String.Format("{0}.neu", data.GeomFile));

            // 2. Использование библиотеки MeshParser для разбора сетки
            var parser = new MeshFileParser(String.Format("{0}\\{1}.neu", init.Vars.OutputPath, data.GeomFile));
            MeshData mesh = parser.Parse();

            if (data.IsOutputToFile)
            {
                // вывод в XML файл сведений о сетке
                OutputHelper.OutputMeshDataToXmlFile(mesh,
                                                     String.Format("{0}\\{1}.xml", init.Vars.OutputPath, data.OutputFile));
                // вывод в текстовый файл сетки для Mathcad
                OutputHelper.OutputMeshToTextFile(mesh,
                                                  String.Format("{0}\\{1}.txt", init.Vars.OutputPath, data.OutputFile));
            }
        }
    }
}