﻿using System;
using System.Windows.Forms;
using EMFFM.NetgenUtils.Helpers;
using NetgenMeshWorker2.Common;

namespace NetgenMeshWorker2.Forms
{
    /// <summary>
    /// Окно с настройками приложения Netgen и работы с ним через NetgenUtils
    /// </summary>
    public partial class EnvPrefWnd : Form
    {
        /// <summary>
        /// Инициализация окна и его компонентов
        /// </summary>
        public EnvPrefWnd()
        {
            InitializeComponent();

            InitData();
        }

        /// <summary>
        /// Инициализация параметров
        /// </summary>
        private void InitData()
        {
            textBox1.Text = Global.Variables.NetgenPath;
            textBox2.Text = Global.Variables.OutputPath;
            textBox3.Text = Global.Variables.MeshFilePrefix;
            textBox4.Text = Global.Variables.AppName;
        }

        #region Обработка событий компонентов формы

        /// <summary>
        /// Отмена редактирования параметров
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Сохранение изменений в параметрах
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text)
                && !String.IsNullOrEmpty(textBox4.Text))
            {
                Global.Variables.NetgenPath = textBox1.Text;
                Global.Variables.OutputPath = textBox2.Text;
                Global.Variables.MeshFilePrefix = textBox3.Text;
                Global.Variables.AppName = textBox4.Text;

                VariablesHelper.Save(Global.Args.NetgenParams, Global.Variables);
                this.Close();
            }
            else
            {
                MessageBox.Show("Проверьте обязательные параметры!", "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Выбор каталога с приложением Netgen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PathButton1_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            var result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox1.Text = dlg.SelectedPath;
            }
        }

        /// <summary>
        /// Выбор каталога для выходных файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PathButton2_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            var result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox2.Text = dlg.SelectedPath;
            }
        }

        #endregion
    }
}
