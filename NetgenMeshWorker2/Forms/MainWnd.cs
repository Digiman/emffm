﻿using System;
using System.IO;
using System.Windows.Forms;
using EMFFM.MeshParser;
using EMFFM.NetgenUtils;
using NetgenMeshWorker2.Common;
using NetgenMeshWorker2.Helpers;

namespace NetgenMeshWorker2.Forms
{
    /// <summary>
    /// Главное окно приложения
    /// </summary>
    public partial class MainWnd : Form
    {
        private bool _fileLoaded;
        
        /// <summary>
        /// Файл с геометрией для Netgen (*.geo).
        /// </summary>
        private string _geoFile;

        /// <summary>
        /// Главный рабочий объект для реализации взаимодействия с приложением Netgen для генерации сетки.
        /// Описан в менеджере (библиотека).
        /// </summary>
        private NetgenWorker _worker;

        /// <summary>
        /// Сгенерированная сетка с даными об ее элементах.
        /// </summary>
        private MeshData _mesh;

        /// <summary>
        /// Инициализация главного окна
        /// </summary>
        public MainWnd()
        {
            InitializeComponent();

            InitWindow();
            InitData();
        }

        /// <summary>
        /// Инициализация окна при старте приложения и его компонентов
        /// </summary>
        void InitWindow()
        {
            HideControls();
        }

        /// <summary>
        /// Инициализация начальных (стартовых) параметров
        /// </summary>
        void InitData()
        {
            _fileLoaded = false;

            ChangeStatusText("Приложение готово к работе.");
        }

        #region Обработка событий копонентов формы

        /// <summary>
        /// Открытие файла с данными для генерации сетки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "XML-files|*.xml|Geo files|*.geo";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                try
                {
                    OpenDataFile(dlg.FileName);
                    ShowControls();

                    ChangeStatusText("Файл с данными для генерации сетки открыт!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Выход из приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Выполнения генерации сетки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GenerateButton_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateMesh(_geoFile);

                InfoButton.Enabled = true;
                VisualizeButton.Enabled = true;

                ChangeStatusText("Сетка успешно сгенерирована!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Просмотр сведений о сетке.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InfoButton_Click(object sender, EventArgs e)
        {
            var wnd = new MeshInfoWnd(_mesh);
            wnd.ShowDialog();
        }

        /// <summary>
        /// Просмотр и изменение настроек.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvPrefMenuItem_Click(object sender, EventArgs e)
        {
            var wnd = new EnvPrefWnd();
            wnd.ShowDialog();
        }

        /// <summary>
        /// Справка о программе
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            var wnd = new AboutWnd();
            wnd.ShowDialog();
        }

        /// <summary>
        /// Обработка события выбора меню "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseMenuItem_Click(object sender, EventArgs e)
        {
            _fileLoaded = false;
            HideControls();

            ChangeStatusText("Документ закрыт! Приложение готово к работе.");
        }

        /// <summary>
        /// Обработка события нажатия кнопки "Визуализация"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VisualizeButton_Click(object sender, EventArgs e)
        {
            var wnd = new VisualizerWnd(_mesh);
            wnd.ShowDialog();
        }

        /// <summary>
        /// Обработка события нажатия кнопки "Запуск Netgen".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunNetgenButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_geoFile))
            {
                var runner = new AppRunner(Global.Variables);
                runner.RunAppWithGeometry(_geoFile);
            }
        }

        #endregion

        #region Основные рабочие функции

        /// <summary>
        /// Показ компонентов при открытии файла с данными.
        /// </summary>
        private void HideControls()
        {
            tabControl1.Visible = false;
            GenerateButton.Visible = false;
            InfoButton.Visible = false;
            RunNetgenButton.Visible = false;
            VisualizeButton.Visible = false;

            textBox1.Text = "";
            textBox2.Text = "";
        }

        /// <summary>
        /// Сокрытие компонентов при незагруженном файла с данными.
        /// </summary>
        private void ShowControls()
        {
            tabControl1.Visible = true;
            GenerateButton.Visible = true;
            InfoButton.Visible = true;
            InfoButton.Enabled = false;
            RunNetgenButton.Visible = true;
            RunNetgenButton.Enabled = true;
            VisualizeButton.Visible = true;
            VisualizeButton.Enabled = false;
        }

        /// <summary>
        /// Генерация сетки.
        /// </summary>
        /// <param name="filename">Имя файла с данными.</param>
        private void GenerateMesh(string filename)
        {
            _worker = new NetgenWorker(Global.Variables, filename);
            _mesh = _worker.GenerateMesh();
        }

        /// <summary>
        /// Открытие файла с данными для сетки
        /// </summary>
        /// <param name="filename">Имя файла с данными</param>
        private void OpenDataFile(string filename)
        {
            var ext = FilesHelper.GetFileExtension(filename);
            if (ext == "xml")
            {
                var geoFile = NetgenWorker.GenarateGeoFile(filename);
                textBox1.Text = File.ReadAllText(filename);
                textBox2.Text = File.ReadAllText(geoFile);
                _geoFile = geoFile;
            }
            else
            {
                textBox2.Text = File.ReadAllText(filename);
                _geoFile = filename;
            }
            _fileLoaded = true;
        }

        /// <summary>
        /// Смена текста в панели со статусом.
        /// </summary>
        /// <param name="text">Текст для отображения в панели.</param>
        private void ChangeStatusText(string text)
        {
            statusStrip1.Items[0].Text = text;
        }

        #endregion
    }
}
