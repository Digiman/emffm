﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace NetgenMeshWorker2.Forms
{
    /// <summary>
    /// Форма для отображения сведений о приложении.
    /// </summary>
    public partial class AboutWnd : Form
    {
        /// <summary>
        /// Инициализация формы и ее компонентов.
        /// </summary>
        public AboutWnd()
        {
            InitializeComponent();

            InitData();
        }

        /// <summary>
        /// Инициализация данных на форме.
        /// </summary>
        private void InitData()
        {
            linkLabel1.Links.Add(0, linkLabel1.Text.Length, "https://bitbucket.org/Digiman/emffm/wiki/Home");
        }

        /// <summary>
        /// Звкрытий окна.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var sInfo = new ProcessStartInfo(e.Link.LinkData.ToString());
            Process.Start(sInfo);
        }
    }
}
