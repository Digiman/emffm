﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MeshParser;
using EMFFM.MeshParser.Elements;

namespace NetgenMeshWorker2.Forms
{
    /// <summary>
    /// Окно для просмотра сведения по сетке.
    /// </summary>
    public partial class MeshInfoWnd : Form
    {
        /// <summary>
        /// Сгенерированная сетка.
        /// </summary>
        private readonly MeshData _mesh;

        /// <summary>
        /// Инициализация окна и данных в нем
        /// </summary>
        public MeshInfoWnd(MeshData mesh)
        {
            InitializeComponent();

            _mesh = mesh;

            InitializeWindow();
            InitializeData();
        }

        /// <summary>
        /// Настройка окна и его компонентов.
        /// </summary>
        private void InitializeWindow()
        {
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView2.AllowUserToResizeRows = false;
            dataGridView2.AllowUserToResizeColumns = false;
            dataGridView3.AllowUserToResizeRows = false;
            dataGridView3.AllowUserToResizeColumns = false;
            dataGridView1.ReadOnly = true;
            dataGridView2.ReadOnly = true;
            dataGridView3.ReadOnly = true;
        }

        /// <summary>
        /// Инициализация данными элементы формы.
        /// </summary>
        private void InitializeData()
        {
            textBox1.Text = _mesh.VolumeElements.Count.ToString();
            textBox2.Text = _mesh.Points.Count.ToString();

            PrintVolumeElements(dataGridView1, _mesh.VolumeElements);
            PrintPoints(dataGridView2, _mesh.Points);
            PrintSurfaceElements(dataGridView3, _mesh.SurfaceElements);
        }

        /// <summary>
        /// Вывод списка объемных элементов сетки.
        /// </summary>
        /// <param name="dgv">Дата грид для вывода данных.</param>
        /// <param name="elements">Список объемных элементов сетки (тетраэдров).</param>
        private void PrintVolumeElements(DataGridView dgv, List<VolumeElement> elements)
        {
            dgv.RowCount = elements.Count;
            dgv.ColumnCount = 5;

            dgv.RowHeadersVisible = false;
            dgv.Columns[0].HeaderText = "#";
            dgv.Columns[1].HeaderText = "Узел 1";
            dgv.Columns[2].HeaderText = "Узел 2";
            dgv.Columns[3].HeaderText = "Узел 3";
            dgv.Columns[4].HeaderText = "Узел 4";

            for (int i = 0; i < elements.Count; i++)
            {
                dgv[0, i].Value = i + 1;
                dgv.Columns[0].Width = 50;
                dgv[1, i].Value = elements[i].Nodes[0];
                dgv.Columns[1].Width = 50;
                dgv[2, i].Value = elements[i].Nodes[1];
                dgv.Columns[2].Width = 50;
                dgv[3, i].Value = elements[i].Nodes[2];
                dgv.Columns[3].Width = 50;
                dgv[4, i].Value = elements[i].Nodes[3];
                dgv.Columns[4].Width = 50;
            }
        }

        /// <summary>
        /// Вывод списка поверхностных элементов сетки.
        /// </summary>
        /// <param name="dgv">Дата грид для вывода данных.</param>
        /// <param name="elements">Список поверхностных элементов сетки (треугольников).</param>
        private void PrintSurfaceElements(DataGridView dgv, List<SufraceElement> elements)
        {
            dgv.RowCount = elements.Count;
            dgv.ColumnCount = 4;

            dgv.RowHeadersVisible = false;
            dgv.Columns[0].HeaderText = "#";
            dgv.Columns[1].HeaderText = "Узел 1";
            dgv.Columns[2].HeaderText = "Узел 2";
            dgv.Columns[3].HeaderText = "Узел 3";
            
            for (int i = 0; i < elements.Count; i++)
            {
                dgv[0, i].Value = i + 1;
                dgv.Columns[0].Width = 50;
                dgv[1, i].Value = elements[i].Nodes[0];
                dgv.Columns[1].Width = 50;
                dgv[2, i].Value = elements[i].Nodes[1];
                dgv.Columns[2].Width = 50;
                dgv[3, i].Value = elements[i].Nodes[2];
                dgv.Columns[3].Width = 50;
            }
        }

        /// <summary>
        /// Вывод списка узлов сетки.
        /// </summary>
        /// <param name="dgv">Дата грид для вывода данных.</param>
        /// <param name="points">Список точек.</param>
        private void PrintPoints(DataGridView dgv, List<Point> points)
        {
            dgv.RowCount = points.Count;
            dgv.ColumnCount = 4;

            dgv.RowHeadersVisible = false;
            dgv.Columns[0].HeaderText = "#";
            dgv.Columns[1].HeaderText = "X";
            dgv.Columns[2].HeaderText = "Y";
            dgv.Columns[3].HeaderText = "Z";

            for (int i = 0; i < points.Count; i++)
            {
                dgv[0, i].Value = i + 1;
                dgv.Columns[0].Width = 50;
                dgv[1, i].Value = points[i].X.ToString("F");
                dgv.Columns[1].Width = 60;
                dgv[2, i].Value = points[i].Y.ToString("F");
                dgv.Columns[2].Width = 60;
                dgv[3, i].Value = points[i].Z.ToString("F");
                dgv.Columns[3].Width = 60;
            }
        }

        #region Обработка событий компонентов формы
        
        /// <summary>
        /// Закрытие окна.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Вызов окна экспорта сетки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            var wnd = new ExportWnd(_mesh);
            wnd.ShowDialog();
        } 

        #endregion
    }
}
