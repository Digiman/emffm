﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using EMFFM.MeshParser;
using EMFFM.MeshParser.Elements;
using ILNumerics;
using ILNumerics.Drawing;
using Point = EMFFM.Common.Mesh.Elements.Point;

namespace NetgenMeshWorker2.Forms
{
    // TODO: произвести доработку графического отображения сетки в приложении для работы с сеткой!!!

    /// <summary>
    /// Окно для реализации отображения построенной сетки с помощью библиотеки ILNumerics.
    /// </summary>
    public partial class VisualizerWnd : Form
    {
        /// <summary>
        /// Сгенерированная сетка.
        /// </summary>
        private readonly MeshData _mesh;

        /// <summary>
        /// Инициализация окна и его параметров.
        /// </summary>
        public VisualizerWnd(MeshData mesh)
        {
            InitializeComponent();
            
            _mesh = mesh;
        }

        /// <summary>
        /// Обработка нажатия кнопки "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GeneratePoints(_mesh.Points);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GeneratePointsAndLines(_mesh.VolumeElements);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GenerateLines(_mesh.VolumeElements);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        private void GeneratePoints(List<Point> points)
        {
            try
            {
                var convPoints = ConvertToPoints(points);

                var scene = new ILScene
                {
                    Camera = {convPoints}
                };

                ilPanel1.Scene = scene;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private ILPoints ConvertToPoints(List<Point> points)
        {
            var results = new ILPoints {Positions = ILMath.tosingle(ILMath.randn(3, points.Count)), Color = Color.CadetBlue};

            int i = 0;
            var positions = new float[points.Count, 3];
            foreach (var point in points)
            {
                positions[i, 0] = (float) point.X/100;
                positions[i, 1] = (float) point.Y/100;
                positions[i, 2] = (float) point.Z/100;
                i++;
            }

            results.Positions.Update(positions);

            return results;
        }

        private void GeneratePointsAndLines(List<VolumeElement> volumeElements)
        {
            
        }

        private void GenerateLines(List<VolumeElement> volumeElements)
        {
            var scene = new ILScene();

            scene.Camera.Add(ConvertToLines(volumeElements));

            ilPanel1.Scene = scene;
        }

        private ILLines ConvertToLines(List<VolumeElement> volumeElements)
        {
            var lines = new ILLines();

            return lines;
        }
    }
}
