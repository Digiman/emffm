﻿using EMFFM.NetgenUtils;

namespace NetgenMeshWorker2.Common
{
    /// <summary>
    /// Структура для хранения оперативных данных в приложении
    /// </summary>
    public struct Global
    {
        public static InputArgs Args;

        public static Variables Variables;
    }
}
