﻿using System;
using EMFFM.MeshParser;
using EMFFM.NetgenUtils;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.GeomFiles;
using EMFFM.NetgenUtils.Helpers;
using EMFFM.NetgenUtils.Preprocessing;
using NetgenMeshWorker2.Helpers;

namespace NetgenMeshWorker2
{
    /// <summary>
    /// Класс для работы с сеткой и ее генерацией
    /// </summary>
    public class NetgenWorker
    {
        private readonly Variables _vars;
        private MeshData _mesh;
        private readonly string _geoFile;

        /// <summary>
        /// Инициализация класса для работы с сеткой.
        /// </summary>
        /// <param name="vars">Переменные окружения (параметры NetgenUtils).</param>
        /// <param name="geoFile">Файл с геометрией. Уже сгенерированный в *.geo (полный путь).</param>
        public NetgenWorker(Variables vars, string geoFile)
        {
            _vars = vars;
            _geoFile = geoFile;
        }

        /// <summary>
        /// Генерация сетки.
        /// </summary>
        public MeshData GenerateMesh()
        {
            var meshGen = new MeshGenerator(_vars);

            // 1. Генерация файла сетки
            meshGen.Generate(_geoFile, String.Format("{0}.neu", FilesHelper.GetFileName(_geoFile)));

            // 2. Использование библиотеки MeshParser для разбора сетки
            var parser =
                new MeshFileParser(String.Format("{0}\\{1}{2}.neu", _vars.OutputPath, _vars.MeshFilePrefix,
                    FilesHelper.GetFileName(_geoFile)));
            _mesh = parser.Parse();

            return _mesh;
        }

        /// <summary>
        /// Конвертация XML файла описания геометриив Geo файл, необходимы для Netgen.
        /// </summary>
        /// <param name="filename">Имя файла с данными в XML формате (полный путь).</param>
        /// <returns>Возвращает имя и путь к сгенерированному файлу.</returns>
        public static string GenarateGeoFile(string filename)
        {
            // создание читателя для файла определения и чтение его
            var reader = FactoryHelper.GetFileReader(GeometryTypes.The3D, filename);
            reader.ReadFile();

            string geoFile = FilesHelper.ChangeExtension(filename, "geo");

            // создание генератора и генерация файла геометрии
            var generator = FactoryHelper.GetFileGenerator(GeometryTypes.The3D, geoFile,
                (reader as The3DGeometryFileReader).GetGemetryData);
            generator.CreateFile();

            return geoFile;
        }
    }
}
