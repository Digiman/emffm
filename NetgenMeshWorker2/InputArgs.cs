﻿namespace NetgenMeshWorker2
{
    /// <summary>
    /// Параметры, описанные пользователем для работы
    /// программы в командной строке
    /// </summary>
    public class InputArgs
    {
        /// <summary>
        /// Файл с параметрами среды для NetgenUtils
        /// </summary>
        public string NetgenParams { get; set; }

        /// <summary>
        /// Имя файла с геометрией
        /// </summary>
        public string GeomFile { get; set; }

        /// <summary>
        /// Генерировать ли файл геометрии
        /// </summary>
        public bool IsGenerateGeomFile { get; set; }
    }
}