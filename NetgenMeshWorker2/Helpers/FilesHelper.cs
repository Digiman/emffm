﻿using System;
using System.IO;

namespace NetgenMeshWorker2.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с файлами
    /// </summary>
    static class FilesHelper
    {
        /// <summary>
        /// Получение расширения файла.
        /// </summary>
        /// <param name="filename">Файл для разбора (полный путь к нему).</param>
        /// <returns>Возвращает расширение файла.</returns>
        public static string GetFileExtension(string filename)
        {
            return new FileInfo(filename).Extension.Remove(0, 1);
        }

        /// <summary>
        /// Получение имени файла без его расширения.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        /// <returns>Возвращает имя файла.</returns>
        public static string GetFileName(string filename)
        {
            var mas = new FileInfo(filename).Name.Split('.');
            return mas[0];
        }

        /// <summary>
        /// Изменение расширения файла.
        /// </summary>
        /// <param name="filename">Полный путь к файлу для смены расширения.</param>
        /// <param name="newExtension">Новое расширение.</param>
        /// <returns>Возвращает путь к файлу с измененным расширением.</returns>
        public static string ChangeExtension(string filename, string newExtension)
        {
            var mas = filename.Split('.');
            return String.Format("{0}.{1}", mas[0], newExtension);
        }
    }
}
