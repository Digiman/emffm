﻿using System;
using System.Windows.Forms;
using EMFFM.NetgenUtils.Helpers;
using NetgenMeshWorker2.Common;
using NetgenMeshWorker2.Forms;

namespace NetgenMeshWorker2
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            InitGlobals();
                
            Application.Run(new MainWnd());
        }

        /// <summary>
        /// Инициализация начальных глобальных параметров
        /// </summary>
        private static void InitGlobals()
        {
            Global.Args = new InputArgs();
            Global.Args.NetgenParams = @"DataFiles\Params.xml";
            Global.Variables = VariablesHelper.Load(Global.Args.NetgenParams);
        }
    }
}
