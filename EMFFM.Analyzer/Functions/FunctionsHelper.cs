﻿using EMFFM.Common.Enums;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Mesh.Elements;

namespace EMFFM.Analyzer.Functions
{
    /// <summary>
    /// Вспомогательный класс для работы с базисными функциями.
    /// </summary>
    public static class FunctionsHelper
    {
        /// <summary>
        /// Выполнение рачета вспомогательной матрицы преобразования на основе базисных функций элемента.
        /// </summary>
        /// <typeparam name="TType">Тип элемента сетки.</typeparam>
        /// <param name="element">Сам элемент и данные о нем.</param>
        /// <param name="type">Тип элемента.</param>
        /// <returns>Возвращает расчитанную матрицу W.</returns>
        public static double[,] CalculateWMatrix<TType>(TType element, ElementType type)
        {
            // TODO: написать реализацию расчета матрицы W для других типов элементов (в случае необходимости)!

            switch (type)
            {
                case ElementType.Tetrahedron:
                    return (new TetrahedronFunction(element as Tetrahedron).GetW((element as Element).GetCenterPoint()));
                default:
                    return null;
            }
        }

        /// <summary>
        /// Выполнение рачета вспомогательной матрицы преобразования на основе базисных функций элемента.
        /// </summary>
        /// <typeparam name="TType">Тип элемента сетки.</typeparam>
        /// <param name="element">Сам элемент и данные о нем.</param>
        /// <param name="type">Тип элемента.</param>
        /// <param name="point">Точка, в которой нужно определить значение поля.</param>
        /// <returns>Возвращает расчитанную матрицу W.</returns>
        public static double[,] CalculateWMatrix<TType>(TType element, ElementType type, Point point)
        {
            // TODO: написать реализацию расчета матрицы W для других типов элементов (в случае необходимости)!

            switch (type)
            {
                case ElementType.Tetrahedron:
                    return (new TetrahedronFunction(element as Tetrahedron).GetW(point));
                default:
                    return null;
            }
        }
    }
}
