﻿using EMFFM.Common.Enums;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Analyzer.Functions
{
    /// <summary>
    /// Базовый класс для работы с бизисной функцией.
    /// </summary>
    /// <typeparam name="TType">Тип элементов, для которого определяется базовая функция.</typeparam>
    public abstract class BaseFunction <TType>
    {
        /// <summary>
        /// Элемент и сведения о нем.
        /// </summary>
        protected readonly TType Element;

        /// <summary>
        /// Тип элемента сетки, который является основным.
        /// </summary>
        private readonly ElementType _elementType;

        // координаты для четырех узлов тетраэдра
        protected double x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

        /// <summary>
        /// Инициализация базисных векторных функция для элемента.
        /// </summary>
        /// <param name="element">Элемент.</param>
        /// <param name="type">Тип элемента сетки.</param>
        protected BaseFunction(TType element, ElementType type)
        {
            Element = element;
            _elementType = type;
        }

        /// <summary>
        /// Вычисление значений векторных функция на элементе в заданной точке.
        /// </summary>
        /// <param name="point">Координаты точки внутри элемента.</param>
        /// <returns>Возвращает матрицу [W].</returns>
        public abstract double[,] GetW(Point point);
    }
}
