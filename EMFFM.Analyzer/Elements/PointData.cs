﻿using System;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Analyzer.Elements
{
    /// <summary>
    /// Данные в точке результирующей сетки.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, используемый для хранения значения о поле в заданной точке.</typeparam>
    public class PointData<TValue>
    {
        /// <summary>
        /// Точка (узел сетки анализа) и ее коодинаты.
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// Значение в точке (для поля).
        /// </summary>
        public TValue Value { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public PointData()
        {
            Point = new Point();
        }

        public override string ToString()
        {
            return String.Format("[{0} - {1}]", Point, Value);
        }
    }
}
