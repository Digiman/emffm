﻿using System;
using EMFFM.Common.Enums;

namespace EMFFM.Analyzer.Elements
{
    // TODO: придумать как сохранять данные в формат HDF5 или другой подобого типа!

    /// <summary>
    /// Результирующая сетка с данными после их обработки анализатором и наложения сетки анализа на неструктурированную область из тетраэдров.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, используемый для значений, хранимых в матрице.</typeparam>
    /// <remarks>При использовании определенного тпа данных для значений, в матрице можно будет хранить почти что угодно, а не только интенсивность поля.</remarks>
    public class ResultsMesh <TValue>
    {
        /// <summary>
        /// Данные в виде трехмерной матрицы, хранящей координаты точки и значение интенсивности поля.
        /// </summary>
        /// <remarks>Данные находятся в матирице по следующим правилам: X - Y - Z.</remarks>
        public PointData<TValue>[, ,] Data { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта (по умолчанию).
        /// </summary>
        public ResultsMesh()
        {
            Data = new PointData<TValue>[0, 0, 0];
        }

        /// <summary>
        /// Инициализация сетки с результатами после анализа с параметрами (размерностью трехмерной сетки). 
        /// </summary>
        /// <param name="sizeX">Количество узлов по OX.</param>
        /// <param name="sizeY">Количество узлов по OY.</param>
        /// <param name="sizeZ">Количество узлов по OZ.</param>
        public ResultsMesh(int sizeX, int sizeY, int sizeZ)
        {
            Data = new PointData<TValue>[sizeX, sizeY, sizeZ];
        }

        /// <summary>
        /// Построение матрицы, которая явялется срезом области и по которому можно построить картину распределения поля или плоский график потом.
        /// </summary>
        /// <param name="direction">Направление среза (по какой оси выполняется разрез).</param>
        /// <param name="layerNumber">Номер слоя для выполнения среза (по сути индекс массива для разбора трехмерной матрицы).</param>
        /// <returns>Возвращает матрицы с данными из трехмерной области анализа.</returns>
        public TValue[,] GetLayerData(Direction direction, int layerNumber)
        {
            var size = GetSize(direction);
            var matrix = new TValue[size[0], size[1]];

            // сбор информации из трехмерной матрицы в двухмерную для заданного направления среза
            for (int i = 0; i < size[0]; i++)
            {
                for (int j = 0; j < size[1]; j++)
                {
                    matrix[i, j] = GetValue(direction, i, j, layerNumber);
                }
            }

            return matrix;
        }

        /// <summary>
        /// Получение значения из трехмерной матрицы для заданно точки заданного слоя.
        /// </summary>
        /// <param name="direction">Направление среза.</param>
        /// <param name="i">Индекс по оси OX.</param>
        /// <param name="j">Индекс по оси OY.</param>
        /// <param name="layerNumber">Номер слоя, который срезается.</param>
        /// <returns>Возвращает значение в заданой точке выбранного слоя.</returns>
        private TValue GetValue(Direction direction, int i, int j, int layerNumber)
        {
            switch (direction)
            {
                case Direction.X:
                    return Data[layerNumber, i, j].Value;
                case Direction.Y:
                    return Data[i, layerNumber, j].Value;
                case Direction.Z:
                    return Data[i, j, layerNumber].Value;
            }
            return default(TValue);
        }

        /// <summary>
        /// Определение размера матрицы для плоского среза по направлениям.
        /// </summary>
        /// <param name="direction">Направление среза.</param>
        /// <returns>Возвращает массив со значениями размерностей матрицы.</returns>
        public int[] GetSize(Direction direction)
        {
            var sizes = new int[2];

            switch (direction)
            {
                case Direction.X:
                    sizes[0] = Data.GetLength(1); // Y
                    sizes[1] = Data.GetLength(2); // Z
                    break;
                case Direction.Y:
                    sizes[0] = Data.GetLength(0); // X
                    sizes[1] = Data.GetLength(2); // Z
                    break;
                case Direction.Z:
                    sizes[0] = Data.GetLength(0); // X
                    sizes[1] = Data.GetLength(1); // Y
                    break;
            }

            return sizes;
        }

        /// <summary>
        /// Получение числа слоев для заданного направления среза.
        /// </summary>
        /// <param name="direction">Направление среза.</param>
        /// <returns>Возвращает число слоев в заданном направлении среза.</returns>
        public int GetLayersCount(Direction direction)
        {
            switch (direction)
            {
                case Direction.X:
                    return Data.GetLength(0);
                case Direction.Y:
                    return Data.GetLength(1);
                case Direction.Z:
                    return Data.GetLength(2);
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Получение массива координат узлов сетки по каждому направлению с системе координат.
        /// </summary>
        /// <param name="direction">Направение, в котором нужны координаты.</param>
        /// <returns>Возвращает массив с данными о координтах по заданному направлению.</returns>
        public double[] GetCoordinates(Direction direction)
        {
            var coordinates = new double[0];
            var cell = GetCellSize();
            var startPoint = Data[0, 0, 0].Point;

            double value;
            switch (direction)
            {
                case Direction.X:
                    coordinates = new double[Data.GetLength(0)];
                    value = startPoint.X;
                    for (int i = 0; i < Data.GetLength(0); i++)
                    {
                        coordinates[i] = value;
                        value += cell[0];
                    }
                    break;
                case Direction.Y:
                    coordinates = new double[Data.GetLength(1)];
                    value = startPoint.Y;
                    for (int i = 0; i < Data.GetLength(1); i++)
                    {
                        coordinates[i] = value;
                        value += cell[1];
                    }
                    break;
                case Direction.Z:
                    coordinates = new double[Data.GetLength(2)];
                    value = startPoint.Z;
                    for (int i = 0; i < Data.GetLength(2); i++)
                    {
                        coordinates[i] = value;
                        value += cell[2];
                    }
                    break;
            }
            return coordinates;
        }

        /// <summary>
        /// Определение размера ячейки сетки (для расчета координат узлов по каждой оси).
        /// </summary>
        /// <returns>Возвращает массив, состоящий из трех величин (размеры ячейки по осям координат).</returns>
        private double[] GetCellSize()
        {
            var p1 = Data[0, 0, 0].Point; // точка 1 
            var p2 = Data[1, 1, 1].Point; // точка 2

            return new[] {Math.Abs(p2.X - p1.X), Math.Abs(p2.Y - p1.Y), Math.Abs(p2.Z - p1.Z)};
        }

        /// <summary>
        /// Расчет количества узлов в сетке анализа.
        /// </summary>
        /// <returns>Возвращает найденное число узлов в сетке.</returns>
        public int GetNodesCount()
        {
            return Data.GetLength(0)*Data.GetLength(1)*Data.GetLength(2);
        }
    }
}
