﻿using System.Collections.Generic;
using EMFFM.Analyzer.Helpers;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;

namespace EMFFM.Analyzer.Elements
{
    // TODO: пересмотреть данный класс с целью хранения результатов (теперь они хранятся далеко не здесь)

    /// <summary>
    /// Класс для хранения результатов анализа пля по направлениям.
    /// </summary>
    public class AnalyzeResult <TValue>
    {
        /// <summary>
        /// Результаты анализа поля вдоль направления OX.
        /// </summary>
        public SortedDictionary<double, TValue> ResultX;
        
        /// <summary>
        /// Результаты анализа поля вдоль направления OY.
        /// </summary>
        public SortedDictionary<double, TValue> ResultY;
        
        /// <summary>
        /// Результаты анализа поля вдоль направления OZ.
        /// </summary>
        public SortedDictionary<double, TValue> ResultZ; 

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public AnalyzeResult()
        {
            ResultX = new SortedDictionary<double, TValue>();
            ResultY = new SortedDictionary<double, TValue>();
            ResultZ = new SortedDictionary<double, TValue>();
        }

        /// <summary>
        /// Экспорт результатов в файл.
        /// </summary>
        /// <param name="filename">Имя файла для сохранения результата (полный путь).</param>
        /// <param name="fileType">Тип файла.</param>
        public void Export(string filename, ExportFileType fileType)
        {
            switch (fileType)
            {
                case ExportFileType.TXT: // экспорт в три файла (по направлениям распространения волны)
                    OutputHelper.OutputResultsToTextfile(FilesHelper.AddToFileName(filename, "-X"), ResultX);
                    OutputHelper.OutputResultsToTextfile(FilesHelper.AddToFileName(filename, "-Y"), ResultY);
                    OutputHelper.OutputResultsToTextfile(FilesHelper.AddToFileName(filename, "-Z"), ResultZ);
                    break;
                case ExportFileType.XML: // экспорт в один файл строго формата
                    var list = new List<SortedDictionary<double, TValue>> { ResultX, ResultY, ResultZ };
                    OutputHelper.OutputResultsToXmlfile(filename, list);
                    break;
            }
        }
    }
}
