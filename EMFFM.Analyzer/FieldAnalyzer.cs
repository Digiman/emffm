﻿using System.Linq;
using EMFFM.Analyzer.Elements;
using EMFFM.Analyzer.Complex.Helpers;
using EMFFM.Analyzer.Functions;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.Common.Mesh.Elements;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.Analyzer
{
    /// <summary>
    /// Пока предварительный класс с кодом, который позволяет произвести анализ результатов.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, в которых производится решение (вещественные или скалярные величины).</typeparam>
    /// <typeparam name="TResultValue">Тип данных, который используется для хранения значений.</typeparam>
    /// <remarks>Данный клас потом заменит текущий и неправильно работающий класс FieldAnalyzer.</remarks>
    public class FieldAnalyzer <TValue, TResultValue>
    {
        /// <summary>
        /// Сетка из тетраэдров.
        /// </summary>
        private readonly TMesh<Tetrahedron, Triangle> _mesh;
        
        /// <summary>
        /// Результат решения СЛАУ.
        /// </summary>
        private readonly SolverResult<TValue> _result;

        /// <summary>
        /// Данные о полях на элементе и его ребрах (полные).
        /// </summary>
        private FieldData _fieldData;

        /// <summary>
        /// Инициализация анализатора поля с помощью специальной прямоугольной сетки.
        /// </summary>
        /// <param name="mesh">Сетка из элементов, которая использовалась для решения задачи.</param>
        /// <param name="result">Результаты решения, полученные решателем.</param>
        public FieldAnalyzer(TMesh<Tetrahedron, Triangle> mesh, SolverResult<TValue> result)
        {
            _mesh = mesh;
            _result = result;

            InitFields();
        }

        /// <summary>
        /// Инициализация полей (преобразование их из массива в объекты, описывающие поля).
        /// </summary>
        private void InitFields()
        {
            // TODO: пока здесь будет приведено как только возможно к комплексным значениям (ведь расчет все ранво в них идет, а пока именно они и используются)
            _fieldData = new FieldData(FieldHelper.ConvertFromArray(_result.ResultVector));
        }

        /// <summary>
        /// Выполнение анализа результатов с помощью вспомогательной сетки.
        /// </summary>
        /// <param name="resultsMesh">Вспомогательная сетка и по совместительству результаты анализа.</param>
        /// <param name="coFactor">Корректировочное значение.</param>
        public ResultsMesh<TResultValue> PerformAnalyze(ResultsMesh<TResultValue> resultsMesh, double coFactor)
        {
            // сначала бегаем по трехмерной матрице - по всем точкам прямоугольной сетки
            for (int i = 0; i < resultsMesh.Data.GetLength(0); i++)
                for (int j = 0; j < resultsMesh.Data.GetLength(1); j++)
                    for (int k = 0; k < resultsMesh.Data.GetLength(2); k++)
                    {
                        // теперь самое накладное - определение вхождения точки в тетраэдр и расчет значения поля
                        foreach (var element in _mesh.MeshElements.AsParallel())
                        {
                            if (element.CheckPointInside(resultsMesh.Data[i, j, k].Point))
                            {
                                // расчет значения поля
                                TResultValue result = CalculateLocalElement<Tetrahedron, TResultValue>(element, _fieldData, resultsMesh.Data[i, j, k].Point, coFactor);
                                resultsMesh.Data[i, j, k].Value = result;
                                break;
                            }
                        }
                    }
            return resultsMesh;
        }

        /// <summary>
        /// Вычисление поля на элементе.
        /// </summary>
        /// <typeparam name="TType">Тип данных, которые описывают элемент сетки.</typeparam>
        /// <typeparam name="TValue">Тип данных для описания значений, в которых ведется расчет (вещественные или комплексные).</typeparam>
        /// <param name="element">Данные элемента.</param>
        /// <param name="field">Данные о локальном поле.</param>
        /// <param name="point">Точка, в которой необходимо определить значение поля.</param>
        /// <param name="coFactor">Корректировочное значение.</param>
        /// <remarks>Выполняется преобразование глобального поля к локальному.</remarks>
        private TValue CalculateLocalElement<TType, TValue>(TType element, FieldData field, Point point, double coFactor)
        {
            // TODO: проверить метод для преобразования глобального поля к локальному в заданной точке внутри элемента!

            var elf = new ElementField();

            // определяем векторные соотношения для точки в центре масс элемента
            //double[,] mat = new TetrahedronFunction(element as Tetrahedron).GetW(point);
            // или
            double[,] mat = FunctionsHelper.CalculateWMatrix(element, _mesh.ElementType, point);

            var vec = new System.Numerics.Complex[6];
            int k = 0;
            foreach (var edge in (element as Element).Edges)
            {
                vec[k] = edge.GetLenght(coFactor) * field.EdgeFields[edge.Number - 1].TangentialField;
                elf.EdgeFields.Add(field.EdgeFields[edge.Number - 1]);
                k++;
            }

            System.Numerics.Complex[] result = MathNetComplexHelper.Multiply(ComplexHelper.Convert(mat), vec);

            elf.CenterField = new Field(result);
            field.ElementFields.Add(elf);

            return (TValue) (object) elf.CenterField;
        }
    }
}
