﻿using System;
using EMFFM.Analyzer.Elements;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input.Elements;

namespace EMFFM.Analyzer.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с сеткой анализа.
    /// </summary>
    public static class AnalyzerMeshHelper
    {
        /// <summary>
        /// Создание прямоугольной сетки анализа.
        /// </summary>
        /// <typeparam name="TValue">Тип данных для значений, которые будут храниться в трехмерной матрице анализа.</typeparam>
        /// <param name="box">Область анилиза.</param>
        /// <returns>Возвращает результирующую сетку с данными о точках сетки.</returns>
        public static ResultsMesh<TValue> CreateMesh<TValue>(AnalyzeBox box)
        {
            // размеры области дискретизации
            var a = Math.Abs(box.P2.X - box.P1.X); // x2-x1
            var b = Math.Abs(box.P2.Y - box.P1.Y); // y2-y1
            var c = Math.Abs(box.P2.Z - box.P1.Z); // z2-z1
            // размеры элемента сетки
            var dx = box.Element.SizeX;
            var dy = box.Element.SizeY;
            var dz = box.Element.SizeZ;
            // начальная точка
            var startPoint = box.P1;

            // расчет количества узлов по каждой из осей координат
            var ntx = (int) Math.Floor(a/dx) + 1; // узлов по оси OX (длине)
            var nty = (int) Math.Floor(b/dy) + 1; // узлов по оси OY (ширине)
            int ntz = (int) Math.Floor(c/dz) + 1; // узлов по оси OZ (высоте) 

            var result = new ResultsMesh<TValue>(ntx, nty, ntz);

            double x = startPoint.X, y = startPoint.Y, z = startPoint.Z;
            for (int i = 0; i < ntx; i++)
            {
                y = startPoint.Y;
                for (int j = 0; j < nty; j++)
                {
                    z = startPoint.Z;
                    for (int k = 0; k < ntz; k++)
                    {
                        result.Data[i, j, k] = new PointData<TValue>();
                        result.Data[i, j, k].Point = new Point(x, y, z);
                        z += dz;
                    }
                    y += dy;
                }
                x += dx;
            }

            return result;
        }
    }
}
