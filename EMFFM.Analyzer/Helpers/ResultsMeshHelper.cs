﻿using System;
using System.IO;
using EMFFM.Analyzer.Elements;
using EMFFM.Common.Enums;
using EMFFM.Common.Extensions;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;

namespace EMFFM.Analyzer.Helpers
{
    /// <summary>
    /// Вспомогательный класс с различными методами для работы с результатами анализа в виде сетки анализи.
    /// </summary>
    public static class ResultsMeshHelper
    {
        /// <summary>
        /// Экспорт данных для построения анимации картинки графика поверхности в Matlab.
        /// </summary>
        /// <typeparam name="TValue">Тип данных, которые хранятся в виде результатов в сетке анализа.</typeparam>
        /// <param name="mesh">Сетка анализа с данными.</param>
        /// <param name="direction">Направление среза.</param>
        /// <param name="outputFolder">Выходной каталог, куда помещать данные.</param>
        public static void ExportLayersForAnimate<TValue>(ResultsMesh<TValue> mesh, Direction direction,
            string outputFolder)
        {
            IoHelper.CheckAndCreateDirectory(outputFolder);
            IoHelper.CopyFileToDirectory(@"Matlab/VizualizeAnimation.m", outputFolder);

            var layersCount = mesh.GetLayersCount(direction);

            int lenght = 0;
            for (int i = 0; i < layersCount; i++)
            {
                var layerData = mesh.GetLayerData(direction, i);

                if (lenght <= 0) lenght = layerData.GetLength(0);

                SavePlotToFile(String.Format("{0}plot-{1}.txt", outputFolder, i), layerData);
            }

            SaveCoordinates(mesh, direction, outputFolder);

            string animateFile;
            switch (direction)
            {
                case Direction.X:
                    animateFile = "animate-X.gif";
                    CreateMatlabAnimateStartFile(outputFolder + "CreateAnimation.m", "y.txt", "z.txt", lenght, animateFile);
                    break;
                case Direction.Y:
                    animateFile = "animate-Y.gif";
                    CreateMatlabAnimateStartFile(outputFolder + "CreateAnimation.m", "x.txt", "z.txt", lenght, animateFile);
                    break;
                case Direction.Z:
                    animateFile = "animate-Z.gif";
                    CreateMatlabAnimateStartFile(outputFolder + "CreateAnimation.m", "x.txt", "y.txt", lenght, animateFile);
                    break;
            }
        }

        /// <summary>
        /// Сохранение координат для построения графика поверхности.
        /// </summary>
        /// <typeparam name="TValue">Тип данных, которые хранятся в виде результатов в сетке анализа.</typeparam>
        /// <param name="mesh">Сетка анализа с данными.</param>
        /// <param name="direction">Направление среза.</param>
        /// <param name="outputFolder">Выходной каталог, куда помещать данные.</param>
        private static void SaveCoordinates<TValue>(ResultsMesh<TValue> mesh, Direction direction, string outputFolder)
        {
            var x = mesh.GetCoordinates(Direction.X);
            var y = mesh.GetCoordinates(Direction.Y);
            var z = mesh.GetCoordinates(Direction.Z);
            switch (direction)
            {
                case Direction.X:
                    SaveArrayToFile(outputFolder + "y.txt", y);
                    SaveArrayToFile(outputFolder + "z.txt", z);
                    break;
                case Direction.Y:
                    SaveArrayToFile(outputFolder + "x.txt", x);
                    SaveArrayToFile(outputFolder + "z.txt", z);
                    break;
                case Direction.Z:
                    SaveArrayToFile(outputFolder + "x.txt", x);
                    SaveArrayToFile(outputFolder + "y.txt", y);
                    break;
            }
        }

        /// <summary>
        /// Экспорт данных для слоя.
        /// </summary>
        /// <typeparam name="TValue">Тип данных, которые хранятся в виде результатов в сетке анализа.</typeparam>
        /// <param name="mesh">Сетка анализа с данными.</param>
        /// <param name="direction">Направление среза.</param>
        /// <param name="layerNumber">Номер слоя, по которому делается срез.</param>
        /// <param name="outputFolder">Выходной каталог, куда помещать данные.</param>
        public static void ExportLayerData<TValue>(ResultsMesh<TValue> mesh, Direction direction, int layerNumber,
            string outputFolder)
        {
            IoHelper.CheckAndCreateDirectory(outputFolder);
            IoHelper.CopyFileToDirectory(@"Matlab/Vizualize2.m", outputFolder);

            var layerData = mesh.GetLayerData(direction, layerNumber);

            SavePlotToFile(outputFolder + "plot.txt", layerData);

            var x = mesh.GetCoordinates(Direction.X);
            var y = mesh.GetCoordinates(Direction.Y);
            var z = mesh.GetCoordinates(Direction.Z);
            switch (direction)
            {
                case Direction.X:
                    SaveArrayToFile(outputFolder + "y.txt", y);
                    SaveArrayToFile(outputFolder + "z.txt", z);
                    CreateMatlabStartFile(outputFolder + "DrawImage.m", "plot.txt", "y.txt", "z.txt",
                        layerData.GetLength(0));
                    break;
                case Direction.Y:
                    SaveArrayToFile(outputFolder + "x.txt", x);
                    SaveArrayToFile(outputFolder + "z.txt", z);
                    CreateMatlabStartFile(outputFolder + "DrawImage.m", "plot.txt", "x.txt", "z.txt",
                        layerData.GetLength(0));
                    break;
                case Direction.Z:
                    SaveArrayToFile(outputFolder + "x.txt", x);
                    SaveArrayToFile(outputFolder + "y.txt", y);
                    CreateMatlabStartFile(outputFolder + "DrawImage.m", "plot.txt", "x.txt", "y.txt",
                        layerData.GetLength(0));
                    break;
            }
        }

        /// <summary>
        /// Сохранение матрицы с результами анализа на срезе.
        /// </summary>
        /// <typeparam name="TResultsValue">Тип данных, в которых хранятся результаты анализа.</typeparam>
        /// <param name="filename">Имя файла, куда сохраняется результат.</param>
        /// <param name="matrix">Сама матрица с данными.</param>
        private static void SavePlotToFile<TResultsValue>(string filename, TResultsValue[,] matrix)
        {
            var file = new StreamWriter(filename);

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    var value = (matrix[i, j] as Field).Amplitude.Real.ToString();
                    file.Write("{0} ",value.CorrectDoubleStringValueForMatlab());
                }

                file.WriteLine();
            }

            file.Close();
        }

        /// <summary>
        /// Сохранение данных в виде вектора (одномерного массива) в файл.
        /// </summary>
        /// <typeparam name="TValue">Тип данных элементов массива.</typeparam>
        /// <param name="filename">Имя файла для сохранения данных.</param>
        /// <param name="array">Массив с данными для записи в файл.</param>
        private static void SaveArrayToFile<TValue>(string filename, TValue[] array)
        {
            var file = new StreamWriter(filename);

            for (int i = 0; i < array.Length; i++)
            {
                file.WriteLine(array[i]);
            }

            file.Close();
        }

        /// <summary>
        /// Создание специального тестового файла для того чтобы визуализировать результат в матлабе для заданной поверхности среза.
        /// </summary>
        /// <param name="filename">/Имя файла для записи данных в него.</param>
        /// <param name="plotfile">Файл с матрицей с данными.</param>
        /// <param name="file1">Файл с кооринатами по одной оси.</param>
        /// <param name="file2">Файл с коорлинатами по второй оси.</param>
        /// <param name="size">Размерность векторов и матрицы.</param>
        private static void CreateMatlabStartFile(string filename, string plotfile, string file1, string file2, int size)
        {
            string data = String.Format("Vizualize2('{0}', '{1}', '{2}', {3})", plotfile, file1, file2, size);

            var file = new StreamWriter(filename);
            file.WriteLine(data);
            file.Close();
        }

        /// <summary>
        /// Создание специального тестового файла для того чтобы визуализировать результат в матлабе для заданной поверхности среза.
        /// </summary>
        /// <param name="filename">/Имя файла для записи данных в него.</param>
        /// <param name="file1">Файл с кооринатами по одной оси.</param>
        /// <param name="file2">Файл с коорлинатами по второй оси.</param>
        /// <param name="size">Размерность векторов и матрицы.</param>
        /// <param name="aniFile">Файл с результатами анимации (gif файл).</param>
        private static void CreateMatlabAnimateStartFile(string filename, string file1, string file2, int size, string aniFile)
        {
            string data = String.Format("VizualizeAnimation('{0}', '{1}', {2}, '{3}')", file1, file2, size, aniFile);

            var file = new StreamWriter(filename);
            file.WriteLine(data);
            file.Close();
        }
    }
}
