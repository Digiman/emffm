﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace EMFFM.Analyzer.Helpers
{
    /// <summary>
    /// Вспомогательный класс для релализации вывода даных в файлы.
    /// </summary>
    public static class OutputHelper
    {
        /// <summary>
        /// Вывод результатов анализа в текстовый файл.
        /// </summary>
        /// <param name="filename">Имя файла для вывода.</param>
        /// <param name="results">Результаты.</param>
        public static void OutputResultsToTextfile<TType>(string filename, SortedDictionary<double, TType> results)
        {
            var file = new StreamWriter(filename);

            foreach (var result in results)
            {
                file.WriteLine("{0}\t{1}", result.Key, result.Value);
            }

            file.Close();
        }

        /// <summary>
        /// Вывод результатов анализа в XML-файл.
        /// </summary>
        /// <param name="filename">Имя файла для вывода.</param>
        /// <param name="results">Результаты (список результатов по направлениям).</param>
        public static void OutputResultsToXmlfile<TType>(string filename, IEnumerable<SortedDictionary<double, TType>> results)
        {
            var file = new XmlTextWriter(filename, Encoding.UTF8);

            file.WriteStartDocument();
            file.WriteStartElement("results"); // <results>

            var dirs = new[] {"x", "y", "z"};

            int i = 0;
            foreach (var result in results)
            {
                file.WriteStartElement("result"); // <result>
                file.WriteStartAttribute("direction");
                file.WriteValue(dirs[i]);
                file.WriteStartAttribute("count");
                file.WriteValue(result.Count);

                foreach (var item in result)
                {
                    file.WriteStartElement("data"); // <data>
                    
                    file.WriteStartAttribute("coord");
                    file.WriteValue(item.Key); // координата
                    file.WriteStartAttribute("value");
                    file.WriteValue(item.Value.ToString()); // значение поля

                    file.WriteEndElement(); // </data>
                }

                file.WriteEndElement(); // </result>

                i++;
            }

            file.WriteEndElement(); // </results>

            file.Close();
        }
    }
}
