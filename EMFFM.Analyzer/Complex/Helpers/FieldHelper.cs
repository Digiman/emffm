﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.Analyzer.Complex.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с комплексным полем
    /// </summary>
    public static class FieldHelper
    {
        #region Главные рабочие методы для вывода данных о поле в файл

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="mesh">Сетка из элементов.</param>
        /// <param name="field">Значения поля</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        /// <param name="withEdgesField">Выводить ли значения тангенциальных компонент на ребрах?</param>
        /// <param name="withCoordanates">Выводить также и координаты точки, в которой рассчитано поле?</param>
        public static void OutputFieldToFile(TMesh<Tetrahedron, Triangle> mesh, List<ElementField> field,
            string fileName, bool withEdgesField = false, bool withCoordanates = false)
        {
            var file = new StreamWriter(fileName);

            file.WriteLine(GenerateHeader(withEdgesField, field[0].EdgeFields.Count, withCoordanates));

            for (int i = 0; i < field.Count; i++)
            {
                var line = new StringBuilder(200);

                line.AppendFormat("{0} \t", i + 1);

                if (withEdgesField)
                {
                    for (int j = 0; j < field[0].EdgeFields.Count; j++)
                    {
                        line.AppendFormat("{0} \t", field[i].EdgeFields[j].TangentialField);
                    }
                }

                line.AppendFormat("{0} \t {1} \t {2} \t {3}", field[i].CenterField.FieldX,
                    field[i].CenterField.FieldY, field[i].CenterField.FieldZ,
                    CalculateAmplitude(field[i].CenterField));

                if (withCoordanates)
                {
                    line.AppendFormat("\t {0} \t {1} \t {2}", mesh.MeshElements[i].GetCenterPoint().X,
                        mesh.MeshElements[i].GetCenterPoint().Y, mesh.MeshElements[i].GetCenterPoint().Z);
                }

                file.WriteLine(line.ToString());
            }

            file.Close();
        }

        /// <summary>
        /// Формирование заголовка для файла с данными о полях.
        /// </summary>
        /// <param name="withEdgesField">Включать данные о полях на ребрах?</param>
        /// <param name="withCoordanates">Включать ли данные о координатах?</param>
        /// <returns>Возвращает сформированный заголовок.</returns>
        private static string GenerateHeader(bool withEdgesField, int edgesCount, bool withCoordanates)
        {
            var header = new StringBuilder(100);

            header.Append("# Element \t");

            if (withEdgesField)
            {
                for (int i = 0; i < edgesCount; i++)
                {
                    header.AppendFormat("EdgeField{0} \t", i + 1);
                }
            }

            header.Append("FieldX \t FieldY \t  FieldZ \t |E|");

            if (withCoordanates)
            {
                header.Append(" X \t Y \t Z");
            }

            return header.ToString();
        }

        #endregion

        #region Другие методы для вывода полей в файлы

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="field">Значения поля</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        public static void OutputFieldToFile(List<EdgeField> field, string fileName)
        {
            var file = new StreamWriter(fileName);

            file.WriteLine("# Edge \t Tangential Filed \t Full Field");

            for (int i = 0; i < field.Count; i++)
            {
                file.WriteLine("{0}\t{1}\t{2}", i + 1, field[i].TangentialField.ToString(),
                    field[i].FullField.ToString());
            }

            file.Close();
        }

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="field">Значения поля</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        /// <param name="withEdgesField">Выводить ли значения тангенциальных компонент на ребрах?</param>
        public static void OutputFieldToFile(List<ElementField> field, string fileName, bool withEdgesField = false)
        {
            var file = new StreamWriter(fileName);

            if (withEdgesField)
            {
                var header = new StringBuilder(100);
                header.Append("# Element \t");
                for (int i = 0; i < field[0].EdgeFields.Count; i++)
                {
                    header.AppendFormat("EdgeField{0} \t", i + 1);
                }
                header.Append(" FieldX \t FieldY \t FieldZ \t Eamp");
                file.WriteLine(header.ToString());
            }
            else
            {
                file.WriteLine("# Element \t  FieldX \t FieldY \t  FieldZ \t Eamp");
            }

            for (int i = 0; i < field.Count; i++)
            {
                if (withEdgesField)
                {
                    var line = new StringBuilder(200);
                    line.AppendFormat("{0} \t", i + 1);
                    for (int j = 0; j < field[0].EdgeFields.Count; j++)
                    {
                        line.AppendFormat("{0} \t", field[i].EdgeFields[j].TangentialField);
                    }
                    line.AppendFormat(" {0} \t {1} \t {2} \t {3}", field[i].CenterField.FieldX,
                        field[i].CenterField.FieldY, field[i].CenterField.FieldZ,
                        CalculateAmplitude(field[i].CenterField));
                    file.WriteLine(line.ToString());
                }
                else
                {
                    file.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", i + 1, field[i].CenterField.FieldX,
                        field[i].CenterField.FieldY, field[i].CenterField.FieldZ,
                        CalculateAmplitude(field[i].CenterField));
                }
            }

            file.Close();
        }

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="field">Значения поля</param>
        /// <param name="elementNumbers">Список номеров элементов для вывода результатов</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        /// <param name="withEdgesField">Выводить ли значения тангенциальных компонент на ребрах?</param>
        public static void OutputFieldToFile(List<ElementField> field, int[] elementNumbers, string fileName,
            bool withEdgesField = false)
        {
            var file = new StreamWriter(fileName);

            if (withEdgesField)
            {
                var header = new StringBuilder(100);
                header.Append("# Element \t");
                for (int i = 0; i < field[0].EdgeFields.Count; i++)
                {
                    header.AppendFormat("EdgeField{0} \t", i + 1);
                }
                header.Append(" FieldX \t FieldY \t FieldZ \t Eamp");
                file.WriteLine(header.ToString());
            }
            else
            {
                file.WriteLine("# Element \t  FieldX \t FieldY \t  FieldZ \t Eamp");
            }

            for (int i = 0; i < elementNumbers.Length; i++)
            {
                int curNum = elementNumbers[i] - 1;
                if (withEdgesField)
                {
                    var line = new StringBuilder(200);
                    line.AppendFormat("{0} \t", elementNumbers[i]);
                    for (int j = 0; j < field[0].EdgeFields.Count; j++)
                    {
                        line.AppendFormat("{0} \t", field[curNum].EdgeFields[j].TangentialField);
                    }
                    line.AppendFormat("{0}\t{1}\t{2}\t{3}", field[curNum].CenterField.FieldX,
                        field[curNum].CenterField.FieldY, field[curNum].CenterField.FieldZ,
                        CalculateAmplitude(field[curNum].CenterField));
                    file.WriteLine(line.ToString());
                }
                else
                {
                    file.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", elementNumbers[i], field[curNum].CenterField.FieldX,
                        field[curNum].CenterField.FieldY, field[curNum].CenterField.FieldZ,
                        CalculateAmplitude(field[curNum].CenterField));
                }
            }

            file.Close();
        }

        #endregion

        #region Вспомогательные методы
        
        /// <summary>
        /// Преобразование вектора в список реберных полей
        /// </summary>
        /// <param name="array">Вектор</param>
        /// <returns>Возвращает список полей на ребрах</returns>
        public static List<EdgeField> ConvertFromArray<TValue>(TValue[] array)
        {
            var results = new List<EdgeField>(array.Length);

            results.AddRange(array.Select(value => new EdgeField()
                                                       {
                                                           TangentialField = (System.Numerics.Complex) (object)value
                                                       }));

            return results;
        }

        /// <summary>
        /// Вычисление аплитуды поля.
        /// </summary>
        /// <param name="field">Поле.</param>
        /// <returns>Возвращает значение амлитуды комплексного поля.</returns>
        private static System.Numerics.Complex CalculateAmplitude(Field field)
        {
            // TODO: пересмотреть использование данных методов - их можно отнести к самому полю, ведь это дл него ведется расчет!

            var sum = CalcConj(field.FieldX) + CalcConj(field.FieldY) + CalcConj(field.FieldZ);
            return System.Numerics.Complex.Sqrt(sum).Real;
        }

        /// <summary>
        /// Перемножение комплексного числа и его сопряженного значения.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <returns>Возвращает результирующее комплектное число.</returns>
        private static System.Numerics.Complex CalcConj(System.Numerics.Complex value)
        {
            return value * System.Numerics.Complex.Conjugate(value);
        }

        /// <summary>
        /// Вычисление разницы между тангенциальными компонентами полей
        /// </summary>
        /// <param name="fields1">Поле 1</param>
        /// <param name="fields2">Поле 2</param>
        /// <returns>Возвращает новое результирующее поле</returns>
        public static List<EdgeField> Difference(List<EdgeField> fields1, List<EdgeField> fields2)
        {
            if (fields1.Count != fields2.Count)
            {
                throw new Exception("Нельзя вычислить разницу между полями с различным числом элементов!");
            }

            var result = new List<EdgeField>(fields1.Count);

            for (int i = 0; i < fields1.Count; i++)
            {
                result.Add(new EdgeField()
                {
                    TangentialField = fields1[i].TangentialField - fields2[i].TangentialField
                });
            }

            return result;
        }

        #endregion

        /// <summary>
        /// Вывод итоговых сведений о полях в файл.
        /// </summary>
        /// <param name="mesh">Сетка из элементов.</param>
        /// <param name="field">Значения поля.</param>
        /// <param name="fileName">Имя файла для вывода результата.</param>
        public static void OutputFieldToFile2(TMesh<Tetrahedron, Triangle> mesh, List<ElementField> field, string fileName)
        {
            var file = new StreamWriter(fileName);

            for (int i = 0; i < field.Count; i++)
            {
                var line = new StringBuilder(200);

                line.AppendFormat("{0} \t", i + 1);

                line.AppendFormat("{0}", CalculateAmplitude(field[i].CenterField));

                line.AppendFormat("\t {0} \t {1} \t {2}", mesh.MeshElements[i].GetCenterPoint().X,
                    mesh.MeshElements[i].GetCenterPoint().Y, mesh.MeshElements[i].GetCenterPoint().Z);

                file.WriteLine(line.ToString());
            }

            file.Close();
        }
    }
}
