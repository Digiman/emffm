﻿using EMFFM.Analyzer.Complex.Helpers;
using EMFFM.Analyzer.Elements;
using EMFFM.Analyzer.Functions;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.Common.Mesh.Elements;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.Analyzer.Complex
{
    /// <summary>
    /// Пока предварительный класс с кодом, который позволяет произвести анализ результатов.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, который используется для хранения значений.</typeparam>
    /// <remarks>Данный клас потом заменит текущий и неправильно работающий класс FieldAnalyzer.</remarks>
    public class Worker <TValue>
    {
        /// <summary>
        /// Сетка из тетраэдров.
        /// </summary>
        private readonly TMesh<Tetrahedron, Triangle> _mesh;
        /// <summary>
        /// Результаты решения СЛАУ.
        /// </summary>
        private readonly SolverResult<System.Numerics.Complex> _result;

        /// <summary>
        /// Данные о полях на элементе и его ребрах (полные).
        /// </summary>
        private FieldData _fieldData;

        /// <summary>
        /// Инициализация анализатора поля с помощью специальной прямоугольной сетки.
        /// </summary>
        /// <param name="mesh">Сетка из элементов, которая использовалась для решения задачи.</param>
        /// <param name="result">Результаты решения, полученные решателем.</param>
        public Worker(TMesh<Tetrahedron, Triangle> mesh, SolverResult<System.Numerics.Complex> result)
        {
            _mesh = mesh;
            _result = result;

            InitFields();
        }

        /// <summary>
        /// Инициализация полей (преоюбразование их из массива в объекты, описывающие поля).
        /// </summary>
        private void InitFields()
        {
            _fieldData = new FieldData(FieldHelper.ConvertFromArray(_result.ResultVector));
        }

        /// <summary>
        /// Выполнение анализа результатов с помощью вспомогательной сетки.
        /// </summary>
        /// <param name="resultsMesh">Вспомогательная сетка и по совместительству результаты анализа.</param>
        /// <param name="coFactor">Корректировочное значение.</param>
        public ResultsMesh<TValue> PerformAnalyze(ResultsMesh<TValue> resultsMesh, double coFactor)
        {
            // сначала бегаем по трехмерной матрице - по всем точкам прямоугольной сетки
            for (int i = 0; i < resultsMesh.Data.GetLength(0); i++)
                for (int j = 0; j < resultsMesh.Data.GetLength(1); j++)
                    for (int k = 0; k < resultsMesh.Data.GetLength(2); k++)
                    {
                        // теперь самое накладное - определение вхождения точки в тетраэдр и расчет значения поля
                        foreach (var element in _mesh.MeshElements)
                        {
                            if (element.CheckPointInside(resultsMesh.Data[i, j, k].Point))
                            {
                                // расчет значения поля
                                TValue result = CalculateLocalElement<Tetrahedron,TValue>(element, _fieldData, resultsMesh.Data[i, j, k].Point, coFactor);
                                resultsMesh.Data[i, j, k].Value = result;
                            }
                        }
                    }
            return resultsMesh;
        }

        /// <summary>
        /// Вычисление поля на элементе.
        /// </summary>
        /// <typeparam name="TType">Тип элемента.</typeparam>
        /// <param name="element">Данные элемента.</param>
        /// <param name="field">Данные о локальном поле.</param>
        /// <param name="point">Точка, в которой необходимо определить значение поля.</param>
        /// <param name="coFactor">Корректировочное значение.</param>
        private TValue CalculateLocalElement<TType, TValue>(TType element, FieldData field, Point point, double coFactor)
        {
            var elf = new ElementField();

            // определяем векторные соотношения для точки в центре масс элемента
            //double[,] mat = new TetrahedronFunction(element as Tetrahedron).GetW(point);
            // или
            double[,] mat = FunctionsHelper.CalculateWMatrix(element, _mesh.ElementType, point);

            var vec = new System.Numerics.Complex[6];
            int k = 0;
            foreach (var edge in (element as Element).Edges)
            {
                vec[k] = edge.GetLenght(coFactor) * field.EdgeFields[edge.Number - 1].TangentialField;
                elf.EdgeFields.Add(field.EdgeFields[edge.Number - 1]);
                k++;
            }

            System.Numerics.Complex[] result = MathNetComplexHelper.Multiply(ComplexHelper.Convert(mat), vec);

            elf.CenterField = new Field(result);
            field.ElementFields.Add(elf);

            return (TValue) (object) elf.CenterField;
        }
    }
}
