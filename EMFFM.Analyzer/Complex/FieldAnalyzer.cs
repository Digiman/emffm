﻿using System.Collections.Generic;
using EMFFM.Analyzer.Complex.Helpers;
using EMFFM.Analyzer.Functions;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.Analyzer.Complex
{
    // NOTE: этот класс устарел, со временем будет убран после переработки методов анализа результатов.

    /// <summary>
    /// Класс для выполнения анализа поля после того, как решено СЛАУ.
    /// </summary>
    /// <remarks>Зашито внутри, что сетка только одного типа (тетраэдры и треугольники)!</remarks>
    public class FieldAnalyzer
    {
        private readonly TMesh<Tetrahedron, Triangle> _mesh;
        private readonly SolverResult<System.Numerics.Complex> _result;

        /// <summary>
        /// Данные о полях на элементе и его ребрах (полные).
        /// </summary>
        private FieldData _fieldData;

        /// <summary>
        /// Инициализация анализатора поля в скалярных (вещественных) значениях.
        /// </summary>
        /// <param name="mesh">Сетка из набора элементов.</param>
        /// <param name="result">Результаты, полученные после решения СЛАУ.</param>
        public FieldAnalyzer(TMesh<Tetrahedron, Triangle> mesh, SolverResult<System.Numerics.Complex> result)
        {
            _mesh = mesh;
            _result = result;

            InitFields();
        }

        /// <summary>
        /// Инициализация полей (преоюбразование их из массива в объекты, описывающие поля).
        /// </summary>
        private void InitFields()
        {
            _fieldData = new FieldData(FieldHelper.ConvertFromArray(_result.ResultVector));
        }

        /// <summary>
        /// Выполнение анализа и расчета полей.
        /// </summary>
        /// <param name="coFactor">Корректирующий размерный параметр.</param>
        /// <returns>Возвращает данные о полных полях для элемента.</returns>
        public FieldData PerformAnalyze(double coFactor)
        {
            CalculateFields(coFactor);

            return _fieldData;
        }

        /// <summary>
        /// Вычисление полей.
        /// </summary>
        /// <param name="coFactor">Корректирующий размерный параметр.</param>
        private void CalculateFields(double coFactor)
        {
            switch (_mesh.ElementType) // в зависимости от типа элемента, выполняется анализ поля на нем
            {
                case ElementType.Tetrahedron: // поля для тетраэдра описаны через базисные векторные функции

                    foreach (var element in _mesh.MeshElements) // для каждого элемента ищем его глобальное поле
                    {
                        CalculateLocalElement(element, _fieldData , coFactor);
                    }
                    break;
            }
        }

        /// <summary>
        /// Вычисление поля на элементе.
        /// </summary>
        /// <typeparam name="TType">Тип элемента.</typeparam>
        /// <param name="element">Данные элемента.</param>
        /// <param name="field">Данные о локальном поле.</param>
        /// <param name="coFactor">Корректировочное значение.</param>
        private void CalculateLocalElement<TType>(TType element, FieldData field, double coFactor)
        {
            var elf = new ElementField();

            // определяем векторные соотношения для точки в центре масс элемента
            double[,] mat = FunctionsHelper.CalculateWMatrix(element, _mesh.ElementType);

            var vec = new System.Numerics.Complex[6];
            int k = 0;
            foreach (var edge in (element as Element).Edges)
            {
                vec[k] = edge.GetLenght(coFactor) * field.EdgeFields[edge.Number - 1].TangentialField;
                elf.EdgeFields.Add(field.EdgeFields[edge.Number - 1]);
                k++;
            }

            System.Numerics.Complex[] result = MathNetComplexHelper.Multiply(ComplexHelper.Convert(mat), vec);

            elf.CenterField = new Field(result);
            field.ElementFields.Add(elf);
        }

        /// <summary>
        /// Выполнение сбора значений полей по направлениям и их сортировка.
        /// </summary>
        /// <param name="direction">Направление, в котором собираются значения полей.</param>
        /// <returns>Возвращает отсортированный по ключу (координате) словарь с данными о полях внутри элементов.</returns>
        /// <remarks>Сборка полей по направлениям из значений поля в центре элемента!</remarks>
        public SortedDictionary<double, System.Numerics.Complex> GetSortedFields(FieldDirection direction)
        {
            // NOTE: при сборе полей по координатам, для OZ направления только есть совпадающие ключи (координаты). Почему так - неизвестно.

            var result = new SortedDictionary<double, System.Numerics.Complex>();

            int i = 0; // номер элемента сетки (размерность совпадает с количеством полей на этих элементах)
            foreach (var elementField in _fieldData.ElementFields)
            {
                switch (direction)
                {
                    case FieldDirection.X:
                        result.Add(_mesh.MeshElements[i].GetCenterPoint().X, elementField.CenterField.FieldX);
                        break;
                    case FieldDirection.Y:
                        result.Add(_mesh.MeshElements[i].GetCenterPoint().Y, elementField.CenterField.FieldY);
                        break;
                    case FieldDirection.Z:
                        if (!result.ContainsKey(_mesh.MeshElements[i].GetCenterPoint().Z))
                        {
                            result.Add(_mesh.MeshElements[i].GetCenterPoint().Z, elementField.CenterField.FieldZ);
                        }
                        break;
                }
                i++;
            }

            return result;
        }
    }
}
