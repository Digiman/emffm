﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using EMFFM.Input.Elements;

namespace EMFFM.Input.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы и конвертации входных данных
    /// </summary>
    public static class InputHelper
    {
        /// <summary>
        /// Получение комплексного значения из списка значений во входном файле
        /// </summary>
        /// <param name="name">Имя значения</param>
        /// <param name="complexValues">Список комплексных значений из файла</param>
        /// <returns>Возвращает комплексное число</returns>
        public static Complex GetComplexValue(string name, IEnumerable<ComplexValue> complexValues)
        {
            var singleOrDefault = complexValues.SingleOrDefault(val => val.Name == name);
            return singleOrDefault != null ? singleOrDefault.Convert() : Complex.Zero;
        }
    }
}
