﻿using System.Collections.Generic;
using EMFFM.Common.Input.Elements;
using EMFFM.Input.Elements;

namespace EMFFM.Input
{
    /// <summary>
    /// Класс для описания входных данных, читаемых из файла.
    /// </summary>
    public class InputData
    {
        /// <summary>
        /// Описание задачи (общее и ее параметры).
        /// </summary>
        public Task Task { get; set; }

        /// <summary>
        /// Описание входной геометрии.
        /// </summary>
        public Geometry Geometry { get; set; }

        /// <summary>
        /// Список комплексных чисел.
        /// </summary>
        public List<ComplexValue> ComplexValues { get; set; }

        /// <summary>
        /// Опции и параметры решателя.
        /// </summary>
        public Options Options { get; set; }

        /// <summary>
        /// Список действий.
        /// </summary>
        public List<TaskAction> Actions { get; set; }

        /// <summary>
        /// Описание выходных файлов и даннных.
        /// </summary>
        public Output Output { get; set; }

        /// <summary>
        /// Список материалов.
        /// </summary>
        public List<MaterialBase> Materials { get; set; }

        /// <summary>
        /// Граничные условия.
        /// </summary>
        public List<Boundary> Boundaries { get; set; }

        /// <summary>
        /// Источники излучения.
        /// </summary>
        public List<Source> Sources { get; set; }

        /// <summary>
        /// Область для анализа результатов.
        /// </summary>
        public AnalyzeBox AnalyzeBox { get; set; }

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public InputData()
        {
        	// инициализация массивов
            Actions = new List<TaskAction>();
            Materials = new List<MaterialBase>();
            Sources = new List<Source>();
            ComplexValues = new List<ComplexValue>();

            // инициализация объектов (классов)
            Geometry = new Geometry();
            Task = new Task();
            AnalyzeBox = new AnalyzeBox();
        }
    }
}
