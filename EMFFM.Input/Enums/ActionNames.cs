﻿namespace EMFFM.Input.Enums
{
    // TODO: определить набор названий для действий, которые возможны в программе

    /// <summary>
    /// Название предопределенных действий
    /// </summary>
    public enum ActionNames
    {
        GenerateMesh,
        BuiltGlobalMatrix,
        Solve
    }
}
