﻿namespace EMFFM.Input.Enums
{
    // TODO: внести изменения в типы задач (определить новые и расширенные для новой программы)
    
    /// <summary>
    /// Типы решаемых задач в ПО
    /// </summary>
    public enum TaskClasses
    {
        Common,
        Waveguide,
        Plate,
        Netgen
    }
}
