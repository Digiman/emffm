﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Виды методов решения задачи с использованием МКЭ.
    /// </summary>
    public enum FemType
    {
        /// <summary>
        /// Векторный МКЭ.
        /// </summary>
        Vector,
        /// <summary>
        /// Узловой МКЭ.
        /// </summary>
        Node
    }
}
