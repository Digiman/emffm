﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Типы, используемые при описании распределений для частиц
    /// </summary>
    public enum DistributionTypes
    {
        Random,
        MersenneTwister
    }
}
