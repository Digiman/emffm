﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Типы граничных условий
    /// </summary>
    public enum BoundaryTypes
    {
        Dirichlet, // Дерихле
        Neumann, // Нейман
        Mixed // смешанные
    }
}
