﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Тип пробы для анализа решения
    /// </summary>
    public enum ProbeType
    {
        Domain, Point
    }
}
