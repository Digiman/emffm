﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Типы материалов частицы (плазмона)
    /// </summary>
    /// <remarks>Предопределенные типы с известными параметрами</remarks>
    public enum PlasmonMaterials
    {
        Ag,
        Au,
        Cu
    }
}
