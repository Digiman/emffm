﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Типы файлов с даными (выходных)
    /// </summary>
    public enum DatafileType
    {
        /// <summary>
        /// Файл для хранения сетки, построенной для рассматриваемой области решения.
        /// </summary>
        Mesh,
        /// <summary>
        /// Файл, для хранения вектора с результатами решения СЛАУ.
        /// </summary>
        Result,
        /// <summary>
        /// Файл для хранения матрицы (глобальной).
        /// </summary>
        Matrix,
        /// <summary>
        /// Файл, для хранения вектора воздействий.
        /// </summary>
        Vector
    }
}
