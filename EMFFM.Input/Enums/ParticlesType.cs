﻿namespace EMFFM.Input.Enums
{
    /// <summary>
    /// Типы задания частиц
    /// </summary>
    public enum ParticlesType
    {
        /// <summary>
        /// по закону распределения
        /// </summary>
        Law,
        /// <summary>
        /// одна частица - один КЭ
        /// </summary>
        Single,
        /// <summary>
        /// много частиц (или одна) в нескольких КЭ
        /// </summary>
        Many
    }
}
