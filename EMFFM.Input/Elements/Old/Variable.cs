namespace EMFFM.Input.Elements
{
    /// <summary>
    /// ���������� � ���������.
    /// </summary>
    public class Variable
    {
        /// <summary>
        /// ����� �������.
        /// </summary>
        public double Time { get; set; }
        
        /// <summary>
        /// �������� �� �������.
        /// </summary>
        public double TimeDelta { get; set; }

        /// <summary>
        /// �������� �� �������.
        /// </summary>
        public double FrequencyDelta { get; set; }
        
        /// <summary>
        /// ��� �� �������.
        /// </summary>
        public double FrequencyStep { get; set; }
    }
}