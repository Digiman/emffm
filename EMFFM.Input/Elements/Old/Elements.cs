﻿using EMFFM.Input.Enums;

namespace EMFFM.Input.Elements
{
    // TODO: убрать описанные здесь классы и работу с ними полностью из программы!

    public class Particle
    {
        public ParticlesType Type;

        // название метериала
        public string Material;
    }

    public class ParticleD : Particle
    {
        public ParticlesDistribution DistName;
        public DistributionTypes DistType;
        public int Count; // количество частиц
        public int LowerBound;
        public int TopBound;
    }

    public class ParticleS : Particle
    {
        public int Number;
    }

    public class ParticleM : Particle
    {
        public int[] Numbers;
    }
}
