using EMFFM.Common.Enums;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input.Enums;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// ������� ����� ��� ������ �����������.
    /// </summary>
    public class ProbePoint
    {
        public ProbeType Type { get; set; }

        public Point Point { get; set; }

        public FieldDirection Direction { get; set; }

        public int DomainNumber { get; set; }
    }
}