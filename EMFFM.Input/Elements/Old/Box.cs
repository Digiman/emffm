using System.Collections.Generic;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// �������� ������������� ������� (������� ������)
    /// </summary>
    public class Box
    {
        /// <summary>
        /// ��������� �����
        /// </summary>
        public Point P1 { get; set; }
        /// <summary>
        /// �������� �����
        /// </summary>
        public Point P2 { get; set; }

        /// <summary>
        /// ������ � ��������� ������ �������
        /// </summary>
        List<BoxBoundary> Boundaries { get; set; }

        /// <summary>
        /// ������� � ��� ���������, ������������ ��� ��������� �������
        /// </summary>
        public MeshElement Element { get; set; }

        /// <summary>
        /// ������������� "�������" �������
        /// </summary>
        public Box()
        {
            Boundaries = new List<BoxBoundary>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        public Box(Point p1, Point p2)
        {
            P1 = p1;
            P2 = p2;
            InitBoundaries();
        }

        /// <summary>
        /// ������������� ������ �� 6 ������ (6 ���������� ���������������)
        /// </summary>
        private void InitBoundaries()
        {
            Boundaries = new List<BoxBoundary>(6);
            for (int i = 0; i < 6; i++)
            {
                Boundaries[i] = new BoxBoundary();
            }
        }
    }

    /// <summary>
    /// ������� ������� � �� ���������
    /// </summary>
    internal class BoxBoundary
    {
        /// <summary>
        /// ����� �������
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// �������� �������
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ������������� ������� � ���������� ����������� ��� ������� �������
        /// </summary>
        public BoxBoundary()
        {
            Number = 0;
            Name = "pl0";
        }

        /// <summary>
        /// ������������� ������� � ��������� �����������
        /// </summary>
        /// <param name="number">�����</param>
        /// <param name="name">��������</param>
        public BoxBoundary(int number, string name)
        {
            Number = number;
            Name = name;
        }
    }
}