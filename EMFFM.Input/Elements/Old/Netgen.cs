﻿namespace EMFFM.Input.Elements
{
    /// <summary>
    /// Настройки генератора сеток Netgen
    /// </summary>
    public class Netgen
    {
        /// <summary>
        /// Читать параметры из файла.
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Файл с параметрами.
        /// </summary>
        public string ParamsFile { get; set; }

        /// <summary>
        /// Каталог для выходных файлов.
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        /// Путь к приложению Netgen.
        /// </summary>
        public string NetgenPath { get; set; }

        /// <summary>
        /// Префикс для генерируемых файлов сетки.
        /// </summary>
        public string MeshFilePrefix { get; set; }

        /// <summary>
        /// Название исполняемого файла приложения Netgen.
        /// </summary>
        public string AppName { get; set; }

        /// <summary>
        /// Конструктор по умолчанию (с базовыми параметрами)
        /// </summary>
        public Netgen()
        {
            AppName = "netgen.exe";
        }
    }
}
