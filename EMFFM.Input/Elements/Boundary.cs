using EMFFM.Input.Enums;

namespace EMFFM.Input.Elements
{
    // TODO: ������������ ����� ��� �������� ��������� ������� �� ������� ����� � �������!!!

    /// <summary>
    /// �������� ���������� ������� �� �������.
    /// </summary>
    public class Boundary
    {
        /// <summary>
        /// ��� ���������� �������.
        /// </summary>
        public BoundaryTypes Type { get; set; }
        
        /// <summary>
        /// ����� �������.
        /// </summary>
        public int BoundaryNumber { get; set; }
        
        /// <summary>
        /// �������� �� �������.
        /// </summary>
        /// <remarks>������������ ��� ������� ��������� ������� �� �������.</remarks>
        public double Value { get; set; }

        /// <summary>
        /// ����� �������.
        /// </summary>
        public int DomainNumber { get; set; }
    }
}