namespace EMFFM.Input.Elements
{
    /// <summary>
    /// ������� � ��������������� ������.
    /// </summary>
    public class Domain
    {
        /// <summary>
        /// ��� �������.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// �������� �������.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// �������� ���������.
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// ������������� "������" �������.
        /// </summary>
        public Domain()
        {
        }

        /// <summary>
        /// ������������� ������� � �����������.
        /// </summary>
        /// <param name="code">��� �������.</param>
        /// <param name="name">��������.</param>
        /// <param name="material">�������� ���������.</param>
        public Domain(int code, string name, string material)
        {
            Code = code;
            Name = name;
            MaterialName = material;
        }
    }
}