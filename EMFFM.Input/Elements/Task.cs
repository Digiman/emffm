﻿using EMFFM.Common.Enums;
using EMFFM.Input.Enums;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// Описание задачи для выполнения.
    /// </summary>
    public class Task
    {
        /// <summary>
        /// Название задачи.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Типы значений, в которых ведется решение.
        /// </summary>
        public ValueTypes ValueType { get; set; }
        
        /// <summary>
        /// Тип решателя.
        /// </summary>
        public SolverTypes SolverType { get; set; }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        /// <returns>Пока не используется по назначению!</returns>
        public TaskClasses Class { get; set; }

        /// <summary>
        /// Вид используемого метода конечных элементов (МКЭ).
        /// </summary>
        public FemType FemType { get; set; } 
    }
}
