﻿using System.Numerics;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// Комплексное число из входного файла.
    /// </summary>
    public class ComplexValue
    {
        /// <summary>
        /// Название переменной.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Реальная (вещественная) часть.
        /// </summary>
        public double Real { get; set; }

        /// <summary>
        /// Мнимая часть.
        /// </summary>
        public double Img { get; set; }

        /// <summary>
        /// Преобразование к комплексному числу, используемому при расчетах.
        /// </summary>
        /// <returns>Возвращает преобразованный объект.</returns>
        public Complex Convert()
        {
            return new Complex(Real, Img);
        }
    }
}
