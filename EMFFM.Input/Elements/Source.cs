using EMFFM.Common.Enums;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// �������� ��������� ����������������� ���������.
    /// </summary>
    /// <remarks>� ����� - �������� ��������� ����� ������� �� ����� ������� � ����� ������� ���������� ��� ����� ������.</remarks>
    public class Source
    {
        /// <summary>
        /// ��� ���������.
        /// </summary>
        /// <remarks>���� �� ���� ���� �����!</remarks>
        public SourceTypes Type { get; set; }

        /// <summary>
        /// ������� ������ ��������� (��).
        /// </summary>
        public double Frequency { get; set; }
        
        /// <summary>
        /// ����� ����� ��������� (��).
        /// </summary>
        public double Lambda { get; set; }
        
        /// <summary>
        /// ���������.
        /// </summary>
        public double Amplitude { get; set; }
        
        /// <summary>
        /// ���� (��� aplpha - ����).
        /// </summary>
        public double Phase { get; set; }

        /// <summary>
        /// ����������� ������ ���������.
        /// </summary>
        /// <remarks>����������� ���������� �������</remarks>
        public Direction Direction { get; set; }

        /// <summary>
        /// ���������� ��������� ���������.
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// ����� �������, �� ������� ���������� ��������.
        /// </summary>
        public int Boundary { get; set; }

        /// <summary>
        /// ����� �������, � ������� �������� ���������� ����������� ����.
        /// </summary>
        public int Domain { get; set; }
    }
}