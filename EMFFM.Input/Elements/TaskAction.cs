using System;
using EMFFM.Input.Enums;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// ��������, ������ ����������� � ������ ��� �� �������.
    /// </summary>
    public class TaskAction : IComparable<TaskAction>
    {
        /// <summary>
        /// ����� ��������.
        /// </summary>
        public int Code { get; set; }
        
        /// <summary>
        /// �������� �������� (��������).
        /// </summary>
        public ActionNames Value { get; set; }

        public int CompareTo(TaskAction other)
        {
            if (this.Code < other.Code)
                return -1;
            else if (this.Code == other.Code)
                return 0;
            else
                return 1;
        }

        public override string ToString()
        {
            return String.Format("{0}-{1}", Code, Value);
        }
    }
}