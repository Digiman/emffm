﻿using System;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// Блок для анализа (размер прямоугольной области для алаиза результатов).
    /// </summary>
    public class AnalyzeBox
    {
        /// <summary>
        /// Начальная точка.
        /// </summary>
        public Point P1 { get; set; }
        /// <summary>
        /// Конечная точка.
        /// </summary>
        public Point P2 { get; set; }

        /// <summary>
        /// Элемент и его параметры, используемый при разбиении области.
        /// </summary>
        public MeshElement Element { get; set; }

        /// <summary>
        /// Получение размера области в виде строки.
        /// </summary>
        /// <returns>Возвращает отформатированную строку с указанием размеров области анализа.</returns>
        public string GetSize()
        {
            var sizex = P2.X - P1.X;
            var sizey = P2.Y - P1.Y;
            var sizez = P2.Z - P1.Z;
            return String.Format("{0} x {1} x {2}", sizex, sizey, sizez);
        }
    }
}
