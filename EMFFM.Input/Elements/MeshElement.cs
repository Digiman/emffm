using System;
using EMFFM.Common.Enums;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// �������� �������� �����
    /// </summary>
    /// <remarks>������������ ��� ����������� ���������� ����� (�������, ���������� �����)</remarks>
    public class MeshElement
    {
        /// <summary>
        /// ������ �������� �� ��� OX
        /// </summary>
        public double SizeX { get; set; }

        /// <summary>
        /// ������ �������� �� ��� OY
        /// </summary>
        public double SizeY { get; set; }

        /// <summary>
        /// ������ �������� �� ��� OZ
        /// </summary>
        public double SizeZ { get; set; }

        /// <summary>
        /// ��� �������� �����
        /// </summary>
        public ElementType Type { get; set; }

        /// <summary>
        /// ��������� ������� �������� � ���� ������.
        /// </summary>
        /// <returns>���������� ����������������� ������ � ��������� �������� ������� �������.</returns>
        public override string ToString()
        {
            return String.Format("{0} x {1} x {2}", SizeX, SizeY, SizeZ);
        }
    }
}