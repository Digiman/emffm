using EMFFM.Common.Input.Elements;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// �������� �� ���������� ���������� ����������
    /// </summary>
    public class Material : MaterialBase
    {
        /// <summary>
        /// ������������� ����������.
        /// </summary>
        public double Eps { get; set; }

        /// <summary>
        /// ��������� ����������.
        /// </summary>
        public double Mu { get; set; }

        /// <summary>
        /// ������������.
        /// </summary>
        public double Sigma { get; set; }
    }
}