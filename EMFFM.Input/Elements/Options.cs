namespace EMFFM.Input.Elements
{
    /// <summary>
    /// ����� � ��������� ������.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// ������� �� ����� ��� ���������?
        /// </summary>
        /// <remarks>��� ���������� ��� �����������.</remarks>
        public bool IsBuiltEdges { get; set; }

        /// <summary>
        /// �������� ��������������� ����� � ����?
        /// </summary>
        public bool IsOutputMeshToFile { get; set; }

        /// <summary>
        /// �������� �� ���������� �������, ���������� ��� ������� � ����?
        /// </summary>
        public bool IsOutputGlobalMatrix { get; set; }

        /// <summary>
        /// �������� �� ������ ����������� � ����?
        /// </summary>
        public bool IsOutputForceVector { get; set; }

        /// <summary>
        /// �������� �� ������ � ����������� ������� ���� � ���� ���������?
        /// </summary>
        public bool IsOutputResultVector { get; set; }
    }
}