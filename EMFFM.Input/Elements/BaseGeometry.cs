﻿using System.Collections.Generic;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// Описание базовой геометрии (для встроенного генератора сеток)
    /// </summary>
    public class BaseGeometry
    {
        /// <summary>
        /// список кирпичиков в основной геометрии
        /// </summary>
        public List<Box> Boxes { get; set; }
 
        /// <summary>
        /// Инициализация "пустой" геометрии
        /// </summary>
        public BaseGeometry()
        {
            Boxes = new List<Box>();
        }
    }
}
