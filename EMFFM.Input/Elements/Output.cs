using System.Collections.Generic;
using EMFFM.Input.Enums;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// �������� �������� ������ � ��� ������ ������ � ����� �����������.
    /// </summary>
    public class Output
    {
        /// <summary>
        /// ���� ��� ��������� ��������.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// ������ ������ � �������.
        /// </summary>
        public Dictionary<DatafileType, string> Files { get; set; }

        /// <summary>
        /// ������� ������.
        /// </summary>
        public string FilePrefix { get; set; }

        /// <summary>
        /// ��������� �������������� ���� � ������ path.
        /// </summary>
        /// <param name="fname">��� �����.</param>
        /// <returns>���������� ������ ������������� ���� ��� �����.</returns>
        public string GetFullPath(string fname)
        {
            return Path + fname;
        }

        /// <summary>
        /// ��������� �������������� ���� � ������ path.
        /// </summary>
        /// <param name="type">��� �����.</param>
        /// <returns>���������� ������ ������������� ���� ��� �����.</returns>
        public string GetFullPath(DatafileType type)
        {
            return Path + Files[type];
        }

        /// <summary>
        /// ����������� �� ���������.
        /// </summary>
        public Output()
        {
            Files = new Dictionary<DatafileType, string>();
        }
    }
}