using System.Collections.Generic;

namespace EMFFM.Input.Elements
{
    /// <summary>
    /// �������� ��������� ��� ������������ �������.
    /// </summary>
    public class Geometry
    {
        /// <summary>
        /// ���� � ����������.
        /// </summary>
        public string GometryFile { get; set; }

        /// <summary>
        /// ������ �������� (�������� � ��������� ���������).
        /// </summary>
        public List<Domain> Domains { get; set; }

        /// <summary>
        /// �������������� ������ ��� ����������� � ���������.
        /// </summary>
        public double CoFactor { get; set; }

        /// <summary>
        /// ������������� "�������" ������� � ����������.
        /// </summary>
        public Geometry()
        {
            Domains = new List<Domain>();
        }
    }
}