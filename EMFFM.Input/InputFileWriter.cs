﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Xml;
using EMFFM.Common.Input.Elements;
using EMFFM.Input.Elements;

namespace EMFFM.Input
{
    /// <summary>
    /// Класс для создания входного файла в формате XML.
    /// </summary>
    public static class InputFileWriter
    {
        /// <summary>
        /// Создание файла с входными данными.
        /// </summary>
        /// <param name="data">Данные для входного файла.</param>
        /// <param name="filename">Имя файла для вывода результата.</param>
        /// <seealso cref="https://www.mindmeister.com/316528827"/>
        public static void SaveInputData(InputData data, string filename)
        {
            var writer = new XmlTextWriter(filename, Encoding.UTF8);

            // запись начала документа
            writer.WriteStartDocument();
            writer.WriteStartElement("inputdata"); // <inputdata>

            // запись данных по секциям

            WriteTaskSection(writer, data.Task);

            WriteGeometrySection(writer, data.Geometry);

            WriteComplexValueSection(writer, data.ComplexValues);

            WriteSourcesSection(writer, data.Sources);

            WriteBoundarySection(writer, data.Boundaries);

            WriteMaterialsSection(writer, data.Materials, data.ComplexValues);
            
            WriteOptionsSection(writer, data.Options);
            
            WriteActionsSection(writer, data.Actions);
            
            WriteOutputSection(writer, data.Output);

            WriteAlalyzeBoxSection(writer, data.AnalyzeBox);

            // запись конца документа
            writer.WriteEndElement(); // </inputdata>

            writer.Close();
        }

        #region Методы для записи данных в секции

        /// <summary>
        /// Запись сведений о задаче, главной секции.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="task">Сведения о задаче для записи в файл.</param>
        private static void WriteTaskSection(XmlTextWriter writer, Task task)
        {
            writer.WriteComment("описание класса задачи ");
            writer.WriteStartElement("task"); // <task>

            writer.WriteStartAttribute("name");
            writer.WriteValue(task.Name);
            writer.WriteStartAttribute("values");
            writer.WriteValue(task.ValueType);
            writer.WriteStartAttribute("solver");
            writer.WriteValue(task.SolverType);
            writer.WriteStartAttribute("class");
            writer.WriteValue(task.Class);
            writer.WriteStartAttribute("mesh");
            writer.WriteValue("Netgen");
            writer.WriteStartAttribute("femtype");
            writer.WriteValue(task.FemType);

            writer.WriteEndElement(); // </task>
        }

        /// <summary>
        /// Запись секции со сведеними о геометрии задачи, как пояснение геометрии для генератора сеток.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="geometry">Сведения о геометрии.</param>
        private static void WriteGeometrySection(XmlTextWriter writer, Geometry geometry)
        {
            writer.WriteComment("описание геометрии задачи (для работы с Netgen)");
            writer.WriteStartElement("geometry"); // <geometry>

            writer.WriteStartElement("geofile"); // <geofile>
            writer.WriteStartAttribute("name");
            writer.WriteValue(geometry.GometryFile);
            writer.WriteEndElement(); // </geofile>

            foreach (var domain in geometry.Domains)
            {
                writer.WriteStartElement("domain"); // <domain>
                writer.WriteStartAttribute("code");
                writer.WriteValue(domain.Code);
                writer.WriteStartAttribute("name");
                writer.WriteValue(domain.Name);
                writer.WriteStartAttribute("material");
                writer.WriteValue(domain.MaterialName);
                writer.WriteEndElement(); // </domain>
            }

            writer.WriteStartElement("cofactor"); // <cofactor>
            writer.WriteStartAttribute("value");
            writer.WriteValue(geometry.CoFactor);
            writer.WriteEndElement(); // </cofactor>


            writer.WriteEndElement(); // </geometry>
        }

        /// <summary>
        /// Запись секции со сведениями о комплексных значениях.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="complexValues">Список комплексных значений.</param>
        private static void WriteComplexValueSection(XmlTextWriter writer, List<ComplexValue> complexValues)
        {
            writer.WriteComment("описание комплексных чисел");
            writer.WriteStartElement("complexvalues"); // <complexvalues>

            foreach (var complexValue in complexValues)
            {
                writer.WriteStartElement("value"); // <value>
                writer.WriteStartAttribute("name");
                writer.WriteValue(complexValue.Name);
                writer.WriteStartAttribute("real");
                writer.WriteValue(complexValue.Real);
                writer.WriteStartAttribute("img");
                writer.WriteValue(complexValue.Img);
                writer.WriteEndElement(); // </value>
            }

            writer.WriteEndElement(); // </complexvalues>
        }

        /// <summary>
        /// Запись секции со сведениями об источниках в рамках задачи и их параметров.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="sources">Список источников и их параметров.</param>
        private static void WriteSourcesSection(XmlTextWriter writer, List<Source> sources)
        {
            writer.WriteComment("описание источников");
            writer.WriteStartElement("sources"); // <sources>

            foreach (var source in sources)
            {
                writer.WriteStartElement("source"); // <source>
                writer.WriteStartAttribute("lambda");
                writer.WriteValue(source.Lambda);
                writer.WriteStartAttribute("amplitude");
                writer.WriteValue(source.Amplitude);
                writer.WriteStartAttribute("phase");
                writer.WriteValue(source.Phase);
                writer.WriteStartAttribute("dir");
                writer.WriteValue(source.Direction);
                writer.WriteStartAttribute("boundary");
                writer.WriteValue(source.Boundary);
                writer.WriteStartAttribute("domain");
                writer.WriteValue(source.Domain);

                writer.WriteStartElement("point"); // <point>
                writer.WriteStartAttribute("x");
                writer.WriteValue(source.Point.X);
                writer.WriteStartAttribute("y");
                writer.WriteValue(source.Point.Y);
                writer.WriteStartAttribute("z");
                writer.WriteValue(source.Point.Z);
                writer.WriteEndElement(); // </point>

                writer.WriteEndElement(); // </source>
            }

            writer.WriteEndElement(); // </sources>
        }

        /// <summary>
        /// Запись секции со сведениями о граничных условиях и параметрах границ в задаче.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="boundaries">Сведения о границах областей в задаче.</param>
        private static void WriteBoundarySection(XmlTextWriter writer, List<Boundary> boundaries)
        {
            writer.WriteComment("описание граничных условий");
            writer.WriteStartElement("boundary"); // <boundary>

            foreach (var boundary in boundaries)
            {
                writer.WriteStartElement("condition"); // <condition>
                writer.WriteStartAttribute("type");
                writer.WriteValue(boundary.Type);
                writer.WriteStartAttribute("domain");
                writer.WriteValue(boundary.DomainNumber);
                writer.WriteStartAttribute("value");
                writer.WriteValue(boundary.Value);
                writer.WriteEndElement(); // </condition>
            }

            writer.WriteEndElement(); // </boundary>
        }

        /// <summary>
        /// Запись секции со сведениями о материалах.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="materials">Список материалов.</param>
        /// <param name="complexValues">Список комплексных величин.</param>
        private static void WriteMaterialsSection(XmlTextWriter writer, List<MaterialBase> materials,
            List<ComplexValue> complexValues)
        {
            writer.WriteComment("описание списка материалов");
            writer.WriteStartElement("materials"); // <materials>

            foreach (var material in materials)
            {
                writer.WriteStartElement("material"); // <material>
                writer.WriteStartAttribute("name");
                writer.WriteValue(material.Name);
                writer.WriteStartAttribute("default");
                writer.WriteValue(material.IsDefault);

                if (material is Material)
                {
                    writer.WriteStartAttribute("eps");
                    writer.WriteValue((material as Material).Eps);
                    writer.WriteStartAttribute("mu");
                    writer.WriteValue((material as Material).Mu);
                }
                else if (material is ComplexMaterial)
                {
                    if ((material as ComplexMaterial).Eps.Real != 0 && (material as ComplexMaterial).Eps.Imaginary != 0)
                    {
                        writer.WriteStartAttribute("eps");
                        writer.WriteValue(GetValueName((material as ComplexMaterial).Eps, complexValues));
                    }
                    if ((material as ComplexMaterial).Mu.Real != 0 && (material as ComplexMaterial).Mu.Imaginary != 0)
                    {
                        writer.WriteStartAttribute("mu");
                        writer.WriteValue(GetValueName((material as ComplexMaterial).Mu, complexValues));
                    }
                    writer.WriteStartAttribute("type");
                    writer.WriteValue((material as ComplexMaterial).Type);
                    writer.WriteStartAttribute("model");
                    writer.WriteValue((material as ComplexMaterial).Model);
                }

                writer.WriteEndElement(); // </material>
            }

            writer.WriteEndElement(); // </materials>
        }

        /// <summary>
        /// Получение имени комплексного числа по его значениям.
        /// </summary>
        /// <param name="value">Значение для поиска ему названия.</param>
        /// <param name="complexValues">Список комплексных чисел, которые войдут в файл с задачей и среди которых искать имя.</param>
        /// <returns>Возвращает назвение числа.</returns>
        /// <remarks>Используется для того, чтобы при записи в файл записать название числа, а не его значение.</remarks>
        private static string GetValueName(Complex value, List<ComplexValue> complexValues)
        {
            return complexValues.FirstOrDefault(c => c.Real == value.Real && c.Img == value.Imaginary).Name;
        }

        /// <summary>
        /// Запись секции со сведениями об опциях и параметрах задачи.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="options">Опции и параметры задачи.</param>
        private static void WriteOptionsSection(XmlTextWriter writer, Options options)
        {
            writer.WriteComment("описание опций и параметров решения");
            writer.WriteStartElement("options"); // <options>

            WriteParamElement(writer, "IsOutputMeshToFile", options.IsOutputMeshToFile.ToString());
            WriteParamElement(writer, "IsBuiltEdges", options.IsBuiltEdges.ToString());

            /*WriteParamElement(writer, "IsOutputToFile", options.IsOutputToFile.ToString());
            WriteParamElement(writer, "WithEdgeFields", options.WithEdgeFields.ToString());
            WriteParamElement(writer, "WithoutIterations", options.WithoutIterations.ToString());
            WriteParamElement(writer, "IsProbeOutput", options.IsProbeOutput.ToString());*/

            writer.WriteEndElement(); // </options>
        }

        /// <summary>
        /// Запись блока XML файла с данными об опциях.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="paramName">Название параметра.</param>
        /// <param name="paramValue">Значение параметра.</param>
        private static void WriteParamElement(XmlTextWriter writer, string paramName, string paramValue)
        {
            writer.WriteStartElement("param"); // <param>
            writer.WriteStartAttribute("name");
            writer.WriteValue(paramName);
            writer.WriteStartAttribute("value");
            writer.WriteValue(paramValue);
            writer.WriteEndElement(); // </param>
        }

        /// <summary>
        /// Запись сведений о действиях.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="actions">Список действий для записи в файл.</param>
        private static void WriteActionsSection(XmlTextWriter writer, List<TaskAction> actions)
        {
            writer.WriteComment("описание действий");
            writer.WriteStartElement("actions"); // <actions>

            foreach (var taskAction in actions)
            {
                writer.WriteStartElement("action"); // <action>
                writer.WriteStartAttribute("code");
                writer.WriteValue(taskAction.Code);
                writer.WriteStartAttribute("value");
                writer.WriteValue(taskAction.Value);
                writer.WriteEndElement(); // </action>
            }

            writer.WriteEndElement(); // </actions>
        }

        /// <summary>
        /// Запись сведений о выходных параметрах задачи.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="output">Данные о выходных параметрах для задачи.</param>
        private static void WriteOutputSection(XmlTextWriter writer, Output output)
        {
            writer.WriteComment("описание выходных файлов");
            writer.WriteStartElement("output"); // <output>

            writer.WriteStartElement("path"); // <path>
            writer.WriteStartAttribute("value");
            writer.WriteValue(output.Path);
            writer.WriteEndElement(); // </path>

            foreach (var file in output.Files)
            {
                writer.WriteStartElement("datafile"); // <datafile>
                writer.WriteStartAttribute("type");
                writer.WriteValue(file.Key);
                writer.WriteStartAttribute("name");
                writer.WriteValue(file.Value);
                writer.WriteEndElement(); // </datafile>
            }

            writer.WriteStartElement("fileprefix"); // <fileprefix>
            writer.WriteStartAttribute("value");
            writer.WriteValue(output.FilePrefix);
            writer.WriteEndElement(); // </fileprefix>

            writer.WriteEndElement(); // </output>
        }

        /// <summary>
        /// Запись секции со сведениями об области анализа.
        /// </summary>
        /// <param name="writer">Объект для реализации записи в XML файл.</param>
        /// <param name="analyzeBox">Сведения обл области анализа.</param>
        private static void WriteAlalyzeBoxSection(XmlTextWriter writer, AnalyzeBox analyzeBox)
        {
            writer.WriteComment("описание размера области для выполнения анализа в ней");
            writer.WriteStartElement("analyzebox"); // <analyzebox>

            writer.WriteComment("точки, определяющие размер и границы коробки");
            writer.WriteStartElement("point"); // <point>
            writer.WriteStartAttribute("name");
            writer.WriteValue("p1");
            writer.WriteStartAttribute("x");
            writer.WriteValue(analyzeBox.P1.X);
            writer.WriteStartAttribute("y");
            writer.WriteValue(analyzeBox.P1.Y);
            writer.WriteStartAttribute("z");
            writer.WriteValue(analyzeBox.P1.Z);
            writer.WriteEndElement(); // </point>

            writer.WriteStartElement("point"); // <point>
            writer.WriteStartAttribute("name");
            writer.WriteValue("p2");
            writer.WriteStartAttribute("x");
            writer.WriteValue(analyzeBox.P2.X);
            writer.WriteStartAttribute("y");
            writer.WriteValue(analyzeBox.P2.Y);
            writer.WriteStartAttribute("z");
            writer.WriteValue(analyzeBox.P2.Z);
            writer.WriteEndElement(); // </point>

            writer.WriteComment("описание элемента, на который разбивается данная подобласть");
            writer.WriteStartElement("element"); // <element>
            writer.WriteStartAttribute("type");
            writer.WriteValue(analyzeBox.Element.Type);
            writer.WriteStartAttribute("sizex");
            writer.WriteValue(analyzeBox.Element.SizeX);
            writer.WriteStartAttribute("sizey");
            writer.WriteValue(analyzeBox.Element.SizeY);
            writer.WriteStartAttribute("sizez");
            writer.WriteValue(analyzeBox.Element.SizeZ);
            writer.WriteEndElement(); // </element>

            writer.WriteEndElement(); // </analyzebox>
        } 

        #endregion
    }
}
