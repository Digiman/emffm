﻿using System;
using System.Linq;
using System.Xml;
using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Extensions;
using EMFFM.Common.Helpers;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using EMFFM.Input.Helpers;

namespace EMFFM.Input
{
    /// <summary>
    /// Класс для работы со входным файлом данных (чтения из него)
    /// </summary>
    public static class InputFileReader
    {
        /// <summary>
        /// Чтение исходного файла с данными для задачи
        /// </summary>
        /// <param name="filename">Файл для чтения (путь и название файла)</param>
        /// <seealso cref="https://www.mindmeister.com/316528827"/>
        public static InputData ReadInputFile(string filename)
        {
            try
            {
                // открываем и загружаем документ в переменную doc
                var doc = new XmlDocument();
                doc.Load(filename);

                // создание объекта с входными данными для возврата
                var data = new InputData();

                // чтение секций входного файла и их разбор
                XmlNodeList list = doc.DocumentElement.ChildNodes;

                // вне зависимости от порядка следования элементов (секциий) разбираем их
                foreach (XmlNode item in list)
                {
                    if (item.NodeType == XmlNodeType.Element)
                    {
                        switch (item.Name)
                        {
                            // обязательные секции
                            case "task": // чтение класса задачи для решения, описываемой в файле
                                data.Task = ReadTaskSection(item);
                                break;
                            case "actions": // чтение секции с действиями
                                data.Actions = ReadActionSection(item.ChildNodes);
                                break;
                            case "output": // чтение секции с определенями выходных файлов
                                data.Output = ReadOutputSection(item.ChildNodes);
                                break;
                            case "options": // чтение секции с опциями и параметрами
                                data.Options = ReadOptionsSection(item.ChildNodes);
                                break;
                            case "boundary": // чтение секции с параметрами граничных условий и их тип
                                data.Boundaries = ReadBoundarySection(item.ChildNodes);
                                break;
                            case "materials": // чтение секции с описанием материалов, используемых в задаче
                                data.Materials = ReadMaterialsSection(item.ChildNodes, data.ComplexValues);
                                break;
                            case "sources": // чтение секции с константами
                                data.Sources = ReadSourcesSection(item.ChildNodes);
                                break;
                            case "geometry": // чтение сведений о геометрии
                                data.Geometry = ReadGeometrySection(item.ChildNodes);
                                break;
                            case "analyzebox": // чтение секции с данными о области анализа
                                data.AnalyzeBox = ReadAnalyzeBoxSection(item.ChildNodes);
                                break;
                            // секции, необходимые при работе с комплексными числами
                            case "complexvalues":
                                data.ComplexValues = ReadComplexValuesSection(item.ChildNodes);
                                break;
                            //-----------------------------------------------------------------------
                            // устаревшие блоки
                            //-----------------------------------------------------------------------
                            // секция если для работы с программой Netgen
                            case "netgen": // чтение класса задачи для решения, описываемой в файле
                                //data.NetgenOptions = ReadNetgenSection(item);
                                break;
                            case "probes":
                                //data.ProbePoints = ReadProbesSection(item.ChildNodes);
                                break;
                            case "variables": // чтение секции с описанием переменных и констант
                                //data.Variables = ReadVariablesSection(item.ChildNodes);
                                break;
                            //-----------------------------------------------------------------------
                        }
                    }
                }

                LogHelper.Log(LogMessageType.Info, "Input file succesfull readed!");

                return data;
            }
            catch (Exception ex)
            {
                LogHelper.Log(LogMessageType.Error, String.Format("Catch error in input file reading process. Error message: {0}", ex.Message));
                throw new Exception("Возникло исключение при обработке входного файла! " + ex.Message);
            }
        }

        #region Чтение и разбор секций
        
        /// <summary>
        /// Чтение класс решаемой задачи, описываемой во входном файле.
        /// </summary>
        /// <param name="xmlNode">Узел XML с данными.</param>
        private static Task ReadTaskSection(XmlNode xmlNode)
        {
            var task = new Task();
            if (xmlNode.Attributes != null)
            {
                task.Name = xmlNode.Attributes["name"].Value;
                task.ValueType = (ValueTypes)Enum.Parse(typeof(ValueTypes), xmlNode.Attributes["values"].Value);
                task.SolverType = (SolverTypes) Enum.Parse(typeof (SolverTypes), xmlNode.Attributes["solver"].Value);
                task.Class = (TaskClasses)Enum.Parse(typeof(TaskClasses), xmlNode.Attributes["class"].Value);
                if (xmlNode.Attributes["femtype"] != null)
                    task.FemType = (FemType)Enum.Parse(typeof(FemType), xmlNode.Attributes["femtype"].Value);
            }

            return task;
        }

        /// <summary>
        /// Чтение секции со списком действий.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        private static List<TaskAction> ReadActionSection(XmlNodeList xmlNodeList)
        {
            var actions = new List<TaskAction>();

            // считываем все доступные действия
            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "action":
                        actions.Add(new Elements.TaskAction()
                        {
                            Code = Convert.ToInt32(node.Attributes["code"].Value),
                            Value = (ActionNames)Enum.Parse(typeof(ActionNames), node.Attributes["value"].Value)
                        });
                        break;
                }
            }

            // сортировка действий по коду
            actions.Sort();

            // TODO: добавить обработку списка на уникальность действий, т.е. действия без повтора

            return actions;
        }

        /// <summary>
        /// Чтение секции с данными о выходных файлах и данных.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        private static Output ReadOutputSection(XmlNodeList xmlNodeList)
        {
            var output = new Output();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "path":
                        output.Path = node.Attributes["value"].Value;
                        break;
                    case "datafile":
                        output.Files.Add(
                            (DatafileType)Enum.Parse(typeof(DatafileType), node.Attributes["type"].Value),
                            node.Attributes["name"].Value);
                        break;
                    case "fileprefix":
                        output.FilePrefix = node.Attributes["value"].Value;
                        break;
                }
            }

            return output;
        }

        /// <summary>
        /// Чтение секции с опциями и параметрами.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        private static Options ReadOptionsSection(XmlNodeList xmlNodeList)
        {
            var options = new Options();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "param":
                        ParseOptionParam(node.Attributes["name"].Value, node.Attributes["value"].Value, ref options);
                        break;
                }
            }

            return options;
        }

        /// <summary>
        /// Чтение секции с переменными и константами.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        /// <returns>Возвращает сведения о переменных, использованнх для описания задачи.</returns>
        private static Variable ReadVariablesSection(XmlNodeList xmlNodeList)
        {
            var variables = new Variable();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "var":
                        ParseVariableParam(node.Attributes["name"].Value, node.Attributes["value"].Value, ref variables);
                        break;
                }
            }

            return variables;
        }

        /// <summary>
        /// Чтение секции с параметрами граничных условий.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        /// <returns>Возвращает список граничных условий, определенных для задачи.</returns>
        private static List<Boundary> ReadBoundarySection(XmlNodeList xmlNodeList)
        {
            var boundaries = new List<Boundary>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "condition":
                        var bound = new Boundary
                                        {
                                            Type =
                                                (BoundaryTypes)
                                                Enum.Parse(typeof (BoundaryTypes), node.Attributes["type"].Value),
                                            Value = Convert.ToDouble(node.Attributes["value"].Value)
                                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            bound.BoundaryNumber = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        if (node.Attributes["domain"] != null)
                        {
                            bound.DomainNumber = Convert.ToInt32(node.Attributes["domain"].Value);
                        }
                        boundaries.Add(bound);
                        break;
                }
            }

            return boundaries;
        }

        /// <summary>
        /// Чтение секции с материалами
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа</param>
        /// <param name="complexValues">Список комлексных чисел, которые используеются для описания параметров материалов.</param>
        /// <returns>Возвращает сведения о материалах, которые использованы в задаче.</returns>
        private static List<MaterialBase> ReadMaterialsSection(XmlNodeList xmlNodeList, List<ComplexValue> complexValues)
        {
            var materials = new List<MaterialBase>();

            // считываем все доступные действия
            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "material":
                        var mat = new Material
                        {
                            Name = node.Attributes["name"].Value,
                            Eps = Convert.ToDouble(node.Attributes["eps"].Value.CorrectDoubleStringValue()),
                            Mu = Convert.ToDouble(node.Attributes["mu"].Value.CorrectDoubleStringValue())
                        };
                        if (node.Attributes["default"] != null)
                        {
                            bool val = Convert.ToBoolean(node.Attributes["default"].Value);
                            mat.IsDefault = val;
                        }
                        materials.Add(mat);
                        break;
                    case "complexmaterial": // читаем параметры материала с комплексными значениями параметров
                        var mat2 = new ComplexMaterial
                        {
                            Name = node.Attributes["name"].Value
                        };
                        if (node.Attributes["eps"] != null)
                        {
                            string val = node.Attributes["eps"].Value;
                            mat2.Eps = InputHelper.GetComplexValue(val, complexValues);
                        }
                        if (node.Attributes["mu"] != null)
                        {
                            string val = node.Attributes["mu"].Value;
                            mat2.Mu = InputHelper.GetComplexValue(val, complexValues);
                        }
                        if (node.Attributes["type"] != null)
                        {
                            string val = node.Attributes["type"].Value;
                            mat2.Type = (MaterialTypes)Enum.Parse(typeof(MaterialTypes), val);
                        }
                        if (node.Attributes["model"] != null)
                        {
                            string val = node.Attributes["model"].Value;
                            mat2.Model = (ModelTypes) Enum.Parse(typeof (ModelTypes), val);
                        }
                        if (node.Attributes["default"] != null)
                        {
                            bool val = Convert.ToBoolean(node.Attributes["default"].Value);
                            mat2.IsDefault = val;
                        }
                        materials.Add(mat2);
                        break;
                }
            }

            return materials;
        }

        /// <summary>
        /// Чтение секции с описанием источников.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        /// <returns>Возвращает список источников, которые описаны в задаче.</returns>
        private static List<Source> ReadSourcesSection(XmlNodeList xmlNodeList)
        {
            var sources = new List<Source>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "source":
                        Source src = new Source()
                        {
                            Type = SourceTypes.Simple,
                            Lambda = Convert.ToDouble(node.Attributes["lambda"].Value.CorrectDoubleStringValue()),
                            Amplitude = Convert.ToDouble(node.Attributes["amplitude"].Value.CorrectDoubleStringValue()),
                            Phase = Convert.ToDouble(node.Attributes["phase"].Value.CorrectDoubleStringValue()),
                            Direction = (Direction)Enum.Parse(typeof(Direction), node.Attributes["dir"].Value)
                        };
                        if (node.Attributes["freq"] != null)
                        {
                            src.Frequency = Convert.ToDouble(node.Attributes["freq"].Value.CorrectDoubleStringValue());
                        }
                        if (node.Attributes["boundary"] != null)
                        {
                            src.Boundary = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        if (node.Attributes["domain"] != null)
                        {
                            src.Domain = Convert.ToInt32(node.Attributes["domain"].Value);
                        }
                        src.Point = ReadSourcePoint(node.ChildNodes);
                        sources.Add(src);
                        break;
                    case "esource":
                        sources.Add(new Source()
                        {
                            Type = SourceTypes.Electric,
                            Frequency = Convert.ToDouble(node.Attributes["freq"].Value)
                        });
                        break;
                    case "msource":
                        sources.Add(new Source()
                        {
                            Type = SourceTypes.Magnetic,
                            Frequency = Convert.ToDouble(node.Attributes["freq"].Value)
                        });
                        break;
                }
            }

            return sources;
        }
  
        /// <summary>
        /// Чтение секции с описанием параметров геометрии для работы с Netgen.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML файла.</param>
        /// <returns>Возвращает сведения о геометрии для текущей задачи.</returns>
        private static Geometry ReadGeometrySection(XmlNodeList xmlNodeList)
        {
            var geometry = new Geometry();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "geofile":
                        geometry.GometryFile = node.Attributes["name"].Value;
                        break;
                    case "domains":
                        geometry.Domains = ReadGeometryDomains(node.ChildNodes);
                        break;
                    case "cofactor":
                        geometry.CoFactor = Convert.ToDouble(node.Attributes["value"].Value.CorrectDoubleStringValue());
                        break;
                }
            }

            return geometry;
        }

        /// <summary>
        /// Чтение секции с набором комплексных чисел.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа для разбора.</param>
        /// <returns>Возвращает список комплексных значений, которые используются для описания задачи.</returns>
        private static List<ComplexValue> ReadComplexValuesSection(XmlNodeList xmlNodeList)
        {
            var values = new List<ComplexValue>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "value":
                        values.Add(new ComplexValue()
                        {
                            Name = node.Attributes["name"].Value,
                            Real = Convert.ToDouble(node.Attributes["real"].Value.CorrectDoubleStringValue()),
                            Img = Convert.ToDouble(node.Attributes["img"].Value.CorrectDoubleStringValue())
                        });
                        break;
                }
            }

            return values;
        }

        /// <summary>
        /// Чтение блока с описанием области для анализа.
        /// </summary>
        /// <param name="xmlNodeList">Список узлов из блока файла.</param>
        /// <returns>Возвращает описание области для построения анализирующей сетки.</returns>
        private static AnalyzeBox ReadAnalyzeBoxSection(XmlNodeList xmlNodeList)
        {
            var box = new AnalyzeBox();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "point":
                        var point = new Point()
                        {
                            X = Convert.ToDouble(node.Attributes["x"].Value.CorrectDoubleStringValue()),
                            Y = Convert.ToDouble(node.Attributes["y"].Value.CorrectDoubleStringValue()),
                            Z = Convert.ToDouble(node.Attributes["z"].Value.CorrectDoubleStringValue())
                        };
                        switch (node.Attributes["name"].Value)
                        {
                            case "p1":
                                box.P1 = point;
                                break;
                            case "p2":
                                box.P2 = point;
                                break;
                        }
                        break;
                    case "element":
                        box.Element = new MeshElement
                        {
                            Type = (ElementType)Enum.Parse(typeof(ElementType), node.Attributes["type"].Value),
                            SizeX = Convert.ToDouble(node.Attributes["sizex"].Value.CorrectDoubleStringValue()),
                            SizeY = Convert.ToDouble(node.Attributes["sizey"].Value.CorrectDoubleStringValue()),
                            SizeZ = Convert.ToDouble(node.Attributes["sizez"].Value.CorrectDoubleStringValue())
                        };
                        break;
                }
            }

            return box;
        }

        #region Устаревший код

        /// <summary>
        /// Чтение секции со списоком пробных точек.
        /// </summary>
        /// <param name="xmlNodeList">Часть XML документа с данными для разбора.</param>
        /// <returns>Возвращает набор пробных точек для рассматриваемой задачи.</returns>
        private static List<ProbePoint> ReadProbesSection(XmlNodeList xmlNodeList)
        {
            // TODO: убрать этот метод со временем!

            var points = new List<ProbePoint>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "probe":
                        points.Add(new ProbePoint()
                        {
                            Type = ProbeType.Point,
                            Point = new Point(Convert.ToDouble(node.Attributes["x"].Value.CorrectDoubleStringValue()),
                                Convert.ToDouble(node.Attributes["y"].Value.CorrectDoubleStringValue()),
                                Convert.ToDouble(node.Attributes["z"].Value.CorrectDoubleStringValue())),
                            Direction =
                                (FieldDirection)Enum.Parse(typeof(FieldDirection), node.Attributes["dir"].Value)
                        });
                        break;
                    case "domain":
                        points.Add(new ProbePoint()
                        {
                            Type = ProbeType.Domain,
                            DomainNumber = Convert.ToInt32(node.Attributes["number"].Value)
                        });
                        break;
                }
            }
            return points;
        }

        /// <summary>
        /// Чтение секции с описанием параметров для программы Netgen
        /// </summary>
        /// <param name="xmlNode">Секция документа с парамерами для разбора</param>
        /// <remarks>Возвращает сведения о параметрах приложения Netgen, считанных из фвйла задачи.</remarks>
        private static Netgen ReadNetgenSection(XmlNode xmlNode)
        {
            // TODO: убрать этот метод со временем!

            var options = new Netgen();

            if (Convert.ToBoolean(xmlNode.Attributes["isfile"].Value))
            {
                // читаем имя файла с параметрами
                var list = xmlNode.ChildNodes;
                var tmp = list.Cast<XmlNode>().Single<XmlNode>(node => node.Name == "file").Attributes["value"].Value;
                options.ParamsFile = tmp;
                options.IsFile = true;
            }
            else
            {
                // читаем данные из входного файла, в случае если они определены
                options.IsFile = false;
                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    switch (node.Name)
                    {
                        case "param":
                            switch (node.Attributes["name"].Value)
                            {
                                case "OutputPath":
                                    options.OutputPath = node.Attributes["value"].Value;
                                    break;
                                case "NetgenPath":
                                    options.NetgenPath = node.Attributes["value"].Value;
                                    break;
                                case "MeshFilePrefix":
                                    options.MeshFilePrefix = node.Attributes["value"].Value;
                                    break;
                                case "AppName":
                                    options.AppName = node.Attributes["value"].Value;
                                    break;
                            }
                            break;
                    }
                }
            }

            return options;
        }

        #endregion

        #endregion

        #region Разбор параметров внутри секций
        
        /// <summary>
        /// Разбор параметра из опций.
        /// </summary>
        /// <param name="paramName">Название параметра.</param>
        /// <param name="paramValue">Значение параметра.</param>
        /// <param name="options">Опции в задаче.</param>
        private static void ParseOptionParam(string paramName, string paramValue, ref Options options)
        {
            switch (paramName)
            {
                case "IsOutputMeshToFile":
                    options.IsOutputMeshToFile = Convert.ToBoolean(paramValue);
                    break;
                case "IsBuiltEdges":
                    options.IsBuiltEdges = Convert.ToBoolean(paramValue);
                    break;
                case "IsOutputGlobalMatrix":
                    options.IsOutputGlobalMatrix = Convert.ToBoolean(paramValue);
                    break;
                case "IsOutputForceVector":
                    options.IsOutputForceVector = Convert.ToBoolean(paramValue);
                    break;
                case "IsOutputResultVector":
                    options.IsOutputResultVector = Convert.ToBoolean(paramValue);
                    break;
                    /*case "IsOutputToFile":
                    options.IsOutputToFile = Convert.ToBoolean(paramValue);
                    break;
                case "IsAppend":
                    options.IsAppend = Convert.ToBoolean(paramValue);
                    break;
                case "WithParticles":
                    options.WithParticles = Convert.ToBoolean(paramValue);
                    break;
                case "WithoutIterations": // решаем без итераций по варьируемой величине
                    options.WithoutIterations = Convert.ToBoolean(paramValue);
                    break;
                case "WithEdgeFields":
                    options.WithEdgeFields = Convert.ToBoolean(paramValue);
                    break;
                case "IsProbeOutput":
                    options.IsProbeOutput = Convert.ToBoolean(paramValue);
                    break;*/
            }
        }

        /// <summary>
        /// Разбор областей геометрии
        /// </summary>
        /// <param name="xmlNodeList">Часть XML файла</param>
        /// <returns>Возвращает список областей геометрии</returns>
        private static List<Domain> ReadGeometryDomains(XmlNodeList xmlNodeList)
        {
            var doms = new List<Domain>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "domain":
                        doms.Add(new Domain()
                                     {
                                         Code = Convert.ToInt32(node.Attributes["code"].Value),
                                         Name = node.Attributes["name"].Value,
                                         MaterialName = node.Attributes["material"].Value
                                     });
                        break;
                }
            }

            return doms;
        }

        #region Устаревший код

        /// <summary>
        /// Разбор параметра из переменной.
        /// </summary>
        /// <param name="paramName">Название переменной (параметра).</param>
        /// <param name="paramValue">Значение переменной (параметра).</param>
        /// <param name="variables">Переменные для разбора.</param>
        private static void ParseVariableParam(string paramName, string paramValue, ref Variable variables)
        {
            // TODO: убрать этот метод при переработке входного файла!

            switch (paramName)
            {
                case "Time":
                    variables.Time = Convert.ToDouble(paramValue.CorrectDoubleStringValue());
                    break;
                case "TimeDelta":
                    variables.TimeDelta = Convert.ToDouble(paramValue.CorrectDoubleStringValue());
                    break;
                case "FrequencyDelta":
                    variables.FrequencyDelta = Convert.ToDouble(paramValue.CorrectDoubleStringValue());
                    break;
                case "FrequencyStep":
                    variables.FrequencyStep = Convert.ToDouble(paramValue.CorrectDoubleStringValue());
                    break;
            }
        }

        /// <summary>
        /// Разбор параметра для распределения частиц
        /// </summary>
        /// <param name="node">Узел XML документа с данными</param>
        private static Particle ParseParticleParam(XmlNode node)
        {
            // TODO: убрать этот метод при переработке входного файла!

            Particle particle = null;
            ParticlesType type = (ParticlesType)Enum.Parse(typeof(ParticlesType), node.Attributes["type"].Value);
            switch (type)
            {
                case ParticlesType.Law:
                    ParticleD par = new ParticleD();
                    ParseCustomParticleParam<ParticleD>(node, ref par);
                    particle = par;
                    break;
                case ParticlesType.Single:
                    ParticleS par2 = new ParticleS();
                    ParseCustomParticleParam<ParticleS>(node, ref par2);
                    particle = par2;
                    break;
                case ParticlesType.Many:
                    ParticleM par3 = new ParticleM();
                    ParseCustomParticleParam<ParticleM>(node, ref par3);
                    particle = par3;
                    break;
            }

            return particle;
        }

        /// <summary>
        /// Разбор аргументов в описании частиц
        /// </summary>
        /// <typeparam name="TType">Тип частиц</typeparam>
        /// <param name="node">Данные узла XML файла</param>
        /// <param name="particle">Данные о частицах</param>
        private static void ParseCustomParticleParam<TType>(XmlNode node, ref TType particle)
        {
            foreach (XmlAttribute attr in node.Attributes)
            {
                switch (attr.Name)
                {
                    // общие параметры
                    case "material":
                        (particle as Particle).Material = attr.Value;
                        break;
                    case "type":
                        (particle as Particle).Type = (ParticlesType)Enum.Parse(typeof(ParticlesType), attr.Value);
                        break;
                    // для распределения частиц
                    case "name":
                        (particle as ParticleD).DistName = (ParticlesDistribution)Enum.Parse(typeof(ParticlesDistribution), attr.Value);
                        break;
                    case "lawtype":
                        (particle as ParticleD).DistType = (DistributionTypes)Enum.Parse(typeof(DistributionTypes), attr.Value);
                        break;
                    case "count":
                        (particle as ParticleD).Count = Convert.ToInt32(attr.Value);
                        break;
                    case "lower":
                        (particle as ParticleD).LowerBound = Convert.ToInt32(attr.Value);
                        break;
                    case "top":
                        (particle as ParticleD).TopBound = Convert.ToInt32(attr.Value);
                        break;
                    // для одной частицы (один КЭ)
                    case "number":
                        (particle as ParticleS).Number = Convert.ToInt32(attr.Value);
                        break;
                }
            }
        }

        #endregion

        #endregion

        #region Вспомогательные функции

        /// <summary>
        /// Чтение блока с координатами источника.
        /// </summary>
        /// <param name="xmlChildNodes">Часть XML документа.</param>
        /// <returns>Возвращает считанную точку Point.</returns>
        private static Point ReadSourcePoint(XmlNodeList xmlChildNodes)
        {
            Point point = new Point(0, 0, 0);

            if (xmlChildNodes[0].Name == "point")
            {
                point.X = Convert.ToDouble(xmlChildNodes[0].Attributes["x"].Value.CorrectDoubleStringValue());
                point.Y = Convert.ToDouble(xmlChildNodes[0].Attributes["y"].Value.CorrectDoubleStringValue());
                point.Z = Convert.ToDouble(xmlChildNodes[0].Attributes["z"].Value.CorrectDoubleStringValue());
            }

            return point;
        }

        #endregion
    }
}
