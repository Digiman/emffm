﻿using EMFFM.Common.Helpers;

namespace EMFFM.Input.Samples
{
    /// <summary>
    /// Класс с примерами использования библиотеки
    /// </summary>
    public class Samples
    {
        /// <summary>
        /// Пример использования Input API для разбора входного файла.
        /// </summary>
        public static void Sample1()
        {
            string fname = "InputFile.xml";
            InputData data = InputFileReader.ReadInputFile(fname);
        }

        /// <summary>
        /// Чтение данных из файла и сохранение в файл.
        /// </summary>
        public static void Sample2()
        {
            string fname = "InputFile.xml";
            var data = InputFileReader.ReadInputFile(fname);

            string fname2 = "InputFile2.xml";
            InputFileWriter.SaveInputData(data, fname2);
        }

        /// <summary>
        /// Работа с XML файлов с задачей с помощью сериализации:
        ///     - чтение данных,
        ///     - сохранение данных.
        /// </summary>
        public static void Sample3()
        {
            string fname = "InputFile.xml";
            var data = InputFileReader.ReadInputFile(fname);

            string fname2 = "InputFile2Serialize.xml";
            XmlHelper.SerializeData(fname2, data);
        }
    }
}
