﻿using EMFFM.Manager.Samples;

namespace SamplesDemo.Manual
{
    /// <summary>
    /// Класс с описанием методов, содержащих примеры работы с различными элементами менеджера.
    /// </summary>
    class ManagerSamples : SampleDemo
    {
        public override void Start()
        {
            MeshTest();
            //InputTest();
            MainTest();
        }

        private void MeshTest()
        {
            MeshSamples.GenerateNetgenMesh();
            MeshSamples.GenerateNetgenMesh2();
            MeshSamples.ReadGeoFileAndChechSchema();
        }

        private void InputTest()
        {
            InputSamples.LoadInputDataAndCheck();
            InputSamples.LoadInputDataAndSave();
        }

        private void MainTest()
        {
            MainSample.TestInput();
            MainSample.TestMesh();
            //MainSample.TestSolve();
        }
    }
}
