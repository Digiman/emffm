﻿using EMFFM.NetgenUtils.Samples;

namespace SamplesDemo.Manual
{
    /// <summary>
    /// Класс для демонстрации примеров работы с библиотекой NetgenUtils
    /// </summary>
    class NetgenUtilsSamplesDemo: SampleDemo
    {
        /// <summary>
        /// Начало демонстрации
        /// </summary>
        public override void Start()
        {
            //Sample.TestParser();
            //Sample.Sample2();
            //Sample.Sample3();
            //Sample.Sample4();
            Sample.FactoryTest1();
            Sample.FactoryTest2();
        }
    }
}
