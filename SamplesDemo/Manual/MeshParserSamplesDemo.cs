﻿namespace SamplesDemo.Manual
{
    /// <summary>
    /// Класс для демонстрации примеров работы с библиотекой MeshParser
    /// </summary>
    class MeshParserSamplesDemo : SampleDemo
    {
        /// <summary>
        /// Начало демонстрации
        /// </summary>
        public override void Start()
        {
            EMFFM.MeshParser.Samples.Sample.TestParser();
        }
    }
}
