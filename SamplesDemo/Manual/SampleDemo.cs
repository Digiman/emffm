﻿namespace SamplesDemo.Manual
{
    /// <summary>
    /// Базовый класс для запуска демонстрации примеров
    /// </summary>
    abstract class SampleDemo
    {
        /// <summary>
        /// Начало демонстрации
        /// </summary>
        public abstract void Start();
    }
}
