﻿using System;
using SamplesDemo.Automatic;
using SamplesDemo.Manual;

namespace SamplesDemo
{
    /// <summary>
    /// Главный класс по умолчанию в консольном приложении
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Начало демонстрации в ручном режиме описанную вручную ввиде классов и методов
        /// </summary>
        static void ManualStartSamplesDemonstation()
        {
            //var demo = new NetgenUtilsSamplesDemo();
            var demo = new ManagerSamples();
            demo.Start();
        }
        
        /// <summary>
        /// Главный метод консольного приложения
        /// </summary>
        /// <param name="args">Аргументы командной строки</param>
        static void Main(string[] args)
        {
            Console.WriteLine("Samples demonstration is started...");

            try
            {
                //var samples = new SamplesDataReader("../../DataFiles/Samples1.xml").ReadData();
                //samples.ToJson("test.json");

                //var runner = new SamplesRunner(samples);
                //runner.Run();

                ManualStartSamplesDemonstation();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Samples demonstration is ended...");
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
