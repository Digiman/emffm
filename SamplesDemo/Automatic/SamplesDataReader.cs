﻿using System;
using System.Xml;
using SamplesDemo.Automatic.Elements;

namespace SamplesDemo.Automatic
{
    /// <summary>
    /// Класс для чтения списка примеров, которые нужно запустить для приложения
    /// </summary>
    class SamplesDataReader
    {
        /// <summary>
        /// Файл с данными для демонстрации
        /// </summary>
        private readonly string _filename;

        /// <summary>
        /// Инициализация читателя файла с определениями примеров для демонстрации
        /// </summary>
        /// <param name="filename">Имя файла с данными для демонстрации примеров</param>
        public SamplesDataReader(string filename)
        {
            _filename = filename;
        }

        /// <summary>
        /// Чтение данных из файла
        /// </summary>
        /// <returns>Возвращает объект с прочитанными примерами для выполнения</returns>
        public Samples ReadData()
        {
            var samples = new Samples();

            var doc = new XmlDocument();
            doc.Load(_filename);

            if (doc.DocumentElement != null) ReadDocumentData(doc.DocumentElement, ref samples);

            return samples;
        }

        /// <summary>
        /// Чтение данных из документа XML
        /// </summary>
        /// <param name="root">Корневой элемент с данными из файла</param>
        /// <param name="samples">Примеры</param>
        private void ReadDocumentData(XmlElement root, ref Samples samples)
        {
            samples.Name = root.Attributes["name"].Value;

            foreach (XmlNode module in root.ChildNodes)
            {
                if (module.NodeType == XmlNodeType.Element)
                {
                    samples.Modules.Add(ReadModule(module));
                }
            }
        }

        /// <summary>
        /// Чтение модуля с параметрами
        /// </summary>
        /// <param name="element">Элемент из документа XML</param>
        /// <returns>Возвращает определение для модуля</returns>
        private ModuleSample ReadModule(XmlNode element)
        {
            var module = new ModuleSample(element.Attributes["name"].Value);

            foreach (XmlNode method in element.ChildNodes)
            {
                if (method.NodeType == XmlNodeType.Element)
                {
                    module.Methods.Add(ReadMethods(method));
                }
            }

            return module;
        }

        /// <summary>
        /// Чтение метода
        /// </summary>
        /// <param name="element">Элемент с описанием метода из XML файла</param>
        /// <returns>Возвращаение определение метода</returns>
        private SampleMethod ReadMethods(XmlNode element)
        {
            var method = new SampleMethod();

            if (element.Attributes != null)
            {
                method.Name = element.Attributes["name"].Value;
                method.Fullname = element.Attributes["fullname"].Value;
                method.Enabled = Convert.ToBoolean(element.Attributes["enabled"].Value);
            }

            foreach (XmlNode param in element.ChildNodes)
            {
                if (param.NodeType == XmlNodeType.Element)
                {
                    method.Params.Add(ReadMethodParams(param));
                }
            }

            return method;
        }

        /// <summary>
        /// Чтение параметров метода (если они есть)
        /// </summary>
        /// <param name="node">Узел с данными о параметре из XML файла</param>
        /// <returns>Возвращает обработанный параметр</returns>
        private MethodParams ReadMethodParams(XmlNode node)
        {
            var param = new MethodParams();

            if (node.Attributes != null)
            {
                param.Name = node.Attributes["name"].Value;
                param.Type = node.Attributes["type"].Value;
                param.Value = node.Attributes["value"].Value;
            }

            return param;
        }
    }
}
