﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace SamplesDemo.Automatic.Elements
{
    /// <summary>
    /// Класс для описания списка примеров использования
    /// </summary>
    class Samples
    {
        /// <summary>
        /// Название списка примеров для демонстрации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список модулей дял демонстрации примеров из них
        /// </summary>
        public List<ModuleSample> Modules { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта для примеров
        /// </summary>
        public Samples()
        {
            Modules = new List<ModuleSample>();
        }

        /// <summary>
        /// Конвертация в JSON данных по примерам
        /// </summary>
        /// <param name="filename">Имя файла для вывода результата в формате JSON</param>
        public void ToJson(string filename)
        {
            var file = new StreamWriter(filename);

            var data = JsonConvert.SerializeObject(this);
            file.WriteLine(data);

            file.Close();
        }
    }
}
