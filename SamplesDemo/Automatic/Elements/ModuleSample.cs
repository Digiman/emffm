﻿using System.Collections.Generic;

namespace SamplesDemo.Automatic.Elements
{
    /// <summary>
    /// Класс для описания примеров из модуля
    /// </summary>
    class ModuleSample
    {
        /// <summary>
        /// Название модуля или библиотеки
        /// </summary>
        private string _modulename;

        /// <summary>
        /// Список методов с примерами
        /// </summary>
        public List<SampleMethod> Methods { get; set; }

        /// <summary>
        /// Инициализация "пустого" модуля с примерами
        /// </summary>
        /// <param name="modulename">Название модуля (библиотеки)</param>
        public ModuleSample(string modulename)
        {
            _modulename = modulename;
            Methods = new List<SampleMethod>();
        }
    }
}
