﻿using System.Collections.Generic;

namespace SamplesDemo.Automatic.Elements
{
    /// <summary>
    /// Класс для описания метода с примером в модуле 
    /// </summary>
    class SampleMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Полное имя (включая модуль и весь путь по пространствам имен)
        /// </summary>
        public string Fullname { get; set; }
        /// <summary>
        /// Обрабатывается ли метод
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// Список параметров метода
        /// </summary>
        public List<MethodParams> Params { get; set; } 

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public SampleMethod()
        {
            Params = new List<MethodParams>();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="fullname"></param>
        /// <param name="enabled"></param>
        public SampleMethod(string name, string fullname, bool enabled)
        {
            Name = name;
            Fullname = fullname;
            Enabled = enabled;
            Params = new List<MethodParams>();
        }
    }
}
