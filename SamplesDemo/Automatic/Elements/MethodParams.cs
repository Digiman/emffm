﻿namespace SamplesDemo.Automatic.Elements
{
    /// <summary>
    /// Список параметров модуля
    /// </summary>
    class MethodParams
    {
        /// <summary>
        /// Название параметра
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Значение параметра
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Тип значения параметра
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Инициализация "пустого" параметра
        /// </summary>
        public MethodParams()
        {}
    }
}
