﻿using SamplesDemo.Automatic.Elements;

namespace SamplesDemo.Automatic
{
    /// <summary>
    /// Класс для реализации автоматической обработки запуска примеров
    /// </summary>
    class SamplesRunner
    {
        /// <summary>
        /// Сведения о примерах для демонстрации
        /// </summary>
        private readonly Samples _samples;

        /// <summary>
        /// Инициализация запускальщика примеров для демонстрации
        /// </summary>
        /// <param name="samples">Примеры для демонстрации</param>
        public SamplesRunner(Samples samples)
        {
            _samples = samples;
        }

        /// <summary>
        /// Запуск демонстрации примеров
        /// </summary>
        public void Run()
        {
            foreach (var module in _samples.Modules)
            {
                RunModule(module);
            }
        }

        /// <summary>
        /// Запуск модуля для демонстрации примеров в нем
        /// </summary>
        /// <param name="module">Модуль</param>
        private void RunModule(ModuleSample module)
        {
            foreach (var method in module.Methods)
            {
                RunMethod(method);
            }
        }

        /// <summary>
        /// Запуск методов реализующих примеры
        /// </summary>
        /// <param name="method">Метод</param>
        private void RunMethod(SampleMethod method)
        {
            // TODO: написать здесь код для реализации запуска примера
        }
    }
}
