﻿using System;
using System.Drawing;
using System.Numerics;
using System.Windows.Forms;
using EMFFM.Analyzer.Helpers;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.Manager;
using EMFFM.Manager.Common;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Форма для отображения данных с результатами анализа (для новой версии анализатора по сетке анализа).
    /// </summary>
    /// <typeparam name="TValue">Тип данных, в которых производится рещшение и анализ (вещественные или комплексные).</typeparam>
    /// <typeparam name="TResultValue">Тип данных, в которых представлен результат в сетке анализа (поля).</typeparam>
    public partial class ResultsWnd<TValue, TResultValue>: Form
    {
        /// <summary>
        /// Полные результаты (решения и анализа).
        /// </summary>
        private readonly FullResults<TValue, TResultValue> _results;
        
        /// <summary>
        /// Главный фасад для работы с приложением.
        /// </summary>
        private MainFacade<TValue, TResultValue> _mainFacade;

        /// <summary>
        /// Инициализация формы, ее компонентов и данных на ней.
        /// </summary>
        /// <param name="results">Полные результаты (решения и анализа).</param>
        /// <param name="facade">Главный рабочий класс (фасад ко всем модулям приложения).</param>
        public ResultsWnd(FullResults<TValue, TResultValue> results, MainFacade<TValue, TResultValue> facade)
        {
            _results = results;
            _mainFacade = facade;

            InitializeComponent();

            InitWindow();
        }

        /// <summary>
        /// Инициализация компонентов формы.
        /// </summary>
        private void InitWindow()
        {
            // загрузка списка направлений и выбор однго из них (первого, т.е. по X)
            comboBox1.Items.AddRange(Enum.GetNames(typeof (Direction)));
            comboBox1.SelectedIndex = 0;

            GetLayersCountData();

            // размерность сетки анализа
            textBox1.Text = _results.AnalyzeMesh.GetLayersCount(Direction.X).ToString();
            textBox2.Text = _results.AnalyzeMesh.GetLayersCount(Direction.Y).ToString();
            textBox3.Text = _results.AnalyzeMesh.GetLayersCount(Direction.Z).ToString();

            // количество узлов в сетке анализа
            textBox4.Text = _results.AnalyzeMesh.GetNodesCount().ToString();

            // время решения
            textBox5.Text = _results.SolverResult.SolveTime.ToString();
        }

        #region Обработка событий контролов на форме

        /// <summary>
        /// Обработчка события нажатия кнопки "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обработка события клика по кнопке Построить дяь построения картины слоя среза.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildButton_Click(object sender, EventArgs e)
        {
            var direction = (Direction) comboBox1.SelectedIndex;

            // получение сведения со слоя (значения поля) и размерности матрицы, полученной в результате среза
            var layerData = _results.AnalyzeMesh.GetLayerData(direction, (int) numericUpDown1.Value);
            var size = _results.AnalyzeMesh.GetSize(direction);

            // получение значений координат для среза в зависимости от того, какое используется направление
            double[] coordsX = new double[0], coordsY = new double[0];
            switch (direction)
            {
                case Direction.X:
                    coordsX = _results.AnalyzeMesh.GetCoordinates(Direction.Y);
                    coordsY = _results.AnalyzeMesh.GetCoordinates(Direction.Z);
                    break;
                case Direction.Y:
                    coordsX = _results.AnalyzeMesh.GetCoordinates(Direction.X);
                    coordsY = _results.AnalyzeMesh.GetCoordinates(Direction.Z);
                    break;
                case Direction.Z:
                    coordsX = _results.AnalyzeMesh.GetCoordinates(Direction.X);
                    coordsY = _results.AnalyzeMesh.GetCoordinates(Direction.Y);
                    break;
            }

            // построение вектора (массива) для заданного положения по матрице среза
            double[,] dataY = new double[2, size[0]];
            var nx = numericUpDown2.Value != 0 ? (int) numericUpDown2.Value : (int) Math.Floor(size[0]/2.0);
            var ny = numericUpDown3.Value != 0 ? (int) numericUpDown3.Value : (int) Math.Floor(size[1]/2.0);

            for (int i = 0; i < size[1]; i++)
            {
                dataY[0, i] = Complex.Abs((layerData[i, nx] as Field).Amplitude);
            }
            for (int i = 0; i < size[0]; i++)
            {
                dataY[1, i] = Complex.Abs((layerData[ny, i] as Field).Amplitude);
            }

            // построение графика двухмерного
            GraphHelper.DrawChart(chart1, coordsX, dataY, Color.Blue, Color.DarkRed);
        }

        /// <summary>
        /// Обработка событий клика по кнопке "Экспорт слоя".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Выполняет экспорт для данных, которые нужны для анализа слоя (выбранного в параметрах среза, по которому строится график).</remarks>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            ResultsMeshHelper.ExportLayerData(_results.AnalyzeMesh, (Direction) comboBox1.SelectedIndex,
                (int) numericUpDown1.Value, Globals.InputData.Output.Path);

            MessageBox.Show("Экспорт успешно выполнен!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Экспорт набора данных для выполнения анимации в Matlab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Открывает новое окно для выбора параметров экспорта.</remarks>
        private void ExportAnimateButton_Click(object sender, EventArgs e)
        {
            var wnd = new ExportDirWnd<TValue, TResultValue>(_results);
            wnd.ShowDialog();
        }

        /// <summary>
        /// Обработка выбора плоскости для среза.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLayersCountData();
        }

        #endregion

        #region Вспомогательные функции

        /// <summary>
        /// Получение количества слоев сетки анализа по направлениям.
        /// </summary>
        private void GetLayersCountData()
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                var maxValue = _results.AnalyzeMesh.GetLayersCount((Direction) comboBox1.SelectedIndex);
                numericUpDown1.Minimum = 0;
                numericUpDown1.Maximum = maxValue - 1;
                numericUpDown1.Value = (int) (maxValue/2);
            }
        }

        #endregion
    }
}
