﻿using System;
using System.IO;
using System.Windows.Forms;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно для просмотра файла и его содержимого в виде текстового редактора (без редактирования, только просмотр).
    /// </summary>
    public partial class PreviewFileWnd : Form
    {
        public PreviewFileWnd(string filename)
        {
            InitializeComponent();

            OutputFileData(filename);
        }

        private void OutputFileData(string filename)
        {
            var data = File.ReadAllText(filename);
            textBox1.Text = data;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
