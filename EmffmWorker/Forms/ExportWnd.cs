﻿using System;
using System.Windows.Forms;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Input.Enums;
using EMFFM.Manager;
using EMFFM.Manager.Common;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно для выполнения экспорта всех результатов решения.
    /// </summary>
    public partial class ExportWnd : Form
    {
        /// <summary>
        /// Объект класса, производящего все операции с ядром и вызовом функций.
        /// </summary>
        private readonly IMainFacade<System.Numerics.Complex, Field> _worker;

        /// <summary>
        /// Инициализация окна с результатами.
        /// </summary>
        /// <param name="worker">Главный рабочий класс.</param>
        public ExportWnd(IMainFacade<System.Numerics.Complex, Field> worker)
        {
            InitializeComponent();

            _worker = worker;
        }

        /// <summary>
        /// Обработка кнопки закрытия окна.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Экспорт вектора воздействий.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton1_Click(object sender, EventArgs e)
        {
            // TODO: придумать как экспортировать здесь вектор воздействий
        }

        /// <summary>
        /// Экспорт глобальной матрицы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton2_Click(object sender, EventArgs e)
        {
            // TODO: придумать как экспортировать здесь глобальную матрицу
        }

        /// <summary>
        /// Обработка экспорта результатов решения СЛАУ.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton3_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();
            dlg.Filter = "Текстовые файлы|*.txt";
            dlg.Title = "Сохранение результатов решения";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                _worker.ExportSolveResult(dlg.FileName, ExportFileType.TXT);

                MessageBox.Show("Экспорт успешно завершен!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Обработка результатов анализа.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton4_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();
            dlg.Filter = "Текстовые файлы|*.txt";
            dlg.Title = "Сохранение результатов анализа";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                _worker.ExportAnalyzeResult(dlg.FileName, ExportFileType.TXT);

                MessageBox.Show("Экспорт успешно завершен!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Выполнение экспорта всех результатов и данных.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton5_Click(object sender, EventArgs e)
        {
            _worker.ExportSolveResult(Globals.InputData.Output.GetFullPath(DatafileType.Result), ExportFileType.TXT);

            MessageBox.Show("Экспорт успешно завершен!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }
    }
}
