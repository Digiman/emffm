﻿using System;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Common.Helpers;
using EMFFM.Manager;
using EMFFM.Manager.Common;
using MieTheory;
using MieTheory.Common;
using MieTheory.Input;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно для просмотра плоских графиков с результатами, полученными при расчете разными способами.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, в которых производится рещшение и анализ (вещественные или комплексные).</typeparam>
    /// <typeparam name="TResultValue">Тип данных, в которых представлен результат в сетке анализа (поля).</typeparam>
    public partial class ComboResultsWnd<TValue, TResultValue> : Form
    {
        /// <summary>
        /// Полные результаты (решения и анализа).
        /// </summary>
        private readonly FullResults<TValue, TResultValue> _results;

        /// <summary>
        /// Главный фасад для работы с приложением.
        /// </summary>
        private MainFacade<TValue, TResultValue> _mainFacade;

        /// <summary>
        /// Фасад для работы с теорией Ми.
        /// </summary>
        private IMieFacade _mieFacade;

        /// <summary>
        /// Инициализация формы, ее компонентов и данных на ней.
        /// </summary>
        /// <param name="results">Полные результаты (решения и анализа).</param>
        /// <param name="facade">Главный рабочий класс (фасад ко всем модулям приложения).</param>
        public ComboResultsWnd(FullResults<TValue, TResultValue> results, MainFacade<TValue, TResultValue> facade)
        {
            _results = results;
            _mainFacade = facade;

            InitializeComponent();

            InitWindow();
        }

        /// <summary>
        /// Инициализация компонентов формы.
        /// </summary>
        private void InitWindow()
        {
            // загрузка списка направлений и выбор однго из них (первого, т.е. по X)
            comboBox1.Items.AddRange(Enum.GetNames(typeof (Direction)));
            comboBox1.SelectedIndex = 0;

            GetLayersCountData();
        }

        #region Обрабтчики событий компонентов на форме

        /// <summary>
        /// Обработка нажатия кнопки "Построить".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildButton_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();

            // построение графиков по результатам решения по МКЭ
            GenerateDataForFem();

            // построение графиков по результатам решения по теории Ми
            GenerateDataForMie();
        }

        /// <summary>
        /// Обработка нажатия кнопки "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обработка выбора плоскости для среза.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLayersCountData();
        }

        #endregion

        #region Основные рабочие функции

        /// <summary>
        /// Построение графиков по данным, полученным при решении задачи с использование МКЭ.
        /// </summary>
        private void GenerateDataForFem()
        {
            var direction = (Direction) comboBox1.SelectedIndex;

            // получение сведения со слоя (значения поля) и размерности матрицы, полученной в результате среза
            var layerData = _results.AnalyzeMesh.GetLayerData(direction, (int) numericUpDown1.Value);
            var size = _results.AnalyzeMesh.GetSize(direction);

            // получение значений координат для среза в зависимости от того, какое используется направление
            double[] coordsX = new double[0], coordsY = new double[0];
            switch (direction)
            {
                case Direction.X:
                    coordsX = _results.AnalyzeMesh.GetCoordinates(Direction.Y);
                    coordsY = _results.AnalyzeMesh.GetCoordinates(Direction.Z);
                    break;
                case Direction.Y:
                    coordsX = _results.AnalyzeMesh.GetCoordinates(Direction.X);
                    coordsY = _results.AnalyzeMesh.GetCoordinates(Direction.Z);
                    break;
                case Direction.Z:
                    coordsX = _results.AnalyzeMesh.GetCoordinates(Direction.X);
                    coordsY = _results.AnalyzeMesh.GetCoordinates(Direction.Y);
                    break;
            }

            coordsX = CorrectValues(coordsX, Globals.InputData.Geometry.CoFactor);

            // построение вектора (массива) для заданного положения по матрице среза
            double[,] dataY = new double[2, size[0]];
            var nx = numericUpDown2.Value != 0 ? (int) numericUpDown2.Value : (int) Math.Floor(size[0]/2.0);
            var ny = numericUpDown3.Value != 0 ? (int) numericUpDown3.Value : (int) Math.Floor(size[1]/2.0);

            for (int i = 0; i < size[1]; i++)
            {
                dataY[0, i] = Complex.Abs((layerData[i, nx] as Field).Amplitude);
            }
            for (int i = 0; i < size[0]; i++)
            {
                dataY[1, i] = Complex.Abs((layerData[ny, i] as Field).Amplitude);
            }

            // построение графика двухмерного
            GraphHelper.DrawChart(chart1, coordsX, dataY, Color.Blue, Color.DarkRed);
        }

        /// <summary>
        /// Построение графиков по данным, полученным при решении задачи с использование теории Ми.
        /// </summary>
        private void GenerateDataForMie()
        {
            // 1. Выполнение решения
            var results = SolveForMie();

            // 2. Отображение данных решения.
            int sizeX = results.X.Length;
            int sizeY = results.Y.Length;

            // построение вектора (массива) для заданного положения по матрице среза
            double[,] dataY = new double[2, sizeX];

            var nx = numericUpDown2.Value != 0 ? (int)numericUpDown1.Value : (int)Math.Floor(sizeX / 2.0);
            var ny = numericUpDown3.Value != 0 ? (int)numericUpDown2.Value : (int)Math.Floor(sizeY / 2.0);

            for (int i = 0; i < sizeY; i++)
            {
                dataY[0, i] = results.Z[i, nx];
            }
            for (int i = 0; i < sizeX; i++)
            {
                dataY[1, i] = results.Z[ny, i];
            }

            // построение графика двухмерного
            GraphHelper.DrawChart(chart1, results.X, dataY, Color.DarkOrange, Color.Green);
        }

        /// <summary>
        /// Выполнение решения задачи по теории Ми.
        /// </summary>
        private MieFullResults SolveForMie()
        {
            // формирование исходных данных для задачи по теории Ми
            var data = new InputMieData();
            data.Material =
                (MaterialTypes)
                    Enum.Parse(typeof (MaterialTypes),
                        Globals.InputData.Geometry.Domains.FirstOrDefault(d => d.Name == "Sphere").MaterialName);

            _mieFacade = new MieFacade(data);

            _mieFacade.Calculate();

            return _mieFacade.Results;
        }

        #endregion

        #region Вспомогательные функции

        /// <summary>
        /// Получение количества слоев сетки анализа по направлениям.
        /// </summary>
        private void GetLayersCountData()
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                var maxValue = _results.AnalyzeMesh.GetLayersCount((Direction) comboBox1.SelectedIndex);
                numericUpDown1.Minimum = 0;
                numericUpDown1.Maximum = maxValue - 1;
                numericUpDown1.Value = (int) (maxValue/2);
            }
        }

        /// <summary>
        /// Корректировка значений на заданное значение (перемножение текущих значенйи на него).
        /// </summary>
        /// <param name="values">Массив со значениями для корректировки.</param>
        /// <param name="coFactor">Корректирующзее значение.</param>
        private double[] CorrectValues(double[] values, double coFactor)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] *= coFactor;
            }

            return values;
        }

        #endregion
    }
}
