﻿using System;
using System.IO;
using System.Numerics;
using System.Windows.Forms;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Manager;
using EMFFM.Manager.Common;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Главное окно программы
    /// </summary>
    public partial class MainWnd : Form
    {
        /// <summary>
        /// Загружен ли входной файл с условиями и параметрами задачи.
        /// </summary>
        private bool _fileLoaded;

        /// <summary>
        /// Решена ли загруженная задача?
        /// </summary>
        private bool _isSolved;

        /// <summary>
        /// Главный класс для реализации всех действий по обработке задачи.
        /// </summary>
        //private MainWorker _mainWorker;

        /// <summary>
        /// Главный фасад, который используется для работы с ядром приложения.
        /// </summary>
        private MainFacade<System.Numerics.Complex, EMFFM.Common.FEM.Complex.Elements.Field> _mainFacade; 

        /// <summary>
        /// Инициализация главного окна и его компонентов.
        /// </summary>
        public MainWnd()
        {
            InitializeComponent();

            InitWindow();
            InitData();
        }

        /// <summary>
        /// Настройка окна и его компонентов.
        /// </summary>
        private void InitWindow()
        {
            CalcButton.Enabled = false;
            ResultsButton.Enabled = false;
            InputInfoButton.Visible = false;
            ResetButton.Visible = false;
            MeshInfoButton.Visible = false;
            
            ChangeStatusText("Приложение готово к работе!");
        }

        private void InitData()
        {
            //_mainWorker = new MainWorker();

            _mainFacade = new MainFacade<Complex, Field>();
        }

        #region Обработка событий элементов главного меню

        /// <summary>
        /// Обработка события меню открытия.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenMenuItem_Click(object sender, EventArgs e)
        {
            LoadInputData();
        }

        /// <summary>
        /// Обработка выбора меню "Файл - Выход".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обработка события меню настроек программы Netgen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NetgenMenuItem_Click(object sender, EventArgs e)
        {
            var wnd = new EnvPrefWnd();
            wnd.ShowDialog();
        }

        /// <summary>
        /// Обработка события вызова окна справки О программе.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            // TODO: поместить сюда вызов окна о программе, которое универсальное во всех приложения и имеет общий вид
            var wnd = new AboutWnd();
            wnd.ShowDialog();
        }

        /// <summary>
        ///  Обработка выбора меню "Окна - Результаты 1".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Results1StripMenuItem_Click(object sender, EventArgs e)
        {
            var results = _mainFacade.GetFullResults();

            var wnd = new ResultsWnd<Complex, Field>(results, _mainFacade);
            wnd.ShowDialog();
        }

        /// <summary>
        /// Обработка выбора меню "Окна - Результаты 2".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Results2MenuItem_Click(object sender, EventArgs e)
        {
            var results = _mainFacade.GetFullResults();

            var wnd = new ComboResultsWnd<Complex, Field>(results, _mainFacade);
            wnd.ShowDialog();
        }

        #endregion

        #region Обработка событий кнопок на главной части формы

        /// <summary>
        /// Обработка нажатия кнопки "Загрузить задачу".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            LoadInputData();
        }

        /// <summary>
        /// Обработка нажатия кнопки "Решить".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalcButton_Click(object sender, EventArgs e)
        {
            Calculate();

            // TODO: здесь подумать, как можно отображать процесс решения задачи (вероятно в будущем, пока его не будет)

            MeshInfoButton.Visible = true;
            MeshInfoButton.Enabled = true;
            ResultsButton.Enabled = true;
            ResetButton.Visible = true;

            ChangeStatusText("Задача успешно решена!");

            _isSolved = true;
        }

        /// <summary>
        /// Обработка нажатия кнопки "Результаты".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultsButton_Click(object sender, EventArgs e)
        {
            ShowResults();
        }

        /// <summary>
        /// Обработка нажатия кнопки "Сведения о задаче".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputInfoButton_Click(object sender, EventArgs e)
        {
            var wnd = new TaskInfoWnd(Globals.InputData);
            wnd.ShowDialog();
        }

        /// <summary>
        /// Обработка нажатия кнопки "Сброс".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetButton_Click(object sender, EventArgs e)
        {
            // TODO: написать код для сброса решенной задачи

            InitWindow();
            _mainFacade.ResetTask();

            _fileLoaded = false;
            _isSolved = false;
        }

        /// <summary>
        /// Обработка нажатия кнопки "Сведения о сетке".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MeshInfoButton_Click(object sender, EventArgs e)
        {
            var wnd = new MeshInfoWnd(_mainFacade.GetMeshData);
            wnd.ShowDialog();
        }

        #endregion

        /// <summary>
        /// Загрузка входных данных из файла XML.
        /// </summary>
        private void LoadInputData()
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "XML-файлы|*.xml";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                Globals.InputData = _mainFacade.LoadTask(dlg.FileName);

                ChangeStatusText(String.Format("Задача загружена из файла {0}", new FileInfo(dlg.FileName).Name));

                InputInfoButton.Visible = true;
                CalcButton.Enabled = true;

                _fileLoaded = true;
            }
        }

        /// <summary>
        /// Выполнение решения задачи.
        /// </summary>
        private void Calculate()
        {
            // выполняется решение и анализ результатов
            _mainFacade.SolveAndAnalyze();
        }

        /// <summary>
        /// Показ результатов решения.
        /// </summary>
        private void ShowResults()
        {
            var results = _mainFacade.GetFullResults();

            //var wnd = new ResultsWnd<Complex, Field>(results, _mainFacade);
            //wnd.ShowDialog();

            var wnd = new ComboResultsWnd<Complex, Field>(results, _mainFacade);
            wnd.ShowDialog();
        }

        /// <summary>
        /// Сменить текст в панели статуса.
        /// </summary>
        /// <param name="text">Текст, который будет отображен в панели статуса.</param>
        private void ChangeStatusText(string text)
        {
            statusStrip1.Items[0].Text = text;
        }
    }
}
