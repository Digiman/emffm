﻿using System;
using System.Windows.Forms;
using EMFFM.Analyzer.Helpers;
using EMFFM.Common.Enums;
using EMFFM.Manager.Common;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно для экспорта данных после анализа по направлению для построения анимации в Matlab.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, в которых производится рещшение и анализ (вещественные или комплексные).</typeparam>
    /// <typeparam name="TResultValue">Тип данных, в которых представлен результат в сетке анализа (поля).</typeparam>
    public partial class ExportDirWnd<TValue, TResultValue> : Form
    {
        /// <summary>
        /// Полные результаты (решения и анализа).
        /// </summary>
        private readonly FullResults<TValue, TResultValue> _results;

        /// <summary>
        /// Инициализация окна для экспорта набора данных вдоль направления.
        /// </summary>
        /// <param name="results">Результаты анализа.</param>
        public ExportDirWnd(FullResults<TValue, TResultValue> results)
        {
            _results = results;

            InitializeComponent();

            InitWindow();
        }

        /// <summary>
        /// Инициализация данных в компонентах в окне.
        /// </summary>
        private void InitWindow()
        {
            comboBox1.Items.AddRange(Enum.GetNames(typeof(Direction)));
            comboBox1.SelectedIndex = 0;
        }

        /// <summary>
        /// Нажатие кнопки "Отмена".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Нажатие кнопки "Экспорт".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            ResultsMeshHelper.ExportLayersForAnimate(_results.AnalyzeMesh, (Direction) comboBox1.SelectedIndex,
                Globals.InputData.Output.Path);

            MessageBox.Show("Экспорт набора данных успешно выполнен!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }
    }
}
