﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EMFFM.Common.Input.Elements;
using EMFFM.Input;
using EMFFM.Input.Elements;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно для просмотра сведения о задаче.
    /// </summary>
    public partial class TaskInfoWnd : Form
    {
        /// <summary>
        /// Сведения о задаче.
        /// </summary>
        private readonly InputData _data;

        /// <summary>
        /// Инициализация окна со сведениями о задаче с параметрами.
        /// </summary>
        /// <param name="data">Сведения о задаче, которые нужно отобразить в окне.</param>
        public TaskInfoWnd(InputData data)
        {
            _data = data;

            InitializeComponent();

            OutputData(data);
        }

        /// <summary>
        /// Вывод данных о задаче в компоненты окна.
        /// </summary>
        /// <param name="data"></param>
        private void OutputData(InputData data)
        {
            // 1. Задача
            textBox1.Text = data.Task.Name;
            textBox2.Text = data.Task.ValueType.ToString();
            textBox3.Text = data.Task.SolverType.ToString();
            textBox4.Text = data.Task.Class.ToString();
            textBox5.Text = data.Task.FemType.ToString();

            // 2. Геометрия
            textBox6.Text = data.Geometry.GometryFile;
            textBox7.Text = data.Geometry.CoFactor.ToString();

            // список областей
            OutputDomains(dataGridView1, data.Geometry.Domains);

            // список материалов
            OutputMaterials(dataGridView2, data.Materials);

            // 3. Параметры задачи

            // источники
            OutputSources(dataGridView5, data.Sources);

            // граничные условия
            OutputBoundaries(dataGridView6, data.Boundaries);

            // 4. Опции 
            OutputOptions(dataGridView3, data.Options);
            
            // 5. Блок анализа
            textBox8.Text = data.AnalyzeBox.GetSize();
            textBox9.Text = data.AnalyzeBox.Element.ToString();
            textBox10.Text = data.AnalyzeBox.Element.Type.ToString();

            // 6. Выходные данные
            textBox11.Text = data.Output.Path;
            textBox12.Text = data.Output.FilePrefix;
            OutputOutputFiles(dataGridView4, data.Output);
        }

        #region Методы для вывода набора данных в компоненты формы

        /// <summary>
        /// Вывод списка областей в задаче.
        /// </summary>
        /// <param name="dgv">Таблица, куда помещать данные.</param>
        /// <param name="domains">Список областей.</param>
        private void OutputDomains(DataGridView dgv, List<Domain> domains)
        {
            dgv.RowCount = domains.Count();
            dgv.ColumnCount = 3;

            dgv.Columns[0].HeaderText = "Код";
            dgv.Columns[1].HeaderText = "Название области";
            dgv.Columns[2].HeaderText = "Материал";

            dgv.RowHeadersVisible = false;

            int i = 0;
            foreach (var domain in domains)
            {
                dgv[0, i].Value = domain.Code;
                dgv[1, i].Value = domain.Name;
                dgv[2, i].Value = domain.MaterialName;

                i++;
            }
        }

        /// <summary>
        /// Вывод списка материалов, используемых в задаче.
        /// </summary>
        /// <param name="dgv">Таблица, куда помещать данные.</param>
        /// <param name="materials">Список материалов.</param>
        private void OutputMaterials(DataGridView dgv, List<MaterialBase> materials)
        {
            dgv.RowCount = materials.Count();
            dgv.ColumnCount = 6;

            dgv.Columns[0].HeaderText = "Название";
            dgv.Columns[1].HeaderText = "По умолчанию?";
            dgv.Columns[2].HeaderText = "Eps";
            dgv.Columns[3].HeaderText = "Mu";
            dgv.Columns[4].HeaderText = "Sigma";
            dgv.Columns[5].HeaderText = "Модель";

            dgv.RowHeadersVisible = false;

            int i = 0;
            foreach (var material in materials)
            {
                dgv[0, i].Value = material.Name;
                dgv[1, i].Value = material.IsDefault;

                if (material is Material)
                {
                    dgv[2, i].Value = (material as Material).Eps;
                    dgv[3, i].Value = (material as Material).Mu;
                }
                else if (material is ComplexMaterial)
                {
                    dgv[2, i].Value = (material as ComplexMaterial).Eps.ToString();
                    dgv[3, i].Value = (material as ComplexMaterial).Mu.ToString();
                    dgv[4, i].Value = (material as ComplexMaterial).Sigma.ToString();
                    dgv[5, i].Value = (material as ComplexMaterial).Model.ToString();
                }

                i++;
            }
        }

        /// <summary>
        /// Вывод сведений об источниках.
        /// </summary>
        /// <param name="dgv">Таблица с данными куда выводить их.</param>
        /// <param name="sources">Список источников.</param>
        private void OutputSources(DataGridView dgv, List<Source> sources)
        {
            dgv.RowCount = sources.Count();
            dgv.ColumnCount = 6;

            dgv.Columns[0].HeaderText = "Lambda";
            dgv.Columns[1].HeaderText = "Amplitude";
            dgv.Columns[2].HeaderText = "Phase";
            dgv.Columns[3].HeaderText = "Direction";
            dgv.Columns[4].HeaderText = "Boundary";
            dgv.Columns[5].HeaderText = "Domain";

            dgv.RowHeadersVisible = false;

            int i = 0;
            foreach (var source in sources)
            {
                dgv[0, i].Value = source.Lambda;
                dgv[1, i].Value = source.Amplitude;
                dgv[2, i].Value = source.Phase;
                dgv[3, i].Value = source.Direction;
                dgv[4, i].Value = source.Boundary;
                dgv[5, i].Value = source.Domain;

                i++;
            }
        }

        /// <summary>
        /// Вывод сведений о граничных условиях.
        /// </summary>
        /// <param name="dgv">Таблица с данными куда выводить их.</param>
        /// <param name="boundaries">Список граничных условий.</param>
        private void OutputBoundaries(DataGridView dgv, List<Boundary> boundaries)
        {
            dgv.RowCount = boundaries.Count();
            dgv.ColumnCount = 3;

            dgv.Columns[0].HeaderText = "Тип";
            dgv.Columns[1].HeaderText = "Область";
            dgv.Columns[2].HeaderText = "Значение";

            dgv.RowHeadersVisible = false;

            int i = 0;
            foreach (var boundary in boundaries)
            {
                dgv[0, i].Value = boundary.Type;
                dgv[1, i].Value = boundary.DomainNumber;
                dgv[2, i].Value = boundary.Value;

                i++;
            }
        }

        /// <summary>
        /// Вывод сведений о выходных файлах.
        /// </summary>
        /// <param name="dgv">Таблица с данными куда выводить их.</param>
        /// <param name="output">Выходные параметры.</param>
        private void OutputOutputFiles(DataGridView dgv, Output output)
        {
            dgv.RowCount = output.Files.Count();
            dgv.ColumnCount = 2;

            dgv.Columns[0].HeaderText = "Название";
            dgv.Columns[1].HeaderText = "Значение";

            dgv.RowHeadersVisible = false;

            int i = 0;
            foreach (var file in output.Files)
            {
                dgv[0, i].Value = file.Key;
                dgv[1, i].Value = file.Value;

                i++;
            }
        }

        /// <summary>
        /// Вывод сведений об опциях.
        /// </summary>
        /// <param name="dgv">Таблица с данными куда выводить их.</param>
        /// <param name="options">Опции.</param>
        private void OutputOptions(DataGridView dgv, Options options)
        {
            var typeInfo = options.GetType();
            var properties = typeInfo.GetProperties();

            dgv.RowCount = properties.Length;
            dgv.ColumnCount = 2;

            dgv.Columns[0].HeaderText = "Название";
            dgv.Columns[1].HeaderText = "Значение";

            dgv.RowHeadersVisible = false;

            int i = 0;
            foreach (var propertyInfo in properties)
            {
                dgv[0, i].Value = propertyInfo.Name;
                dgv[1, i].Value = propertyInfo.GetValue(options, null);

                i++;
            }
        }

        #endregion

        #region Обработка событий компонентов формы

        /// <summary>
        /// Клик по кнопке "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Просмотр содерджимого файла геометрии.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var wnd = new PreviewFileWnd(_data.Geometry.GometryFile);
            wnd.ShowDialog();
        }

        #endregion
    }
}
