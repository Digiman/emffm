﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно с информацией о приложении.
    /// </summary>
    public partial class AboutWnd : Form
    {
        /// <summary>
        /// Иницализация окна и данных в нем.
        /// </summary>
        public AboutWnd()
        {
            InitializeComponent();

            linkLabel1.Links.Add(0, linkLabel1.Text.Length, "https://bitbucket.org/Digiman/emffm/wiki/Home");
        }

        /// <summary>
        /// Обработка события нажатия кнопки "Закрыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Клик по ссылке "Подробнее".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var sInfo = new ProcessStartInfo(e.Link.LinkData.ToString());
            Process.Start(sInfo);
        }
    }
}
