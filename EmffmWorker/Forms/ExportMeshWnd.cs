﻿using System;
using System.Windows.Forms;
using EMFFM.Common.Enums;
using EMFFM.MeshParser;

namespace EmffmWorker.Forms
{
    /// <summary>
    /// Окно для реализации экпорта сетки в заданный формат
    /// </summary>
    public partial class ExportMeshWnd : Form
    {
        /// <summary>
        /// Сгенерированная сетка.
        /// </summary>
        private readonly MeshData _mesh;

        /// <summary>
        /// Инициализация окна для экспорта.
        /// </summary>
        /// <param name="mesh">Сгенерированная сетка.</param>
        public ExportMeshWnd(MeshData mesh)
        {
            InitializeComponent();

            _mesh = mesh;
        }

        /// <summary>
        /// Зкрытие окна.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Экспорт данных о сетке в файл.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            var fileType = ExportFileType.XML;
            if (radioButton1.Checked) fileType = ExportFileType.XML;
            if (radioButton2.Checked) fileType = ExportFileType.TXT;

            SaveDialogExecute(fileType);
        }

        /// <summary>
        /// Вызов диалога с параметрами для сохранения сетки в файле.
        /// </summary>
        /// <param name="type">Тип файла для сохранения.</param>
        private void SaveDialogExecute(ExportFileType type)
        {
            var dlg = new SaveFileDialog();
            switch (type)
            {
                case ExportFileType.XML:
                    dlg.Filter = "XML-files|*.xml";
                    break;
                case ExportFileType.TXT:
                    dlg.Filter = "Text files|*.txt";
                    break;
            }
            var dlgResult = dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName) && dlgResult == DialogResult.OK)
            {
                try
                {
                    _mesh.ExportMesh(dlg.FileName, type);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }

                MessageBox.Show("Экспорт успешно завершен!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                this.Close();
            }
        }
    }
}
