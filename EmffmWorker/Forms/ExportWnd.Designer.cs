﻿namespace EmffmWorker.Forms
{
    partial class ExportWnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ExportButton5 = new System.Windows.Forms.Button();
            this.ExportButton4 = new System.Windows.Forms.Button();
            this.ExportButton3 = new System.Windows.Forms.Button();
            this.ExportButton2 = new System.Windows.Forms.Button();
            this.ExportButton1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(199, 250);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ExportButton5);
            this.groupBox1.Controls.Add(this.ExportButton4);
            this.groupBox1.Controls.Add(this.ExportButton3);
            this.groupBox1.Controls.Add(this.ExportButton2);
            this.groupBox1.Controls.Add(this.ExportButton1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 232);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Данные для экспорта";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Все данные";
            // 
            // ExportButton5
            // 
            this.ExportButton5.Location = new System.Drawing.Point(168, 187);
            this.ExportButton5.Name = "ExportButton5";
            this.ExportButton5.Size = new System.Drawing.Size(75, 23);
            this.ExportButton5.TabIndex = 8;
            this.ExportButton5.Text = "Экспорт";
            this.ExportButton5.UseVisualStyleBackColor = true;
            this.ExportButton5.Click += new System.EventHandler(this.ExportButton5_Click);
            // 
            // ExportButton4
            // 
            this.ExportButton4.Location = new System.Drawing.Point(168, 145);
            this.ExportButton4.Name = "ExportButton4";
            this.ExportButton4.Size = new System.Drawing.Size(75, 23);
            this.ExportButton4.TabIndex = 7;
            this.ExportButton4.Text = "Экспорт";
            this.ExportButton4.UseVisualStyleBackColor = true;
            this.ExportButton4.Click += new System.EventHandler(this.ExportButton4_Click);
            // 
            // ExportButton3
            // 
            this.ExportButton3.Location = new System.Drawing.Point(168, 103);
            this.ExportButton3.Name = "ExportButton3";
            this.ExportButton3.Size = new System.Drawing.Size(75, 23);
            this.ExportButton3.TabIndex = 6;
            this.ExportButton3.Text = "Экспорт";
            this.ExportButton3.UseVisualStyleBackColor = true;
            this.ExportButton3.Click += new System.EventHandler(this.ExportButton3_Click);
            // 
            // ExportButton2
            // 
            this.ExportButton2.Location = new System.Drawing.Point(168, 61);
            this.ExportButton2.Name = "ExportButton2";
            this.ExportButton2.Size = new System.Drawing.Size(75, 23);
            this.ExportButton2.TabIndex = 5;
            this.ExportButton2.Text = "Экспорт";
            this.ExportButton2.UseVisualStyleBackColor = true;
            this.ExportButton2.Click += new System.EventHandler(this.ExportButton2_Click);
            // 
            // ExportButton1
            // 
            this.ExportButton1.Location = new System.Drawing.Point(168, 19);
            this.ExportButton1.Name = "ExportButton1";
            this.ExportButton1.Size = new System.Drawing.Size(75, 23);
            this.ExportButton1.TabIndex = 4;
            this.ExportButton1.Text = "Экспорт";
            this.ExportButton1.UseVisualStyleBackColor = true;
            this.ExportButton1.Click += new System.EventHandler(this.ExportButton1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Результаты анализа";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Результаты решения СЛАУ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Глобальная матрица";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Вектор воздействий";
            // 
            // ExportWnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 282);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CloseButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportWnd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Экспорт результатов";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ExportButton5;
        private System.Windows.Forms.Button ExportButton4;
        private System.Windows.Forms.Button ExportButton3;
        private System.Windows.Forms.Button ExportButton2;
        private System.Windows.Forms.Button ExportButton1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}