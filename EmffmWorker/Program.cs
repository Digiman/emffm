﻿using System;
using System.Windows.Forms;
using EMFFM.Manager;
using EMFFM.Manager.Common;
using EMFFM.NetgenUtils.Helpers;
using EmffmWorker.Forms;
using EmffmWorker.Helpers;

namespace EmffmWorker
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            InitApp();

            Application.Run(new MainWnd());
        }

        /// <summary>
        /// Начальная инициализация приложения и библиотек.
        /// </summary>
        private static void InitApp()
        {
            // TODO: написать код для инициализации приложения и начальных его параметров.
            
            Globals.Args = new InputArgs();
            Globals.Args.NetgenParams = AppHelper.ReadConfigValue("NetgenParams");
            Globals.Variables = VariablesHelper.Load(Globals.Args.NetgenParams);
        }
    }
}
