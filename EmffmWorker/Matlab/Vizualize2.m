% ����������� ������ ��� ������� ������ � ������������ ������� (������
% ������) � ������������ - ���������� ������� ������
function Vizualize2(plot, xfile, yfile, lenght)

    % ������ ���������� ������� �� ����� ����������
    function [matrix] = ImportMatrix(filename, size)
        matrix = zeros(size, size);
        file = fopen(filename,'r');
        [matrix, size] = fscanf(file,'%f',[size, size]);
        fclose(file);
    end
        
    % ������ ������� �� ���������� �����
    function [array] = ImportArray(filename, arraySize)
        array = zeros(arraySize,1);
        file = fopen(filename,'r');
        array = fscanf(file,'%f');
        fclose(file);
    end

    % main part
    matrix = ImportMatrix(plot, lenght);
    x = ImportArray(xfile, lenght);
    y = ImportArray(yfile, lenght);
    surf(x, y, matrix);
end