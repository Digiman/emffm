% ����������� ������ ��� ������� ������ � ������������ ������� (������
% ������) � ������������ (� ���������) - ���������� ������� ������
function VizualizeAnimation(xfile, yfile, lenght, aniFile)

    % ������ ���������� ������� �� ����� ����������
    function [matrix] = ImportMatrix(filename, size)
        matrix = zeros(size, size);
        file = fopen(filename,'r');
        [matrix, size] = fscanf(file,'%f',[size, size]);
        fclose(file);
    end
        
    % ������ ������� �� ���������� �����
    function [array] = ImportArray(filename, arraySize)
        array = zeros(arraySize,1);
        file = fopen(filename,'r');
        array = fscanf(file,'%f');
        fclose(file);
    end

    function Animate(xfile, yfile, lenght, aniFile)
        x = ImportArray(xfile, lenght);
        y = ImportArray(yfile, lenght);
        
        % �������� ������ ��� �������� ������� (������ ������) � ���� �������
        plot='plot-0.txt';
        matrix = ImportMatrix(plot, lenght);
        % ���������� ������� � ���������� ��� ��� ��������
        figure('Renderer','zbuffer');
        surf(x,y,matrix)
        axis tight
        set(gca,'nextplot','replacechildren')
        f = getframe;
        [im,map] = rgb2ind(f.cdata,256);
        im(:,:,1,lenght) = im;
        for k = 1:lenght-1
            % �������� ������ ��� �������� ������� � ���� �������
            plot=strcat('plot-', int2str(k), '.txt');
            matrix = ImportMatrix(plot, lenght);
            surf(x,y,matrix)
            f = getframe;
            im(:,:,1,k) = rgb2ind(f.cdata, map);
            %imwrite(f.cdata,strcat('file',int2str(k),'.png'));
        end
        imwrite(im,map,aniFile,'DelayTime',0,'LoopCount',inf)
    end
    
    % main part
    Animate(xfile, yfile, lenght, aniFile)
end