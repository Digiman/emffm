﻿namespace EmffmWorker.Elements
{
    /// <summary>
    /// Класс для хранение свдения для задания масштабов на графике (от минимального до максимального).
    /// </summary>
    class MinMaxData
    {
        public double Min { get; set; }
        public double Max { get; set; }
    }
}
