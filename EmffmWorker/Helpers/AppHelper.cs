﻿using System.Configuration;
using System.Windows.Forms;

namespace EmffmWorker.Helpers
{
    /// <summary>
    /// Класс для работы с кинфигурацией приложения, размещенной в файле App.config.
    /// </summary>
    public static class AppHelper
    {
        /// <summary>
        /// Чтение значение параметра по его названию.
        /// </summary>
        /// <param name="valueName">Название конфигурационного параметра.</param>
        /// <returns>Возвращает текстовое значение конфигурации.</returns>
        public static string ReadConfigValue(string valueName)
        {
            return ConfigurationManager.AppSettings[valueName];
        }

        /// <summary>
        /// Получение каталога, в котором размещено приложения (откуда оно запущено).
        /// </summary>
        public static string GetApplicationPath
        {
            get { return Application.StartupPath; } 
        }
    }
}
