﻿using System;
using EMFFM.Common.Enums;

namespace EMFFM.Common.Mesh.Elements
{
    /// <summary>
    /// Описание точки в пространстве
    /// </summary>
    public class Point : IEquatable<Point>
    {
        /// <summary>
        /// Координата X.
        /// </summary>
        public double X;

        /// <summary>
        /// Координата Y.
        /// </summary>
        public double Y;

        /// <summary>
        /// Координата Z.
        /// </summary>
        public double Z;

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию для "пустого" объекта.
        /// </summary>
        public Point() { }

        /// <summary>
        /// Инициализация точки 2D.
        /// </summary>
        /// <param name="x">Координата X.</param>
        /// <param name="y">Коордианта Y.</param>
        public Point(double x, double y)
        {
            X = x;
            Y = y;
            Z = 0;
        }

        /// <summary>
        /// Инициализация точки в 3D.
        /// </summary>
        /// <param name="x">Координата X.</param>
        /// <param name="y">Координата Y.</param>
        /// <param name="z">Координата Z.</param>
        public Point(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Копирование координат точки.
        /// </summary>
        /// <param name="p">Точка.</param>
        public Point(Point p)
        {
            X = p.X;
            Y = p.Y;
            Z = p.Z;
        }

        #endregion

        public bool Equals(Point other)
        {
            return this.X != other.X || this.Y != other.Y || this.Z != other.Z;
        }

        public override bool Equals(object obj)
        {
            var p = (Point) obj;
            return this.X != p.X || this.Y != p.Y || this.Z != p.Z;
        }

        public override string ToString()
        {
            return String.Format("( {0}, {1}, {2} )", X, Y, Z);
        }

        public double this[int ind]
        {
            get
            {
                double result = 0;
                switch (ind)
                {
                    case 0:
                        result = X;
                        break;
                    case 1:
                        result = Y;
                        break;
                    case 2:
                        result = Z;
                        break;
                }
                return result;
            }
        }

        #region Перегрузка операторов

        public static bool operator ==(Point lhs, Point rhs)
        {
            if (lhs.X == rhs.X && lhs.Y == rhs.Y && lhs.Z == rhs.Z)
                return true;
            else
                return false;
        }

        public static bool operator !=(Point lhs, Point rhs)
        {
            return !(lhs == rhs);
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z);
        }

        public static Point operator -(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
        }

        public static Point operator *(double value, Point p)
        {
            return new Point(value*p.X, value*p.Y, value*p.Z);
        }

        #endregion

        /// <summary>
        /// Возвращается значение типа перечисления, указывающее на положение точки относительно отрезка.
        /// </summary>
        /// <param name="p0">Точка 1 отрезка.</param>
        /// <param name="p1">Точка 2 отрезка.</param>
        /// <returns>Возвращает оиентацйию точки относитлеьно отрезка.</returns>
        public Orientations Classify(Point p0, Point p1)
        {
            Point p2 = this;

            Point a = p1 - p0;
            Point b = p2 - p0;
            double sa = a.X*b.Y - b.X*a.Y;
            
            if (sa > 0.0)
                return Orientations.LEFT;
            if (sa < 0.0)
                return Orientations.RIGHT;
            if ((a.X*b.X < 0.0) || (a.Y*b.Y < 0.0))
                return Orientations.BEHIND;
            if (a.Length() < b.Length())
                return Orientations.BEYOND;
            if (p0 == p2)
                return Orientations.ORIGIN;
            if (p1 == p2)
                return Orientations.DESTINATION;
            return Orientations.BETWEEN;
        }

        /// <summary>
        /// Определение длины вектора.
        /// </summary>
        /// <returns>Возвращает длину текущего вектора.</returns>
        private double Length()
        {
            return System.Math.Sqrt(X*X + Y*Y);
        }

        /// <summary>
        /// Играет роль хэш-функции для определенного типа. 
        /// </summary>
        /// <returns>
        /// Хэш-код для текущего объекта (класса точки).
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            return (X + Y + Z).GetHashCode();
        }
    }
}
