﻿using System;

namespace EMFFM.Common.Mesh.Elements
{
    /// <summary>
    /// Описание узла сетки
    /// </summary>
    public class Node : IEquatable<Node>
    {
        /// <summary>
        /// Координаты узла.
        /// </summary>
        public Point Coord { get; set; }

        /// <summary>
        /// Номер узла (глобальный).
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Локальный номер узла (в элементе).
        /// </summary>
        public int LocalNumber { get; set; }

        /// <summary>
        /// Граничный ли узел?
        /// </summary>
        /// <remarks>Может использоваться для реализации узлового МКЭ, когда работа ведется с узлами.</remarks>
        public bool IsBoundary { get; set; }

        /// <summary>
        /// Номер граничного условия.
        /// </summary>
        /// <remarks>
        /// 1. Используется при задании граничных условий на ребрах.
        /// 2. Может использоваться для реализации узлового МКЭ, когда работа ведется с узлами.
        /// </remarks>
        public int BoundaryNumber { get; set; }

        #region Конструкторы

        /// <summary>
        /// Инициализация узла
        /// </summary>
        /// <param name="p">Координаты узла</param>
        /// <param name="num">Номер узла</param>
        public Node(Point p, int num)
        {
            Coord = p;
            Number = num;
        }

        /// <summary>
        /// Инициализация узла
        /// </summary>
        /// <param name="p">Координаты узла</param>
        /// <param name="num">Номер узла</param>
        /// <param name="localNumber">Локальный номер элемента.</param>
        public Node(Point p, int num, int localNumber)
        {
            Coord = p;
            Number = num;
            LocalNumber = localNumber;
        }

        /// <summary>
        /// Инициализация узла в 2D
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Коордианата Y</param>
        /// <param name="num">Номер узла</param>
        public Node(double x, double y, int num)
        {
            Coord = new Point(x, y, 0);
            Number = num;
        }

        /// <summary>
        /// Инициализация узла в 3D
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="z">Координата Z</param>
        /// <param name="num">Номер узла</param>
        public Node(double x, double y, double z, int num)
        {
            Coord = new Point(x, y, z);
            Number = num;
        }

        /// <summary>
        /// Копирование узла
        /// </summary>
        /// <param name="node">Узел</param>
        public Node(Node node)
        {
            Coord = node.Coord;
            Number = node.Number;
            LocalNumber = node.LocalNumber;
        }

        /// <summary>
        /// Копирование узла
        /// </summary>
        /// <param name="node">Узел</param>
        /// <param name="localNumber">Локальный номер узла</param>
        public Node(Node node, int localNumber)
        {
            Coord = node.Coord;
            Number = node.Number;
            LocalNumber = localNumber;
        }

        #endregion

        public bool Equals(Node other)
        {
            return this.Coord == other.Coord && this.Number == other.Number;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Node);
        }

        public override int GetHashCode()
        {
            return Number.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0} ({1}) {2}", Number, LocalNumber, Coord.ToString());
        }

        #region Перегрузка операторов

        public static bool operator ==(Node lhs, Node rhs)
        {
            if (lhs.Coord == rhs.Coord && lhs.Number == rhs.Number)
                return true;
            else
                return false;
        }


        public static bool operator !=(Node lhs, Node rhs)
        {
            return !(lhs == rhs);
        }

        #endregion
    }
}
