﻿using System;
using EMFFM.Common.Math;

namespace EMFFM.Common.Mesh.Elements
{
    /// <summary>
    /// Описание ребра (стороны элемента).
    /// </summary>
    /// <remarks>Используется для рализации векторного МКЭ, где главным элементом является ребро, а не узел.</remarks>
    public class Edge : IEquatable<Edge>, IComparable<Edge>
    {
        /// <summary>
        /// Вершина 1 для ребра.
        /// </summary>
        public Node Vertex1 { get; set; }

        /// <summary>
        /// Вершина 2 для ребра.
        /// </summary>
        public Node Vertex2 { get; set; }

        /// <summary>
        /// Номер ребра (глобальный).
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Локальный номер ребра (в элементе).
        /// </summary>
        public int LocalNumber { get; set; }

        /// <summary>
        /// Граничное ли ребро?
        /// </summary>
        public bool IsBoundary { get; set; }

        /// <summary>
        /// Номер граничного условия.
        /// </summary>
        /// <remarks>Используется при задании граничных условий на ребрах.</remarks>
        public int BoundaryNumber { get; set; }

        #region Конструкторы

        /// <summary>
        /// Инициализация ребра для двух узлов.
        /// </summary>
        /// <param name="n1">Узел 1.</param>
        /// <param name="n2">Узел 2.</param>
        public Edge(Node n1, Node n2)
        {
            Vertex1 = n1;
            Vertex2 = n2;
            Number = -1; // ребро не пронумеровано!
        }

        /// <summary>
        /// Инициализация ребра с параметрами.
        /// </summary>
        /// <param name="n1">Узел 1.</param>
        /// <param name="n2">Узел 2.</param>
        /// <param name="number">Номер ребра (глобальный).</param>
        public Edge(Node n1, Node n2, int number)
        {
            Vertex1 = n1;
            Vertex2 = n2;
            Number = number;
        }

        /// <summary>
        /// Инициализация ребра с параметрами.
        /// </summary>
        /// <param name="n1">Узел 1.</param>
        /// <param name="n2">Узел 2.</param>
        /// <param name="isBoundary">Является ли ребро граничным?</param>
        /// <param name="boundNumber">Номер границы (граничного усллвия).</param>
        public Edge(Node n1, Node n2, bool isBoundary, int boundNumber)
        {
            Vertex1 = n1;
            Vertex2 = n2;
            Number = -1; // ребро не нумеровано!
            IsBoundary = isBoundary;
            BoundaryNumber = boundNumber;
        }

        /// <summary>
        /// Инициализация ребра с параметрами.
        /// </summary>
        /// <param name="n1">Узел 1.</param>
        /// <param name="n2">Узел 2.</param>
        /// <param name="number">Номер ребра.</param>
        /// <param name="isBoundary">Является ли ребро граничным?</param>
        /// <param name="boundNumber">Номер границы (граничное условие).</param>
        public Edge(Node n1, Node n2, int number, bool isBoundary, int boundNumber)
        {
            Vertex1 = n1;
            Vertex2 = n2;
            Number = number;
            IsBoundary = isBoundary;
            BoundaryNumber = boundNumber;
        }

        /// <summary>
        /// Копирование данных ребер.
        /// </summary>
        /// <param name="e">Ребро для копирвоания.</param>
        public Edge(Edge e)
        {
            Vertex1 = e.Vertex1;
            Vertex2 = e.Vertex2;
            Number = -1; // ребро не пронумеровано!
        }

        #endregion

        /// <summary>
        /// Занумеровано ли ребро?
        /// </summary>
        /// <remarks>Позволяет определить, дан ли номер ребру или нет?</remarks>
        public bool IsNumerated
        {
            get
            {
                // если занумерован, то будет номер его на -1
                return (Number != -1);
            }
        }

        /// <summary>
        /// Указывает, равен ли текущий объект другому объекту того же типа.
        /// </summary>
        /// <returns>
        /// Возвращает true, если текущий объект равен параметру <paramref name="other"/>, в противном случае — false.
        /// </returns>
        /// <param name="other">Объект, который требуется сравнить с данным объектом.</param>
        public bool Equals(Edge other)
        {
            // NOTE: сравнение для выполнения операции Distinct (выбор уникальных значений)
            return ((Vertex1 == other.Vertex1) && (Vertex2 == other.Vertex2) ||
                    (Vertex1 == other.Vertex2) && (Vertex2 == other.Vertex1));
        }

        /// <summary>
        /// Определяет, равен ли заданный объект <see cref="T:System.Object"/> текущему объекту <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// Значение true, если заданный объект <see cref="T:System.Object"/> равен текущему объекту <see cref="T:System.Object"/>; в противном случае — значение false.
        /// </returns>
        /// <param name="obj">Элемент <see cref="T:System.Object"/>, который требуется сравнить с текущим элементом <see cref="T:System.Object"/>. </param><filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            var other = (Edge) obj;
            return ((Vertex1 == other.Vertex1) && (Vertex2 == other.Vertex2) ||
                    (Vertex1 == other.Vertex2) && (Vertex2 == other.Vertex1));
        }

        /// <summary>
        /// Сравнивает текущий объект с другим объектом того же типа.
        /// </summary>
        /// <returns>
        /// Значение, указывающее, каков относительный порядок сравниваемых объектов.Расшифровка возвращенных значений приведена ниже.Значение Описание Меньше нуля Значение этого объекта меньше значения параметра <paramref name="other"/>.Нуль Значение этого объекта равно значению параметра <paramref name="other"/>. Больше нуля. Значение этого объекта больше значения параметра <paramref name="other"/>. 
        /// </returns>
        /// <param name="other">Объект, который требуется сравнить с данным объектом.</param>
        public int CompareTo(Edge other)
        {
            if (Number > other.Number)
                return 1;
            else if (Number < other.Number)
                return -1;
            else
                return 0;
        }

        /// <summary>
        /// Играет роль хэш-функции для определенного типа. 
        /// </summary>
        /// <returns>
        /// Хэш-код для текущего объекта <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            return (Vertex1.Number + Vertex2.Number).GetHashCode();
        }

        /// <summary>
        /// Возвращает объект <see cref="T:System.String"/>, который представляет текущий объект <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// Объект <see cref="T:System.String"/>, представляющий текущий объект <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            return String.Format("{0} [ {1} -> {2} ]", Number, Vertex1.ToString(), Vertex2.ToString());
        }

        /// <summary>
        /// Вычисление длины ребра (стороны)
        /// </summary>
        /// <returns>Возвращает вычисленное значение длины</returns>
        public double GetLenght()
        {
            return GeometryHelper.GetDistance(Vertex1.Coord, Vertex2.Coord);
        }

        /// <summary>
        /// Вычисление длины ребра (стороны)
        /// </summary>
        /// <param name="coFactor">Поправка размеров относительно 1 метра (например, для нм => 1E-9)</param>
        /// <returns>Возвращает вычисленное значение длины</returns>
        public double GetLenght(double coFactor)
        {
            return GeometryHelper.GetDistance(Vertex1.Coord, Vertex2.Coord) * coFactor;
        }

        #region Перегрузка операторов

        public static bool operator ==(Edge lhs, Edge rhs)
        {
            // NOTE: сравниваем две вершины, причем не важно какая из них 1-ая или 2-ая
            if (((lhs.Vertex1 == rhs.Vertex1) && (lhs.Vertex2 == rhs.Vertex2)) ||
                ((lhs.Vertex1 == rhs.Vertex2) && (lhs.Vertex2 == rhs.Vertex1)))
                return true;
            else
                return false;
        }

        public static bool operator !=(Edge lhs, Edge rhs)
        {
            return !(lhs == rhs);
        }

        #endregion
    }
}
