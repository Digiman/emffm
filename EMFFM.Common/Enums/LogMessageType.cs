﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы сообщений в логе
    /// </summary>
    public enum LogMessageType
    {
        /// <summary>
        /// Информационное сообщение
        /// </summary>
        Info,
        /// <summary>
        /// Предупреждение
        /// </summary>
        Warning,
        /// <summary>
        /// Отладочное сообщение
        /// </summary>
        Debug,
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        Error
    }
}
