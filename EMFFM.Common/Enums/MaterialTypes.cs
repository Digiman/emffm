﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы материалов для частиц
    /// </summary>
    /// <remarks>Используются для законов Лоренца Дебая Друде</remarks>
    public enum MaterialTypes
    {
        Ag,
        Al,
        Au,
        Cu,
        Cr,
        Ni,
        W,
        Ti,
        Be,
        Pd,
        Pt,
        /// <summary>
        /// pure water (triply distilled)
        /// </summary>
        H2O
    }
}
