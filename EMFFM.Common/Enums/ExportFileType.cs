﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Тип форматов для экспорта.
    /// </summary>
    public enum ExportFileType
    {
        /// <summary>
        /// Файл в формате XML.
        /// </summary>
        XML,
        /// <summary>
        /// Текстовый файл.
        /// </summary>
        TXT,
        /// <summary>
        /// Файл в формате HDF 5.
        /// </summary>
        HDF5
    }
}
