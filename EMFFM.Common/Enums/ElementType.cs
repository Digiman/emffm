﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Тип элементов сетки.
    /// </summary>
    public enum ElementType
    {
        /// <summary>
        /// Треугольник.
        /// </summary>
        Triangle,
        /// <summary>
        /// Прямоугольник
        /// </summary>
        Rectangle,
        /// <summary>
        /// Параллелепипед.
        /// </summary>
        Parallelepiped,
        /// <summary>
        /// Тетраэдр.
        /// </summary>
        Tetrahedron
    }
}
