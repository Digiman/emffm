﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы значений
    /// </summary>
    public enum ValueTypes
    {
        /// <summary>
        /// Комплексное значение
        /// </summary>
        Complex,
        /// <summary>
        /// Скалярное значение
        /// </summary>
        Scalar
    }
}
