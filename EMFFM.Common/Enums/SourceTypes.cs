﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы источников электромагнитного излучения
    /// </summary>
    /// <remarks>По характеру</remarks>
    public enum SourceTypes
    {
        Simple, Electric, Magnetic
    }
}
