﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Направления для единичного вектора (вдоль одной из осей в декартовой системе координат). 
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Вдоль оси OX.
        /// </summary>
        X, 
        /// <summary>
        /// Вдоль оси OY.
        /// </summary>
        Y,
        /// <summary>
        /// Вдоль оси OZ.
        /// </summary>
        Z
    }
}
