﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы ориентации
    /// </summary>
    public enum Orientations : byte
    {
        LEFT = 1,
        RIGHT = 2,
        BEYOND = 3,
        BEHIND = 4,
        BETWEEN = 5,
        ORIGIN = 6,
        DESTINATION = 7
    }
}
