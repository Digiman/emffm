﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Направления поля
    /// </summary>
    public enum FieldDirection
    {
        X, Y, Z, 
        All // по всем направлениям
    }
}
