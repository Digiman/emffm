﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы моделей для определения параметров частиц
    /// </summary>
    public enum ModelTypes
    {
        /// <summary>
        /// Модель Лоренца-Друде
        /// </summary>
        LD, 
        /// <summary>
        /// Модуль Друде
        /// </summary>
        D
    }
}
