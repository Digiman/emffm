﻿namespace EMFFM.Common.Enums
{
    /// <summary>
    /// Типы решателей
    /// По областям рассмотрения задачи
    /// </summary>
    public enum SolverTypes
    {
        FrequencyDomain,
        TimeDomain
    }
}
