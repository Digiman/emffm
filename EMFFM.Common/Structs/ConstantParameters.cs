﻿namespace EMFFM.Common.Structs
{
    /// <summary>
    /// Константные физические параметры
    /// </summary>
    public struct ConstantParameters
    {
        /// <summary>
        /// Скорость света в вакууме (м/с)
        /// </summary>
        public const double LightSpeed = 299792458;

        /// <summary>
        /// Магнитная постоянная
        /// </summary>
        public const double Mue0 = 4*System.Math.PI*1E-7;

        /// <summary>
        /// Электрическая постоянная
        /// </summary>
        public const double Eps0 = 1/(LightSpeed*LightSpeed*Mue0);

        /// <summary>
        /// Импеданс поверхности в свободном пространстве
        /// </summary>
        public static double Z0 = System.Math.Sqrt(Mue0/Eps0);

        public const double Twopic = 1.883651567308853e+09; // twopic=2*pi*c where c is speed of light
        public const double Invsqrt2 = 0.707106781186547; // 1/sqrt(2)
        public const double Ehbar = 1.519250349719305e+15; // e/hbar where hbar=h/(2*pi) and e=1.6e-19 
    }
}
