﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Structs;

namespace EMFFM.Common.Materials
{
    /// <summary>
    /// Класс для вычисления параметров по правилам Лоренца Друде
    /// </summary>
    public class LorenzDrude
    {
        private readonly double _omegalight; // angular frequency of light (rad/s)
        private const double Invsqrt2 = 0.707106781186547; // 1/sqrt(2)

        /// <summary>
        /// Длина волны.
        /// </summary>
        private double _lambda;

        /// <summary>
        /// Список материалов.
        /// </summary>
        private readonly List<ComplexMaterial> _materials;

        /// <summary>
        /// Иниуиализация объекта.
        /// </summary>
        /// <param name="lambda">Длина волны падающего света.</param>
        public LorenzDrude(double lambda)
        {
            _lambda = lambda;
            _omegalight = ConstantParameters.Twopic * (System.Math.Pow(lambda, -1)); // angular frequency of light (rad/s)
            _materials = new ComplexMaterials().Materials;
        }

        /// <summary>
        /// Вычисление магнитной постоянной.
        /// </summary>
        /// <param name="material">Материал частицы.</param>
        /// <param name="model">Модель рамсчета.</param>
        /// <returns>Возвращает значение постоянной (epsilon).</returns>
        public System.Numerics.Complex Calculate(MaterialTypes material, ModelTypes model)
        {
            System.Numerics.Complex epsilon = new System.Numerics.Complex(), epsilon_D, epsilon_L;

            epsilon_D = CalculateEpsilonD(GetMaterial(material));

            switch (model)
            {
                case ModelTypes.D: // модель Друде
                    epsilon = epsilon_D;
                    break;
                case ModelTypes.LD: // модель лоренца Друде
                    epsilon_L = CalculateEpsilonL(GetMaterial(material));
                    epsilon = epsilon_D + epsilon_L;
                    break;
            }

            return epsilon;
        }

        /// <summary>
        /// Вычисление диэлектрической приницаемости для условия Друде
        /// </summary>
        /// <param name="material">Материал</param>
        /// <returns>Возвращает значение диэлектрической проницаемости</returns>
        private System.Numerics.Complex CalculateEpsilonD(ComplexMaterial material)
        {
            var tmp1 = new System.Numerics.Complex(System.Math.Pow(_omegalight, 2), material.Gamma[0] * _omegalight);
            tmp1 = System.Numerics.Complex.Pow(tmp1, -1);
            var tmp2 = material.F[0]*System.Math.Pow(material.Omegap, 2);
            return 1 - (tmp2*tmp1);
        }

        /// <summary>
        /// Вычисление диэлектрической проницаемости для условия Лоренца
        /// </summary>
        /// <param name="material">Материал</param>
        /// <returns>Возвращает значение диэлектрической проницаемости</returns>
        private System.Numerics.Complex CalculateEpsilonL(ComplexMaterial material)
        {
            var result = System.Numerics.Complex.Zero;
            for (int i = 1; i < material.Order; i++)
            {
                var tmp = new System.Numerics.Complex(System.Math.Pow(material.Omega[i], 2) - System.Math.Pow(_omegalight, 2),
                                      -material.Gamma[i]*_omegalight);
                tmp = System.Numerics.Complex.Pow(tmp, -1);
                var tmp2 = material.F[i]*System.Math.Pow(material.Omegap, 2);
                result += tmp*tmp2;
            }

            return result;
        }

        /// <summary>
        /// Расчет комплексного преломления частицы
        /// </summary>
        /// <param name="epsilon">Электрическая постоянная</param>
        /// <returns>Возвращает значение преломления</returns>
        public System.Numerics.Complex CalculateRefractiveIndex(System.Numerics.Complex epsilon)
        {
            var n = System.Math.Sqrt(System.Math.Sqrt(System.Math.Pow(epsilon.Real, 2) + System.Math.Pow(epsilon.Imaginary, 2)) + epsilon.Real);
            var k = System.Math.Sqrt(System.Math.Sqrt(System.Math.Pow(epsilon.Real, 2) + System.Math.Pow(epsilon.Imaginary, 2)) - epsilon.Real);
            return new System.Numerics.Complex(n, k)*Invsqrt2;
        }

        /// <summary>
        /// Получение материала по его типу
        /// </summary>
        /// <param name="type">Тип материала</param>
        /// <returns>Возвращает параметры материала</returns>
        private ComplexMaterial GetMaterial(MaterialTypes type)
        {
            return _materials.SingleOrDefault(mat => mat.Type == type);
        }
    }
}
