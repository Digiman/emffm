﻿using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Input.Elements;
using EMFFM.Common.Math;
using EMFFM.Common.Structs;

namespace EMFFM.Common.Materials
{
    /// <summary>
    /// Класс для построение списка материалов и задания их параметров
    /// Материалы (MaterialTypes):
    ///     - 'Ag'  = silver
    ///     - 'Al'  = aluminum
    ///     - 'Au'  = gold
    ///     - 'Cu'  = copper
    ///     - 'Cr'  = chromium
    ///     - 'Ni'  = nickel
    ///     - 'W'   = tungsten
    ///     - 'Ti'  = titanium
    ///     - 'Be'  = beryllium
    ///     - 'Pd'  = palladium
    ///     - 'Pt'  = platinum
    ///     - 'H2O' = pure water (triply distilled)
    /// Модель: 
    ///     Choose 'LD' or 'D' for Lorentz-Drude or Drude model.
    /// </summary>
    public class ComplexMaterials
    {
        /// <summary>
        /// Список материалов.
        /// </summary>
        private readonly List<ComplexMaterial> _materials;

        /// <summary>
        /// Инициализация сложных материалов.
        /// </summary>
        public ComplexMaterials()
        {
            _materials = Init();
        }

        /// <summary>
        /// Получение списка сложных материалов.
        /// </summary>
        public List<ComplexMaterial> Materials
        {
            get { return _materials; }
        }

        /// <summary>
        /// Построение списка материалов и их параметров
        /// </summary>
        /// <returns>Возвращает список параметров материалов</returns>
        private List<ComplexMaterial> Init()
        {
            var list = new List<ComplexMaterial>();

            list.Add(InitAg());
            list.Add(InitAl());
            list.Add(InitAu());
            list.Add(InitCu());
            list.Add(InitCr());
            list.Add(InitNi());
            list.Add(InitW());
            list.Add(InitTi());
            list.Add(InitBe());
            list.Add(InitPd());
            list.Add(InitPt());
            list.Add(InitH2O());

            return list;
        }

        private ComplexMaterial InitAg()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Ag;
            mat.Omegap = 9.01*ConstantParameters.Ehbar;
            mat.F = new[] {0.845, 0.065, 0.124, 0.011, 0.840, 5.646};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.048, 3.886, 0.452, 0.065, 0.916, 2.419}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.816, 4.481, 8.185, 9.083, 20.29}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitAl()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Al;
            mat.Omegap = 14.98*ConstantParameters.Ehbar;
            mat.F = new[] {0.523, 0.227, 0.050, 0.166, 0.030};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.047, 0.333, 0.312, 1.351, 3.382}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.162, 1.544, 1.808, 3.473}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitAu()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Au;
            mat.Omegap = 9.03*ConstantParameters.Ehbar;
            mat.F = new[] {0.760, 0.024, 0.010, 0.071, 0.601, 4.384};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.053, 0.241, 0.345, 0.870, 2.494, 2.214}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.415, 0.830, 2.969, 4.304, 13.32}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitCu()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Cu;
            mat.Omegap = 10.83*ConstantParameters.Ehbar;
            mat.F = new[] {0.575, 0.061, 0.104, 0.723, 0.638};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.030, 0.378, 1.056, 3.213, 4.305}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.291, 2.957, 5.300, 11.18}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitCr()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Cr;
            mat.Omegap = 10.75*ConstantParameters.Ehbar;
            mat.F = new[] {0.168, 0.151, 0.150, 1.149, 0.825};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.047, 3.175, 1.305, 2.676, 1.335}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.121, 0.543, 1.970, 8.775}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitNi()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Ni;
            mat.Omegap = 15.92*ConstantParameters.Ehbar;
            mat.F = new[] {0.096, 0.100, 0.135, 0.106, 0.729};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.048, 4.511, 1.334, 2.178, 6.292}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.174, 0.582, 1.597, 6.089}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitW()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.W;
            mat.Omegap = 13.22*ConstantParameters.Ehbar;
            mat.F = new[] {0.206, 0.054, 0.166, 0.706, 2.590};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.064, 0.530, 1.281, 3.332, 5.836}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 1.004, 1.917, 3.580, 7.498}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitTi()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Ti;
            mat.Omegap = 7.29*ConstantParameters.Ehbar;
            mat.F = new[] {0.148, 0.899, 0.393, 0.187, 0.001};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.082, 2.276, 2.518, 1.663, 1.762}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.777, 1.545, 2.509, 1.943}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitBe()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Be;
            mat.Omegap = 18.51*ConstantParameters.Ehbar;
            mat.F = new[] {0.084, 0.031, 0.140, 0.530, 0.130};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.035, 1.664, 3.395, 4.454, 1.802}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.100, 1.032, 3.183, 4.604}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitPd()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Pd;
            mat.Omegap = 9.72*ConstantParameters.Ehbar;
            mat.F = new[] {0.330, 0.649, 0.121, 0.638, 0.453};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.008, 2.950, 0.555, 4.621, 3.236}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.336, 0.501, 1.659, 5.715}, ConstantParameters.Ehbar);
            return mat;
        }

        private ComplexMaterial InitPt()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.Pt;
            mat.Omegap = 9.59*ConstantParameters.Ehbar;
            mat.F = new[] {0.333, 0.191, 0.659, 0.547, 3.576};
            mat.Gamma = ArrayHelper.Multiply(new[] {0.080, 0.517, 1.838, 3.668, 8.517}, ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0.000, 0.780, 1.314, 3.141, 9.249}, ConstantParameters.Ehbar);
            return mat;
        }

        public ComplexMaterial InitH2O()
        {
            var mat = new ComplexMaterial();
            mat.Type = MaterialTypes.H2O;
            mat.Omegap = 9.59*ConstantParameters.Ehbar;
            mat.F = new[] {0, 1.0745e-05, 3.1155e-03, 1.6985e-04, 1.1795e-02, 1.7504e+02};
            mat.Gamma = ArrayHelper.Multiply(new[] {0, 0.0046865, 0.059371, 0.0040546, 0.037650, 7.66167},
                ConstantParameters.Ehbar);
            mat.Omega = ArrayHelper.Multiply(new[] {0, 0.013691, 0.069113, 0.21523, 0.40743, 15.1390},
                ConstantParameters.Ehbar);
            return mat;
        }
    }
}
