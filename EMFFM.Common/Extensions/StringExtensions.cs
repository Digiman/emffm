﻿using System.Linq;

namespace EMFFM.Common.Extensions
{
    /// <summary>
    /// Расширяющие методы для строк String
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Корректировка строки с вещественным числом
        /// Замена . на , если необходимо
        /// </summary>
        /// <param name="str">Входная строка</param>
        /// <returns>Возвращает скорректированную строку</returns>
        public static string CorrectDoubleStringValue(this string str)
        {
            if (str.Contains('.'))
            {
                return str.Replace('.', ',');
            }
            return str;
        }

        /// <summary>
        /// Корректировка строки с вещественным числом
        /// Замена , на . если необходимо
        /// </summary>
        /// <param name="str">Входная строка</param>
        /// <returns>Возвращает скорректированную строку</returns>
        public static string CorrectDoubleStringValueForMatlab(this string str)
        {
            if (str.Contains(','))
            {
                return str.Replace(',', '.');
            }
            return str;
        }
    }
}
