﻿using System;

namespace EMFFM.Common.Math
{
    /// <summary>
    /// Вспомогательный класс для работы с массивами
    /// </summary>
    public static class ArrayHelper
    {
        /// <summary>
        /// Инициализация вектора с постоянным значением
        /// </summary>
        /// <typeparam name="TType">Тип значений вектора</typeparam>
        /// <param name="size">Размер массива</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает созданный массив</returns>
        public static TType[] InitValueVector<TType>(int size, TType value)
        {
            var array = new TType[size];

            for (int i = 0; i < size; i++)
            {
                array[i] = value;
            }

            return array;
        }

        /// <summary>
        /// Вычисление разницы между двумя миссивами
        /// </summary>
        /// <param name="array1">Базовый массив (вектор)</param>
        /// <param name="array2">Отнимаемый массив (вектор)</param>
        /// <returns>Возвращает новый массив в результатами поэлементной разности</returns>
        /// <remarks>Отнимает значения одного массива из другого</remarks>
        public static double[] Difference(double[] array1, double[] array2)
        {
            if (array1.Length != array2.Length)
            {
                throw new Exception("Нельзя вычислить разницу между массивами разной размерности!");
            }

            var result = new double[array1.Length];

            for (int i = 0; i < array1.Length; i++)
            {
                result[i] = array1[i] - array2[i];
            }

            return result;
        }

        /// <summary>
        /// Перемножение почленное элементов массива на значение
        /// </summary>
        /// <param name="array">Массив</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает обработанный массив</returns>
        public static double[] Multiply(double[] array, double value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = value * array[i];
            }

            return array;
        }
    }
}
