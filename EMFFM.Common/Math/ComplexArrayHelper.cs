﻿using System.Numerics;

namespace EMFFM.Common.Math
{
    /// <summary>
    /// Вспомогательный класс для работы с комплексными векторами (одномерными массивами)
    /// </summary>
    public static class ComplexArrayHelper
    {
        /// <summary>
        /// Инициализация вектора с постоянным значением
        /// (в комплексных числах)
        /// </summary>
        /// <param name="size">Размер массива</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает созданный массив</returns>
        public static Complex[] InitValueVector(int size, double value)
        {
            var array = new Complex[size];

            for (int i = 0; i < size; i++)
            {
                array[i] = new Complex(value, 0);
            }

            return array;
        }

        /// <summary>
        /// Инициализация вектора с постоянным значением
        /// (в комплексных числах)
        /// </summary>
        /// <param name="size">Размер массива</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает созданный массив</returns>
        public static Complex[] InitValueVector(int size, Complex value)
        {
            var array = new Complex[size];

            for (int i = 0; i < size; i++)
            {
                array[i] = value;
            }

            return array;
        }

        /// <summary>
        /// Вычисление разницы между данными двух массивов.
        /// </summary>
        /// <param name="array1">Массив 1.</param>
        /// <param name="array2">Массив 2.</param>
        /// <returns>Возвращает новый массив с данными, соответствующие поэлементной разницt входных массивов (array1-array2),</returns>
        /// <remarks>Используется для расчета разницы полей.</remarks>
        public static Complex[] Difference(Complex[] array1, Complex[] array2)
        {
            var result = new Complex[array1.Length];

            int size = array1.Length;
            for (int i = 0; i < size; i++)
            {
                result[i] = array1[i] - array2[i];
            }
            return result;
        }
    }
}
