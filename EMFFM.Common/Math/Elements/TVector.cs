﻿using System;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Common.Math.Elements
{
    /// <summary>
    /// Вектор
    /// </summary>
    public class TVector
    {
        protected double _x, _y, _z;

        #region Конструкторы
        
        public TVector() {}

        /// <summary>
        /// Инициализация вектора
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="z">Координата Z</param>
        public TVector(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        /// <summary>
        /// Копирование векторов
        /// </summary>
        /// <param name="rhs">Векторя для копирования</param>
        public TVector(TVector rhs)
        {
            _x = rhs._x;
            _y = rhs._y;
            _z = rhs._z;
        }

        /// <summary>
        /// Создание вектора из двух точек
        /// </summary>
        /// <param name="p1">Координаты точки 1</param>
        /// <param name="p2">Координаты точки 2</param>
        /// <remarks>Точка 2 базовая!</remarks>
        public TVector(Point p1, Point p2)
        {
            _x = p1.X - p2.X; // x2-x1
            _y = p1.Y - p2.Y; // y2-y1
            _z = p1.Z - p2.Z; // z2-z1
        } 

        #endregion

        public override string ToString()
        {
            return String.Format("( {0}, {1}, {2} )", _x, _y, _z);
        }

        /// <summary>
        /// Смешанное произведение векторов
        /// </summary>
        /// <param name="v1">Вектор 1</param>
        /// <param name="v2">Вектор 2</param>
        /// <param name="v3">Вектор 3</param>
        /// <returns>Возвращает значение смешанного произведения 3-ех векторов</returns>
        public static double VectorMult(TVector v1, TVector v2, TVector v3)
        {
            double[,] mat =
            {
                { v1._x, v1._y, v1._z },
                { v2._x, v2._y, v2._z },
                { v3._x, v3._y, v3._z }
            };

            // вычисляем определитель матрицы
            return MathHelper.Determinant(mat);
        }

        #region Определение операций над векторами
        
        public static TVector operator +(TVector lhs, TVector rhs)
        {
            TVector result = new TVector(lhs);
            result._x += rhs._x;
            result._y += rhs._y;
            result._z += rhs._z;
            return result;
        }

        public static TVector operator -(TVector lhs, TVector rhs)
        {
            TVector result = new TVector(lhs);
            result._x -= rhs._x;
            result._y -= rhs._y;
            result._z -= rhs._z;
            return result;
        }

        public static TVector operator *(double lhs, TVector rhs)
        {
            return new TVector(lhs * rhs._x, lhs * rhs._y, lhs * rhs._z);
        }

        public static TVector operator *(TVector lhs, double rhs)
        {
            return rhs * lhs;
        }

        /// <summary>
        /// Скалярное произведение векторов
        /// </summary>
        /// <param name="lhs">Левый вектор</param>
        /// <param name="rhs">Правый вектор</param>
        /// <returns>Возвращает значение скалярного произведения двух векторов</returns>
        public static double operator *(TVector lhs, TVector rhs)
        {
            return lhs._x * rhs._x + lhs._y * rhs._y + lhs._z * rhs._z;
        } 

        #endregion
    }
}
