﻿using EMFFM.Common.Enums;

namespace EMFFM.Common.Math.Elements
{
    /// <summary>
    /// Класс для описания единичного вектора
    /// </summary>
    public class UnitVector : TVector
    {
        /// <summary>
        /// Инициализация единичного вектора
        /// </summary>
        /// <param name="direction">Направление</param>
        /// <remarks>В положительном направлении по оси задается</remarks>
        public UnitVector(Direction direction)
        {
            switch(direction)
            {
                case Direction.X:
                    _x = 1;
                    _y = 0;
                    _z = 0;
                    break;
                case Direction.Y:
                    _x = 0;
                    _y = 1;
                    _z = 0;
                    break;
                case Direction.Z:
                    _x = 0;
                    _y = 0;
                    _z = 1;
                    break;
            }
        }
    }
}
