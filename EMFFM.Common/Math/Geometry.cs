﻿using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Common.Math
{
    // TODO: перенести этот класс в библиотеку EMFFM.Math

    /// <summary>
    /// Класс для реализации формур геометрии и аналитической геометрии.
    /// </summary>
    public static class GeometryHelper
    {
        /// <summary>
        /// Вычисление расстояния между двумя точками.
        /// </summary>
        /// <param name="p1">Точка 1.</param>
        /// <param name="p2">Точка 2.</param>
        /// <returns>Возвращает вычисленное значение расстояния.</returns>
        public static double GetDistance(Point p1, Point p2)
        {
            return System.Math.Sqrt(MathHelper.Sqr(p2.X - p1.X) + MathHelper.Sqr(p2.Y - p1.Y) + MathHelper.Sqr(p2.Z - p1.Z));
        }

        /// <summary>
        /// Вычисление расстояния между двумя точками.
        /// </summary>
        /// <param name="p1">Точка 1.</param>
        /// <param name="p2">Точка 2.</param>
        /// <param name="coFactor">Корректирующий множитель для нормирования размерности.</param>
        /// <returns>Возвращает вычисленное значение расстояния.</returns>
        public static double GetDistance(Point p1, Point p2, double coFactor)
        {
            return System.Math.Sqrt(MathHelper.Sqr(p2.X - p1.X) + MathHelper.Sqr(p2.Y - p1.Y) + MathHelper.Sqr(p2.Z - p1.Z)) * coFactor;
        }

        /// <summary>
        /// Вычисление расстояния от точки до начала координат.
        /// </summary>
        /// <param name="p">Точка.</param>
        /// <returns>Возвращает вычисленное значение расстояния.</returns>
        public static double GetDistance(Point p)
        {
            return System.Math.Sqrt(MathHelper.Sqr(p.X) + MathHelper.Sqr(p.Y) + MathHelper.Sqr(p.Z));
        }

        /// <summary>
        /// Вычисление расстояния от точки до начала координат.
        /// </summary>
        /// <param name="p">Точка</param>
        /// <param name="coFactor">Корректирующий множитель для нормирования размерности.</param>
        /// <returns>Возвращает вычисленное значение расстояния.</returns>
        public static double GetDistance(Point p, double coFactor)
        {
            return System.Math.Sqrt(MathHelper.Sqr(p.X) + MathHelper.Sqr(p.Y) + MathHelper.Sqr(p.Z)) * coFactor;
        }

        /// <summary>
        /// Опрделение центра отрезка между двумя точками.
        /// </summary>
        /// <param name="p1">Точка 1.</param>
        /// <param name="p2">Точка 2.</param>
        /// <returns>Возвращает среднюю  точку.</returns>
        public static Point GetCenterPoint(Point p1, Point p2)
        {
            return new Point((p1.X - p2.X) / 2, (p1.Y - p2.Y) / 2, (p1.Z - p2.Z) / 2);
        }
    }
}
