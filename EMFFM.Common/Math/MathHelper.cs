﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Common.Math
{
    /// <summary>
    /// Вспомогательный класс для математических вычислений
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Вычисление квадрата числа.
        /// </summary>
        /// <param name="val">Значение.</param>
        /// <returns>Возвращает val^2.</returns>
        public static double Sqr(double val)
        {
            return val * val;
        }

        /// <summary>
        /// Вычисление определителя матрицы 3 на 3.
        /// </summary>
        /// <param name="mat">Матрицы 3 на 3 для вычисления определителя.</param>
        /// <returns>Возвращает значение определителя.</returns>
        /// <remarks>Вычисление по правилю Саррюса! (или правило треугольника).</remarks>
        public static double Determinant(double[,] mat)
        {
            // a11 * a22 * a33 + a12 * a23 * a31 + a13 * a21 * a32 - a13 * a22 * a31 - a12 * a21 * a33 - a11 * a23 * a32
            return mat[0, 0] * mat[1, 1] * mat[2, 2] +
                   mat[0, 1] * mat[1, 2] * mat[2, 0] +
                   mat[0, 2] * mat[1, 0] * mat[2, 1] -
                   mat[0, 2] * mat[1, 1] * mat[2, 0] -
                   mat[0, 1] * mat[1, 0] * mat[2, 2] -
                   mat[0, 0] * mat[1, 2] * mat[2, 1];
        }

        /// <summary>
        /// Вычисление суммы координат.
        /// </summary>
        /// <param name="nodes">Список узлов элемента.</param>
        /// <param name="coordNumber">Номер координаты для суммирования(0-x, 1-y, 2-z).</param>
        /// <returns>Возвращает значение суммы координат.</returns>
        public static double SumOfCooodinates(List<Node> nodes, int coordNumber)
        {
            return nodes.Sum(node => node.Coord[coordNumber]);
        }
    }
}
