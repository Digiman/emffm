﻿using System;
using System.Numerics;

namespace EMFFM.Common.Math
{
    /// <summary>
    /// Вспомогательный класс для работы с комплексными матрицами
    /// </summary>
    public static class ComplexMatrixHelper
    {
        /// <summary>
        /// Генерация матрицы заполненной заданными значениями
        /// </summary>
        /// <param name="rows">Количество строк</param>
        /// <param name="cols">Количество столбцов</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает инициализированную матрицу</returns>
        public static Complex[,] ValueComplexMatrix(int rows, int cols, Complex value)
        {
            var mat = new Complex[rows, cols];

            for (int i = 0; i < rows; i++)

                for (int j = 0; j < cols; j++)
                    mat[i, j] = value;

            return mat;
        }

        /// <summary>
        /// Генерация матрицы, заполненной нулями
        /// </summary>
        /// <param name="rows">Количество строк</param>
        /// <param name="cols">Количество столбцов</param>
        /// <returns>Возвращает инициализированную нулями матрицу</returns>
        public static Complex[,] ZerosComplexMatrix(int rows, int cols)
        {
            return ValueComplexMatrix(rows, cols, new Complex(0, 0));
        }

        /// <summary>
        /// Сложение комплексных матриц
        /// </summary>
        /// <param name="mat1">Матрица 1</param>
        /// <param name="mat2">Матрица 2</param>
        /// <returns>Возвращает новую матрицу, результат сложения</returns>
        /// <remarks>Сложение квадратных матриц</remarks>
        public static Complex[,] ComplexSum(Complex[,] mat1, Complex[,] mat2)
        {
            // проверка на размерность матриц
            if (mat1.GetLength(0) != mat2.GetLength(0))
            {
                throw new Exception("Размерности матриц не совпадают! Не возможно выполнить сложение!");
            }

            // выделение памяти под новую матрицу
            int size1 = mat1.GetLength(0); // строк
            int size2 = mat1.GetLength(1); // столбцов
            var mat = new Complex[size1, size2];

            for (int i = 0; i < size1; i++)
                for (int j = 0; j < size2; j++)
                    mat[i, j] = mat1[i, j] + mat2[i, j];

            return mat;
        }

        /// <summary>
        /// Разность комплексных матриц
        /// </summary>
        /// <param name="mat1">Матрица 1</param>
        /// <param name="mat2">Матрица 2</param>
        /// <returns>Возвращает новую матрицу, результат сложения</returns>
        /// <remarks>Сложение квадратных матриц</remarks>
        public static Complex[,] ComplexSub(Complex[,] mat1, Complex[,] mat2)
        {
            // проверка на размерность матриц
            if (mat1.GetLength(0) != mat2.GetLength(0))
            {
                throw new Exception("Размерности матриц не совпадают! Не возможно выполнить сложение!");
            }

            // выделение памяти под новую матрицу
            int size1 = mat1.GetLength(0); // строк
            int size2 = mat1.GetLength(1); // столбцов
            var mat = new Complex[size1, size2];

            for (int i = 0; i < size1; i++)
                for (int j = 0; j < size2; j++)
                    mat[i, j] = mat1[i, j] - mat2[i, j];

            return mat;
        }

        /// <summary>
        /// Скалярное умножение комплексной матрицы на вещественное число
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="matrix">Матрица комплексных чисел</param>
        /// <returns>Возвращает результирующую матрицу</returns>
        public static Complex[,] ComplexMultiply(double value, Complex[,] matrix)
        {
            var complexValue = new Complex(value, 0);

            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    matrix[i, j] *= complexValue;

            return matrix;
        }

        /// <summary>
        /// Скалярное умножение комплексной матрицы на комплексное число
        /// </summary>
        /// <param name="value">Комлексное значение</param>
        /// <param name="matrix">Матрица комплексных чисел</param>
        /// <returns>Возвращает результирующую матрицу</returns>
        public static Complex[,] ComplexMultiply(Complex value, Complex[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    matrix[i, j] *= value;

            return matrix;
        }

        /// <summary>
        /// Перемножение комплексной матрицы на комплексный вектор
        /// </summary>
        /// <param name="vector">Вектор комплексных чисел</param>
        /// <param name="matrix">Матрица комплексных чисел</param>
        /// <returns>Возвращает результирующий вектор</returns>
        public static Complex[] ComplexMultiply(Complex[,] matrix, Complex[] vector)
        {
            var result = new Complex[matrix.GetLength(0)]; // размерность по количеству строк в матрице

            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    result[i] += matrix[i, j] * vector[i];

            return result;
        }

        /// <summary>
        /// Перемножение действительное для числа и комплексной матрицы
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="matrix">Комплексная матрица</param>
        /// <returns>Возвращает обработанную комплексную матрицу</returns>
        public static Complex[,] RealMultiply(double value, Complex[,] matrix)
        {
            var result = new Complex[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                    result[i, j] = value*matrix[i, j];
                    //result[i, j] = ComplexHelper.RealMultiply(value, matrix[i, j]);
            }

            return result;
        }
    }
}
