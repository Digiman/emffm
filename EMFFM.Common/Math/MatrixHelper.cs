﻿using System;

namespace EMFFM.Common.Math
{
    /// <summary>
    /// Класс-помощник для работы с матрицами
    /// </summary>
    /// <remarks>Можно потом использовать будет как оболочку для матриц из библиотеки Math.Net Numerics</remarks>
    public static class MatrixHelper
    {
        /// <summary>
        /// Генерация матрицы заполненной заданными значениями
        /// </summary>
        /// <param name="rows">Количество строк</param>
        /// <param name="cols">Количество столбцов</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает инициализированную матрицу</returns>
        public static double[,] ValueMatrix(int rows, int cols, double value)
        {
            double[,] mat = new double[rows, cols];

            for (int i = 0; i < rows; i++)

                for (int j = 0; j < cols; j++)
                    mat[i, j] = value;

            return mat;
        }

        /// <summary>
        /// Генерация матрицы, заполненной нулями
        /// </summary>
        /// <param name="rows">Количество строк</param>
        /// <param name="cols">Количество столбцов</param>
        /// <returns>Возвращает инициализированную нулями матрицу</returns>
        public static double[,] ZerosMatrix(int rows, int cols)
        {
            return ValueMatrix(rows, cols, 0);
        }

        /// <summary>
        /// Скалярное умножение матрицы на число
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="matrix">Матрица</param>
        /// <returns>Возвращает результирующую матрицу</returns>
        public static double[,] Multiply(double value, double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    matrix[i, j] *= value;

            return matrix;
        }

        /// <summary>
        /// Перемножение матрицы на вектор
        /// </summary>
        /// <param name="vector">Вектор</param>
        /// <param name="matrix">Матрица</param>
        /// <returns>Возвращает результирующий вектор</returns>
        public static double[] Multiply(double[,] matrix, double[] vector)
        {
            double[] result = new double[matrix.GetLength(0)]; // размерность по количеству строк в матрице

            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    result[i] += matrix[i, j] * vector[i];

            return result;
        }

        /// <summary>
        /// Сложение матриц
        /// </summary>
        /// <param name="mat1">Матрица 1</param>
        /// <param name="mat2">Матрица 2</param>
        /// <returns>Возвращает новую матрицу, результат сложения</returns>
        /// <remarks>Сложение квадратных матриц</remarks>
        public static double[,] Sum(double[,] mat1, double[,] mat2)
        {
            // проверка на размерность матриц
            if (mat1.GetLength(0) != mat2.GetLength(0))
            {
                throw new Exception("Размерности матриц не совпадают! Не возможно выполнить сложение!");
            }

            // выделение памяти под новую матрицу
            int size1 = mat1.GetLength(0); // строк
            int size2 = mat1.GetLength(1); // столбцов
            var mat = new double[size1, size2];

            for (int i = 0; i < size1; i++)
                for (int j = 0; j < size2; j++)
                    mat[i, j] = mat1[i, j] + mat2[i, j];

            return mat;
        }

        /// <summary>
        /// Транспонирование матрицы
        /// </summary>
        /// <typeparam name="TType">Тип элементов матрицы</typeparam>
        /// <param name="matrix">Исходная матрица</param>
        /// <returns>Возвращает транспонированную матрицу</returns>
        public static TType[,] Transpose<TType>(TType[,] matrix)
        {
            var mat = new TType[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    mat[j, i] = matrix[i, j];

            return mat;
        }

        /// <summary>
        /// Вставка малой матрицы в большую в указанное место
        /// </summary>
        /// <typeparam name="TType">Тип элементов матриц</typeparam>
        /// <param name="matrix">Исходная матрица</param>
        /// <param name="mat">Вставляемая матрица</param>
        /// <param name="rowStart"></param>
        /// <param name="rowEnd"></param>
        /// <param name="colStart"></param>
        /// <param name="colEnd"></param>
        /// <remarks>Только для квадратных матриц!</remarks>
        public static void BuiltMatrix<TType>(ref TType[,] matrix, TType[,] mat, int rowStart, int rowEnd, int colStart, int colEnd)
        {
            int k = 0, l = 0;
            for (int i = rowStart - 1; i < rowEnd; i++)
            {
                l = 0;
                for (int j = colStart - 1; j < colEnd; j++)
                {
                    matrix[i, j] = mat[k, l];
                    l++;
                }
                k++;
            }
        }
    }
}
