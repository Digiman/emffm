﻿using System.Collections.Generic;

namespace EMFFM.Common.FEM.Complex.Elements
{
    /// <summary>
    /// Комплексное поле на элементе.
    /// </summary>
    public class ElementField
    {
        /// <summary>
        /// Поля на ребрах элемента.
        /// </summary>
        public readonly List<EdgeField> EdgeFields;

        /// <summary>
        /// Поле в центре элемента.
        /// </summary>
        public Field CenterField;

        /// <summary>
        /// Инициализация "пустого" поля на элементе.
        /// </summary>
        public ElementField()
        {
            EdgeFields = new List<EdgeField>();
        }

        /// <summary>
        /// Инициализация поля на элементе с параметрами.
        /// </summary>
        /// <param name="field">Поле в центре элемента.</param>
        public ElementField(Field field)
        {
            EdgeFields = new List<EdgeField>();
            CenterField = field;
        }
    }
}
