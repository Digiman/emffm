﻿using System.IO;

namespace EMFFM.Common.FEM.Complex.Elements
{
    /// <summary>
    /// Класс для хранения результатов текущей итерации решения
    /// </summary>
    /// <remarks>Содержит основные данные о решении</remarks>
    public class Result
    {
        public string FileName;
        public string ForceFileName;

        public System.Numerics.Complex[] Forces;
        public System.Numerics.Complex[] Results;

        public WaveData Wave;
        public double Time;

        public double Frequency;
        public System.Numerics.Complex Epsilon;

        /// <summary>
        /// Вывод сведений о результатах в файл
        /// </summary>
        /// <param name="fname">Имя файла</param>
        public void OutputToFile(string fname)
        {
            var file = new StreamWriter(fname);

            file.WriteLine("WaveData (параметры источника):");
            file.WriteLine("\t Point: {0}", Wave.Point);
            file.WriteLine("\t Lambda: {0} м", Wave.Lambda);
            file.WriteLine("\t Amplitude: {0}", Wave.Amplitude);
            file.WriteLine("\t WaveNumber: {0}", Wave.WaveNumber);
            file.WriteLine("\t Omega: {0} рад/с", Wave.Omega);
            file.WriteLine("\t Phase: {0}", Wave.Phase);
            file.WriteLine("\t UnitVector: {0}", Wave.Vector);

            file.WriteLine("Files:");
            file.WriteLine("\t Result: {0}", FileName);
            file.WriteLine("\t ForceVector: {0}", ForceFileName);

            file.WriteLine("Time: {0} ms", Time);

            file.WriteLine("Frequency: {0} Гц", Frequency);
            file.WriteLine("Epsilon: {0}", Epsilon);

            file.Close();
        }
    }
}
