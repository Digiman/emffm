﻿using EMFFM.Common.Math.Elements;
using EMFFM.Common.Mesh.Elements;

namespace EMFFM.Common.FEM.Complex.Elements
{
    // TODO: описать параметры волны для комплексного вида

    /// <summary>
    /// Описание источника волны (ЭМ излучения)
    /// </summary>
    /// <remarks>Постоянные значения</remarks>
    public class WaveData
    {
        /// <summary>
        /// Координаты точечного источника.
        /// </summary>
        public Point Point;

        /// <summary>
        /// Длина волны.
        /// </summary>
        public double Lambda;

        /// <summary>
        /// Амлитуда.
        /// </summary>
        public double Amplitude;
        /// <summary>
        /// Фаза.
        /// </summary>
        public double Phase;

        /// <summary>
        /// Циклическая (круговая) частота (рад/с).
        /// </summary>
        public double Omega;
        /// <summary>
        /// Волновое число.
        /// </summary>
        public double WaveNumber; 

        /// <summary>
        /// Единичный вектор, показывающий направление волны.
        /// </summary>
        public UnitVector Vector; 
    }
}
