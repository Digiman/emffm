﻿namespace EMFFM.Common.FEM.Complex.Elements
{
    /// <summary>
    /// Комплексное поле (по трем направлениям)
    /// </summary>
    public class Field
    {
        /// <summary>
        /// Поле в направлении по оси OX.
        /// </summary>
        public System.Numerics.Complex FieldX;

        /// <summary>
        /// Поле в направлении оси OY.
        /// </summary>
        public System.Numerics.Complex FieldY;

        /// <summary>
        /// Поле в направлении оси OZ.
        /// </summary>
        public System.Numerics.Complex FieldZ;

        /// <summary>
        /// Инициализация "пустого" поля.
        /// </summary>
        public Field()
        { }

        /// <summary>
        /// Инициализация поля с заданными значениями по направлениям.
        /// </summary>
        /// <param name="x">Поле по OX.</param>
        /// <param name="y">Поле по OY.</param>
        /// <param name="z">Поле по OZ.</param>
        public Field(System.Numerics.Complex x, System.Numerics.Complex y, System.Numerics.Complex z)
        {
            FieldX = x;
            FieldY = y;
            FieldZ = z;
        }

        /// <summary>
        /// Иниуиализация поля с заданными значениями по направлениями.
        /// </summary>
        /// <param name="fieldArray">Значения полей по направлениям(OX, OY, OZ).</param>
        public Field(System.Numerics.Complex[] fieldArray)
        {
            FieldX = fieldArray[0];
            FieldY = fieldArray[1];
            FieldZ = fieldArray[2];
        }

        /// <summary>
        /// Вычисление аплитуды поля.
        /// </summary>
        /// <returns>Возвращает значение амлитуды комплексного поля.</returns>
        public System.Numerics.Complex Amplitude
        {
            // TODO: проверить вычисление амплитуды поля!

            get
            {
                var sum = CalcConj(FieldX) + CalcConj(FieldY) + CalcConj(FieldZ);
                return System.Numerics.Complex.Sqrt(sum).Real;
            }
        }

        /// <summary>
        /// Перемножение комплексного числа и его сопряженного значения.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <returns>Возвращает результирующее комплектное число.</returns>
        private System.Numerics.Complex CalcConj(System.Numerics.Complex value)
        {
            return value*System.Numerics.Complex.Conjugate(value);
        }
    }
}
