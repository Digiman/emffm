﻿using System;

namespace EMFFM.Common.FEM.Complex.Elements
{
    /// <summary>
    /// Описание комплексного числа
    /// </summary>
    public class ComplexDouble
    {
        private double _real;
        private double _imaginary;

        /// <summary>
        /// Пустой конструктор
        /// </summary>
        public ComplexDouble()
        {
        }

        /// <summary>
        /// Инициализация комплексного числа
        /// </summary>
        /// <param name="real">Вещественная часть</param>
        /// <param name="imaginary">Мнимая часть</param>
        public ComplexDouble(double real, double imaginary)
        {
            _real = real;
            _imaginary = imaginary;
        }

        /// <summary>
        /// Конструктор копирования
        /// </summary>
        /// <param name="value">Комплексное число для копирования</param>
        public ComplexDouble(ComplexDouble value)
        {
            _real = value.Real;
            _imaginary = value.Imaginary;
        }

        /// <summary>
        /// Вещественная часть
        /// </summary>
        public double Real
        {
            get { return _real; }
            set { _real = value; }
        }

        /// <summary>
        /// Мнимая часть
        /// </summary>
        public double Imaginary
        {
            get { return _imaginary; }
            set { _imaginary = value; }
        }

        /// <summary>
        /// Вывод отформатированной строки с комплексным числом
        /// </summary>
        /// <returns>Возвращает строку с комплексным числом</returns>
        public override string ToString()
        {
            // формат строки: Real +/- i Imagimary
            return String.Format("{0} {1} i{2}", _real, GetSign(), _imaginary);
        }

        /// <summary>
        /// Получение знака перед мнимой частью
        /// </summary>
        /// <returns>Возвращает строку со знаком</returns>
        private string GetSign()
        {
            return _imaginary < 0 ? "-" : "+";
        }

        /// <summary>
        /// Магнитуда (абсолютное значение)
        /// </summary>
        public double Magnitude
        {
            get { return System.Math.Sqrt(_real * _real + _imaginary * _imaginary); }
        }

        /// <summary>
        /// Фаза
        /// </summary>
        /// <remarks>В радианах!</remarks>
        public double Phase
        {
            get { return System.Math.Atan2(_imaginary, _real); }
        }

        #region Операции над комплексными числами
        
        public static ComplexDouble operator +(ComplexDouble lhs, ComplexDouble rhs)
        {
            ComplexDouble val = new ComplexDouble();
            val.Real = lhs.Real + rhs.Real;
            val.Imaginary = lhs.Imaginary + rhs.Imaginary;
            return val;
        }

        public static ComplexDouble operator -(ComplexDouble lhs, ComplexDouble rhs)
        {
            ComplexDouble val = new ComplexDouble();
            val.Real = lhs.Real - rhs.Real;
            val.Imaginary = lhs.Imaginary - rhs.Imaginary;
            return val;
        }

        public static ComplexDouble operator *(ComplexDouble lhs, ComplexDouble rhs)
        {
            ComplexDouble val = new ComplexDouble();
            val.Real = lhs.Real * rhs.Real - lhs.Imaginary * rhs.Imaginary;
            val.Imaginary = lhs.Imaginary * rhs.Real + lhs.Real * lhs.Imaginary;
            return val;
        }

        public static ComplexDouble operator /(ComplexDouble lhs, ComplexDouble rhs)
        {
            ComplexDouble val = new ComplexDouble();
            val.Real = (lhs.Real * rhs.Real + lhs.Imaginary * rhs.Imaginary) /
                       (rhs.Real * rhs.Real + rhs.Imaginary * rhs.Imaginary);
            val.Imaginary = (lhs.Imaginary * rhs.Real - lhs.Real * lhs.Imaginary) /
                            (rhs.Real * rhs.Real + rhs.Imaginary * rhs.Imaginary);
            return val;
        } 

        #endregion

        #region Операторы сравнения
        
        public static bool operator ==(ComplexDouble lhs, ComplexDouble rhs)
        {
            if (lhs.Real == rhs.Real && lhs.Imaginary == rhs.Imaginary)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(ComplexDouble lhs, ComplexDouble rhs)
        {
            if (lhs == rhs)
            {
                return false;
            }
            else
            {
                return true;
            }
        } 

        #endregion
    }
}
