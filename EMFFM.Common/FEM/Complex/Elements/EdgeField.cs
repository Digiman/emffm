﻿namespace EMFFM.Common.FEM.Complex.Elements
{
    /// <summary>
    /// Комплексное поле на ребре.
    /// </summary>
    public class EdgeField
    {
        /// <summary>
        /// Тангенциальное значение поля на ребре.
        /// </summary>
        public System.Numerics.Complex TangentialField { get; set; }

        /// <summary>
        /// Полное поле.
        /// </summary>
        public System.Numerics.Complex FullField { get; set; }
    }
}
