﻿using System.Collections.Generic;

namespace EMFFM.Common.FEM.Complex.Elements
{
    /// <summary>
    /// Описание результирующего поля (на ребрах и на элементах в целом).
    /// </summary>
    /// <remarks>Для одного решения (одной итерации)!</remarks>
    public class FieldData
    {
        /// <summary>
        /// Список полей на ребрах элементов.
        /// </summary>
        public readonly List<EdgeField> EdgeFields;
        
        /// <summary>
        /// Список полей по элементам.
        /// </summary>
        public readonly List<ElementField> ElementFields;
        
        /// <summary>
        /// Сведения о волне (параметры источника ЭМИ).
        /// </summary>
        public WaveData Wave;

        /// <summary>
        /// Инициализация "пустого" объекта.
        /// </summary>
        public FieldData()
        {
            EdgeFields = new List<EdgeField>();
            ElementFields = new List<ElementField>();
        }

        /// <summary>
        /// Иннициализация объекта с параметрами.
        /// </summary>
        /// <param name="edgeFields">Поля на ребрах.</param>
        public FieldData(List<EdgeField> edgeFields)
        {
            EdgeFields = edgeFields;
            ElementFields = new List<ElementField>();
        }

        /// <summary>
        /// Иннициализация объекта с параметрами.
        /// </summary>
        /// <param name="edgeFields">Поля на ребрах.</param>
        /// <param name="wave">Сведения о волне.</param>
        public FieldData(List<EdgeField> edgeFields, WaveData wave)
        {
            EdgeFields = edgeFields;
            Wave = wave;
            ElementFields = new List<ElementField>();
        }
    }
}
