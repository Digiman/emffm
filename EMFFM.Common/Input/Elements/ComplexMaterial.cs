﻿using System.Numerics;
using EMFFM.Common.Enums;

namespace EMFFM.Common.Input.Elements
{
    /// <summary>
    /// Материал с комплексными значениями параметров
    /// </summary>
    public class ComplexMaterial : MaterialBase
    {
        /// <summary>
        /// Электрическая постоянная.
        /// </summary>
        public Complex Eps { get; set; }

        /// <summary>
        /// Магнитная постоянная.
        /// </summary>
        public Complex Mu { get; set; }

        /// <summary>
        /// сигма
        /// </summary>
        public Complex Sigma { get; set; }

        // Параметры материала для модели Лоренца Друде.

        /// <summary>
        /// Тип модели для учета условий Лоренца-Друде.
        /// </summary>
        public ModelTypes Model { get; set; }

        private double[] _omega;
        private int _order;

        /// <summary>
        /// Тип материала (или проще говоря, сам материал).
        /// </summary>
        public MaterialTypes Type { get; set; }

        public double Omegap { get; set; }

        public double[] F { get; set; }

        public double[] Gamma { get; set; }

        public double[] Omega
        {
            get { return _omega; }
            set
            {
                _omega = value;
                Order = _omega.Length;
            }
        }

        /// <summary>
        /// Порядок.
        /// </summary>
        public int Order
        {
            get { return _omega.Length; }
            set { _order = value; }
        }

        public ComplexMaterial()
        {
            Eps = Complex.Zero;
            Mu = Complex.Zero;
            Sigma = Complex.Zero;
        }
    }
}
