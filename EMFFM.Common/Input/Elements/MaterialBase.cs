﻿namespace EMFFM.Common.Input.Elements
{
    /// <summary>
    /// Базовый класс для материалов.
    /// </summary>
    public class MaterialBase
    {
        /// <summary>
        /// Название материала.
        /// </summary>
        public string Name;

        /// <summary>
        /// Является ли материалом по умолчанию?
        /// </summary>
        public bool IsDefault;
    }
}
