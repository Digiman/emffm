﻿using System.ComponentModel;

namespace EMFFM.Common.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы со строками
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Преобразование строки в массив значений
        /// </summary>
        /// <typeparam name="TType">Тип значений массива</typeparam>
        /// <param name="str">Строка с массивом</param>
        /// <param name="splitter">Разделитель</param>
        /// <returns>Возвращает массив чисел</returns>
        public static TType[] StringToArray<TType>(string str, char splitter = ',')
        {
            string[] mas = str.Split(splitter);

            TType[] array = new TType[mas.Length];

            for (int i = 0; i < mas.Length; i++)
            {
                //array[i] = mas[i];
                var c = new TypeConverter();
                array[i] = (TType) c.ConvertTo(mas[i], typeof (TType));
            }

            return array;
        }

        /// <summary>
        /// Получение имени файла без его расширения
        /// </summary>
        /// <param name="fileName">Имя файла из входного файла</param>
        /// <returns>Возвращает имя файла</returns>
        public static string GetFileNameWithoutExtension(string fileName)
        {
            return fileName.Split('.')[0];
        }
    }
}
