﻿using System.Numerics;

namespace EMFFM.Common.Helpers
{
    // TODO: по максимуму модифицировать и расширить этот класс всеми методами работы с Complex

    /// <summary>
    /// Вспомогательный класс для работы с комплексными числами
    /// </summary>
    public static class ComplexHelper
    {
        /// <summary>
        /// Перемножение действительной части
        /// </summary>
        /// <param name="value">Вещественное значение</param>
        /// <param name="complex">Комплексное число</param>
        /// <returns>Возвращает новое комплексное число</returns>
        public static Complex RealMultiply(double value, Complex complex)
        {
            return new Complex(value * complex.Real, value * complex.Imaginary);
        }

        /// <summary>
        /// Конвертация вещественной матрицы в комплексную
        /// </summary>
        /// <param name="matrix">Матрица вещественных чисел</param>
        /// <returns>Возвращает комплексную матрицу</returns>
        /// <remarks>Переводит как вечественную часть комплексного числа</remarks>
        public static Complex[,] Convert(double[,] matrix)
        {
            var result = new Complex[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                    result[i, j] = new Complex(matrix[i, j], 0);
            }

            return result;
        }

        /// <summary>
        /// Конвертация вещественного вектора в комплексный
        /// </summary>
        /// <param name="vector">Вектор вещественных чисел</param>
        /// <returns>Возвращает вектор комплексных чисел</returns>
        /// <remarks>Переводит как вечественную часть комплексного числа</remarks>
        public static Complex[] Convert(double[] vector)
        {
            var result = new Complex[vector.Length];

            for (int i = 0; i < vector.GetLength(0); i++)
            {
                result[i] = new Complex(vector[i], 0);
            }

            return result;
        }
    }
}
