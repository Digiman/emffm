﻿using System.Collections.Generic;
using System.IO;

namespace EMFFM.Common.Helpers
{
    /// <summary>
    /// Вспомогательный класс для обработки ввода/вывода
    /// </summary>
    /// <remarks>Работа с файлами по сути</remarks>
    public static class IoHelper
    {
        #region Очистка папок от файлов

        /// <summary>
        /// Очистка каталога с тестовыми файлами
        /// </summary>
        /// <param name="path">Каталог</param>
        public static void ClearTestsPath(string path)
        {
            IEnumerable<string> files = Directory.EnumerateFiles(path);

            foreach (var file in files)
            {
                File.Delete(file);
            }
        }

        /// <summary>
        /// Очистка каталога с выходными файлами
        /// </summary>
        /// <param name="path">Путь к каталогу выходных файлов</param>
        public static void ClearOutputPath(string path)
        {
            IEnumerable<string> files = Directory.EnumerateFiles(path);

            foreach (var file in files)
            {
                File.Delete(file);
            }
        }

        /// <summary>
        /// Очистка каталога от папок и файлов
        /// </summary>
        /// <param name="path">Путь к каталогу</param>
        public static void ClearPath(string path)
        {
            var content = Directory.EnumerateFileSystemEntries(path);

            foreach (var element in content)
            {
                Directory.Delete(element, true);
            }
        }

        #endregion

        /// <summary>
        /// Проверка каталога и создание его в случае отсутствия
        /// </summary>
        /// <param name="path">Путь к каталогу (полный)</param>
        public static void CheckAndCreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// Копирование заданного файла в каталог.
        /// </summary>
        /// <param name="file">Файл дял копирования.</param>
        /// <param name="folder">Папка для помещения копируемого файла (папка назначения).</param>
        public static void CopyFileToDirectory(string file, string folder)
        {
            if (!File.Exists(folder + "\\" + FilesHelper.GetFileName(file, true)))
            {
                File.Copy(file, folder + "\\" + FilesHelper.GetFileName(file, true));
            }
        }
    }
}
