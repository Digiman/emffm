﻿using System;
using System.Diagnostics;
using EMFFM.Common.Enums;
using NLog;

namespace EMFFM.Common.Helpers
{
    /// <summary>
    /// Помошник по ведению логов
    /// Статический интерфейс над библиотекой для логирования (в данном случае - NLog2)
    /// </summary>
    public static class LogHelper
    {
        /// <summary>
        /// Инициализация логгера.
        /// </summary>
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Запись сообщения в лог файл.
        /// </summary>
        /// <param name="type">Тип сообщения.</param>
        /// <param name="message">Сообщение, которое надо записать в файл лога.</param>
        public static void Log(LogMessageType type, string message)
        {
            ReinitLogger();
            Log(message, type);
        }

        /// <summary>
        /// Запись сообщения в лог файл.
        /// </summary>
        /// <param name="type">Тип сообщения.</param>
        /// <param name="format">Строка с форматом сообщения.</param>
        /// <param name="args">Аргумент форматной строки.</param>
        public static void Log(LogMessageType type, string format, object args)
        {
            string message = String.Format(format, args);
            Log(message, type);
        }

        /// <summary>
        /// Запись сообщения в лог файл.
        /// </summary>
        /// <param name="type">Тип сообщения.</param>
        /// <param name="format">Строка с форматом сообщения.</param>
        /// <param name="args">Аргументы для сообщения по заданному формату.</param>
        public static void Log(LogMessageType type, string format, params object[] args)
        {
            string message = String.Format(format, args);
            Log(message, type);
        }

        /// <summary>
        /// Перенастройка логгера на новый метод и класс, который логируется.
        /// </summary>
        private static void ReinitLogger()
        {
            // пропускаем два кадра вверх по стеку - переходим к вызвавшему LogHelper.Log методу
            var frame = new StackFrame(2, false);
            // определяем полное имя класса, в котором расположен вызвавщий метод
            _logger = LogManager.GetLogger(frame.GetMethod().DeclaringType.FullName);
        }

        /// <summary>
        /// Запись сообщения указанноо типа в лог.
        /// </summary>
        /// <param name="message">Сообщение, которое будет записано в лог.</param>
        /// <param name="type">Тип сообщения.</param>
        private static void Log(string message, LogMessageType type)
        {
            switch (type)
            {
                case LogMessageType.Info:
                    LogInfo(message);
                    break;
                case LogMessageType.Error:
                    LogError(message);
                    break;
                case LogMessageType.Warning:
                    LogWarning(message);
                    break;
                case LogMessageType.Debug:
                    LogDebug(message);
                    break;
            }
        }

        /// <summary>
        /// Информационное сообщение.
        /// </summary>
        /// <param name="str">Текст сообщения.</param>
        private static void LogInfo(string str)
        {
            _logger.Info(str);
        }

        /// <summary>
        /// Отладочное сообщение.
        /// </summary>
        /// <param name="str">Текст сообщения.</param>
        private static void LogDebug(string str)
        {
            _logger.Debug(str);
        }

        /// <summary>
        /// Сообщение с предупреждением.
        /// </summary>
        /// <param name="str">Текст сообщения.</param>
        private static void LogWarning(string str)
        {
            _logger.Warn(str);
        }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        /// <param name="str">Текст сообщения.</param>
        private static void LogError(string str)
        {
            _logger.Error(str);
        }
    }
}
