﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using EMFFM.Common.Enums;

namespace EMFFM.Common.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с XML документами.
    /// </summary>
    public static class XmlHelper
    {
        /// <summary>
        /// Выполнение валидации файла XML по схеме.
        /// </summary>
        /// <param name="schemaFile">Файл со схемой.</param>
        /// <param name="xmlFile">XML-файл для валидации.</param>
        /// <returns>Возвращает False, если ошибок не найдено.</returns>
        public static bool ValidateFile(string schemaFile, string xmlFile)
        {
            var schemas = new XmlSchemaSet();
            schemas.Add("", schemaFile);

            LogHelper.Log(LogMessageType.Debug, "Attempting to validate XML file " + xmlFile);
            var custOrdDoc = XDocument.Load(xmlFile);
            var errors = false;
            custOrdDoc.Validate(schemas, (o, e) =>
            {
                LogHelper.Log(LogMessageType.Debug, String.Format("{0}", e.Message));
                errors = true;
            });
            LogHelper.Log(LogMessageType.Debug, String.Format("custOrdDoc {0}", errors ? "did not validate" : "validated"));

            return errors;
        }

        /// <summary>
        /// Выполнение валидации файла XML по схемам (набору схем).
        /// </summary>
        /// <param name="schemaFiles">Список файлов со схемами.</param>
        /// <param name="xmlFile">XML-файл для валидации.</param>
        /// <returns>Возвращает False, если ошибок не найдено.</returns>
        public static bool ValidateFile(List<string> schemaFiles, string xmlFile)
        {
            var schemas = new XmlSchemaSet();
            foreach (var schemaFile in schemaFiles)
            {
                schemas.Add("", schemaFile);
            }

            LogHelper.Log(LogMessageType.Debug, "Attempting to validate XML file " + xmlFile);
            var custOrdDoc = XDocument.Load(xmlFile);
            var errors = false;
            custOrdDoc.Validate(schemas, (o, e) =>
            {
                LogHelper.Log(LogMessageType.Debug, String.Format("{0}", e.Message));
                errors = true;
            });
            LogHelper.Log(LogMessageType.Debug, String.Format("custOrdDoc {0}", errors ? "did not validate" : "validated"));

            return errors;
        }

        /// <summary>
        /// Сохранение данных в XML файл с помощью сериализации.
        /// </summary>
        /// <typeparam name="T">Тип данных, которые сохраняются в файл.</typeparam>
        /// <param name="filename">Имя файла, в который нужно сохранить данные.</param>
        /// <param name="data">Данные для сохранения в файл (сериализации).</param>
        public static void SerializeData<T>(string filename, T data)
        {
            var serializer = new XmlSerializer(typeof (T));

            var stream = new StreamWriter(filename);
            serializer.Serialize(stream, data);
            stream.Close();
        }

        /// <summary>
        /// Чтение данных из файла путем дессериализации.
        /// </summary>
        /// <typeparam name="T">Тип данных, которые читаются из файла.</typeparam>
        /// <param name="filename">Имя файла, откуда производится чтение.</param>
        /// <returns>Возвращает данные, считанные и разобранные из файла.</returns>
        public static T DeserializeData<T>(string filename)
        {
            var serializer = new XmlSerializer(typeof (T));

            var stream = new StreamReader(filename);
            var data = (T) serializer.Deserialize(stream);
            stream.Close();

            return data;
        }
    }
}
