﻿using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using System.Windows.Forms.DataVisualization.Charting;

namespace EMFFM.Common.Helpers
{
    /// <summary>
    /// Класс для реализации методов работы с графиками (обработки данных и подготовки для них).
    /// </summary>
    /// <remarks>Используется для построения плоских графиков сечений по результатам анализа полей в теории Ми и по основному МКЭ.</remarks>
    public static class GraphHelper
    {
        #region Функции для построения графиков

        /// <summary>
        /// Построение плоского графика по данным анализа.
        /// </summary>
        /// <param name="chart">График, на котором нужно отобразить данные.</param>
        /// <param name="coordsX">Координаты по одной из осей.</param>
        /// <param name="dataY">Данные для графика.</param>
        /// <param name="color">Цвет линии графика для кривой.</param>
        /// <remarks>Для одного графика (одан серия).</remarks>
        public static void DrawChart(Chart chart, double[] coordsX, double[] dataY, Color color, int width)
        {
            var dict = new Dictionary<double, double>();
            for (int i = 0; i < coordsX.Length; i++)
            {
                dict.Add(coordsX[i], dataY[i]);
            }

            chart.Series.Add(CreateDataSeries(dict, color, width));

            chart.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
            chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
        }

        /// <summary>
        /// Построение плоского графика по данным анализа.
        /// </summary>
        /// <param name="chart">График, на котором нужно отобразить данные.</param>
        /// <param name="coordsX">Координаты по одной из осей.</param>
        /// <param name="dataY">Данные для графика.</param>
        /// <param name="color1">Цвет линии графика для кривой 1.</param>
        /// <param name="color2">Цвет линии графика для кривой 2.</param>
        /// <remarks>Для двух графиков сразу (две серии).</remarks>
        public static void DrawChart(Chart chart, double[] coordsX, double[,] dataY, Color color1, Color color2)
        {
            var dict1 = new Dictionary<double, double>();
            for (int i = 0; i < coordsX.Length; i++)
            {
                dict1.Add(coordsX[i], dataY[0, i]);
            }

            var dict2 = new Dictionary<double, double>();
            for (int i = 0; i < coordsX.Length; i++)
            {
                dict2.Add(coordsX[i], dataY[1, i]);
            }

            chart.Series.Add(CreateDataSeries(dict1, color1));
            chart.Series.Add(CreateDataSeries(dict2, color2));

            chart.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
            chart.ChartAreas[0].AxisY.Minimum = GetMinimumValue(dataY);
            chart.ChartAreas[0].AxisY.Maximum = GetMaximumValue(dataY);
            chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
        }

        #endregion

        #region Формирование серий для графиков из данных анализа

        /// <summary>
        /// Формирование данных для графика, которые будут на нем отображены.
        /// </summary>
        /// <param name="results">Результаты для отображения.</param>
        /// <param name="color">Цвет линии графика.</param>
        /// <param name="width">Толщина линии графика.</param>
        /// <returns>Возвращает серию с данными в виде точек на графике с настроенным типом графика.</returns>
        private static Series CreateDataSeries(Dictionary<double, double> results, Color color, int width = 2)
        {
            var series = new Series();

            foreach (var result in results)
            {
                series.Points.AddXY(result.Key, result.Value);
            }

            series.ChartType = SeriesChartType.Spline;
            series.Color = color;
            series.BorderWidth = width;

            return series;
        }

        /// <summary>
        /// Формирование данных для графика, которые будут на нем отображены.
        /// </summary>
        /// <param name="results">Результаты для отображения.</param>
        /// <param name="color">Цвет линии графика.</param>
        /// <returns>Возвразает серию с данными в виде точек на графике с настроенным типом графика.</returns>
        /// <remarks>Для комплексных числе используется абсолютное значение (величина) (Abs(value))!</remarks>
        private static Series CreateDataSeries(Dictionary<double, Complex> results, Color color)
        {
            var series = new Series();

            foreach (var result in results)
            {
                series.Points.AddXY(result.Key, Complex.Abs(result.Value));
            }

            series.ChartType = SeriesChartType.Spline;
            series.Color = color;

            return series;
        }

        #endregion

        #region Вспомогательные функции

        /// <summary>
        /// Получение минимального значения из массива для графика по Y (значения).
        /// </summary>
        /// <param name="data">Массив с данными.</param>
        /// <returns>Возвращает минимальное значение во всем массиве.</returns>
        private static double GetMinimumValue(double[,] data)
        {
            double min = 1;
            for (int i = 0; i < data.GetLength(1); i++)
            {
                if (data[0, i] < min) min = data[0, i];
            }
            for (int i = 0; i < data.GetLength(1); i++)
            {
                if (data[1, i] < min) min = data[1, i];
            }
            return min;
        }

        /// <summary>
        /// Получение максимального значения из массива для графика по Y (значения).
        /// </summary>
        /// <param name="data">Массив с данными.</param>
        /// <returns>Возвращает максимальное значение во всем массиве.</returns>
        private static double GetMaximumValue(double[,] data)
        {
            double max = 0;
            for (int i = 0; i < data.GetLength(1); i++)
            {
                if (data[0, i] > max) max = data[0, i];
            }
            for (int i = 0; i < data.GetLength(1); i++)
            {
                if (data[1, i] > max) max = data[1, i];
            }
            return max;
        }

        /// <summary>
        /// Получение минимального значения из массива для графика по Y (значения).
        /// </summary>
        /// <param name="data">Массив с данными.</param>
        /// <returns>Возвращает минимальное значение во всем массиве.</returns>
        private static double GetMinimumValue(double[] data)
        {
            double min = 1;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] < min) min = data[i];
            }
            return min;
        }

        /// <summary>
        /// Получение максимального значения из массива для графика по Y (значения).
        /// </summary>
        /// <param name="data">Массив с данными.</param>
        /// <returns>Возвращает максимальное значение во всем массиве.</returns>
        private static double GetMaximumValue(double[] data)
        {
            double max = 0;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] > max) max = data[i];
            }
            return max;
        }

        #endregion
    }
}
