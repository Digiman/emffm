﻿using System;
using System.IO;

namespace EMFFM.Common.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с файлами.
    /// </summary>
    public static class FilesHelper
    {
        /// <summary>
        /// Получение расширения файла.
        /// </summary>
        /// <param name="filename">Файл для разбора (полный путь к нему).</param>
        /// <returns>Возвращает расширение файла.</returns>
        public static string GetFileExtension(string filename)
        {
            return new FileInfo(filename).Extension.Remove(0, 1);
        }

        /// <summary>
        /// Получение имени файла без его расширения.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        /// <param name="withExtension">Получать имя файла с его расширением.</param>
        /// <returns>Возвращает имя файла.</returns>
        public static string GetFileName(string filename, bool withExtension = false)
        {
            if (withExtension)
            {
                return new FileInfo(filename).Name;
            }
            var mas = new FileInfo(filename).Name.Split('.');
            return mas[0];
        }

        /// <summary>
        /// Изменение расширения файла.
        /// </summary>
        /// <param name="filename">Полный путь к файлу для смены расширения.</param>
        /// <param name="newExtension">Новое расширение.</param>
        /// <returns>Возвращает путь к файлу с измененным расширением.</returns>
        public static string ChangeExtension(string filename, string newExtension)
        {
            var mas = filename.Split('.');
            return String.Format("{0}.{1}", mas[0], newExtension);
        }

        /// <summary>
        /// Добавление текста к имени файла.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь к нему).</param>
        /// <param name="appendText">Текст для добавления к имени файла.</param>
        /// <returns>Возвращает новое имя файла с путем к нему.</returns>
        public static string AddToFileName(string filename, string appendText)
        {
            var name = GetFileName(filename);
            var ext = GetFileExtension(filename);
            var length = (name + "." + ext).Length;
            var path = filename.Remove(filename.Length - length, length);
            var newname = String.Format("{0}{1}", name, appendText);
            return String.Format("{0}{1}.{2}", path, newname, ext);
        }
    }
}
