﻿using System;
using System.IO;
using EMFFM.NetgenUtils.Helpers;

namespace EMFFM.NetgenUtils.Preprocessing
{
    /// <summary>
    /// Класс для генерации сетки с использованием Netgen.
    /// </summary>
    /// <remarks>Препроцессинг исходной задачи.</remarks>
    public class MeshGenerator
    {
        /// <summary>
        /// Параметры окружения для запуска приложения.
        /// </summary>
        private readonly Variables _vars;

        /// <summary>
        /// Инициализация генератора сетки Netgen
        /// </summary>
        /// <param name="vars">Переменные окружения</param>
        public MeshGenerator(Variables vars)
        {
            _vars = vars;
        }

        /// <summary>
        /// Генерация сетки.
        /// </summary>
        /// <param name="geomFile">Входной файл с геометрией задачи(*.geo).</param>
        /// <param name="outputFile">Выходной файл с данными сетки (*.mesh).</param>
        public void Generate(string geomFile, string outputFile)
        {
            // обработка файлов - полные пути к ним
            geomFile = Path.GetFullPath(geomFile);
            outputFile = String.Format("{0}\\{1}{2}", _vars.OutputPath, _vars.MeshFilePrefix, outputFile);
            // формирование строки с аргументами
            string args = GenerateArgumentsString(geomFile, outputFile, "Neutral Format");
            // запуск приложения
            LaunchHelper.ExecuteProgram(_vars.NetgenPath, _vars.AppName, args);
        }

        /// <summary>
        /// Генерация строки с аргументами для запуска Netgen.
        /// </summary>
        /// <param name="geomFile">Входной файл с геометрией.</param>
        /// <param name="outputFile">Выходной файл с сеткой.</param>
        /// <param name="meshType">Тип файла сетки для выхода.</param>
        /// <param name="withTestout">Выводить ли лог работы Netgen в файл (*.out).</param>
        /// <param name="isDefaultMesh">Генерировать ли сетку без указания типа ее экспорта (сетки по умолчанию (*.vol)).</param>
        /// <returns>Возвращает результирующую строку с аргументами.</returns>
        private string GenerateArgumentsString(string geomFile, string outputFile, string meshType, bool withTestout = false, bool isDefaultMesh = false)
        {
            string str = "";
            if (isDefaultMesh)
            {
                str = String.Format("-batchmode -geofile=\"{0}\" -meshfile=\"{1}\"", geomFile, outputFile);
            }
            else
            {
                if (withTestout)
                {
                    str = String.Format("-batchmode -geofile=\"{0}\" -meshfile=\"{1}\" -meshfiletype=\"{2}\" -testout=\"{3}\\testout.out\"", geomFile, outputFile, meshType, _vars.OutputPath);
                }
                else
                {
                    str = String.Format("-batchmode -geofile=\"{0}\" -meshfile=\"{1}\" -meshfiletype=\"{2}\"", geomFile, outputFile, meshType);
                }
            }
            return str;
        }
    }
}
