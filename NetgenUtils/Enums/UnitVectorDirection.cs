﻿namespace EMFFM.NetgenUtils.Enums
{
    /// <summary>
    /// Направление единичного вектора
    /// </summary>
    public enum UnitVectorDirection
    {
        /// <summary>
        /// Вектор, направленный вверх
        /// </summary>
        Top,
        /// <summary>
        /// Вектора, направленный вниз
        /// </summary>
        Bottom, 
        /// <summary>
        /// Вектор, направленный влево от направления просмотра
        /// </summary>
        Left, 
        /// <summary>
        /// Вектор, направленный вправо от направления просмотра
        /// </summary>
        Right, 
        /// <summary>
        /// Вектор, направленный в сторону взгляда
        /// </summary>
        Back, 
        /// <summary>
        /// Вектор направленный в противоположную сторону взгляда
        /// </summary>
        Forward
    }
}