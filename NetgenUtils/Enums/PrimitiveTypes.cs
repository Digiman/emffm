﻿namespace EMFFM.NetgenUtils.Enums
{
    /// <summary>
    /// Описание типов примитивов
    /// </summary>
    public enum PrimitiveTypes
    {
        /// <summary>
        /// Сфера
        /// </summary>
        Sphere,
        /// <summary>
        /// Параллелепипед
        /// </summary>
        Orthobrick,
        /// <summary>
        /// Цилиндр
        /// </summary>
        Cylinder,
        /// <summary>
        /// Конус
        /// </summary>
        Cone,
        /// <summary>
        /// Плоскость
        /// </summary>
        Plane,
        /// <summary>
        /// Сложный составной объект
        /// </summary>
        Complex
    }
}