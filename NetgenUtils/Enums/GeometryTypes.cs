﻿namespace EMFFM.NetgenUtils.Enums
{
    /// <summary>
    /// Типы геометрии для файлов с ее описанием
    /// </summary>
    public enum GeometryTypes
    {
        /// <summary>
        /// Файл с твердотельной объемной геометрией 
        /// </summary>
        The3D,
        /// <summary>
        /// Файл с плоской геометрией (для 2D сетки)
        /// </summary>
        The2D
    }
}
