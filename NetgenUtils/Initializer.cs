﻿using System;
using System.IO;
using System.Xml;
using EMFFM.Common.Helpers;
using EMFFM.NetgenUtils.Helpers;

namespace EMFFM.NetgenUtils
{
    /// <summary>
    /// Класс для инициализации переменных окружения
    /// </summary>
    /// <remarks>Инициализируем на основе XML-файла с параметрами!</remarks>
    public class Initializer
    {
        private Variables _vars;

        private bool _isApplicationExist;

        /// <summary>
        /// Параметры окружения
        /// </summary>
        public Variables Vars
        {
            get { return _vars; }
        }

        /// <summary>
        /// Существует ли каталог с программой и само приложение
        /// </summary>
        public bool IsApplicationExist
        {
            get { return _isApplicationExist; }
        }

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public Initializer()
        {
        }

        /// <summary>
        /// Инициализация начальных параметров окружения
        /// </summary>
        /// <param name="fname">Файл с параметрами окружения</param>
        public Initializer(string fname)
        {
            _vars = new Variables();
            _vars.InitDefaultParams();

            ReadInitFile(fname);

            // проверка, существует ли приложение по указанному пути
            CheckApplication(_vars.NetgenPath, _vars.AppName);

            // проверка каталога для помещения результатов в него
            CheckOutputPath(_vars.OutputPath);
        }

        /// <summary>
        /// Инициализация окружения с заданными параметрами
        /// </summary>
        /// <param name="netgenPath">Путь к каталогу с программой Netgen</param>
        /// <param name="outputPath">Каталог для выходных файлов</param>
        /// <param name="meshFilePrefix">Префикс для файлов</param>
        /// <param name="appName">Имя файла приложения</param>
        public Initializer(string netgenPath, string outputPath, string meshFilePrefix, string appName)
        {
            _vars = new Variables(netgenPath, outputPath, meshFilePrefix, appName);

            // проверка, существует ли приложение по указанному пути
            CheckApplication(_vars.NetgenPath, _vars.AppName);
        }

        /// <summary>
        /// Чтение файла с начальными данными
        /// </summary>
        /// <param name="fname">Файл с данными</param>
        private void ReadInitFile(string fname)
        {
            _vars = VariablesHelper.Load(fname);
        }

        /// <summary>
        /// Проверка наличия приложения на запускаемом компьютере
        /// </summary>
        /// <param name="applicationPath">Путь к приложения для проверки</param>
        /// <param name="appName">Имя файла приложения</param>
        private void CheckApplication(string applicationPath, string appName)
        {
            if (!File.Exists(applicationPath + "\\" + appName))
            {
                _isApplicationExist = false;
                throw new Exception("Netgen application is not exist! Check application path in params file!");
            }
            _isApplicationExist = true;
        }

        /// <summary>
        /// Проверяет на существование выходного каталога и создает его в случае отсутствия
        /// </summary>
        /// <param name="outputPath">Путь к выходному каталогу</param>
        private void CheckOutputPath(string outputPath)
        {
            IoHelper.CheckAndCreateDirectory(outputPath);
        }
    }
}
