﻿using System;
using EMFFM.Common.Mesh.Elements;
using EMFFM.NetgenUtils.GeomFiles.Elements;

namespace EMFFM.NetgenUtils.GeomFiles
{
    /// <summary>
    /// Класс с геометрическими примитивами для создания трехмерой геометрии
    /// </summary>
    /// <remarks>На основе Constructive Solid Geometry (CSG)</remarks>
    static class GeomPrimitives
    {
        /// <summary>
        /// Бесконечная плоскость
        /// </summary>
        /// <param name="p">Произвольная точка пространства</param>
        /// <param name="v">Внешний нормальный единичный вектор</param>
        /// <returns>Возвращает строку с описанием примитива</returns>
        public static string Plane(FilePoint p, UnitVector v)
        {
            return String.Format("plane ({0}, {1}, {2}; {3}, {4}, {5})", p.X, p.Y, p.Z, v.I, v.J, v.K);
        }

        /// <summary>
        /// Параллелепипед (кирпич)
        /// </summary>
        /// <param name="p1">Точка 1</param>
        /// <param name="p2">Противоположная точка 2</param>
        /// <returns>Возвращает строку с описанием примитива</returns>
        public static string Orthobrick(FilePoint p1, Point p2)
        {
            return String.Format("orthobrick ({0}, {1}, {2}; {3}, {4}, {5})", p1.X, p1.Y, p1.Z, p2.X, p2.Y, p2.Z);
        }

        /// <summary>
        /// Сфера
        /// </summary>
        /// <param name="p">Центр сферы</param>
        /// <param name="radius">Радиус сферы</param>
        /// <returns>Возвращает строку с описанием примитива</returns>
        public static string Sphere(FilePoint p, double radius)
        {
            return String.Format("sphere ({0}, {1}, {2}; {3})", p.X, p.Y, p.Z, radius);
        }

        /// <summary>
        /// Цилиндр
        /// </summary>
        /// <param name="a">Точка 1 на главной оси цилиндра</param>
        /// <param name="b">Точка 2 на главной оси цилиндра</param>
        /// <param name="radius">Радиус оснвоания цииндра</param>
        /// <returns>Возвращает строку с описанием примитива</returns>
        public static string Cylinder(FilePoint a, FilePoint b, double radius)
        {
            return String.Format("cylinder ({0}, {1}, {2}; {3}, {4}, {5}; {6})", a.X, a.Y, a.Z, b.X, b.Y, b.Z, radius);
        }

        /// <summary>
        /// Конус
        /// </summary>
        /// <param name="a">Точка 1 на главной оси конуса</param>
        /// <param name="radiusA">Радиус основания 1</param>
        /// <param name="b">Точка 2 на главной оси конуса</param>
        /// <param name="radiusB">Радиус основания 2</param>
        /// <returns>Возвращает строку с описанием примитива</returns>
        public static string Cone(FilePoint a, double radiusA, FilePoint b, double radiusB)
        {
            return String.Format("cone ({0}, {1}, {2}; {3}; {4}, {5}, {6}; {7})", a.X, a.Y, a.Z, radiusA, b.X, b.Y, b.Z, radiusB);
        }
    }
}
