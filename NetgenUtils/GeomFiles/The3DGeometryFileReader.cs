﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using EMFFM.Common.Extensions;
using EMFFM.Common.Helpers;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.GeomFiles.Elements;

namespace EMFFM.NetgenUtils.GeomFiles
{
    /// <summary>
    /// Класс для реализации чтения файла определения геометрии из XML файла
    /// </summary>
    public class The3DGeometryFileReader : DefinitionFileReader
    {
        /// <summary>
        /// Список геометрических объектов для файла
        /// </summary>
        private GeometryData _data;

        /// <summary>
        /// Файл, солержащий схему XML Schema дл проверки валидности документа с геометрией в формате XML.
        /// </summary>
        private const string SchemaFile = @"Schemas\GeomFile.xsd";

        /// <summary>
        /// Инициализация объекта для чтения файла определения геометрии
        /// </summary>
        /// <param name="filename">Имя файла с данными</param>
        public The3DGeometryFileReader(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Чтение файла с данными
        /// </summary>
        public override void ReadFile()
        {
            if (CheckSchema() == false)
            {
                var doc = new XmlDocument();
                doc.Load(Filename);

                if (doc.DocumentElement != null) _data = ReadDocumentData(doc.DocumentElement.ChildNodes);
                else throw new Exception("Error occured while reading the geometry definition file!");
            }
            else
            {
                throw new Exception("Geomety file isn't valid to schema!");
            }
        }

        /// <summary>
        /// Проверка файла по схеме на правильность.
        /// </summary>
        /// <returns>Возвращает False, если файл проверен успешно и соответствует схеме (не найдено ошибок), иначе - True (есть ошибки).</returns>
        private bool CheckSchema()
        {
            var result = XmlHelper.ValidateFile(SchemaFile, Filename);

            return result;
        }

        /// <summary>
        /// Получение прочитанных и обработанных данных из файла
        /// </summary>
        public GeometryData GetGemetryData
        {
            get { return _data; }
        }

        /// <summary>
        /// Чтение данных из XML файла
        /// </summary>
        /// <param name="xmlNodeList">Узлы главного узла XML документа</param>
        private GeometryData ReadDocumentData(XmlNodeList xmlNodeList)
        {
            var data = new GeometryData();
            
            foreach (XmlNode node in xmlNodeList)
            {
                if (node.NodeType == XmlNodeType.Element)
                {
                    switch (node.Name)
                    {
                        case "points":
                            ReadPointsSection(ref data, node.ChildNodes);
                            break;
                        case "vectors":
                            ReadVectorsSection(ref data, node.ChildNodes);
                            break;
                        case "solids":
                            ReadSolidsSection(ref data, node.ChildNodes);
                            break;
                        case "colors":
                            ReadColorsSection(ref data, node.ChildNodes);
                            break;
                        case "output":
                            ReadOutputSection(ref data, node.ChildNodes);
                            break;
                    }
                }
            }

            AssociateFullData(ref data);

            return data;
        }

        /// <summary>
        /// Дополнительная обработка данных до полной адекватности.
        /// </summary>
        /// <param name="data">Сведения о геометрии.</param>
        private void AssociateFullData(ref GeometryData data)
        {
            foreach (var solid in data.Solids)
            {
                switch (solid.Type)
                {
                    case PrimitiveTypes.Sphere:
                        (solid as Sphere).Point = data.Points.Single(p => p.Name == (solid as Sphere).Point.Name);
                        break;
                    case PrimitiveTypes.Orthobrick:
                        (solid as Orthobrick).Point1 = data.Points.Single(p => p.Name == (solid as Orthobrick).Point1.Name);
                        (solid as Orthobrick).Point2 = data.Points.Single(p => p.Name == (solid as Orthobrick).Point2.Name);
                        break;
                    case PrimitiveTypes.Cylinder:
                        (solid as Cylinder).Point1 = data.Points.Single(p => p.Name == (solid as Cylinder).Point1.Name);
                        (solid as Cylinder).Point2 = data.Points.Single(p => p.Name == (solid as Cylinder).Point2.Name);
                        break;
                    case PrimitiveTypes.Cone:
                        (solid as Cone).Point1 = data.Points.Single(p => p.Name == (solid as Cone).Point1.Name);
                        (solid as Cone).Point2 = data.Points.Single(p => p.Name == (solid as Cone).Point2.Name);
                        break;
                    case PrimitiveTypes.Plane:
                        (solid as Plane).Point = data.Points.Single(p => p.Name == (solid as Plane).Point.Name);
                        (solid as Plane).Vector = data.Vectors.Single(p => p.Name == (solid as Plane).Vector.Name);
                        break;
                }
            }
        }

        #region Чтение секций файла
        
        /// <summary>
        /// Чтение секции с описанием координат точек.
        /// </summary>
        /// <param name="data">Выходные данные.</param>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        private void ReadPointsSection(ref GeometryData data, XmlNodeList xmlNodeList)
        {
            data.Points = new List<FilePoint>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "point":
                        data.Points.Add(new FilePoint()
                        {
                            Name = node.Attributes["name"].Value,
                            X = Convert.ToDouble(node.Attributes["x"].Value.CorrectDoubleStringValue()),
                            Y = Convert.ToDouble(node.Attributes["y"].Value.CorrectDoubleStringValue()),
                            Z = Convert.ToDouble(node.Attributes["z"].Value.CorrectDoubleStringValue())
                        });
                        break;
                }
            }
        }

        /// <summary>
        /// Чтение секции с описанием векторов.
        /// </summary>
        /// <param name="data">Выходные данные.</param>
        /// <param name="xmlNodeList">Часть XML документа.</param>
        private void ReadVectorsSection(ref GeometryData data, XmlNodeList xmlNodeList)
        {
            data.Vectors = new List<UnitVector>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "vector":
                        data.Vectors.Add(new UnitVector(node.Attributes["name"].Value,
                            (UnitVectorDirection)Enum.Parse(typeof(UnitVectorDirection), node.Attributes["direction"].Value)));
                        break;
                }
            }
        }

        /// <summary>
        /// Чтение секции с описанием объектов геометрии
        /// </summary>
        /// <param name="data">Выходные данные</param>
        /// <param name="xmlNodeList">Часть XML документа</param>
        private void ReadSolidsSection(ref GeometryData data, XmlNodeList xmlNodeList)
        {
            data.Solids = new List<Solid>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "sphere": // чтение сведений о сфере
                        Sphere sp = new Sphere()
                        {
                            Type = PrimitiveTypes.Sphere,
                            Name = node.Attributes["name"].Value,
                            Radius = Convert.ToDouble(node.Attributes["radius"].Value),
                            Point = new FilePoint(node.Attributes["p"].Value)
                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            sp.BoundaryCondition = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        data.Solids.Add(sp);
                        break;
                    case "orthobrick": // чтение сведений о кирпиче
                        Orthobrick ot = new Orthobrick()
                        {
                            Type = PrimitiveTypes.Orthobrick,
                            Name = node.Attributes["name"].Value,
                            Point1 = new FilePoint(node.Attributes["p1"].Value),
                            Point2 = new FilePoint(node.Attributes["p2"].Value)
                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            ot.BoundaryCondition = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        data.Solids.Add(ot);
                        break;
                    case "cylinder": // чтение сведений о цилиндре
                        Cylinder cl = new Cylinder()
                        {
                            Type = PrimitiveTypes.Cylinder,
                            Name = node.Attributes["name"].Value,
                            Point1 = new FilePoint(node.Attributes["p1"].Value),
                            Point2 = new FilePoint(node.Attributes["p2"].Value),
                            Radius = Convert.ToDouble(node.Attributes["radius"].Value)
                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            cl.BoundaryCondition = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        data.Solids.Add(cl);
                        break;
                    case "cone": // чтение сведений о конусе
                        Cone co = new Cone()
                        {
                            Type = PrimitiveTypes.Cone,
                            Name = node.Attributes["name"].Value,
                            Point1 = new FilePoint(node.Attributes["p1"].Value),
                            Radius1 = Convert.ToDouble(node.Attributes["radius1"].Value),
                            Point2 = new FilePoint(node.Attributes["p2"].Value),
                            Radius2 = Convert.ToDouble(node.Attributes["radius2"].Value)
                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            co.BoundaryCondition = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        data.Solids.Add(co);
                        break;
                    case "plane": // чтение сведений о плоскости
                        Plane pl = new Plane()
                        {
                            Type = PrimitiveTypes.Plane,
                            Name = node.Attributes["name"].Value,
                            Point = new FilePoint(node.Attributes["p"].Value),
                            Vector = new UnitVector(node.Attributes["v"].Value)
                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            pl.BoundaryCondition = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        data.Solids.Add(pl);
                        break;
                    case "complex": // чтение коплексного объекта
                        Complex cmp = new Complex()
                        {
                            Type = PrimitiveTypes.Complex,
                            Name = node.Attributes["name"].Value,
                            Data = node.Attributes["data"].Value
                        };
                        if (node.Attributes["boundary"] != null)
                        {
                            cmp.BoundaryCondition = Convert.ToInt32(node.Attributes["boundary"].Value);
                        }
                        data.Solids.Add(cmp);
                        break;
                }
            }
        }

        /// <summary>
        /// Чтение секции с описанием цветов объектов
        /// </summary>
        /// <param name="data">Выходные данные</param>
        /// <param name="xmlNodeList">Часть XML документа</param>
        private void ReadColorsSection(ref GeometryData data, XmlNodeList xmlNodeList)
        {
            data.Colors = new List<SolidColor>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "color":
                        data.Colors.Add(new SolidColor()
                        {
                            Name = node.Attributes["name"].Value,
                            R = Convert.ToInt32(node.Attributes["r"].Value),
                            G = Convert.ToInt32(node.Attributes["g"].Value),
                            B = Convert.ToInt32(node.Attributes["b"].Value)
                        });
                        break;
                }
            }
        }

        /// <summary>
        /// Чтение секции с параметрами построения объектов 
        /// </summary>
        /// <param name="data">Выходные данные</param>
        /// <param name="xmlNodeList">Часть XML документа</param>
        private void ReadOutputSection(ref GeometryData data, XmlNodeList xmlNodeList)
        {
            data.Tlos = new List<Tlo>();

            foreach (XmlNode node in xmlNodeList)
            {
                switch (node.Name)
                {
                    case "tlo":
                        data.Tlos.Add(new Tlo()
                        {
                            Name = node.Attributes["name"].Value,
                            Transparent = Convert.ToBoolean(node.Attributes["transparent"].Value),
                            ColorName = node.Attributes["color"].Value
                        });
                        break;
                }
            }
        } 

        #endregion
    }
}
