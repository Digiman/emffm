﻿namespace EMFFM.NetgenUtils.GeomFiles
{
    /// <summary>
    /// Базовый класс для чтения данных из файлов определения в XML формате
    /// </summary>
    public abstract class DefinitionFileReader
    {
        /// <summary>
        /// Входной файл с данными о геометрии в формате XML
        /// </summary>
        protected readonly string Filename;

        /// <summary>
        /// Инициализация класса для чтения файла определения геометрии
        /// </summary>
        /// <param name="filename">Входной файл с данными</param>
        protected DefinitionFileReader(string filename)
        {
            Filename = filename;
        }

        /// <summary>
        /// Чтение файла XML с даннымио геометрии
        /// </summary>
        public abstract void ReadFile();
    }
}
