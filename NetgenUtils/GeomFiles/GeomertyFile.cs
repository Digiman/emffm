﻿namespace EMFFM.NetgenUtils.GeomFiles
{
    /// <summary>
    /// Базовый класс для создания файла геометрии для программы Netgen.
    /// </summary>
    public abstract class GeomertyFile
    {
        /// <summary>
        /// Имя файла для сохранения результата генерации.
        /// </summary>
        protected string Filename;

        /// <summary>
        /// Инициализация класса для генерации файла геометрии.
        /// </summary>
        /// <param name="filename">Имя файла с результатом.</param>
        protected GeomertyFile(string filename)
        {
            Filename = filename;
        }

        /// <summary>
        /// Создание файла геометрии.
        /// </summary>
        public abstract void CreateFile();
    }
}
