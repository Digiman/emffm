﻿namespace EMFFM.NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Описание цвета для объектов геометрии
    /// </summary>
    public class SolidColor
    {
        /// <summary>
        /// Название цвета
        /// </summary>
        public string Name;

        public int R;
        public int G;
        public int B;

        /// <summary>
        /// Иницизация "пустого" цвета
        /// </summary>
        public SolidColor() {}

        /// <summary>
        /// Инициализация цвета
        /// </summary>
        /// <param name="name">Название</param>
        /// <param name="r">Компонента 1</param>
        /// <param name="g">Компонента 2</param>
        /// <param name="b">Компонента 3</param>
        public SolidColor(string name, int r, int g, int b)
        {
            Name = name;
            R = r;
            G = g;
            B = b;
        }
    }
}
