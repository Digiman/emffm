﻿namespace EMFFM.NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Описание объекта для построения примитива в Netgen
    /// (TOL - Top Level Object)
    /// </summary>
    public class Tlo
    {
        /// <summary>
        /// Имя объекта для построения
        /// </summary>
        public string Name;
        /// <summary>
        /// Прозрачность
        /// </summary>
        public bool Transparent;
        /// <summary>
        /// Название цвета
        /// </summary>
        public string ColorName;

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public Tlo() {}

        /// <summary>
        /// Инициализация объекта с параметрами
        /// </summary>
        /// <param name="name">Имя объекта для рисования</param>
        /// <param name="transparent">Рисовать прозрачным?</param>
        /// <param name="color">Имя цвета рисуемого объекта</param>
        public Tlo(string name, bool transparent, string color)
        {
            Name = name;
            Transparent = transparent;
            ColorName = color;
        }
    }
}
