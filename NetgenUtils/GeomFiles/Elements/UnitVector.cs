﻿using System;
using EMFFM.NetgenUtils.Enums;

namespace EMFFM.NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Класс для описания единичного вектора (по нормали)
    /// </summary>
    public class UnitVector
    {
        public int I;
        public int J;
        public int K;

        /// <summary>
        /// Имя объекта
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Инициализация "пустого" вектора
        /// </summary>
        public UnitVector() {}

        /// <summary>
        /// Инициализация вектора с заданным именем
        /// </summary>
        /// <param name="name">Имя вектора</param>
        public UnitVector(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Инициализация единичного вектора для описания направления
        /// </summary>
        /// <param name="direction">Направление вектора</param>
        public UnitVector(UnitVectorDirection direction)
        {
            InitByDirection(direction);
        }

        /// <summary>
        /// Инициализация единичного вектора для описания направления
        /// </summary>
        /// <param name="name">Имя вектора</param>
        /// <param name="direction">Направление вектора</param>
        public UnitVector(string name, UnitVectorDirection direction)
        {
            Name = name;
            InitByDirection(direction);
        }

        /// <summary>
        /// Инициализация значеий вектора по типу направления
        /// </summary>
        /// <param name="direction">Направление вектора</param>
        private void InitByDirection(UnitVectorDirection direction)
        {
            switch (direction)
            {
                case UnitVectorDirection.Forward:
                    I = 0;
                    J = 0;
                    K = -1;
                    break;
                case UnitVectorDirection.Left:
                    I = -1;
                    J = 0;
                    K = 0;
                    break;
                case UnitVectorDirection.Right:
                    I = 1;
                    J = 0;
                    K = 0;
                    break;
                case UnitVectorDirection.Top:
                    I = 0;
                    J = 1;
                    K = 0;
                    break;
                case UnitVectorDirection.Bottom:
                    I = 0;
                    J = -1;
                    K = 0;
                    break;
                case UnitVectorDirection.Back:
                    I = 0;
                    J = 0;
                    K = 1;
                    break;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} ({1}, {2}, {3})", Name, I, J, K);
        }
    }
}
