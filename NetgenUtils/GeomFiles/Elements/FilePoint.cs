﻿using EMFFM.Common.Mesh.Elements;

namespace EMFFM.NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Описание точки, прочитанной из файла с геометрией
    /// </summary>
    public class FilePoint : Point
    {
        /// <summary>
        /// Название точки
        /// </summary>
        public string Name;

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public FilePoint() : base()
        {}

        /// <summary>
        /// Инициализация точки с названием
        /// </summary>
        /// <param name="name">Имя точки</param>
        public FilePoint(string name) : base()
        {
            Name = name;
        }
    }
}
