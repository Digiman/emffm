﻿using System.Collections.Generic;

namespace EMFFM.NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Класс для описания геометрии
    /// </summary>
    /// <remarks>Содержит данные из XML файла определения геометрии</remarks>
    public class GeometryData
    {
        /// <summary>
        /// Список точек и их координаты
        /// </summary>
        public List<FilePoint> Points;
        /// <summary>
        /// Список векторов
        /// </summary>
        public List<UnitVector> Vectors;
        /// <summary>
        /// Список твердотельных объектов
        /// </summary>
        public List<Solid> Solids;
        /// <summary>
        /// Список цветов
        /// </summary>
        public List<SolidColor> Colors;
        /// <summary>
        /// Список объектов для построения
        /// </summary>
        public List<Tlo> Tlos;

        /// <summary>
        /// Инициализация "пустого" объекта с данными о геметрии
        /// </summary>
        public GeometryData()
        {
            Points = new List<FilePoint>();
            Vectors = new List<UnitVector>();
            Solids = new List<Solid>();
            Colors = new List<SolidColor>();
            Tlos = new List<Tlo>();
        }
    }
}
