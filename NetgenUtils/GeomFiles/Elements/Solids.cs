﻿using System;
using EMFFM.NetgenUtils.Enums;

namespace EMFFM.NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Твердотельный базовый объект
    /// </summary>
    public class Solid
    {
        /// <summary>
        /// Тип объекта
        /// </summary>
        public PrimitiveTypes Type;
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name;
        /// <summary>
        /// Код граничного условия
        /// </summary>
        public int BoundaryCondition;

        public override string ToString()
        {
            return String.Format("{0} is {1}", Name, Type);
        }
    }

    /// <summary>
    /// Сфера
    /// </summary>
    public class Sphere : Solid
    {
        /// <summary>
        /// Центр сферы
        /// </summary>
        public FilePoint Point;
        /// <summary>
        /// Радиус сферы
        /// </summary>
        public double Radius;

        public override string ToString()
        {
            return GeomPrimitives.Sphere(Point, Radius);
        }
    }

    /// <summary>
    /// Плоскость
    /// </summary>
    public class Plane : Solid
    {
        /// <summary>
        /// Координата точки, принадлежащей плоскости
        /// </summary>
        public FilePoint Point;
        /// <summary>
        /// Вектор, определяющий направления плоскости (вектор нормали)
        /// </summary>
        public UnitVector Vector;

        public override string ToString()
        {
            return GeomPrimitives.Plane(Point, Vector);
        }
    }

    /// <summary>
    /// Параллелепипед
    /// </summary>
    public class Orthobrick : Solid
    {
        /// <summary>
        /// Нижний левый угол
        /// </summary>
        public FilePoint Point1;
        /// <summary>
        /// Верхний правый угол
        /// </summary>
        public FilePoint Point2;

        public override string ToString()
        {
            return GeomPrimitives.Orthobrick(Point1, Point2);
        }
    }

    /// <summary>
    /// Конус
    /// </summary>
    public class Cone : Solid
    {
        /// <summary>
        /// Центр первого основания
        /// </summary>
        public FilePoint Point1;
        /// <summary>
        /// Радиус первого основания
        /// </summary>
        public double Radius1;
        /// <summary>
        /// Центр второго основания
        /// </summary>
        public FilePoint Point2;
        /// <summary>
        /// Радиус второго основания
        /// </summary>
        public double Radius2;

        public override string ToString()
        {
            return GeomPrimitives.Cone(Point1, Radius1, Point2, Radius2);
        }
    }

    /// <summary>
    /// Цилиндр
    /// </summary>
    public class Cylinder : Solid
    {
        /// <summary>
        /// Центр нижнего основания цлилиндра
        /// </summary>
        public FilePoint Point1;
        /// <summary>
        /// Центр верхнего основания цилиндра
        /// </summary>
        public FilePoint Point2;
        /// <summary>
        /// Радиус основания цилиндра
        /// </summary>
        public double Radius;

        public override string ToString()
        {
            return GeomPrimitives.Cylinder(Point1, Point2, Radius);
        }
    }

    /// <summary>
    /// Сложный объект
    /// </summary>
    public class Complex : Solid
    {
        /// <summary>
        /// Данные объекта
        /// (в виде строки с условиями для несколькоих объектов и их пересечения)
        /// </summary>
        public string Data;

        public override string ToString()
        {
            return Data;
        }
    }
}
