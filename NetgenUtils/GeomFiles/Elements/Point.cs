﻿using System;
using System.Linq;

namespace NetgenUtils.GeomFiles.Elements
{
    /// <summary>
    /// Описание точки по ее координатам
    /// </summary>
    public class Point
    {
        public double X;
        public double Y;
        public double Z;

        public string Name;

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public Point() {}

        /// <summary>
        /// Инициализация точки с названием
        /// </summary>
        /// <param name="name">Имя точки</param>
        public Point(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Инициализация точки в 3D
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="z">Координата Z</param>
        public Point(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return String.Format("( {0}, {1}, {2} )", X, Y, Z);
        }
    }
}
