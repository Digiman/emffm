﻿using System;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.Helpers;
using EMFFM.NetgenUtils.Old;
using EMFFM.NetgenUtils.Preprocessing;

namespace EMFFM.NetgenUtils.Samples
{
    /// <summary>
    /// Класс с примерами использования модуля
    /// </summary>
    public static class Sample
    {
        /// <summary>
        /// Пример демонстрирует использование:
        ///     - инициализатора параметров окружения (с чтением из файла),
        ///     - генерацию сетки на основе некоторого входного файла геометрии.
        /// </summary>
        public static void Sample1()
        {
            Initializer init = new Initializer(@"\DataFiles\Params.xml");
            MeshGenerator mesh = new MeshGenerator(init.Vars);
            mesh.Generate("test1.geo", "test1.mesh");
        }

        /// <summary>
        /// Пример демонстрирует:
        ///     - генерацию файла геометрии по входному XML файлу.
        /// </summary>
        /// <remarks>Старый варант! Без фабрики!</remarks>
        public static void Sample2()
        {
            string testfile = @"\DataFiles\NetgenGeometryDefinition1.xml";
            var reader = new GeomFiles.The3DGeometryFileReader(testfile);
            reader.ReadFile();
            GeomFiles.Elements.GeometryData data = reader.GetGemetryData;

            GeomFileGenerator.Generate("test-file.geo", GeometryTypes.The3D, data);
        }

        /// <summary>
        /// Пример демонстрирует:
        ///     - использование инициализатора параметров окружения,
        ///     - генерацию файла геометрии на основе входного файла XML формата,
        ///     - генерация сетки по сгенерированному файлу геометрии.
        /// </summary>
        /// <remarks>Старый варант! Без фабрики!</remarks>
        public static void Sample3()
        {
            // 1. Инициализация параметров выполнения
            Initializer init = new Initializer(@"\DataFiles\Params.xml");

            // 2. Генерация файла геометрии
            string testfile = @"\DataFiles\NetgenGeometryDefinition1.xml";
            GeomFiles.The3DGeometryFileReader reader = new GeomFiles.The3DGeometryFileReader(testfile);
            GeomFiles.Elements.GeometryData data = reader.GetGemetryData;

            string outputGeoFile = String.Format("{0}\\{1}", init.Vars.OutputPath, "test-file.geo");
            GeomFileGenerator.Generate(outputGeoFile, GeometryTypes.The3D, data);

            // 3. Вызов программы Netgen для генерации сетки
            MeshGenerator mesh = new MeshGenerator(init.Vars);
            mesh.Generate(outputGeoFile, "test-file.mesh");
        }

        /// <summary>
        /// Пример демонстрирует использование:
        ///     - инициализатора параметров окружения (заданными вручную),
        ///     - генерацию сетки на основе некоторого входного файла геометрии.
        /// </summary>
        /// <remarks>В зависимости от используемого компьютера, параметры заданные вручную могут сильно отличаться!</remarks>
        public static void Sample4()
        {
            Initializer init = new Initializer(@"C:\Program Files\Netgen-5.0_x64\bin\", @"\output", "mesh-", "netgen.exe");
            MeshGenerator mesh = new MeshGenerator(init.Vars);
            mesh.Generate("test1.geo", "test1.mesh");
        }

        /// <summary>
        /// Пример для тестирования абстрактной фабрики:
        ///     - чтение файла определений из XML,
        ///     - генерация файла геометрии для Netgen,
        ///     - работа с трехмерной геометрией.
        /// Здесь принимают участие один фабричный метод (для получения генератора).
        /// </summary>
        public static void FactoryTest1()
        {
            string testfile = @"\DataFiles\NetgenGeometryDefinition1.xml";
            
            // создание читателя для файла определения и чтение его
            var reader = new GeomFiles.The3DGeometryFileReader(testfile);
            reader.ReadFile();
            
            // создание генератора и генерация файла геометрии
            var generator = FactoryHelper.GetFileGenerator(GeometryTypes.The3D, "test-file-1.geo", reader.GetGemetryData);
            generator.CreateFile();
        }

        /// <summary>
        /// Пример для тестирования абстрактной фабрики:
        ///     - чтение файла определений из XML,
        ///     - генерация файла геометрии для Netgen,
        ///     - работа с трехмерной геометрией.
        /// Здесь принимают участие два фабричных метода (для полкчения читателя и генератора).
        /// </summary>
        public static void FactoryTest2()
        {
            string testfile = @"\DataFiles\NetgenGeometryDefinition1.xml";

            // создание читателя для файла определения и чтение его
            var reader = FactoryHelper.GetFileReader(GeometryTypes.The3D, testfile);
            reader.ReadFile();

            // создание генератора и генерация файла геометрии
            var generator = FactoryHelper.GetFileGenerator(GeometryTypes.The3D, "test-file-2.geo", (reader as GeomFiles.The3DGeometryFileReader).GetGemetryData);
            generator.CreateFile();
        }
    }
}
