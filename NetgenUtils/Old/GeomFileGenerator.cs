﻿using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.GeomFiles;
using EMFFM.NetgenUtils.GeomFiles.Elements;

namespace EMFFM.NetgenUtils.Old
{
    /// <summary>
    /// Генератор файлов с геометрией для программы Netgen
    /// на основе Constructive Solid Geometry (CSG)
    /// </summary>
    /// <remarks>Старый вариант класса для реализации генерации файла геометрии</remarks>
    public static class GeomFileGenerator
    {
        /// <summary>
        /// Генерация файла с геометрией
        /// </summary>
        /// <param name="geomFile">Имя файла</param>
        /// <param name="type">Тип файла</param>
        /// <param name="geomData">Данные о геометрии</param>
        public static void Generate(string geomFile, GeometryTypes type, GeometryData geomData)
        {
            switch(type)
            {
                case GeometryTypes.The3D:
                    Generate3DGomertyFile(geomFile, geomData);
                    break;
                case GeometryTypes.The2D:
                    Generate2DGomertyFile(geomFile);
                    break;
            }
        }

        /// <summary>
        /// Генерация файлов для трехмерной геометрии
        /// </summary>
        /// <param name="fname">Имя файла с геометрией для вывода</param>
        /// <param name="geomData">Данные о геометрии</param>
        private static void Generate3DGomertyFile(string fname, GeometryData geomData)
        {
            var file = new Algebraic3DFile(fname, geomData);
            file.CreateFile();
            file.Dispose();
        }

        /// <summary>
        /// Генерация файлов для трехмерной геометрии
        /// </summary>
        /// <param name="fname">Имя файла с геометрией для вывода</param>
        private static void Generate2DGomertyFile(string fname)
        {
            // TODO: описать метод для вызова генератора файла с двухмерной (плоской) геометрией
        }
    }
}
