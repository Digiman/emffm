﻿<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="geometrydata">
    <xs:annotation>
      <xs:documentation>
        Описание геометрии задачи для генерации конечноэлементой сетки
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        
        <xs:element name="points">
          <xs:annotation>
            <xs:documentation>
              Список базовых точек для построения объектов
            </xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="unbounded" name="point">
                <xs:annotation>
                  <xs:documentation>
                    Точка и ее координаты
                  </xs:documentation>
                </xs:annotation>
                <xs:complexType>
                  <xs:attribute name="name" type="xs:string" use="required" />
                  <xs:attribute name="x" type="xs:short" use="required" />
                  <xs:attribute name="y" type="xs:short" use="required" />
                  <xs:attribute name="z" type="xs:short" use="required" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
        <xs:element name="vectors" minOccurs="0">
          <xs:annotation>
            <xs:documentation>
              Список нормальных единичных векторов (для описания плоскостей)
            </xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="unbounded" name="vector">
                <xs:complexType>
                  <xs:attribute name="name" type="xs:string" use="required" />
                  <xs:attribute name="direction" type="VectorDirections" use="required" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
        <xs:element name="solids">
          <xs:annotation>
            <xs:documentation>
              Описание тел и объектов геометрии
            </xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:choice maxOccurs="unbounded">
                
                <xs:element maxOccurs="unbounded" name="plane">
                  <xs:annotation>
                    <xs:documentation>
                      Описание бесконечной плоскости
                    </xs:documentation>
                  </xs:annotation>
                  <xs:complexType>
                    <xs:attribute name="name" type="xs:string" use="required" />
                    <xs:attribute name="p" type="xs:string" use="required" />
                    <xs:attribute name="v" type="xs:string" use="required" />
                    <xs:attribute name="boundary" type="xs:unsignedByte" use="optional" />
                  </xs:complexType>
                </xs:element>
                
                <xs:element name="sphere">
                  <xs:annotation>
                    <xs:documentation>
                      Описание сферы
                    </xs:documentation>
                  </xs:annotation>
                  <xs:complexType>
                    <xs:attribute name="name" type="xs:string" use="required" />
                    <xs:attribute name="p" type="xs:string" use="required" />
                    <xs:attribute name="radius" type="xs:unsignedByte" use="required" />
                    <xs:attribute name="boundary" type="xs:unsignedByte" use="optional" />
                  </xs:complexType>
                </xs:element>
                
                <xs:element name="complex">
                  <xs:annotation>
                    <xs:documentation>
                      Описание комбинированного тела
                    </xs:documentation>
                  </xs:annotation>
                  <xs:complexType>
                    <xs:attribute name="name" type="xs:string" use="required" />
                    <xs:attribute name="data" type="xs:string" use="required" />
                    <xs:attribute name="boundary" type="xs:unsignedByte" use="optional" />
                  </xs:complexType>
                </xs:element>
                
                <xs:element name="orthobrick">
                  <xs:annotation>
                    <xs:documentation>
                      Описание кирпича (параллелепипеда)
                    </xs:documentation>
                  </xs:annotation>
                  <xs:complexType>
                    <xs:attribute name="name" type="xs:string" use="required" />
                    <xs:attribute name="p1" type="xs:string" use="required" />
                    <xs:attribute name="p2" type="xs:string" use="required" />
                    <xs:attribute name="boundary" type="xs:unsignedByte" use="optional" />
                  </xs:complexType>
                </xs:element>
                
              </xs:choice>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
        <xs:element name="colors" minOccurs="0">
          <xs:annotation>
            <xs:documentation>
              Список цветов для отображения тел
            </xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="unbounded" name="color">
                <xs:complexType>
                  <xs:attribute name="name" type="xs:string" use="required" />
                  <xs:attribute name="r" type="xs:unsignedByte" use="required" />
                  <xs:attribute name="g" type="xs:unsignedByte" use="required" />
                  <xs:attribute name="b" type="xs:unsignedByte" use="required" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
        <xs:element name="output">
          <xs:annotation>
            <xs:documentation>
              Описание объектов для отображения
            </xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="unbounded" name="tlo">
                <xs:complexType>
                  <xs:attribute name="name" type="xs:string" use="required" />
                  <xs:attribute name="transparent" type="xs:boolean" use="optional" />
                  <xs:attribute name="color" type="xs:string" use="optional" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  
  <!-- собственные типы -->
  <xs:simpleType name="VectorDirections">
    <xs:annotation>
      <xs:documentation>
        Направление единичного вектора в пространстве
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Back"/>
      <xs:enumeration value="Forward"/>
      <xs:enumeration value="Top"/>
      <xs:enumeration value="Bottom"/>
      <xs:enumeration value="Right"/>
      <xs:enumeration value="Left"/>
    </xs:restriction>
  </xs:simpleType>
  <!-- ENDS собственные типы -->
</xs:schema>