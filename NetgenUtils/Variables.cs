﻿namespace EMFFM.NetgenUtils
{
    /// <summary>
    /// Описание переменных окружения модуля
    /// </summary>
    public class Variables
    {
        /// <summary>
        /// Каталог с приложением Netgen (bin каталог)
        /// </summary>
        public string NetgenPath;
        /// <summary>
        /// Каталог для выходных файлов
        /// </summary>
        public string OutputPath;
        /// <summary>
        /// Префикс, добавляемый к имени файлов с сеткой
        /// </summary>
        public string MeshFilePrefix;
        /// <summary>
        /// Название исполняемого файла приложения (netgen.exe)
        /// </summary>
        public string AppName;

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public Variables()
        {
        }

        /// <summary>
        /// Инициализация объекта с параметрами
        /// </summary>
        /// <param name="netgenPath">Каталог с приложением</param>
        /// <param name="outputPath">Выходной каталог</param>
        /// <param name="meshFilePrefix">Префикс файлов</param>
        /// <param name="appName">Название исполняемого файла приложения</param>
        public Variables(string netgenPath, string outputPath, string meshFilePrefix, string appName)
        {
            NetgenPath = netgenPath;
            OutputPath = outputPath;
            MeshFilePrefix = meshFilePrefix;
            AppName = appName;
        }

        /// <summary>
        /// Инициализация параметров по умолчанию
        /// </summary>
        /// <remarks>Только для конкретного компьютера работает!</remarks>
        public void InitDefaultParams()
        {
            NetgenPath = @"c:\Program Files\Netgen-5.0_x64\bin\";
            OutputPath = @"d:\NetgenOutput\";
            MeshFilePrefix = "Mesh-";
            AppName = "netgen.exe";
        }
    }
}
