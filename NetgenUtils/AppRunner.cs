﻿using System;
using System.IO;
using EMFFM.NetgenUtils.Helpers;

namespace EMFFM.NetgenUtils
{
    /// <summary>
    /// Класс для запуска приложения Netgen.
    /// </summary>
    /// <remarks>Для работы с приложением Netgen, запуск GUI.</remarks>
    public class AppRunner
    {
        private readonly Variables _vars;

        /// <summary>
        /// Инициализация запусаальщика прилодения Netgen.
        /// </summary>
        /// <param name="vars">Параметры окружения.</param>
        public AppRunner(Variables vars)
        {
            _vars = vars;
        }

        /// <summary>
        /// Запуск приложения с файлом геометрии.
        /// </summary>
        /// <param name="geomFile">Файл геометрии</param>
        public void RunAppWithGeometry(string geomFile)
        {
            // обработка файлов - полные пути к ним
            geomFile = Path.GetFullPath(geomFile);
            // формирование строки с аргументами
            string args = GenerateArgumentsString(geomFile);
            // запуск приложения
            LaunchHelper.ExecuteProgram(_vars.NetgenPath, _vars.AppName, args);
        }

        /// <summary>
        /// Запуск приложения без параметров.
        /// </summary>
        public void RunApp()
        {
            LaunchHelper.ExecuteProgram(_vars.NetgenPath, _vars.AppName);
        }

        /// <summary>
        /// Генерация строки с аргументами для командной строки.
        /// </summary>
        /// <param name="geomFile">Файл геометрии (полный путь).</param>
        /// <returns>Возвращает строку с аргументами для приложения.</returns>
        private string GenerateArgumentsString(string geomFile)
        {
            return String.Format("-geofile={0}", geomFile);
        }
    }
}
