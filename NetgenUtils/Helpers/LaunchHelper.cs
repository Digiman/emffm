﻿using System;
using System.Diagnostics;

namespace EMFFM.NetgenUtils.Helpers
{
    /// <summary>
    /// Вспомогательный класс для запуска приложения с параметрами
    /// </summary>
    static class LaunchHelper
    {
        /// <summary>
        /// Запуск приложения без параметров
        /// </summary>
        /// <param name="appPath">Путь к приложению</param>
        /// <param name="appName">Имя файла приложения</param>
        public static void ExecuteProgram(string appPath, string appName)
        {
            ExecuteProgram(appPath, appName, "");
        }
        
        /// <summary>
        /// Запуск приложения с параметрами командной строки
        /// </summary>
        /// <param name="appPath">Путь к приложению</param>
        /// <param name="appName">Название файла приложения</param>
        /// <param name="arguments">Аргументы командной строки</param>
        public static void ExecuteProgram(string appPath, string appName, string arguments)
        {
            Process prc = null;

            try
            {
                // Устанавливаем параметры запуска процесса
                prc = new Process();
                prc.StartInfo.FileName = String.Format(@"{0}\{1}", appPath, appName);
                prc.StartInfo.Arguments = arguments;

                // Старт приложения
                prc.Start();

                // Ожидание завершения процесса
                prc.WaitForExit();
            }
            finally
            {
                if (prc != null) prc.Close();
            }
        }
    }
}
