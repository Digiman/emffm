﻿using System;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.GeomFiles;
using EMFFM.NetgenUtils.GeomFiles.Elements;

namespace EMFFM.NetgenUtils.Helpers
{
    /// <summary>
    /// Вспомогательный класс для реализации фабричных методов для работы абстрактной фабрики
    /// для реализауии чтения и формирования файлов геометрии для программы Netgen.
    /// </summary>
    public static class FactoryHelper
    {
        /// <summary>
        /// Получение конкретного читателя для файла определения с геометрией в формате XML.
        /// </summary>
        /// <param name="geotype">Тип геометрии.</param>
        /// <param name="filename">Файл для чтения.</param>
        /// <returns>Возвращает объект требуемого класса для чтения данных о геометрии.</returns>
        public static DefinitionFileReader GetFileReader(GeometryTypes geotype, string filename)
        {
            switch (geotype)
            {
                case GeometryTypes.The3D:
                    return new The3DGeometryFileReader(filename);
                case GeometryTypes.The2D:
                    throw new NotImplementedException("Читателя для двухмерной геометрии не создано!");
                default:
                    return null;
            }
        }
        
        /// <summary>
        /// Получение конкретного генератора для запись файла геометрии в подходищем формате.
        /// </summary>
        /// <param name="geotype">Тип геометрии.</param>
        /// <param name="filename">Файл для выходного результата работы генератора (полный путь к файлу).</param>
        /// <param name="data">Данные по геометрии.</param>
        /// <returns>Возвращает объект требуемого класса для генерации файла геометрии.</returns>
        public static GeomertyFile GetFileGenerator(GeometryTypes geotype, string filename, GeometryData data)
        {
            switch (geotype)
            {
                case GeometryTypes.The3D:
                    return new Algebraic3DFile(filename, data);
                case GeometryTypes.The2D:
                    throw new NotImplementedException("Генератора для двухмерной геометрии не создано!");
                default:
                    return null;
            }
        }
    }
}
