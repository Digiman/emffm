﻿using System;
using System.Text;
using System.Xml;

namespace EMFFM.NetgenUtils.Helpers
{
    /// <summary>
    /// Вспомогательный классс для работы с переменными окружения
    /// </summary>
    public static class VariablesHelper
    {
        /// <summary>
        /// Чтение файла с параметрами.
        /// </summary>
        /// <param name="filename">Файл с параметрами и путь к нему.</param>
        /// <returns>Возвращает считанные параметры из файла.</returns>
        public static Variables Load(string filename)
        {
            var vars = new Variables();

            var doc = new XmlDocument();
            doc.Load(filename);

            if (doc.DocumentElement != null)
                ParseInitialData(doc.DocumentElement.ChildNodes, vars);
            else
            {
                throw new Exception("Environment data file is empty!");
            }

            return vars;
        }

        /// <summary>
        /// Разбор параметров в файле XML
        /// </summary>
        /// <param name="nodeList">Список узлов главного элемента XML дерева</param>
        private static void ParseInitialData(XmlNodeList nodeList, Variables vars)
        {
            foreach (XmlNode node in nodeList)
            {
                switch (node.Name)
                {
                    case "param":
                        if (node.Attributes != null)
                            switch (node.Attributes["name"].Value)
                            {
                                case "OutputPath":
                                    vars.OutputPath = node.Attributes["value"].Value;
                                    break;
                                case "NetgenPath":
                                    vars.NetgenPath = node.Attributes["value"].Value;
                                    break;
                                case "MeshFilePrefix":
                                    vars.MeshFilePrefix = node.Attributes["value"].Value;
                                    break;
                                case "AppName":
                                    vars.AppName = node.Attributes["value"].Value;
                                    break;
                            }
                        break;
                }
            }
        }

        /// <summary>
        /// Сохранение переменных окружения в файл.
        /// </summary>
        /// <param name="filename">Имя файла для сохранения.</param>
        /// <param name="vars">Паременные окружения.</param>
        public static void Save(string filename, Variables vars)
        {
            var file = new XmlTextWriter(filename, Encoding.UTF8);

            file.WriteStartDocument();
            file.WriteStartElement("params");

            file.WriteStartElement("param");
            file.WriteStartAttribute("name");
            file.WriteValue("NetgenPath");
            file.WriteStartAttribute("value");
            file.WriteValue(vars.NetgenPath);
            file.WriteEndElement();

            file.WriteStartElement("param");
            file.WriteStartAttribute("name");
            file.WriteValue("OutputPath");
            file.WriteStartAttribute("value");
            file.WriteValue(vars.OutputPath);
            file.WriteEndElement();

            file.WriteStartElement("param");
            file.WriteStartAttribute("name");
            file.WriteValue("MeshFilePrefix");
            file.WriteStartAttribute("value");
            file.WriteValue(vars.MeshFilePrefix);
            file.WriteEndElement();

            if (!String.IsNullOrEmpty(vars.AppName))
            {
                file.WriteStartElement("param");
                file.WriteStartAttribute("name");
                file.WriteValue("AppName");
                file.WriteStartAttribute("value");
                file.WriteValue(vars.AppName);
                file.WriteEndElement();
            }

            file.WriteEndElement();

            file.Close();
        }
    }
}
