﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.DataImporter;
using EMFFM.DataImporter.Elements;

namespace DataViewer.Forms
{
    /// <summary>
    /// Главное окно приложения просмотрщика данных на графиках.
    /// </summary>
    public partial class DataViewer : Form
    {
        /// <summary>
        /// Фасад для работы с библиотекой импорта.
        /// </summary>
        private readonly IImporterFacade _importerFacade;

        /// <summary>
        /// Данные для графика.
        /// </summary>
        private GraphData _graphData;

        /// <summary>
        /// Инициализация окна с данным.
        /// </summary>
        public DataViewer()
        {
            _importerFacade = new ImporterFacade();

            InitializeComponent();

            HideContols();

            ChangeStatusText("Программа готова к работе!");
        }

        #region Обработка событий контролов

        /// <summary>
        /// Обработка события выбора меню "Выход".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обработка события выбора меню "Открыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        /// <summary>
        /// Обработка события выбора меню "Экспорт".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            ExportData();
        }

        #endregion

        #region Вспомогательные функции для работы с компонентами на форме

        /// <summary>
        /// Открытие контролов.
        /// </summary>
        private void ShowControls()
        {
            groupBox1.Visible = true;
            ExportButton.Visible = true;
        }

        /// <summary>
        /// Сокрытие контролов.
        /// </summary>
        private void HideContols()
        {
            groupBox1.Visible = false;
            ExportButton.Visible = false;
        }

        /// <summary>
        /// Смена сообщения в панели статусной.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        private void ChangeStatusText(string message)
        {
            statusStrip1.Items[0].Text = message;
        }

        #endregion

        #region Основные рабочие функции с логикой

        /// <summary>
        /// Открытие файла с данными.
        /// </summary>
        /// <remarks>Данные для импорта проходят здесь, чтобы потом проверить работу импорта и построить графики по ним.</remarks>
        private void OpenFile()
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "XML файлы|*.xml";
            dlg.Title = "Открытие файла параметров импорта";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                _graphData = _importerFacade.Import(dlg.FileName);

                ShowData();

                ShowControls();

                ChangeStatusText(String.Format("Загружены данные из файла {0}.",
                    FilesHelper.GetFileName(dlg.FileName, true)));
            }
        }

        /// <summary>
        /// Показать данные на форме (рисование графиков).
        /// </summary>
        private void ShowData()
        {
            // построение графиков и вывод их в окне
            chart1.Series.Clear();
            chart1.AntiAliasing = AntiAliasingStyles.All;

            // построение графиков
            int i = 0;
            foreach (var serie in _graphData.SeriesDatas)
            {
                GraphHelper.DrawChart(chart1, serie.DataX, serie.DataY, serie.Color, serie.Width);
                if (!String.IsNullOrEmpty(serie.Name)) chart1.Series[i].Name = serie.Name;
                if (!String.IsNullOrEmpty(serie.Style))
                    chart1.Series[i].BorderDashStyle = (ChartDashStyle) Enum.Parse(typeof (ChartDashStyle), serie.Style);

                i++;
            }

            // настройка и показ легенды на графике
            chart1.Legends.Clear();
            chart1.Legends.Add(new Legend("Description"));
            chart1.Legends[0].Position.Auto = true;
            chart1.Legends[0].LegendStyle = LegendStyle.Table;
            chart1.Legends[0].TableStyle = LegendTableStyle.Wide;
            chart1.Legends[0].Docking = Docking.Top;
            chart1.Legends[0].Alignment = StringAlignment.Center;
            chart1.Legends[0].IsDockedInsideChartArea = true;

            // установка минимума для графика (корректнее, чем автоматически)
            chart1.ChartAreas[0].AxisY.Minimum = GetMinimumFromAllData(_graphData.SeriesDatas);
        }

        /// <summary>
        /// Получение минимального значения для всех данных в сериях.
        /// </summary>
        /// <param name="series">Серии с данными для графиков.</param>
        /// <returns>Возвращает найденное минимальное значение.</returns>
        private double GetMinimumFromAllData(List<SeriesData> series)
        {
            double min = 1;
            foreach (var seriesData in series)
            {
                for (int i = 0; i < seriesData.DataY.Length; i++)
                {
                    if (seriesData.DataY[i] < min) min = seriesData.DataY[i];
                }
            }
            return min;
        }

        /// <summary>
        /// Экспорт данных.
        /// </summary>
        private void ExportData()
        {
            var dlg = new SaveFileDialog();
            dlg.Filter = "XML файлы|*.xml";
            dlg.ShowDialog();
            if (!String.IsNullOrEmpty(dlg.FileName))
            {
                _graphData.Export(dlg.FileName, ExportFileType.XML);

                MessageBox.Show("Экспорт успещно завершен!", "Информация", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        #endregion
    }
}
