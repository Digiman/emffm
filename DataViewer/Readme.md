﻿# Приложение DataViewer

Приложенеи предназначено для просмотра графиков функций, котоыре строятся на оонове данных, полученных в результате решения задачи о распределении ЭМП вблизи частицы различными методами.
Данные подготовливаются специальным образом так, чтобы получить плоские (двухмерные) графики.

Эти данные предствляют собой два файла текстовых, где один содердит данные для графика по оси абсцисс, а другой - данные по оси ординат.

Для щагрузки и отображения нескольких графиков, нужно подготовить спецальный файл XML, где указать все серии (то есть графики), которые нужно отобразить.
