﻿namespace EMFFM.MainApp.Common
{
    /// <summary>
    /// Тип треугольников для оргоганальной сетки
    /// </summary>
	enum TriangleStyle
    {
    	Right, // побочная диагональ
        Left // главная диагональ
    }

    /// <summary>
    /// Тип источников
    /// </summary>
    /// <remarks>По функции источника</remarks>
    enum SourceFunc
    {
    	Gausian,
        Hertz
    }

    /// <summary>
    /// Типы материалов частицы (плазмона)
    /// </summary>
    /// <remarks>Предопределенные типы с известными параметрами</remarks>
    enum PlasmonMaterials
    {
    	Ag,
        Au,
        Cu
    }

    /// <summary>
    /// Направления поля
    /// </summary>
    enum FieldDirection
    {
    	X, Y, Z, 
        All // по всем направлениям
    }

    /// <summary>
    /// Направление вектора
    /// </summary>
    enum VectorDirection
    {
        X, Y, Z
    }
}