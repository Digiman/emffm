﻿using System;
using System.Collections.Generic;
using System.IO;
using EMFFM.Common.Math.Elements;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Common.Structs;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using EMFFM.MainApp.Common;
using EMFFM.MainApp.FEM;
using EMFFM.MainApp.Helpers;
using EMFFM.MainApp.Mesh;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp
{
    /// <summary>
    /// Класс для описания тестовых функция для классов и моделей программы
    /// </summary>
    /// <remarks>Ручные тесты, не автоматизированные! По тестам для написанных алгоритмов!</remarks>
    class TestFunctions
    {
        static string outputTestFolder = "tests/";
        
        /// <summary>
        /// Тестирование классов для точек и узлов
        /// </summary>
        public static void TestPointsAndNodes()
        {
            // инициализация тестовых объетов
            Point p1 = new Point(5, 5);
            Point p2 = new Point(5, 5, 7);
            Point p3 = new Point(5, 5, 7);

            Console.WriteLine("Points:\n{0}\n{1}\n{2}", p1, p2, p3);

            Node n1 = new Node(10, 5, 7, 1);
            Node n2 = new Node(10, 5, 7, 2);
            Node n3 = new Node(8, 6, 2, 3);
            Node n4 = new Node(p1, 4);
            Node n5 = new Node(p2, 5);

            Console.WriteLine("Nodes:\n{0}\n{1}\n{2}\n{3}\n{4}", n1, n2, n3, n4, n5);

            // проверка
            if (p1 != p2)
            {
                Console.WriteLine("Точка p1 не равна p2!");
            }
            if (!p2.Equals(p3))
            {
                Console.WriteLine("Точка p2 не равна p3!");
            }
            else
            {
                Console.WriteLine("Точка p2 равна p3!");
            }
        }

        /// <summary>
        /// Тестирование класса для вектора
        /// </summary>
        public static void TestVectors()
        {
            TVector v1 = new TVector(4, 5, 2);
            TVector v2 = new TVector(5, -2, -4);

            Console.WriteLine("Vectors:\n v1: {0}\n v2: {1}", v1, v2);

            Console.WriteLine("v1 + v2 = {0}", v1 + v2);
            Console.WriteLine("v1 - v2 = {0}", v1 - v2);
            Console.WriteLine("v1 * v2 = {0}", v1 * v2);
            Console.WriteLine("v1 * 2 = {0}", v1 * 2);
            Console.WriteLine("2 * v1 = {0}", 2 * v1);
        }

        /// <summary>
        /// Тестирование алгоритма разбиения на параллелипипеды
        /// </summary>
        public static void TestBox()
        {
            // задание геометрии для разбиения
            GeomData data = new GeomData() { A = 10, B = 5, C = 3, Da = 1, Db = 1, Dc = 1 };

            DateTime start = DateTime.Now;

            // разбиение
            ParMesh mesh = new ParMesh(data);
            List<Parallelepiped> elements = mesh.GenerateMesh();

            DateTime end = DateTime.Now;

            Console.WriteLine("Parallelepiped - Calculated time: {0} ms", (end - start).TotalMilliseconds);

            // вывод списка полученных элементов
            StreamWriter file = new StreamWriter(outputTestFolder + "out.txt");
            foreach (var item in elements)
            {
                file.WriteLine("{0}, Volume: {1}", item.ToString(), item.GetVolume());
            }
            file.Close();

            //double[,] t = elements[1].GetLocalE();
            //Output.OutputHelper.OutputMatrixToFile<double>(t, outputTestFolder + "EkParMatrix.txt");
        }

        /// <summary>
        /// Тестирование сетки из тетраэдров
        /// </summary>
        public static void TestBox2()
        {
            // задание геометрии для разбиения
            GeomData data = new GeomData() { A = 10, B = 5, C = 3, Da = 1, Db = 1, Dc = 1 };

            DateTime start = DateTime.Now;

            // разбиение
            TetMesh mesh = new TetMesh(data);
            List<Tetrahedron> elements = mesh.GenerateMesh();

            DateTime end = DateTime.Now;

            Console.WriteLine("Tetrahedron - Calculated time: {0} ms", (end - start).TotalMilliseconds);

            // вывод списка полученных элементов
            StreamWriter file = new StreamWriter(outputTestFolder + "out2.txt");
            int count = 1;
            double vol = 0;
            foreach (var item in elements)
            {
                file.WriteLine("{0}, Volume: {1}", item.ToString(), item.GetVolume());
                if (count % 5 == 0)
                {
                    file.WriteLine("Common Volume: {0}", vol);
                    vol = 0;
                }
                vol += item.GetVolume();
                count++;
            }
            file.Close();
        }

        /// <summary>
        /// Тестирование дискретизации пластины на прямоугольники (ортогональные)
        /// </summary>
        public static void TestPlateRect()
        {
            // задание геометрии для разбиения
            GeomData data = new GeomData() { A = 10, B = 5, Da = 2, Db = 1 };

            DateTime start = DateTime.Now;

            // разбиение
            RectMesh mesh = new RectMesh(data);
            List<Rectangle> elements = mesh.GenerateMesh();

            DateTime end = DateTime.Now;

            Console.WriteLine("Rectangle - Calculated time: {0} ms", (end - start).TotalMilliseconds);

            // вывод списка полученных элементов
            StreamWriter file = new StreamWriter(outputTestFolder + "out3.txt");
            foreach (var item in elements)
            {
                file.WriteLine("{0}, Area: {1}, Perimeter: {2}", item.ToString(), item.GetArea(), item.GetPerimeter());
            }
            file.Close();
        }

        /// <summary>
        /// Тестирование дискретизации пластины на прямоугольные элементы и их векторизация
        /// </summary>
        public static void TestPlateRect2()
        {
            // задание геометрии для разбиения
            GeomData data = new GeomData() { A = 5, B = 5, Da = 1, Db = 1 };

            DateTime start = DateTime.Now;

            // разбиение
            RectMesh mesh = new RectMesh(data);
            List<Rectangle> elements = mesh.GenerateMesh();
            int size = EdgeMesh.BuiltEdges<Rectangle>(ref elements);

            DateTime end = DateTime.Now;

            Console.WriteLine("Rectangle - Calculated time: {0} ms", (end - start).TotalMilliseconds);

            // вывод списка полученных элементов
            StreamWriter file = new StreamWriter(outputTestFolder + "out3_1.txt");
            foreach (var item in elements)
            {
                file.WriteLine("{0}, Area: {1}, Perimeter: {2}", item.ToString(), item.GetArea(), item.GetPerimeter());
                file.WriteLine("Edges: {0}", item.EdgesToString());
            }
            file.Close();

            // генерация глобальной матрицы
            double[,] matrix = MatrixBuilder.BuiltGlobalMatrix(size, elements, 0, 10);
            
            // вывод матрицы в файл
            OutputHelper.OutputMatrixToFile<double>(matrix, outputTestFolder + "out3_1_mat.txt");
        }

        /// <summary>
        /// Тестирование дискретизации пластины на треугольники (ортогональные)
        /// </summary>
        public static void TestPlateTri()
        {
            // задание геометрии для разбиения
            GeomData data = new GeomData() { A = 10, B = 5, Da = 2, Db = 1 };

            DateTime start = DateTime.Now;

            // разбиение
            TriMesh mesh = new TriMesh(data, TriangleStyle.Right);
            List<Triangle> elements = mesh.GenerateMesh();

            DateTime end = DateTime.Now;

            Console.WriteLine("Triangle - Calculated time: {0} ms", (end - start).TotalMilliseconds);

            // вывод списка полученных элементов
            StreamWriter file = new StreamWriter(outputTestFolder + "out4.txt");
            foreach (var item in elements)
            {
                file.WriteLine("{0}, Area: {1}, Perimeter: {2}", item.ToString(), item.GetArea(), item.GetPerimeter());
            }
            file.Close();
        }

        /// <summary>
        /// Тестирование генерации случайных частиц
        /// </summary>
        /// <remarks>Уже заменено действием автоматизированным!</remarks>
        public static void TestParticlesGenerator()
        {
            Particle data = new ParticleD()
            {
                DistName = ParticlesDistribution.Random,
                DistType = DistributionTypes.MersenneTwister,
                Count = 10,
                LowerBound = 5,
                TopBound = 100
            };

            Particles.ParticleBuilder gen = new Particles.ParticleBuilder(new List<Particle> { data });

            int[] res = gen.Numbers;

            StreamWriter file = new StreamWriter(outputTestFolder + "perticles.txt");

            for (int i = 0; i < res.Length; i++)
            {
                file.WriteLine(res[i]);
            }

            file.Close();
        }
    }
}
