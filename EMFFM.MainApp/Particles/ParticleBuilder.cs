﻿using System;
using System.Collections.Generic;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using MathNet.Numerics.Random;

namespace EMFFM.MainApp.Particles
{
    /// <summary>
    /// Класс для генерации частиц (наночастиц) по законам распределения
    /// </summary>
    class ParticleBuilder
    {
        int[] _numbers;

        public ParticleBuilder(List<Particle> particlesData)
        {
            foreach (var item in particlesData)
            {
                switch (item.Type)
                {
                	case ParticlesType.Law:
                        BuiltByLaw(item as ParticleD);
                        break;
                    case ParticlesType.Single:
                        BuiltSingle(item as ParticleS);
                        break;
                    case ParticlesType.Many:
                        BuiltMany(item as ParticleM);
                        break;
                }
            }
        }

        public int[] Numbers
        {
            get
            {
                return this._numbers;
            }
        }

        #region Генерация частиц в зависимости от их типа
        
        /// <summary>
        /// Генерация по заданному закону распределения
        /// </summary>
        /// <param name="particleData">Данные о частицах</param>
        private void BuiltByLaw(ParticleD particleData)
        {
            switch (particleData.DistName) // выбираем основной тип распределения
            {
                case ParticlesDistribution.Random: // случайная генерация номеров элементов
                    switch (particleData.DistType) // выбираем подтип (конкретный тип, если он есть)
                    {
                        case DistributionTypes.Random:
                            _numbers = GenerateNumbers(particleData.Count, particleData.LowerBound, particleData.TopBound, new Random(1), NumberGenerators.GetRandom);
                            break;
                        case DistributionTypes.MersenneTwister:
                            _numbers = GenerateNumbers(particleData.Count, particleData.LowerBound, particleData.TopBound, new MersenneTwister(1), NumberGenerators.MersenneTwister);
                            break;
                    }
                    break;
            }
        }

        /// <summary>
        /// Генерация одной частицы
        /// </summary>
        /// <param name="particleData">Данные о частице</param>
        private void BuiltSingle(ParticleS particleData)
        {
            if (_numbers != null)
            {
                _numbers[_numbers.Length] = particleData.Number;
            }
            else
            {
                _numbers = new int[1];
                _numbers[0] = particleData.Number;
            }
        }

        /// <summary>
        /// Генерация множества частиц
        /// </summary>
        /// <param name="particleData">Данные о частицах</param>
        private void BuiltMany(ParticleM particleData)
        {
            // TODO: Implement this method

        } 

        #endregion

        private int[] GenerateNumbers(int count, int min, int max, object initializer, Func<object, int, int, int> generator)
        {
            int[] result = new int[count];

            for (int i = 0; i < count; i++)
            {
                result[i] = generator(initializer, min, max);
            }

            return result;
        }
    }

    /// <summary>
    /// Класс для описания функций генерации чисел
    /// </summary>
    /// <remarks>Генерация на основе законов распределения и других правил</remarks>
    class NumberGenerators
    {
        /// <summary>
        /// Вычисление значения слудующего случайного числа
        /// </summary>
        /// <param name="initializer">Инициализатор</param>
        /// <param name="min">Минимальное значение</param>
        /// <param name="max">Максимальное значение</param>
        /// <returns>Возвращает случайное число</returns>
        /// <remarks>Использует стандартный генератор Random (класс .net)</remarks>
        public static int GetRandom(object initializer, int min, int max)
        {
            return (initializer as Random).Next(min, max);
        }

        /// <summary>
        /// Вычисление значения слудующего случайного числа
        /// </summary>
        /// <param name="initializer">Инициализатор</param>
        /// <param name="min">Минимальное значение</param>
        /// <param name="max">Максимальное значение</param>
        /// <returns>Возвращает случайное число</returns>
        /// <remarks>Использует алгоритм Mersenne Twister</remarks>
        public static int MersenneTwister(object initializer, int min, int max)
        {
            return (initializer as MersenneTwister).Next(min, max);
        }
    }
}
