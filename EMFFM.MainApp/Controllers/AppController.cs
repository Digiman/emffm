﻿using System;
using System.Diagnostics;
using EMFFM.Input;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using NLog;
using EMFFM.MainApp.Helpers;
using System.Collections.Generic;

namespace EMFFM.MainApp.Controllers
{
    /// <summary>
    /// Главный класс для управления работой программы
    /// </summary>
    class AppController
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();

        InputData _data; // исходные данные из файла для работы контроллера

        Dictionary<ActionNames, Action<InputData>> _actions;

        /// <summary>
        /// Инициализация контроллера программы
        /// </summary>
        /// <param name="data">Входные данные из файла с данными</param>
        public AppController(InputData data)
        {
            _data = data;
            SetAssociations(data.Task);
        }

        /// <summary>
        /// Установка словаря ассоциация действий InputAPI с функциями их исполняющими
        /// </summary>
        private void SetAssociations(Task task)
        {
            logger.Debug("Initialize all actions!");
            
            // TODO: описание привязки инициализации по типу задачи в контроллере
            // NOTE: по сути здесь для аждого типа задачи выбирается свой список ассоциаций, единственная зависимость контроллера

            switch(task.Class)
            {
            	case TaskClasses.Common:
                    _actions = ActionHelper.CommonTasks();
                    break;
                case TaskClasses.Netgen:
                    _actions = ActionHelper.NetgenTasks();
                    break;
            }
        }
  
        /// <summary>
        /// Запуск обработки всех действий
        /// </summary>
        public void Run()
        {
            // выполняем действия в порядке их расположения в списке (по возрастанию кода)
            foreach (var item in _data.Actions)
            {
                Run(item);
            }
        }

        /// <summary>
        /// Запуск на обработку определенного действия
        /// </summary>
        /// <param name="action">Действие для выполнения</param>
        public void Run(EMFFM.Input.Elements.Action action)
        {
            logger.Info("Run action {0}", action);

            // засекаем время работы
            Stopwatch time = new Stopwatch();
            time.Start();

            _actions[action.Value](_data);

            time.Stop();

            logger.Info("Ends action {0} for {1} ms", action, time.ElapsedMilliseconds);
        }
    }
}
