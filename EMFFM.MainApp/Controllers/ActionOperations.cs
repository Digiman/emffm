﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Scalar.Elements;
using EMFFM.Common.Helpers;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.MainApp.FEM;
using EMFFM.MainApp.Mesh;
using EMFFM.MainApp.Helpers;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp.Controllers
{
    /// <summary>
    /// Класс с методами, описывающими последовательности операций для действий
    /// </summary>
    class ActionOperations
    {
        // NOTE: здесь в зависимости от ассоциаций и будут определятся классы решаемых задач! Т.е. какое действие будет присвоено для его выполнения.

        #region Действия для типа Common
        
        // TODO: удалить редундантный код после завершения переделки к новой архитектуре!

        /// <summary>
        /// Выполнение последовательности действий для генерации сетки
        /// </summary>
        /// <param name="data">Исходные данные для действия</param>
        /// <remarks>Используется встроенный генератор сетки (простые ругулярные ортогональные)</remarks>
        public static void GenerateMeshAction(InputData data)
        {
            // 1. Подготовка объекта для описания геометрии области дискретизации
            /*GeomData geomData = InputHelper.GetGeomData(data);

            // 2. Определение типа сетки и выбор соответсвующего класса для ее генерации
            switch (data.Element.Type)
            {
                case ElementType.Rectangle: // генерация сетки для прямоугольников
                    MeshBuilder.GenerateRectangleMesh(geomData, data.Output, data.Options);
                    break;
                case ElementType.Triangle: // генерация сетки для треугольников
                    MeshBuilder.GenerateTriangleMesh(geomData, data.Output, data.Options);
                    break;
                case ElementType.Parallelepiped: // генерация сетки для параллелепипедов
                    MeshBuilder.GenerateParallepipedMesh(geomData, data.Output, data.Options);
                    break;
                case ElementType.Tetrahedron: // генерация сетки для тетраэдров
                    MeshBuilder.GenerateTetrahedronMesh(geomData, data.Output, data.Options);
                    break;
            }*/
        }

        /*/// <summary>
        /// Выполнение последовательности действий для генерации частиц
        /// </summary>
        /// <param name="data">Исходные данные для действия</param>
        public static void GenerateParticlesAction(InputData data)
        {
            // TODO: описать метод действия для генерации частиц (GenerateParticle)
            Particles.ParticleBuilder pb = new Particles.ParticleBuilder(data.Particles);
            int[] nums = pb.Numbers;

            if (data.Options.IsOutputToFile)
            {
                OutputHelper.OutputVectorToFile<int>(nums, data.Output.GetFullPath(DatafileType.Particles));
            }
        }*/

        /*/// <summary>
        /// Построение глобальной матрицы
        /// </summary>
        /// <param name="data">Исходные даннве для действия</param>
        /// <remarks>Тестовый метод как и само действие!</remarks>
        public static void BuiltGlobalMatrixAction(InputData data)
        {
            // 1. Задание геометрии для разбиения
            GeomData geomData = InputHelper.GetGeomData(data);

            double[,] matrix = new double[0, 0];

            // 2. Определение типа сетки и выбор соответсвующего класса для ее генерации
            switch (data.Element.Type)
            {
                case ElementType.Rectangle: // генерация сетки для прямоугольников
                    matrix = BuiltGlobalMatrix<Rectangle, RectMesh>(data, new RectMesh(geomData));

                    matrix = BoundaryBuilder.ZeroBoundaryEdges(matrix, new int[] { 1, 2 });
                    break;
                case ElementType.Triangle: // генерация сетки для треугольников
                    matrix = BuiltGlobalMatrix<Triangle, TriMesh>(data, new TriMesh(geomData, TriangleStyle.Left));
                    break;
                case ElementType.Parallelepiped: // генерация сетки для параллелепипедов
                    matrix = BuiltGlobalMatrix<Parallelepiped, ParMesh>(data, new ParMesh(geomData));
                    break;
                case ElementType.Tetrahedron: // генерация сетки для тетраэдров
                    matrix = BuiltGlobalMatrix<Tetrahedron, TetMesh>(data, new TetMesh(geomData));
                    break;
            }

            if (data.Options.IsOutputToFile)
            {
                OutputHelper.OutputMatrixToFile<double>(matrix, data.Output.GetFullPath(DatafileType.Matrix));
            }
        }*/

        /// <summary>
        /// Решение задачи
        /// </summary>
        /// <param name="data">Входные данные</param>
        public static void SolveAction(InputData data)
        {
            // TODO: раcписать здесь порядок выполнения действий, необходимых для решения (задача Common)
            // NOTE: решаем по этапам

            // 1. Построение глобальной матрицы (левой части СЛАУ)
            // 1.1. Определение параметров геометрии
            //GeomData geomData = InputHelper.GetGeomData(data);
            double[,] globalMatrix = new double[0, 0];

            // 1.2. Определение типа элементов и генерации матрицы (см. алгоитм в методе BuiltGlobalMatrixAction)

            // 2. Построение вектора воздействий (правой части СЛАУ)


            // 3. Анализ результатов решения
        } 

        #endregion

        #region Действия для задачи типа Netgen
        
        /// <summary>
        /// Генерация сетки с использованием программы Netgen
        /// </summary>
        /// <param name="data">Входные данные</param>
        public static void GenerateNetgenMesh(InputData data)
        {
            string fileName = StringHelper.GetFileNameWithoutExtension(data.Geomerty.GometryFile);
            // используем генератор сеток Netgen
            NetgenMeshHelper.GenerateNetgenMesh(data, fileName);
        }

        /// <summary>
        /// Генерация глобальной матрицы на основании сетки из Netgen
        /// </summary>
        /// <param name="data">Входные данные</param>
        public static void BuiltGlobalMatrixNetgen(InputData data)
        {
            // 1. Генерация сетки
            string fileName = StringHelper.GetFileNameWithoutExtension(data.Geomerty.GometryFile);
            // используем генератор сеток Netgen
            TMesh<Tetrahedron, Triangle> mesh = NetgenMeshHelper.GenerateNetgenMesh(data, fileName);

            // 2. Построение глобальной матрицы
            double[,] matrix = BuiltGlobalMatrixNetgen(data, mesh);

            if (data.Options.IsOutputToFile)
            {
                // NOTE: проба параллельной либы через таски
                var task = new Task(() => OutputHelper.OutputMatrixToFile<double>(matrix, data.Output.GetFullPath(DatafileType.Matrix)));
                task.Start();
            }
        }

        /// <summary>
        /// Решения задачи с использованием программы Netgen
        /// </summary>
        /// <param name="data">Входные данные</param>
        public static void SolveNetgen(InputData data)
        {
            // 1. Генерация сетки
            string fileName = StringHelper.GetFileNameWithoutExtension(data.Geomerty.GometryFile);
            // используем генератор сеток Netgen
            TMesh<Tetrahedron, Triangle> mesh = NetgenMeshHelper.GenerateNetgenMesh(data, fileName);

            // формируем объект для построения глобальных матриц
            GlobalMatrix<Tetrahedron, Triangle> matrix = new GlobalMatrix<Tetrahedron, Triangle>(data, mesh);

            // формируем объект для построения векторов в воздействиями (значения падающего поля)
            WaveData wave = InputHelper.GetWaveData(data); // формируем сведения о волне от источника
            List<Edge> boundSource = MeshHelper.GetBoundaryEdges<Tetrahedron, Triangle>(mesh, data.Sources.Single(s => s.Type == SourceTypes.Simple).Boundary);
            ForceVector vector = new ForceVector(wave, SourceFunctions.MonoWave, mesh.EdgesCount, boundSource);

            // 2. Решение задачи
            int[] boundNumsBoundary = MeshHelper.GetNumbersOfBoundaryEdges<Tetrahedron, Triangle>(mesh, data.Boundaries.Single(b => b.Type == BoundaryTypes.Dirichlet).BoundaryNumber);
            Solver<Tetrahedron, Triangle> solver = new Solver<Tetrahedron, Triangle>(data, wave, matrix, vector, boundNumsBoundary);
            solver.Run(); // запуск
            
            if (data.Options.IsOutputToFile)
            {
                int count = 0;
            	foreach (var item in solver.Results)
                {
                    item.OutputToFile(String.Format("{0}resultdata-{1}.txt", data.Output.Path, count));
                    count++;
                }
            }

            // 3. Обработка результатов решения
            FieldAnalysis<Tetrahedron, Triangle> analysis = new FieldAnalysis<Tetrahedron, Triangle>(solver.Results, mesh, ElementType.Tetrahedron);
            //analysis.CalculateDifference();
            analysis.CalculateFields(data.Geomerty.CoFactor);
            analysis.PrintElementsField(data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields);

            // 4. Вывод результатов на основе указанных проб
            if (data.Options.IsProbeOutput)
            {
                foreach (var item in data.ProbePoints)
                {
                    if (item.Type == ProbeType.Domain)
                    {
                        int[] nums = MeshHelper.GetElementsByDomain(mesh, item.DomainNumber);
                        analysis.PrintElementsField(nums, data.Output.Path, data.Output.FilePrefix, data.Options.WithEdgeFields, item.DomainNumber);
                    }
                }
            }
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Генерация глобальной матрицы в зависимости от типов элементов
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <typeparam name="TType2">Класс генератор сетки</typeparam>
        /// <param name="data">Входные данные</param>
        /// <param name="meshClass">Экземпляр класса для генерации сетки</param>
        /// <returns>Возвращает глобальную матрицу</returns>
        /// <remarks>Генерирует с учетом частиц (и без них), а также с параметром K</remarks>
        private static double[,] BuiltGlobalMatrix<TType, TType2>(InputData data, TType2 meshClass)
        {
            // NOTE: не используется!
            List<TType> elements = (meshClass as IMesh<TType>).GenerateMesh();
            int size = EdgeMesh.BuiltEdges(ref elements);

            if (data.Options.WithParticles)
            {
                //Particles.ParticleBuilder pb = new Particles.ParticleBuilder(data.Particles);
                //elements = MaterialsBuilder.SetMaterials(elements, data.Materials, pb.Numbers, data.Particles);
            }
            else
            {
                elements = MaterialsBuilder.SetDefaultMaterials(elements, data.Materials.Single(m => m.IsDefault == true));
            }

            double freq = ParametersBuilder.GetFrequency(data.Sources, SourceTypes.Simple);
            double K = ParametersBuilder.CalculateWaveNumber(freq);
            double omega = ParametersBuilder.CalculateOmega(freq);

            return MatrixBuilder.BuiltGlobalMatrix(size, elements, K, omega);
        }

        /// <summary>
        /// Генерация глобальной матрицы для сетки из Netgen
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="data">Входные данные</param>
        /// <param name="meshData">Построенная сетка</param>
        /// <returns>Возвращает глобальную матрицу</returns>
        private static double[,] BuiltGlobalMatrixNetgen<TType, TType2>(InputData data, TMesh<TType, TType2> meshData)
        {
            // NOTE: не используется!
            MaterialsBuilder.SetMaterialsByDomain(ref meshData, data.Geomerty, data.Materials, data.Plasmons);

            double freq = ParametersBuilder.GetFrequency(data.Sources, SourceTypes.Simple);
            double K = ParametersBuilder.CalculateWaveNumber(freq);
            double omega = ParametersBuilder.CalculateOmega(freq);

            return MatrixBuilder.BuiltGlobalMatrix(meshData.EdgesCount, meshData.MeshElements, K, omega);
        }

        #endregion
    }
}
