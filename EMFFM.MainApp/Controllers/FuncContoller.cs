﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMFFM.MainApp.Common;

namespace EMFFM.MainApp.Controllers
{
    // TODO: убрать класс или продумать как его использовать для выполнения функции (через делегаты)

    class FuncContoller<Enum, TTypeIn, TTypeOut>
    {
        Dictionary<Enum, Func<TTypeIn, TTypeOut>> _functions;

        public FuncContoller()
        {
        }

        private void SetAssociation()
        {
        }

        public TTypeOut Execute<TTypeIn, TTypeOut>(Func<TTypeIn, TTypeOut> function, TTypeIn inValue)
        {
            return function(inValue);
        }
    }
}
