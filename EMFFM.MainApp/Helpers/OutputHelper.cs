﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp.Helpers
{
    /// <summary>
    /// Вспомогательный класс для функций вывода данных в файлы
    /// </summary>
    /// <remarks>Генерирует текстовые файлы и XML файлы</remarks>
    static class OutputHelper
    {
        #region Работа с текстовыми файлами
        
        /// <summary>
        /// Печать матрицы в файл
        /// </summary>
        /// <typeparam name="TType">Тип элементов матрицы</typeparam>
        /// <param name="matrix">Матрица с данными</param>
        /// <param name="fname">Путь и название файла для вывода</param>
        public static void OutputMatrixToFile<TType>(TType[,] matrix, string fname)
        {
            // создаем пустой файл для выводв данных
            StreamWriter file = new StreamWriter(fname);

            // вывод матрицы в файл
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                    file.Write("{0}\t", matrix[i, j]);
                file.WriteLine();
            }

            file.Close();
        }

        /// <summary>
        /// Печать вектора (массива) в файл
        /// </summary>
        /// <typeparam name="TType">Тип элементов вектора (массива)</typeparam>
        /// <param name="vector">Вектор (массив)</param>
        /// <param name="fname">Путь и название файла для вывода</param>
        public static void OutputVectorToFile<TType>(TType[] vector, string fname)
        {
            // создаем пустой файл для выводв данных
            StreamWriter file = new StreamWriter(fname);

            // вывод вектора (массива) в файл
            for (int i = 0; i < vector.Length; i++)
            {
                file.WriteLine("{0}", vector[i]);
            }

            file.Close();
        }

        /// <summary>
        /// Печать в файл сведений об элементах полученной в программе сетки
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов сетки</param>
        /// <param name="fname">Путь и название файла для вывода данных</param>
        /// <param name="withEdges">Выводить ли данные о ребрах</param>
        /// <param name="isAppend">Дописывать ли в файл</param>
        public static void OutputMeshDataToFile<TType>(List<TType> elements, string fname, bool withEdges = false, bool isAppend = false)
        {
            StreamWriter file = new StreamWriter(fname, isAppend);
            foreach (var item in elements)
            {
                file.WriteLine("{0}", item.ToString());
                if (withEdges)
                {
                    file.WriteLine("Edges: {0}", (item as Element).EdgesToString());
                }
            }
            file.Close();
        }

        /// <summary>
        /// Вывод диагонали матрицы в файл
        /// </summary>
        /// <param name="mat">Матрица с данными</param>
        /// <param name="fname">Имя файла для вывода</param>
        public static void PrintDiagonalToFile(double[,] mat, string fname)
        {
            double[] diag = new double[mat.GetLength(0)];
            for (int i = 0; i < mat.GetLength(0); i++)
            {
                diag[i] = mat[i, i];
            }
            OutputVectorToFile<double>(diag, fname);
        }

        #endregion

        #region Работа с XML файлами
        
        /// <summary>
        /// Вывод списка элементов сетки в XML файл
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов</param>
        /// <param name="fname">Путь и имя файла для вывода</param>
        /// <param name="withEdges">Выводить данные о ребрах</param>
        public static void OutputMeshDataToXmlFile<TType>(List<TType> elements, string fname, bool withEdges = false)
        {
            XmlTextWriter file = new XmlTextWriter(fname, Encoding.UTF8);

            file.WriteStartDocument();

            file.WriteStartElement("mesh"); // открываем <mesh>
            // пишем список элементов
            file.WriteStartElement("elements"); // открываем <elements>
            file.WriteStartAttribute("count");
            file.WriteValue(elements.Count);

            // пишем сами элементы
            foreach (var item in elements)
            {
                file.WriteStartElement("element"); // открываем <element>

                file.WriteStartAttribute("number");
                file.WriteValue((item as Element).Number);

                // пишем список узлов элемента
                file.WriteStartElement("nodes"); // открываем <nodes>
                foreach (Node node in (item as Element).Nodes)
                {
                    file.WriteStartElement("node"); // открываем <node>
                    file.WriteStartAttribute("number");
                    file.WriteValue(node.Number);
                    file.WriteStartAttribute("x");
                    file.WriteValue(node.Coord.X);
                    file.WriteStartAttribute("y");
                    file.WriteValue(node.Coord.Y);
                    file.WriteStartAttribute("z");
                    file.WriteValue(node.Coord.Z);
                    file.WriteEndElement(); // закрываем </node>
                }
                file.WriteEndElement(); // закрываем </nodes>

                if (withEdges)
                {
                    // пишем список ребер элемента
                    file.WriteStartElement("edges"); // открываем <nodes>
                    foreach (Edge edge in (item as Element).Edges)
                    {
                        file.WriteStartElement("edge"); // открываем <edge>
                        file.WriteStartAttribute("number");
                        file.WriteValue(edge.Number);
                        file.WriteStartAttribute("vetr1");
                        file.WriteValue(edge.Vertex1.Number);
                        file.WriteStartAttribute("vert2");
                        file.WriteValue(edge.Vertex2.Number);
                        if (edge.IsBoundary)
                        {
                            file.WriteStartAttribute("isBoundary");
                            file.WriteValue(edge.IsBoundary);
                            file.WriteStartAttribute("boundary");
                            file.WriteValue(edge.BoundaryNumber);
                        }
                        file.WriteEndElement(); // закрываем </edge>
                    }
                    file.WriteEndElement(); // закрываем </nodes>
                }

                file.WriteStartElement("center"); // открываем <center>
                Point tmp = (item as Element).GetCenterPoint();
                file.WriteStartAttribute("x");
                file.WriteValue(tmp.X);
                file.WriteStartAttribute("y");
                file.WriteValue(tmp.Y);
                file.WriteStartAttribute("z");
                file.WriteValue(tmp.Z);
                file.WriteEndElement(); // закрываем </center>

                file.WriteEndElement(); // закрываем </element>
            }

            file.WriteEndElement(); // закрываем </elements>

            file.WriteEndElement(); // закрываем </mesh>

            file.Close();
        } 

        #endregion
    }
}
