﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MainApp.Mesh;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с сеткой
    /// </summary>
    static class MeshHelper
    {
        /// <summary>
        /// Генерация номеров граничных ребер по данным сетки
        /// </summary>
        /// <typeparam name="TType">Тиип объемных элементов</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов</typeparam>
        /// <param name="mesh">Сетка</param>
        /// <param name="boundaryNumber">Номер границы</param>
        /// <returns>возвращает массив номеров граничных ребер</returns>
        public static int[] GetNumbersOfBoundaryEdges<TType, TType2>(TMesh<TType, TType2> mesh, int boundaryNumber)
        {
            List<int> numbers = new List<int>();

            foreach (var element in mesh.MeshElements)
            {
                foreach (var edge in (element as Element).Edges)
                {
                    if (edge.IsBoundary && edge.BoundaryNumber == boundaryNumber)
                    {
                        numbers.Add(edge.Number);
                    }
                }
            }
            numbers.Sort();

            return numbers.Distinct().ToArray();
        }

        /// <summary>
        /// Генерация списка ребер
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        /// <param name="mesh">Сетка</param>
        /// <param name="withBoundary">Включая граничные ребра</param>
        /// <returns>Возвращает список ребер</returns>
        public static List<Edge> GetEdges<TType, TType2>(TMesh<TType, TType2> mesh, bool withBoundary = false)
        {
            List<Edge> edges = new List<Edge>(withBoundary ? mesh.EdgesCount : mesh.EdgesCount - mesh.BoundaryEdgesCount);

            if (withBoundary) // если с граничными
            {
                foreach (var element in mesh.MeshElements)
                {
                    foreach (var edge in (element as Element).Edges)
                    {
                        edges.Add(edge);
                    }
                }
            }
            else // если без граничных
            {
                foreach (var element in mesh.MeshElements)
                {
                    foreach (var edge in (element as Element).Edges)
                    {
                        if (!edge.IsBoundary)
                        {
                            edges.Add(edge);
                        }
                    }
                }
            }
            edges = edges.AsParallel().Distinct().AsParallel().ToList();
            edges.Sort();

            return edges;
        }

        /// <summary>
        /// Получение списка граничных ребер
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        /// <param name="mesh">Сетка</param>
        /// <param name="boundaryNumber">Номер границы</param>
        /// <returns>Возвращает список граничных ребер</returns>
        public static List<Edge> GetBoundaryEdges<TType, TType2>(TMesh<TType, TType2> mesh, int boundaryNumber)
        {
            List<Edge> edges = new List<Edge>();

            foreach (var element in mesh.MeshElements)
            {
                foreach (var edge in (element as Element).Edges)
                {
                    if (edge.IsBoundary && edge.BoundaryNumber == boundaryNumber)
                    {
                        edges.Add(edge);
                    }
                }
            }
            edges.Sort();

            return edges.Distinct().ToList();
        }

        /// <summary>
        /// Получение списка номеров элементов, находящихся в области
        /// </summary>
        /// <typeparam name="TType">Тип объемных элементов сетки</typeparam>
        /// <typeparam name="TType2">Тип поверхностных элементов сетки</typeparam>
        /// <param name="mesh"></param>
        /// <param name="domainNumber">Номер области</param>
        /// <returns>Возвращает массив номеров элементов из заданной области</returns>
        public static int[] GetElementsByDomain<TType, TType2>(TMesh<TType, TType2> mesh, int domainNumber)
        {
            List<int> result = new List<int>();

            foreach (var element in mesh.MeshElements)
            {
                if ((element as Element).SubDomain == domainNumber)
                {
                    result.Add((element as Element).Number);
                }
            }

            return result.ToArray();
        }
    }
}
