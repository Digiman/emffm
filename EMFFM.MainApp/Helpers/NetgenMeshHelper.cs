﻿using System;
using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.MainApp.Mesh;
using EMFFM.MainApp.Mesh.Elements;
using EMFFM.MeshParser;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.Old;

namespace EMFFM.MainApp.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с сеткой, генерируемой программой Netgen 
    /// </summary>
    /// <remarks>
    /// Работа с сеткой и ее обработка.
    /// Генерирует сетку из тетраэдров. Можно также задавать свои параметры для генерации.
    /// </remarks>
    static class NetgenMeshHelper
    {
        /// <summary>
        /// Генерация сетки с помощью пограммы Netgen
        /// </summary>
        /// <param name="data">Входные данные</param>
        /// <param name="fileName">Имя файла с геометрией (без расширения!)</param>
        /// <returns>Возвращает список элементов сетки (тетраэдры)</returns>
        public static TMesh<Tetrahedron, Triangle> GenerateNetgenMesh(InputData data, string fileName)
        {
            TMesh<Tetrahedron, Triangle> meshData = new TMesh<Tetrahedron, Triangle>(ElementType.Tetrahedron);

            // 1. Использование библиотеки NetgenUtils
            NetgenUtils.Initializer init = new NetgenUtils.Initializer(data.NetgenOptions.ParamsFile);
            NetgenUtils.Preprocessing.MeshGenerator meshGen = new NetgenUtils.Preprocessing.MeshGenerator(init.Vars);
            string geoFile = String.Format("input\\{0}.geo", fileName);

            if (data.Geomerty.IsGenerateGeomFile)
            {
                // 1.1. Генерация файла геометрии
                NetgenUtils.GeomFiles.The3DGeometryFileReader reader = new NetgenUtils.GeomFiles.The3DGeometryFileReader(String.Format("input\\{0}.xml", fileName));
                NetgenUtils.GeomFiles.Elements.GeometryData geomDataFile = reader.GetGemetryData;

                GeomFileGenerator.Generate(geoFile, GeometryTypes.The3D, geomDataFile);
            }
            // 1.2. Генерация файла сетки
            meshGen.Generate(geoFile, String.Format("{0}.neu", fileName));

            // 2. Использование библиотеки MeshParser для разбора сетки
            MeshFileParser parser = new MeshFileParser(String.Format("{0}\\{1}.neu", init.Vars.OutputPath, fileName));
            MeshData mesh = parser.Parse();
            if (data.Options.IsOutputToFile)
            {
                EMFFM.MeshParser.Helpers.OutputHelper.OutputMeshDataToXmlFile(mesh, String.Format("{0}\\{1}.xml", init.Vars.OutputPath, fileName));
            }

            // 3. Обработка данных сетки - составление списка элементов (тетраэдров)
            List<Tetrahedron> elements = GenerateVolumeElementsFromMeshData(mesh, data.Options.IsBuiltEdges);
            meshData.MeshElements = elements;

            // 3.1. Нумерация ребер элементов (глобальная нумерация)
            if (data.Options.IsBuiltEdges)
            {
                meshData.EdgesCount = EdgeMesh.BuiltEdges(ref elements);
            }

            // 4. Обработка списка поверхностных элементов
            List<Triangle> surfs = GenerateSurfaceElementsFromMeshData(mesh, data.Options.IsBuiltEdges);
            meshData.SurfElements = surfs;

            // 4.1. Нумерация ребер поверхностных элементов (глобальная нумерация)
            if (data.Options.IsBuiltEdges)
            {
                meshData.BoundaryEdgesCount = EdgeMesh.BuiltEdges(ref surfs);
            }

            // 5. Генерация граничных ребер
            GenerateBoundaryEdges(ref meshData);

            // 6. Вывод в файл результата
            if (data.Options.IsOutputToFile)
            {
                //Output.OutputHelper.OutputMeshDataToFile<Tetrahedron>(elements, data.Output.GetFullPath(DatafileType.Mesh), data.Options.IsBuiltEdges, data.Options.IsAppend);
                string xmlFileName = StringHelper.GetFileNameWithoutExtension(data.Output.GetFullPath(DatafileType.Mesh)) + ".xml";
                OutputHelper.OutputMeshDataToXmlFile<Tetrahedron>(elements, xmlFileName, data.Options.IsBuiltEdges);
            }

            return meshData;
        }

        /// <summary>
        /// Обработка сетки из программы Netgen из тетраэдров (объемные элементы)
        /// </summary>
        /// <param name="data">Данные сетки Netgen</param>
        /// <param name="isBuiltEdges">Генерировать ли ребра для элементов</param>
        /// <returns>Возвращает список элементов (тетраэдров)</returns>
        private static List<Tetrahedron> GenerateVolumeElementsFromMeshData(MeshData data, bool isBuiltEdges = false)
        {
            List<Tetrahedron> elements = new List<Tetrahedron>(data.VolumeElements.Count);

            int nodeNumber, elementNumber = 1;

            foreach (var vel in data.VolumeElements)
            {
                List<Node> nodes = new List<Node>(vel.Nodes.Length);
                for (int i = 0; i < vel.Nodes.Length; i++)
                {
                    nodeNumber = vel.Nodes[i];
                    Point p = new Point(data.Points[nodeNumber - 1].X, data.Points[nodeNumber - 1].Y, data.Points[nodeNumber - 1].Z);
                    nodes.Add(new Node(p, nodeNumber));
                }

                Tetrahedron tet = new Tetrahedron(nodes, elementNumber, vel.SubDomain);
                if (isBuiltEdges)
                {
                    tet.AssociateEdges();
                }
                elements.Add(tet);

                elementNumber++;
            }

            return elements;
        }

        /// <summary>
        /// Генерация поверхностной сетки (треугольние элементы)
        /// </summary>
        /// <param name="data">Данные сетки Netgen</param>
        /// <param name="isBuiltEdges">Генерировать ли ребра для элементов</param>
        /// <returns>Возвращает список поверхностных элементов</returns>
        private static List<Triangle> GenerateSurfaceElementsFromMeshData(MeshData data, bool isBuiltEdges = false)
        {
            List<Triangle> elements = new List<Triangle>(data.SurfaceElements.Count);

            int nodeNumber, elementNumber = 1;

            foreach (var sel in data.SurfaceElements)
            {
                List<Node> nodes = new List<Node>(sel.Nodes.Length);
                for (int i = 0; i < sel.Nodes.Length; i++)
                {
                    nodeNumber = sel.Nodes[i];
                    Point p = new Point(data.Points[nodeNumber - 1].X, data.Points[nodeNumber - 1].Y, data.Points[nodeNumber - 1].Z);
                    nodes.Add(new Node(p, nodeNumber));
                }

                Triangle tet = new Triangle(nodes, elementNumber, sel.BoundaryCond);
                if (isBuiltEdges)
                {
                    // сразу выполнии ассоциацию для ребер на границах (на поверхностях)
                    tet.AssociateBoundaryEdges(sel.BoundaryCond);
                }
                elements.Add(tet);

                elementNumber++;
            }

            return elements;
        }

        /// <summary>
        /// Обработка ребер для граничных условий
        /// </summary>
        /// <param name="mesh">Сетка со списками элементов</param>
        private static void GenerateBoundaryEdges(ref TMesh<Tetrahedron, Triangle> mesh)
        {
            List<Edge> surfEdges = new List<Edge>(mesh.BoundaryEdgesCount);

            foreach (var surfElem in mesh.SurfElements)
            {
                surfEdges.AddRange(surfElem.Edges);
            }

            foreach (var surfEdge in surfEdges)
            {
                foreach (var element in mesh.MeshElements)
                {
                    foreach (var edge in element.Edges)
                    {
                        if (edge == surfEdge)
                        {
                            edge.IsBoundary = true;
                            edge.BoundaryNumber = surfEdge.BoundaryNumber;
                        }
                    }
                }
            }
        }
    }
}
