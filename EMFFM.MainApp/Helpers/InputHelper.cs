﻿using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Scalar.Elements;
using EMFFM.Common.Math.Elements;
using EMFFM.Input;
using EMFFM.Input.Elements;
using EMFFM.MainApp.FEM;

namespace EMFFM.MainApp.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы и конвертации входных данных
    /// </summary>
    static class InputHelper
    {
        /*/// <summary>
        /// Конвертация входных данных в формат для генерации сеток
        /// </summary>
        /// <param name="data">Входные данные</param>
        /// <returns>Возвращает данные для генератора сеток</returns>
        public static GeomData GetGeomData(InputData data)
        {
            GeomData geomData = new GeomData();

            // размеры области дискретизации
            geomData.A = System.Math.Abs(data.Box.P2.X - data.Box.P1.X); // x2-x1
            geomData.B = System.Math.Abs(data.Box.P2.Y - data.Box.P1.Y); // y2-y1
            geomData.C = System.Math.Abs(data.Box.P2.Z - data.Box.P1.Z); // z2-z1
            // размеры элемента сетки
            geomData.Da = data.Element.Da;
            geomData.Db = data.Element.Db;
            geomData.Dc = data.Element.Dc;
            // начальная точка
            geomData.StartPoint = data.Box.P1;

            return geomData;
        }*/

        /// <summary>
        /// Получение параметров волны
        /// </summary>
        /// <param name="data">Входные данные</param>
        /// <returns>Возвращает постоянные сведения о волне</returns>
        public static WaveData GetWaveData(InputData data)
        {
            WaveData wave = new WaveData();

            Source source = data.Sources.Single(s => s.Type == SourceTypes.Simple);

            wave.Point = source.Point;
            wave.Amplitude = source.Amplitude;
            wave.Phase = source.Phase;
            wave.Vector = new UnitVector(source.Direction);
            wave.Omega = ParametersBuilder.CalculateOmega(ParametersBuilder.GetFrequency(source.Lambda));
            wave.WaveNumber = ParametersBuilder.CalculateWaveNumber(ParametersBuilder.GetFrequency(source.Lambda));

            return wave;
        }
    }
}
