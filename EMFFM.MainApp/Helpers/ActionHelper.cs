﻿using System;
using System.Collections.Generic;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.MainApp.Controllers;

namespace EMFFM.MainApp.Helpers
{
    /// <summary>
    /// Класс для описания ассоциаций действий в соответствии с типом задачи
    /// </summary>
    class ActionHelper
    {
        // NOTE: для каджого типа задачи определены свои действия. Они в общем одинаковы, но для разных задач они могут отсутствовать вообще (как, например, генерация частиц в Netgen)

        /// <summary>
        /// Генерация списка действий и ассоциация для общего класса задач
        /// </summary>
        /// <returns>Возвращает список ассоциаций для действий из Input API</returns>
        public static Dictionary<ActionNames, Action<InputData>> CommonTasks()
        {
            Dictionary<ActionNames, Action<InputData>>  actions = new Dictionary<ActionNames, Action<InputData>>(Enum.GetNames(typeof(ActionNames)).Length);

            actions.Add(ActionNames.GenerateMesh, ActionOperations.GenerateMeshAction);
            //actions.Add(ActionNames.GenerateParticles, ActionOperations.GenerateParticlesAction);
            //actions.Add(ActionNames.BuiltGlobalMatrix, ActionOperations.BuiltGlobalMatrixAction);

            return actions;
        }

        /// <summary>
        /// Генерация списка действий и ассоциаций для задачи с использованием Netgen
        /// </summary>
        /// <returns>Возвращает список ассоциаций для действий из Input API</returns>
        public static Dictionary<ActionNames, Action<InputData>> NetgenTasks()
        {
            Dictionary<ActionNames, Action<InputData>> actions = new Dictionary<ActionNames, Action<InputData>>(Enum.GetNames(typeof(ActionNames)).Length);

            actions.Add(ActionNames.GenerateMesh, ActionOperations.GenerateNetgenMesh);
            actions.Add(ActionNames.BuiltGlobalMatrix, ActionOperations.BuiltGlobalMatrixNetgen);
            actions.Add(ActionNames.Solve, ActionOperations.SolveNetgen);

            return actions;
        }
    }
}
