﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using EMFFM.Common.FEM.Scalar.Elements;

namespace EMFFM.MainApp.Helpers
{
    // TODO: подумать как оставить код без изменения, в лишь привязать к типу полей (скалярное комплексное)

    /// <summary>
    /// Вспомогательный класс для работы с полем
    /// </summary>
    static class FieldHelper
    {
        /// <summary>
        /// Чтение поля, полученного в результате решения СЛАУ из результирующего файла
        /// </summary>
        /// <param name="fname">Имя результирующего файла с данными</param>
        /// <returns>Возвращает список полей на ребрах</returns>
        public static List<EdgeField> ReadFieldFromResultFile(string fname)
        {
            List<EdgeField> data = new List<EdgeField>();

            StreamReader file = new StreamReader(fname);

            while (!file.EndOfStream)
            {
                EdgeField field = new EdgeField() { TangentialField = Convert.ToDouble(file.ReadLine()) };
                data.Add(field);
            }

            file.Close();

            return data;
        }

        /// <summary>
        /// Вычисление разницы между тангенциальными компонентами полей
        /// </summary>
        /// <param name="fields1">Поле 1</param>
        /// <param name="fields2">Поле 2</param>
        /// <returns>Возвращает новое результирующее поле</returns>
        public static List<EdgeField> Difference(List<EdgeField> fields1, List<EdgeField> fields2)
        {
            if (fields1.Count != fields2.Count)
            {
                throw new Exception("Нельзя вычислить разницу между полями с различным числом элементов!");
            }

            List<EdgeField> result = new List<EdgeField>(fields1.Count);

            for (int i = 0; i < fields1.Count; i++)
            {
                result.Add(new EdgeField()
                {
                    TangentialField = fields1[i].TangentialField - fields2[i].TangentialField
                });
            }

            return result;
        }

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="field">Значения поля</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        public static void OutputFieldToFile(List<EdgeField> field, string fileName)
        {
            StreamWriter file = new StreamWriter(fileName);

            file.WriteLine("# Edge \t Tangential Filed \t Full Field");

            for (int i = 0; i < field.Count; i++)
            {
                file.WriteLine("{0}\t{1}\t{2}", i + 1, field[i].TangentialField, field[i].FullField);
            }

            file.Close();
        }

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="field">Значения поля</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        /// <param name="withEdgesField">Выводить ли значения тангенциальных компонент на ребрах?</param>
        public static void OutputFieldToFile(List<ElementField> field, string fileName, bool withEdgesField = false)
        {
            StreamWriter file = new StreamWriter(fileName);

            if (withEdgesField)
            {
                StringBuilder header = new StringBuilder(100);
                header.Append("# Element \t");
                for (int i = 0; i < field[0].EdgeFields.Count; i++)
                {
                    header.AppendFormat("EdgeField{0} \t", i + 1);
                }
                header.Append(" FieldX \t FieldY \t FieldZ");
                file.WriteLine(header.ToString());
            }
            else
            {
                file.WriteLine("# Element \t  FieldX \t FieldY \t  FieldZ");
            }

            for (int i = 0; i < field.Count; i++)
            {
                if (withEdgesField)
                {
                    StringBuilder line = new StringBuilder(200);
                    line.AppendFormat("{0} \t", i + 1);
                    for (int j = 0; j < field[0].EdgeFields.Count; j++)
                    {
                        line.AppendFormat("{0} \t", field[i].EdgeFields[j].TangentialField);
                    }
                    line.AppendFormat(" {0} \t {1} \t {2}", field[i].CenterField.FieldX, field[i].CenterField.FieldY, field[i].CenterField.FieldZ);
                    file.WriteLine(line.ToString());
                }
                else
                {
                    file.WriteLine("{0}\t{1}\t{2}\t{3}", i + 1, field[i].CenterField.FieldX, field[i].CenterField.FieldY, field[i].CenterField.FieldZ);
                }
            }

            file.Close();
        }

        /// <summary>
        /// Вывод поля в файл
        /// </summary>
        /// <param name="field">Значения поля</param>
        /// <param name="elementNumbers">Список номеров элементов для вывода результатов</param>
        /// <param name="fileName">Имя файла для вывода результата</param>
        /// <param name="withEdgesField">Выводить ли значения тангенциальных компонент на ребрах?</param>
        public static void OutputFieldToFile(List<ElementField> field, int[] elementNumbers, string fileName, bool withEdgesField = false)
        {
            StreamWriter file = new StreamWriter(fileName);

            if (withEdgesField)
            {
                StringBuilder header = new StringBuilder(100);
                header.Append("# Element \t");
                for (int i = 0; i < field[0].EdgeFields.Count; i++)
                {
                    header.AppendFormat("EdgeField{0} \t", i + 1);
                }
                header.Append(" FieldX \t FieldY \t FieldZ");
                file.WriteLine(header.ToString());
            }
            else
            {
                file.WriteLine("# Element \t  FieldX \t FieldY \t  FieldZ");
            }

            for (int i = 0; i < elementNumbers.Length; i++)
            {
                int curNum = elementNumbers[i] - 1;
                if (withEdgesField)
                {
                    StringBuilder line = new StringBuilder(200);
                    line.AppendFormat("{0} \t", elementNumbers[i]);
                    for (int j = 0; j < field[0].EdgeFields.Count; j++)
                    {
                        line.AppendFormat("{0} \t", field[curNum].EdgeFields[j].TangentialField);
                    }
                    line.AppendFormat(" {0} \t {1} \t {2}", field[curNum].CenterField.FieldX, field[curNum].CenterField.FieldY, field[curNum].CenterField.FieldZ);
                    file.WriteLine(line.ToString());
                }
                else
                {
                    file.WriteLine("{0}\t{1}\t{2}\t{3}", elementNumbers[i], field[curNum].CenterField.FieldX, field[curNum].CenterField.FieldY, field[curNum].CenterField.FieldZ);
                }
            }

            file.Close();
        }
    }
}
