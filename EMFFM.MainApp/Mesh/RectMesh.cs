﻿using System.Collections.Generic;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Common.Structs;
using EMFFM.MainApp.Mesh.Elements;
using NLog;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Генерация сетки из прямоугольников
    /// </summary>
    class RectMesh : IMesh<Rectangle>
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();

        int _ntx, _nty, _nel, _nuz;
        readonly GeomData _geomData;

        /// <summary>
        /// Инициализация класса
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        public RectMesh(GeomData geomData)
        {
            logger.Info("Initializing rectangle mesh...");

            _geomData = geomData;

            // Вычисляем параметры для разбиения
            _ntx = (int)System.Math.Floor(geomData.A / geomData.Da) + 1;     // узлов по оси OX (длине)
            _nty = (int)System.Math.Floor(geomData.B / geomData.Db) + 1;     // узлов по оси OY (ширине)
            _nel = (int)System.Math.Floor((geomData.A * geomData.B) / (geomData.Da * geomData.Db)); // кол-во элементов сетки
            _nuz = _ntx * _nty;   // кол-во узлов сетки
        }

        /// <summary>
        /// Генерация сетки из прямоугольников (плоская сетка)
        /// </summary>
        /// <returns>Возвращает список прямоугольников</returns>
        public List<Rectangle> GenerateMesh()
        {
            logger.Info("Calculating 2D plate points...");

            // 1. Дискретизируем ортогональной сеткой верхнюю плоскость (XY)
            List<Point> points = Get2DPoints(_nuz, _ntx, _nty, _geomData.Da, _geomData.Db, _geomData.StartPoint.X, _geomData.StartPoint.Y);

            logger.Info("Generate rectangle elements...");

            // 2. Формирование итоговой нумерации элементов и узлов
            List<Rectangle> result = GetElements(_ntx, _nty, _nel, points);

            logger.Info("Ends generating rectangle elements...");

            return result;
        }

        /// <summary>
        /// Вычисление координат узлов для сетки на плоскости XOY
        /// </summary>
        /// <param name="nuz">Количестве узлов</param>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="da">Шаг сетки по оси OX</param>
        /// <param name="db">Шаг сетки по оси OY</param>
        /// <param name="x0">Начальная координата X0</param>
        /// <param name="y0">Начальная координата Y0</param>
        /// <returns>Возвращает список точек</returns>
        private List<Point> Get2DPoints(int nuz, int ntx, int nty, double da, double db, double x0, double y0)
        {
            double[,] cuz = new double[nuz, 2];
            double sx = x0 - da, sy;
            int uzcount = 0, i, j;
            // определение координат узлов
            for (i = 0; i < ntx; i++)
            {
                sx += da;
                sy = y0 - db;
                for (j = 0; j < nty; j++)
                {
                    uzcount++;
                    sy = sy + db;
                    cuz[uzcount - 1, 0] = sx; // координата X
                    cuz[uzcount - 1, 1] = sy; // координата Y
                }
            }
            // формируем массив точек для возврата
            List<Point> pnt = new List<Point>(nuz);
            for (i = 0; i < nuz; i++)
            {
                pnt.Add(new Point(cuz[i, 0], cuz[i, 1]));
            }
            return pnt;
        }

        /// <summary>
        /// Формирование итоговой нумерации и элементов (завершение дискретизации области)
        /// </summary>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="nel">Количество элементов на слое</param>
        /// <param name="points">Список координат точек для плоскости</param>
        /// <returns>Возвращает список элементов</returns>
        private List<Rectangle> GetElements(int ntx, int nty, int nel, List<Point> points)
        {
            List<Rectangle> rects = new List<Rectangle>(nel); // размерность равна кол-во элементов

            int elcount = 1; // глобальный номер элемента (параллелепипеда) (с 1)
            int uzcount = 0;
            // бегаем теперь по плоской сетке слоя
            for (int j = 0; j < ntx - 1; j++)
            {
                for (int k = 0; k < nty - 1; k++)
                {
                    // формирование списка узлов элемента
                    List<Node> nodes = new List<Node>(4); // 4 узла

                    uzcount = uzcount + 1; // номер левого нижнего узла (базовый) (с 1)

                    // формирование узлов
                    nodes.Add(new Node(points[uzcount - 1], uzcount)); // узел 1 (левый нижний)
                    nodes.Add(new Node(points[uzcount], uzcount + 1)); // узел 2 (левый верхний)
                    nodes.Add(new Node(points[uzcount + nty], uzcount + nty + 1)); // узел 3 (правый верхний)
                    nodes.Add(new Node(points[uzcount + nty - 1], uzcount + nty)); // узел 4 (правый нижний)

                    // добавление узлов и номера к элементу
                    Rectangle par = new Rectangle(nodes, elcount);
                    // ассоциация ребер с узлами элемента
                    par.AssociateEdges();

                    elcount++; // увеличиваем счетчик элементов на +1

                    // добавляем элемент в результирующий список
                    rects.Add(par);
                }
                uzcount++;
            }

            return rects;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<Edge> GenerateBoundaryEdges()
        {
            // TODO: придумать здесь как генерировать граничные ребра на основании общего алгоритма сеток

            int count = 2 * _ntx + 2 * _nty - 4; // общее кол-во граничных узлов
            int[] GranUzl = new int[count]; // выделим в памяти размер под массив граничных узлов
            int n1 = 0, m1 = 0, i, j;
            for (i = 0; i < _ntx; i++)
                for (j = 0; j < _nty; j++)
                {
                    m1++;
                    if ((i == 0) || (i == _ntx - 1))
                    {
                        GranUzl[n1] = m1;
                        n1++;
                    }
                    else if ((j == 0) || (j == _nty - 1))
                    {
                        GranUzl[n1] = m1;
                        n1++;
                    }
                }
            // сгенерируем список ребер
            List<Edge> edges = new List<Edge>();

            return edges;
        }
    }
}
