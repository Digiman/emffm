﻿using System.Collections.Generic;
using EMFFM.Common.Structs;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using EMFFM.MainApp.Common;
using EMFFM.MainApp.Helpers;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Класс дя построения конечноэлементной сетки
    /// </summary>
    /// <remarks>По сути выступает как класс-оболочка для работы с сетками, выводом их в файлы и т.п.</remarks>
    class MeshBuilder
    {
        /// <summary>
        /// Построение сетки из прямоугольников (плоских)
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        /// <param name="outputData">Данные для выходных файлов</param>
        /// <param name="options">Опции</param>
        /// <returns>Возвращает список элементов</returns>
        public static List<Rectangle> GenerateRectangleMesh(GeomData geomData, Output outputData, Options options)
        {
            // 1. Генерация сетки (списка элементов) (узловая сетка)
            RectMesh mesh = new RectMesh(geomData);
            List<Rectangle> elements = mesh.GenerateMesh();
            // 1.1. Нумерация ребер
            if (options.IsBuiltEdges)
            {
                EdgeMesh.BuiltEdges<Rectangle>(ref elements);
            }

            // 2. Вывод в файл результата
            if (options.IsOutputToFile)
            {
                OutputHelper.OutputMeshDataToFile<Rectangle>(elements, outputData.GetFullPath(DatafileType.Mesh), options.IsBuiltEdges, options.IsAppend);
            }

            return elements;
        }

        /// <summary>
        /// Построение сетки из параллелепипедов
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        /// <param name="outputData">Данные для выходных файлов</param>
        /// <param name="options">Опции</param>
        /// <returns>Возвращает список элементов</returns>
        public static List<Parallelepiped> GenerateParallepipedMesh(GeomData geomData, Output outputData, Options options)
        {
            // 1. Генерация сетки (списка элементов) (узловая сетка)
            ParMesh mesh = new ParMesh(geomData);
            List<Parallelepiped> elements = mesh.GenerateMesh();
            // 1.1. Нумерация ребер
            if (options.IsBuiltEdges)
            {
                EdgeMesh.BuiltEdges<Parallelepiped>(ref elements);
            }

            // 2. Вывод в файл результата
            if (options.IsOutputToFile)
            {
                OutputHelper.OutputMeshDataToFile<Parallelepiped>(elements, outputData.GetFullPath(DatafileType.Mesh), options.IsBuiltEdges, options.IsAppend);
            }

            return elements;
        }

        /// <summary>
        /// Построение сетки из треугольников (плоских)
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        /// <param name="outputData">Данные для выходных файлов</param>
        /// <param name="options">Опции</param>
        /// <returns>Возвращает список элементов</returns>
        public static List<Triangle> GenerateTriangleMesh(GeomData geomData, Output outputData, Options options)
        {
            // TODO: описать в входных параметрах тип элемента сетки

            // 1. Генерация сетки (списка элементов) (узловая сетка)
            TriMesh mesh = new TriMesh(geomData, TriangleStyle.Left);
            List<Triangle> elements = mesh.GenerateMesh();
            // 1.1. Нумерация ребер
            if (options.IsBuiltEdges)
            {
                EdgeMesh.BuiltEdges<Triangle>(ref elements);
            }

            // 2. Вывод в файл результата
            if (options.IsOutputToFile)
            {
                OutputHelper.OutputMeshDataToFile<Triangle>(elements, outputData.GetFullPath(DatafileType.Mesh), options.IsBuiltEdges, options.IsAppend);
            }

            return elements;
        }

        /// <summary>
        /// Построение сетки из тетраэдров
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        /// <param name="outputData">Данные для выходных файлов</param>
        /// <param name="options">Опции</param>
        /// <returns>Возвращает список элементов</returns>
        public static List<Tetrahedron> GenerateTetrahedronMesh(GeomData geomData, Output outputData, Options options)
        {
            // 1. Генерация сетки (списка элементов) (узловая сетка)
            TetMesh mesh = new TetMesh(geomData);
            List<Tetrahedron> elements = mesh.GenerateMesh();
            // 1.1. Нумерация ребер
            if (options.IsBuiltEdges)
            {
                EdgeMesh.BuiltEdges<Tetrahedron>(ref elements);
            }

            // 2. Вывод в файл результата
            if (options.IsOutputToFile)
            {
                OutputHelper.OutputMeshDataToFile<Tetrahedron>(elements, outputData.GetFullPath(DatafileType.Mesh), options.IsBuiltEdges, options.IsAppend);
            }

            return elements;
        }
    }
}
