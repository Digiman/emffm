﻿using System.Collections.Generic;
using EMFFM.Common.Enums;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Описание сетки
    /// </summary>
    /// <remarks>Собственно все данные о сетке, которая используется для решения</remarks>
    class TMesh<TType, TType2>
    {
        public ElementType ElementType;

        /// <summary>
        /// Список элементов сетки
        /// </summary>
        /// <remarks>Объемные элементы (тетраэдры)</remarks>
        public List<TType> MeshElements;

        /// <summary>
        /// Список поверхностных элементов
        /// </summary>
        /// <remarks>Поверхностные элементы (треугольники)</remarks>
        public List<TType2> SurfElements;

        /// <summary>
        /// Количество ребер в сетке всего
        /// </summary>
        public int EdgesCount;

        /// <summary>
        /// Количество ребер на гранях
        /// </summary>
        /// <remarks>Все ребра для всех плоскостей!</remarks>
        public int BoundaryEdgesCount;

        /// <summary>
        /// Инициализация хранилища сетки
        /// </summary>
        /// <param name="type">Тип элементов сетки</param>
        public TMesh(ElementType type)
        {
            ElementType = type;
        }

        /// <summary>
        /// Инициализация хранилица сетки
        /// </summary>
        /// <param name="type">Тип элементов сетки</param>
        /// <param name="elements">Список элементов</param>
        /// <param name="edgesCount">Количество ребер</param>
        public TMesh(ElementType type, List<TType> elements, int edgesCount)
        {
            ElementType = type;
            MeshElements = elements;
            EdgesCount = edgesCount;
        }
    }
}
