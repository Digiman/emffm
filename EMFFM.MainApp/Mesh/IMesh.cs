﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Интерфейс для описания классов для генерации сеток
    /// </summary>
    interface IMesh <TType>
    {
        List<TType> GenerateMesh();
    }
}
