﻿using System.Collections.Generic;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Common.Structs;
using EMFFM.MainApp.Mesh.Elements;
using NLog;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Класс для генерации сетки из параллелипипедов
    /// </summary>
    class ParMesh : IMesh<Parallelepiped>
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        GeomData _geomData;
        int _ntx, _nty, _nuz, _nel;

        /// <summary>
        /// Инициализация генератора сетки из параллелепипедов
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        public ParMesh(GeomData geomData)
        {
            _geomData = geomData;

            logger.Info("Initializing parallelepiped mesh...");

            // Вычисляем параметры для разбиения
            _ntx = (int)System.Math.Floor(geomData.A / geomData.Da) + 1;     // узлов по оси OX (длине)
            _nty = (int)System.Math.Floor(geomData.B / geomData.Db) + 1;     // узлов по оси OY (ширине)
            _nel = (int)System.Math.Floor((geomData.A * geomData.B) / (geomData.Da * geomData.Db)); // кол-во элементов сетки
            _nuz = _ntx * _nty;            // кол-во узлов сетки
        }

        /// <summary>
        /// Генерация сетки из параллелипипедов
        /// </summary>
        /// <returns>Возвращает список из параллелипипедов</returns>
        public List<Parallelepiped> GenerateMesh()
        {
            logger.Info("Calculating 2D plate points...");
            
            // 1. Дискретизируем ортогональной сеткой верхнюю плоскость (XY)
            List<Point> points = Get2DPoints(_nuz, _ntx, _nty, _geomData.Da, _geomData.Db, _geomData.StartPoint.X, _geomData.StartPoint.Y);

            logger.Info("Generate parallelepiped elements...");

            // 2. Вычисляем количество плоских слоев параллелепипедов по высоте области
            int nc = (int)System.Math.Floor(_geomData.C / _geomData.Dc);

            // 3. Формирование итоговой нумерации элементов и узлов
            List<Parallelepiped> result = GetElements(_ntx, _nty, _nel, _nuz, nc, _geomData.Dc, points, _geomData.StartPoint.Z);

            logger.Info("Ends generating parallelepiped elements...");

            return result;
        }

        /// <summary>
        /// Вычисление координат узлов для сетки на плоскости XOY
        /// </summary>
        /// <param name="nuz">Количестве узлов</param>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="da">Шаг сетки по оси OX</param>
        /// <param name="db">Шаг сетки по оси OY</param>
        /// <param name="x0">Начальная коордианат X0</param>
        /// <param name="y0">Начальная координата Y0</param>
        /// <returns>Возвращает список точек</returns>
        private List<Point> Get2DPoints(int nuz, int ntx, int nty, double da, double db, double x0, double y0)
        {
            double[,] cuz = new double[nuz, 2];
            double sx = x0 - da, sy;
            int uzcount = 0, i, j;
            // определение координат узлов
            for (i = 0; i < ntx; i++)
            {
                sx += da;
                sy = y0 - db;
                for (j = 0; j < nty; j++)
                {
                    uzcount++;
                    sy = sy + db;
                    cuz[uzcount - 1, 0] = sx; // координата X
                    cuz[uzcount - 1, 1] = sy; // координата Y
                }
            }
            // формируем массив точек для возврата
            List<Point> pnt = new List<Point>(nuz);
            for (i = 0; i < nuz; i++)
            {
                pnt.Add(new Point(cuz[i, 0], cuz[i, 1]));
            }
            return pnt;
        }

        /// <summary>
        /// Формирование итоговой нумерации и элементов (завершение дискретизации области)
        /// </summary>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="nel">Количество элементов на слое</param>
        /// <param name="nuz">Количество узлов плоской сетки на слое</param>
        /// <param name="nc">Количество слоев</param>
        /// <param name="dc">Шаг сетки по OZ</param>
        /// <param name="points">Список координат точек для плоскости</param>
        /// <param name="z0">Начальная координата Z0</param>
        /// <returns>Возвращает список элементов</returns>
        private List<Parallelepiped> GetElements(int ntx, int nty, int nel, int nuz, int nc, double dc, List<Point> points, double z0)
        {
            List<Parallelepiped> pars = new List<Parallelepiped>(nel * nc); // размерность равна кол-ву слоев на кол-во элементов на каждом слое

            double curz = z0; // начальное значение по оси OZ для узлов слоя (с 0, от нижнего слоя)
            int elcount = 1; // глобальный номер элемента (параллелепипеда) (с 1)
            int globuzcount = 0; // номер текущего узла сетки (с 1)
            // начинаем бегать по слоям
            for (int i = 0; i < nc; i++)
            {
                int uzcount = 0;
                // бегаем теперь по плоской сетке слоя
                for (int j = 0; j < ntx - 1; j++)
                {
                    for (int k = 0; k < nty - 1; k++)
                    {
                        // формирование списка узлов элемента
                        List<Node> nodes = new List<Node>(8); // 8 узлов

                        uzcount = uzcount + 1; // номер левого нижнего узла (базовый) (с 1)

                        // узлы текущего слоя Z[i]
                        nodes.Add(new Node(new Point(points[uzcount - 1].X, points[uzcount - 1].Y, curz), globuzcount + uzcount)); // узел 1 (левый нижний)
                        nodes.Add(new Node(new Point(points[uzcount].X, points[uzcount].Y, curz), globuzcount + uzcount + 1)); // узел 2 (левый верхний)
                        nodes.Add(new Node(new Point(points[uzcount + nty].X, points[uzcount + nty].Y, curz), globuzcount + uzcount + nty + 1)); // узел 3 (правый верхний)
                        nodes.Add(new Node(new Point(points[uzcount + nty - 1].X, points[uzcount + nty - 1].Y, curz), globuzcount + uzcount + nty)); // узел 4 (правый нижний)
                        
                        // узлы следующего слоя Z[i+1]
                        nodes.Add(new Node(new Point(points[uzcount - 1].X, points[uzcount - 1].Y, curz + dc), globuzcount + uzcount + nuz)); // узел 1 (левый нижний)
                        nodes.Add(new Node(new Point(points[uzcount].X, points[uzcount].Y, curz + dc), globuzcount + uzcount + 1 + nuz)); // узел 2 (левый верхний)
                        nodes.Add(new Node(new Point(points[uzcount + nty].X, points[uzcount + nty].Y, curz + dc), globuzcount + uzcount + nty + 1 + nuz)); // узел 3 (правый верхний)
                        nodes.Add(new Node(new Point(points[uzcount + nty - 1].X, points[uzcount + nty - 1].Y, curz + dc), globuzcount + uzcount + nty + nuz)); // узел 4 (правый нижний)

                        // добавление узлов и номера к элементу
                        Parallelepiped par = new Parallelepiped(nodes, elcount);
                        par.AssociateEdges();

                        elcount++; // увеличиваем счетчик элементов на +1

                        // добавляем элемент в результирующий список
                        pars.Add(par);
                    }
                    uzcount++;
                }
                curz += dc; // увеличиваем значении координаты Z для текущего слоя
                globuzcount += nuz; // увеличиваем базисный номер узла для нумерации
            }

            return pars;
        }

        public void GenerateBoundaryEdges()
        {
        	
        }
    }
}
