﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Mesh.Elements;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Класс для векторизации узловых элементов (нумерация ребер)
    /// </summary>
    class EdgeMesh
    {
        /// <summary>
        /// Нумерация ребер для сетки 
        /// </summary>
        /// <typeparam name="TType">Тип элементов</typeparam>
        /// <param name="elements">Список элементов</param>
        /// <returns>Возвращаемое число указывает размерность глобальной матрицы и вектора</returns>
        public static int BuiltEdges<TType>(ref List<TType> elements)
        {
            // 1. Сборка всех ребер элементов в список
            int elementEdgesCount = (elements[0] as Element).Edges.Count;
            List<Edge> edges = new List<Edge>(elements.Count * elementEdgesCount);
            foreach (var item in elements)
            {
                edges.AddRange((item as Element).Edges);
            }

            // 2. Выполнение уникальной нумерации узлов
            List<Edge> numbered = NumerateEdges(edges);

            // 3. Объединение уникальных номеров с номерами ребер в элементах
            foreach (var item in elements.AsParallel())
            {
                foreach (var edge in (item as Element).Edges)
                {
                    foreach (var edgenum in numbered.AsParallel())
                    {
                        if (edge == edgenum)
                        {
                            edge.Number = edgenum.Number;
                        }
                    }
                }
            }

            return numbered.Count;
        }

        /// <summary>
        /// Выполнение уникальной нумерации ребер (без повторов)
        /// </summary>
        /// <param name="edges">Список узлов</param>
        /// <returns>Возвращает список пронумерованных уникально ребер элементов сетки</returns>
        /// <remarks>Вход - список всех ребер сетки, выход - нумерованные ребра без повторов</remarks>
        private static List<Edge> NumerateEdges(List<Edge> edges)
        {
            List<Edge> noduplicates = edges.AsParallel().Distinct().ToList();

            int curnum = 1; // начальный номер для текущего ребра
            foreach (var item in noduplicates)
            {
                item.Number = curnum;
                curnum++;
            }
            return noduplicates;
        }
    }
}
