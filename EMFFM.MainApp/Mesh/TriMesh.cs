﻿using System.Collections.Generic;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Common.Structs;
using EMFFM.MainApp.Common;
using EMFFM.MainApp.Mesh.Elements;
using NLog;

namespace EMFFM.MainApp.Mesh
{
    /// <summary>
    /// Генерация сетки из ортогональных треугольников
    /// </summary>
    class TriMesh : IMesh<Triangle>
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();

        int _ntx, _nty, _nel, _nuz;
        readonly GeomData _geomData;
        readonly TriangleStyle _type;

        /// <summary>
        /// Инициализация сетки из треугольников
        /// </summary>
        /// <param name="geomData">Геометрия области дискретизации</param>
        /// <param name="type">Стиль треугольников</param>
        public TriMesh(GeomData geomData, TriangleStyle type)
        {
            _geomData = geomData;
            _type = type;

            logger.Info("Initializing triangle mesh...");

            // Вычисляем параметры для разбиения
            _ntx = (int)System.Math.Floor(geomData.A / geomData.Da) + 1;     // узлов по оси OX (длине)
            _nty = (int)System.Math.Floor(geomData.B / geomData.Db) + 1;     // узлов по оси OY (ширине)
            _nel = (int)System.Math.Floor((geomData.A * geomData.B) / (geomData.Da * geomData.Db)); // кол-во элементов сетки (прямоугольников)
            _nuz = _ntx * _nty;  // кол-во узлов сетки
            _nel *= 2; // количество элементов (треугольников)
        }
        
        /// <summary>
        /// Разбиение области на треугольники
        /// </summary>
        /// <returns>Возвращает список элементов</returns>
        public List<Triangle> GenerateMesh()
        {
            logger.Info("Calculating 2D plate points...");

            // 1. Дискретизируем ортогональной сеткой верхнюю плоскость (XY)
            List<Point> points = Get2DPoints(_nuz, _ntx, _nty, _geomData.Da, _geomData.Db, _geomData.StartPoint.X, _geomData.StartPoint.Y);

            logger.Info("Generate triangle elements...");

            // 2. Формирование итоговой нумерации элементов и узлов
            List<Triangle> result = new List<Triangle>();
            if (_type == TriangleStyle.Left)
            {
                result = GetLeftElements(_ntx, _nty, _nel, points);
            }
            else if (_type == TriangleStyle.Right)
            {
                result = GetRightElements(_ntx, _nty, _nel, points);
            }

            logger.Info("Ends generating triangle elements...");

            return result;
        }

        /// <summary>
        /// Вычисление координат узлов для сетки на плоскости XOY
        /// </summary>
        /// <param name="nuz">Количестве узлов</param>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="da">Шаг сетки по оси OX</param>
        /// <param name="db">Шаг сетки по оси OY</param>
        /// <param name="x0">Начальная координата X0</param>
        /// <param name="y0">Начальная координата Y0</param>
        /// <returns>Возвращает список точек</returns>
        private List<Point> Get2DPoints(int nuz, int ntx, int nty, double da, double db, double x0, double y0)
        {
            double[,] cuz = new double[nuz, 2];
            double sx = x0 - da, sy;
            int uzcount = 0, i, j;
            // определение координат узлов
            for (i = 0; i < ntx; i++)
            {
                sx += da;
                sy = y0 - db;
                for (j = 0; j < nty; j++)
                {
                    uzcount++;
                    sy = sy + db;
                    cuz[uzcount - 1, 0] = sx; // координата X
                    cuz[uzcount - 1, 1] = sy; // координата Y
                }
            }
            // формируем массив точек для возврата
            List<Point> pnt = new List<Point>(nuz);
            for (i = 0; i < nuz; i++)
            {
                pnt.Add(new Point(cuz[i, 0], cuz[i, 1]));
            }
            return pnt;
        }

        /// <summary>
        /// Формирование итоговой нумерации и элементов (завершение дискретизации области)
        /// (левые треугольники - гипотенуза главная диагональ)
        /// </summary>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="nel">Количество элементов на слое</param>
        /// <param name="points">Список координат точек для плоскости</param>
        /// <returns>Возвращает список элементов</returns>
        private List<Triangle> GetLeftElements(int ntx, int nty, int nel, List<Point> points)
        {
            List<Triangle> trs = new List<Triangle>(nel); // размерность равна кол-во элементов

            int elcount = 1; // глобальный номер элемента (параллелепипеда) (с 1)
            int uzcount = 0;
            // бегаем теперь по плоской сетке слоя
            for (int j = 0; j < ntx - 1; j++)
            {
                for (int k = 0; k < nty - 1; k++)
                {
                    // формирование списка узлов элемента
                    List<Node> nodes1 = new List<Node>(3); // 3 узла
                    List<Node> nodes2 = new List<Node>(3); // 3 узла

                    uzcount = uzcount + 1; // номер левого нижнего узла (базовый) (с 1)

                    // формирование узлов для нижнего элемента (1-4-2)
                    nodes1.Add(new Node(points[uzcount - 1], uzcount)); // узел 1 (левый нижний)
                    nodes1.Add(new Node(points[uzcount + nty - 1], uzcount + nty)); // узел 4 (правый нижний)
                    nodes1.Add(new Node(points[uzcount], uzcount + 1)); // узел 2 (левый верхний)

                    // формирование узлов для верхнего элемента (2-3-4)
                    nodes2.Add(new Node(points[uzcount], uzcount + 1)); // узел 2 (левый верхний)
                    nodes2.Add(new Node(points[uzcount + nty], uzcount + nty + 1)); // узел 3 (правый верхний)
                    nodes2.Add(new Node(points[uzcount + nty - 1], uzcount + nty)); // узел 4 (правый нижний)

                    // добавление узлов и номера к элементу
                    Triangle par1 = new Triangle(nodes1, elcount); // нижний элемент
                    par1.AssociateEdges();
                    Triangle par2 = new Triangle(nodes2, elcount + 1); // верхний элемент
                    par2.AssociateEdges();

                    elcount += 2; // увеличиваем счетчик элементов на +2 (базовый номер в паре)

                    // добавляем элемент в результирующий список
                    trs.Add(par1);
                    trs.Add(par2);
                }
                uzcount++;
            }

            return trs;
        }

        /// <summary>
        /// Формирование итоговой нумерации и элементов (завершение дискретизации области)
        /// (правые треугольники - гипотенуза побочная диагональ)
        /// </summary>
        /// <param name="ntx">Количество узлов по оси OX</param>
        /// <param name="nty">Количество узлов по оси OY</param>
        /// <param name="nel">Количество элементов на слое</param>
        /// <param name="points">Список координат точек для плоскости</param>
        /// <returns>Возвращает список элементов</returns>
        private List<Triangle> GetRightElements(int ntx, int nty, int nel, List<Point> points)
        {
            List<Triangle> trs = new List<Triangle>(nel); // размерность равна кол-во элементов

            int elcount = 1; // глобальный номер элемента (параллелепипеда) (с 1)
            int uzcount = 0;
            // бегаем теперь по плоской сетке слоя
            for (int j = 0; j < ntx - 1; j++)
            {
                for (int k = 0; k < nty - 1; k++)
                {
                    // формирование списка узлов элемента
                    List<Node> nodes1 = new List<Node>(3); // 3 узла
                    List<Node> nodes2 = new List<Node>(3); // 3 узла

                    uzcount = uzcount + 1; // номер левого нижнего узла (базовый) (с 1)

                    // формирование узлов для нижнего элемента (1-4-3)
                    nodes1.Add(new Node(points[uzcount - 1], uzcount)); // узел 1 (левый нижний)
                    nodes1.Add(new Node(points[uzcount + nty - 1], uzcount + nty)); // узел 4 (правый нижний)
                    nodes1.Add(new Node(points[uzcount + nty], uzcount + nty + 1)); // узел 3 (правый верхний)

                    // формирование узлов для верхнего элемента (2-3-1)
                    nodes2.Add(new Node(points[uzcount], uzcount + 1)); // узел 2 (левый верхний)
                    nodes2.Add(new Node(points[uzcount + nty], uzcount + nty + 1)); // узел 3 (правый верхний)
                    nodes2.Add(new Node(points[uzcount - 1], uzcount)); // узел 1 (левый нижний)

                    // добавление узлов и номера к элементу
                    Triangle par1 = new Triangle(nodes1, elcount); // нижний элемент
                    par1.AssociateEdges();
                    Triangle par2 = new Triangle(nodes2, elcount + 1); // верхний элемент
                    par2.AssociateEdges();

                    elcount += 2; // увеличиваем счетчик элементов на +2 (базовый номер в паре)

                    // добавляем элемент в результирующий список
                    trs.Add(par1);
                    trs.Add(par2);
                }
                uzcount++;
            }

            return trs;
        }
    }
}
