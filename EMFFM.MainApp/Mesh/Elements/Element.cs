﻿using System.Collections.Generic;
using System.Text;
using EMFFM.Common.Math;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Input.Elements;

namespace EMFFM.MainApp.Mesh.Elements
{
    /// <summary>
    /// Базовый класс для всех элементов
    /// </summary>
    class Element
    {
        /// <summary>
        /// Узлы элемента
        /// </summary>
        public List<Node> Nodes { get; set; }

        /// <summary>
        /// Ребра элемента
        /// </summary>
        public List<Edge> Edges { get; set; }

        /// <summary>
        /// Материал элемента
        /// </summary>
        /// <remarks>Электромагнитные параметры области, занимаемой элементом</remarks>
        public Material Material { get; set; }

        /// <summary>
        /// Номер элемента
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Номер области в описываемой задаче
        /// </summary>
        /// <remarks>Область из генерации сетки с помощью Netgen</remarks>
        public int SubDomain { get; set; }

        /// <summary>
        /// Номер граничного условия
        /// </summary>
        /// <remarks>Для поверхностных плоских элементов!</remarks>
        public int BoundaryCond { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} [", Number); // номер элемента
            // записываем список узлов элемента
            for (int i = 0; i < Nodes.Count; i++)
            {
                if (i != Nodes.Count - 1)
                {
                    sb.AppendFormat("{0}, ", Nodes[i].ToString());
                }
                else
                {
                    sb.AppendFormat("{0}", Nodes[i].ToString());
                }
            }
            sb.Append("]");

            return sb.ToString();
        }

        /// <summary>
        /// Генерация строки с данными о ребрах элемента 
        /// </summary>
        public string EdgesToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("[ ");
            for (int i = 0; i < Edges.Count; i++)
            {
                if (i != Edges.Count - 1)
                {
                    sb.AppendFormat("{0}, ", Edges[i].ToString());
                }
                else
                {
                    sb.AppendFormat("{0}", Edges[i].ToString());
                }
            }
            sb.Append("]");

            return sb.ToString();
        }

        public virtual void AssociateEdges()
        {
        }

        /// <summary>
        /// Определение центра элемента
        /// </summary>
        /// <returns>Возвращает точку, расположенную в центре элемента</returns>
        public Point GetCenterPoint()
        {
            int count = Nodes.Count; // число вершин

            double x = MathHelper.SumOfCooodinates(Nodes, 0) / count;
            double y = MathHelper.SumOfCooodinates(Nodes, 1) / count;
            double z = MathHelper.SumOfCooodinates(Nodes, 2) / count;

            return new Point(x, y, z);
        }
    }
}
