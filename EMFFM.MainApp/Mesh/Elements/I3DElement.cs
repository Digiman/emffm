﻿using System;
using System.Linq;

namespace EMFFM.MainApp.Mesh.Elements
{
    /// <summary>
    /// Интерфейс для описания общих методов и свойств для трехмерных КЭ
    /// </summary>
    interface I3DElement
    {
        double GetVolume();
    }
}
