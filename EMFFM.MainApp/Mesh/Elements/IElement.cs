﻿using System;
using System.Linq;

namespace EMFFM.MainApp.Mesh.Elements
{
    /// <summary>
    /// Описание общих функций для элементов
    /// </summary>
    interface IElement
    {
        /// <summary>
        /// Ассоциация узлов элемента с его ребрами
        /// </summary>
        /// <remarks>В соответствии с принятой локальной нумерацией!</remarks>
        void AssociateEdges();

        double[,] GetLocalE();

        double[,] GetLocalF();
    }
}
