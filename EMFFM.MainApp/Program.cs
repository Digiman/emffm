﻿using System;
using EMFFM.Common.Helpers;
using EMFFM.Input;
using EMFFM.MainApp.Controllers;
using System.IO;
using NLog;

namespace EMFFM.MainApp
{
    /// <summary>
    /// Главный класс программы для обработки запуска расчетов
    /// </summary>
    static class Program
    {
        static Logger logger = LogManager.GetCurrentClassLogger();

        static string DefaultInputFile = "input\\in5.xml";

        /// <summary>
        /// Входные данные для программы
        /// </summary>
        static InputData inData;
        
        /// <summary>
        /// Разбор входного файла с данными для задачи
        /// </summary>
        /// <param name="fname">Файл с данными</param>
        static void ParseInputFile(string fname)
        {
            if (File.Exists(fname))
            {
                logger.Info("Начало чтения и разбора входного файла...");
                
                // разбор входного файла с помощью специального класса
                inData = InputFileReader.ReadInputFile(fname);

                logger.Info("Разбор входного файла успешно завершен!");
            }
            else
            {
                throw new Exception(String.Format("Указанного файла с входными данными не существует! Файл: {0}", Path.GetFullPath(fname)));
            }
        }

        /// <summary>
        /// Вызов списка тестовых функций
        /// </summary>
        static void TestFunction()
        {
            IoHelper.ClearTestsPath("tests");   
            
            logger.Debug("Start test functions...");

            //TestFunctions.TestPointsAndNodes();

            //TestFunctions.TestVectors();

            TestFunctions.TestBox(); // тест параллелипепедами

            TestFunctions.TestBox2(); // тест тетраэдрами

            TestFunctions.TestPlateRect(); // тест прямоугольников

            TestFunctions.TestPlateRect2(); // тест прямоугольников с ребрами

            TestFunctions.TestPlateTri(); // тест треугольников

            TestFunctions.TestParticlesGenerator();

            logger.Debug("Ends of test functions...");
        }

        /// <summary>
        /// Разбор и обработка аргументов из командной строки
        /// </summary>
        /// <param name="args">Аргументы</param>
        static void ParseArguments(string[] args)
        {
            // разбор аргументов из командной строки
            if (args.Length == 0)
            {
                logger.Debug("Command line arguments is null! Trying to use default input file...");

                ParseInputFile(DefaultInputFile);
            }
            else
            {
                ParseInputFile(args[0]);
            }
        }

        /// <summary>
        /// Главная функция консольного приложения
        /// </summary>
        /// <param name="args">Аргументы командной строки</param>
        static void Main(string[] args)
        {
            logger.Log(LogLevel.Info, "Starting EMFFM...");

            try
            {
                DateTime start = DateTime.Now;

                // разбор аргументов командной строки
                ParseArguments(args);

                // очистка рабочей папки для вывода файлов результатов
                IoHelper.ClearOutputPath(inData.Output.Path);

                // тестовые функции
                //TestFunction();

                // создание и запуск главного контроллера всей программы
                AppController app = new AppController(inData);
                app.Run();

                DateTime end = DateTime.Now;
                logger.Info("Time to run:  {0} ms", (end - start).TotalMilliseconds);
            }
            catch (Exception ex)
            {
                logger.Error(String.Format("Возникло исключение: {0}", ex.Message));
            }

            logger.Info("Ending is EMFFM...");
            // ждем ответа пользователя (TODO: потом убрать ожидание ответа пользователя)
            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}
