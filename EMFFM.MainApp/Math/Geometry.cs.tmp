﻿using System;
using System.Linq;
using EMFFM.MainApp.Mesh;

namespace EMFFM.MainApp.Math
{
    /// <summary>
    /// Класс для реализации формур геометрии и аналитической геометрии
    /// </summary>
    public static class Geometry
    {
        /// <summary>
        /// Вычисление расстояния между двумя точками
        /// </summary>
        /// <param name="p1">Точка 1</param>
        /// <param name="p2">Точка 2</param>
        /// <returns>Возвращает вычисленное значение расстояния</returns>
        public static double GetDistance(Point p1, Point p2)
        {
            return System.Math.Sqrt(MathHelper.Sqr(p2.X - p1.X) + MathHelper.Sqr(p2.Y - p1.Y) + MathHelper.Sqr(p2.Z - p1.Z));
        }

        /// <summary>
        /// Вычисление расстояния от точки до начала координат
        /// </summary>
        /// <param name="p">Точка</param>
        /// <returns>Возвращает вычисленное значение расстояния</returns>
        public static double GetDistance(Point p)
        {
            return System.Math.Sqrt(MathHelper.Sqr(p.X) + MathHelper.Sqr(p.Y) + MathHelper.Sqr(p.Z));
        }
    }
}
