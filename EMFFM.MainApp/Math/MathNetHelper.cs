﻿using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Double.Solvers.Iterative;
using MathNet.Numerics.LinearAlgebra.Double.Solvers.StopCriterium;
using MathNet.Numerics.LinearAlgebra.Double.Solvers;

namespace EMFFM.MainApp.Math
{
    /// <summary>
    /// Вспомогательный класс для работы с библиотекой Math.NET Numerics
    /// </summary>
    static class MathNetHelper
    {
        /// <summary>
        /// Метод для решения СЛАУ вида Ax=B с настройками метода решения
        /// </summary>
        /// <param name="matrixA">Матрица A</param>
        /// <param name="vectorB">Вектор B</param>
        /// <returns>Возвращает результат решения (вектор x)</returns>
        public static Vector Solve(Matrix matrixA, Vector vectorB)
        {
            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor = new Iterator(new IIterationStopCriterium[] { iterationCountStopCriterium, residualStopCriterium });

            // Create Bi-Conjugate Gradient Stabilized solver
            var solver = new BiCgStab(monitor);

            // solve SLAU
            return solver.Solve(matrixA, vectorB);
        }

        /// <summary>
        /// Метод для решения СЛАУ вида Ax=B с настройками метода решения
        /// </summary>
        /// <param name="matrixA">Матрица A</param>
        /// <param name="vectorB">Вектор B</param>
        /// <returns>Возвращает результат решения (вектор x)</returns>
        public static double[] Solve(double[,] matrixA, double[] vectorB)
        {
            Matrix MatrixA = new SparseMatrix(matrixA);
            Vector VectorB = new SparseVector(vectorB);

            // Stop calculation if 1000 iterations reached during calculation
            var iterationCountStopCriterium = new IterationCountStopCriterium(1000);

            // Stop calculation if residuals are below 1E-10 --> the calculation is considered converged
            var residualStopCriterium = new ResidualStopCriterium(1e-10);

            // Create monitor with defined stop criteriums
            var monitor = new Iterator(new IIterationStopCriterium[] { iterationCountStopCriterium, residualStopCriterium });

            // Create Bi-Conjugate Gradient Stabilized solver
            var solver = new BiCgStab(monitor);

            // solve SLAU
            return solver.Solve(MatrixA, VectorB).ToArray();
        }
        
        /// <summary>
        /// Перемножение матрицы на вектор
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <param name="vector">Вектор</param>
        /// <returns>Возвращает результирующий вектор</returns>
        public static double[] Multiply(double[,] matrix, double[] vector)
        {
            Matrix mat = new DenseMatrix(matrix);
            Vector vec = new DenseVector(vector);

            return mat.Multiply(vec).ToArray();
        }
    }
}
