﻿namespace EMFFM.MainApp.FEM
{
    /// <summary>
    /// Класс для генерации граничных условий и их применения к глобальной матрице
    /// </summary>
    class BoundaryBuilder
    {
        /// <summary>
        /// Применение граничных условий Дирихле
        /// </summary>
        /// <param name="matrix">Глобальная матрица [K]</param>
        /// <param name="b">Вектор свободных членов {b} (воздействий)</param>
        /// <param name="nd">Массив номеров ребер для границы Г1 (для условий Дерихле)</param>
        /// <param name="p">Значения на границе для каждого граничного ребра {p}</param>
        /// <remarks>По алгоритму из книги "Jianming Jin The Finite Element Method in Electromagnetics - 2nd edition  2002"</remarks>
        public static void ApplyDirichletBoundaryCondition(ref double[,] matrix, ref double[] b, int[] nd, double[] p)
        {
            // BUG: данный метод обработки не работает!
            double bigValue = 10E15; // или 10^70
            for (int i = 0; i < nd.Length; i++)
            {
                // K(nd[i], nd[i]) = 10^70
                matrix[nd[i], nd[i]] = bigValue;
                // b(nd[i]) = p(i) * 10^70
                b[nd[i]] = p[i] * bigValue;
            }
        }

        /// <summary>
        /// Применение граничных условий Дирихле
        /// </summary>
        /// <param name="matrix">Глобальная матрица [K]</param>
        /// <param name="b">Вектор свободных членов {b} (воздействий)</param>
        /// <param name="nd">Массив номеров ребер для границы Г1 (для условий Дерихле)</param>
        /// <param name="p">Значения на границе для каждого граничного ребра {p}</param>
        /// <remarks>По алгоритму из книги "Jianming Jin The Finite Element Method in Electromagnetics - 2nd edition  2002"</remarks>
        public static void ApplyDirichletBoundaryCondition2(ref double[,] matrix, ref double[] b, int[] nd, double[] p)
        {
            for (int i = 0; i < nd.Length; i++)
            {
                b[nd[i] - 1] = p[i];
                matrix[nd[i] - 1, nd[i] - 1] = 1;
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (j == nd[i] - 1)
                        continue;
                    b[j] = b[j] - matrix[j, nd[i] - 1] * p[i];
                    matrix[nd[i] - 1, j] = 0;
                    matrix[j, nd[i] - 1] = 0;
                }
            }
        }

        #region Неиспользуемое

        /// <summary>
        /// Зануление значения для граничных ребер в глобальной матрице
        /// </summary>
        /// <param name="matrix">Глобальная матрица [A]</param>
        /// <param name="numbers">Номера граничных ребер</param>
        /// <returns>Возвращает обработанную матрицу</returns>
        /// <remarks>Метод устарел! Заменен правильным алгоритмом для Дирихле ГУ!</remarks>
        public static double[,] ZeroBoundaryEdges(double[,] matrix, int[] numbers)
        {
            // NOTE: метд работает, но он не понадобиться, так как нужно еще изменять и вектор и вычеркивать матрицы
            for (int i = 0; i < numbers.Length; i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    matrix[numbers[i] - 1, j] = 0;
                    matrix[j, numbers[i] - 1] = 0;
                    matrix[numbers[i] - 1, numbers[i] - 1] = 1;
                }
            }

            return matrix;
        }

        #endregion
    }
}
