﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Input.Elements;
using EMFFM.Input.Enums;
using EMFFM.MainApp.Mesh;
using EMFFM.MainApp.Mesh.Elements;

namespace EMFFM.MainApp.FEM
{
    /// <summary>
    /// Класс для установки параметров материалов для элементов сетки
    /// </summary>
    class MaterialsBuilder
    {
        /// <summary>
        /// Установка электромагнитных параметров для элементов сетки 
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов</param>
        /// <param name="materials">Список материалов</param>
        /// <param name="particles">Номера частиц (номера элементов с частицами)</param>
        /// <param name="particlesData">Данные о частицах</param>
        /// <returns>Возвращает модифицированнй список элементов</returns>
        public static List<TType> SetMaterials<TType>(List<TType> elements, List<Material> materials, int[] particles, List<Particle> particlesData)
        {
            // назначаем материал по умолчанию всем элементам (воздух обычно)
            SetDefaultMaterials(elements, materials.Single(mat => mat.IsDefault == true));

            // назначаем материал только для элементов-частиц
            foreach (var particle in particlesData)
            {
                switch (particle.Type)
                {
                    case ParticlesType.Law:
                        for (int i = 0; i < particles.Length; i++)
                        {
                            (elements[particles[i] - 1] as Element).Material = materials.Single(mat => mat.Name == particle.Material);
                        }
                        break;
                    case ParticlesType.Single:
                        (elements[(particle as ParticleS).Number] as Element).Material = materials.Single(mat => mat.Name == particle.Material);
                        break;
                    case ParticlesType.Many:
                        break;
                }
            }

            return elements;
        }

        /// <summary>
        /// Назначение метериала по умолчанию для всех элементов
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="elements">Список элементов</param>
        /// <param name="defaultMaterial">Материал по умолчанию (воздух)</param>
        /// <returns>Возвращает модифицированный список элементов</returns>
        public static List<TType> SetDefaultMaterials<TType>(List<TType> elements, Material defaultMaterial)
        {
            // назначаем материал по умолчанию всем элементам (воздух обычно)
            foreach (var elem in elements)
            {
                (elem as Element).Material = defaultMaterial;
            }

            return elements;
        }

        /// <summary>
        /// Назначение материалов элементам по их расположению в областях
        /// </summary>
        /// <typeparam name="TType">Тип элементов сетки</typeparam>
        /// <param name="meshData">Сетка</param>
        /// <param name="geometry">Геометрия задачи</param>
        /// <param name="materials">Список материалов</param>
        /// <remarks>На основании областей, сгенерированных в Netgen</remarks>
        public static void SetMaterialsByDomain<TType, TType2>(ref TMesh<TType, TType2> meshData, Geometry geometry, List<Material> materials, List<PlasmonData> plasmons)
        {
            foreach (var element in meshData.MeshElements)
            {
                (element as Element).Material = GetMaterial(geometry.Domains.Single(dom => dom.Code == (element as Element).SubDomain).MaterialName, materials, plasmons);
            }
        }

        #region Вспомогательные методы
        
        /// <summary>
        /// Получение материала из списка материалов по имени
        /// </summary>
        /// <param name="name">Название материала</param>
        /// <param name="materials">Список материалов</param>
        /// <param name="plasmons"></param>
        /// <returns>Возвращает найденный материал</returns>
        /// <remarks>Также ассоциирует данные о плазмоне для материала</remarks>
        private static Material GetMaterial(string name, List<Material> materials, List<PlasmonData> plasmons)
        {
            Material material = materials.SingleOrDefault(mat => mat.Name == name);

            if (material.IsPlasmon)
            {
                material.PlasmonData = plasmons.Single(s => s.Material.ToString() == material.PlasmonMaterial);
            }

            return material;
        } 

        #endregion
    }
}
