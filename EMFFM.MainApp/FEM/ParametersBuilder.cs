﻿using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Input.Elements;

namespace EMFFM.MainApp.FEM
{
    // TODO: полностью переписать этот класс, передалть расчет параметров, сделать его единственным местом их расчета

    /// <summary>
    /// Класс для вычисления параметров, необходимых для решения
    /// </summary>
    /// <remarks>Вычисление всех вспомогательных параметров и величин здесь!</remarks>
    class ParametersBuilder
    {
        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        
        static double AbsPermeable = 1.25663706144E-06; // магнитная постоянная
        static double AbsPermitt = 8.85418782E-12; // электрическая постоянная
        static double FreeSpaceVel = 1.0 / System.Math.Sqrt(AbsPermeable * AbsPermitt); // фазовая скорость распространения волны
        static double LightSpeed = 299792458; // скорость света в вакууме(м/с)

        static double ehbar = 1.519250349719305e+15; // e/hbar where hbar=h/(2*pi) and e=1.6e-19

        /// <summary>
        /// Вычисление значения волнового числа K
        /// </summary>
        /// <param name="freq">Частота источника (MHz)</param>
        /// <returns>Возвращает значение волнового числа</returns>
        /// <remarks>За поднобностями смотреть:
        /// http://ru.wikipedia.org/wiki/%D0%A3%D1%80%D0%B0%D0%B2%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F_%D0%9C%D0%B0%D0%BA%D1%81%D0%B2%D0%B5%D0%BB%D0%BB%D0%B0#.D0.9F.D0.BB.D0.BE.D1.81.D0.BA.D0.B8.D0.B5_.D1.8D.D0.BB.D0.B5.D0.BA.D1.82.D1.80.D0.BE.D0.BC.D0.B0.D0.B3.D0.BD.D0.B8.D1.82.D0.BD.D1.8B.D0.B5_.D0.B2.D0.BE.D0.BB.D0.BD.D1.8B
        /// Код заимстован в EMAP3 (и выше).
        /// </remarks>
        public static double CalculateWaveNumber(double freq)
        {
            // NOTE: проверенный метод для определения волнового числа
            double  WaveLength;
            
            double OperateFreq = freq; // частота фунционирования источника

            //WaveLength = FreeSpaceVel / (OperateFreq * 1.0E+06);
            WaveLength = FreeSpaceVel / OperateFreq ;
            double WaveNumber = 2.0 * System.Math.PI / WaveLength; // модуль волнового вектора

            double Omega = WaveNumber * FreeSpaceVel;

            logger.Debug("OperateFrequency = {0} MHz", OperateFreq);
            logger.Debug("WaveLength = {0} m", WaveLength);
            logger.Debug("FreeSpaceVelocity = {0} m/s", FreeSpaceVel);
            logger.Debug("WaveNumber = {0}", WaveNumber);
            logger.Debug("Omega = {0} rad/s", Omega);

            return WaveNumber;
        }

        /// <summary>
        /// Вычисление значения круговой частоты (омега)
        /// </summary>
        /// <param name="freq">Частота источника (Hz)</param>
        /// <returns>Возвращает значение частоты</returns>
        public static double CalculateOmega(double freq)
        {
            double operateFreq = freq; // круговая частота фунционирования источника

            //double waveLength = FreeSpaceVel / operateFreq;
            //double waveNumber = 2.0 * System.Math.PI / waveLength; // модуль волнового вектора
            //double omega = waveNumber * FreeSpaceVel;

            double omega = 2 * System.Math.PI * freq;

            //logger.Debug("WaveLength = {0} m", waveLength);
            //logger.Debug("WaveNumber = {0}", waveNumber);
            logger.Debug("Omega = {0} rad/s", omega);
            //logger.Debug("Omega2 = {0} rad/s", omega);

            return omega;
        }

        /// <summary>
        /// Получение значения частоты из входного файла
        /// </summary>
        /// <param name="sources">Список источников во входном файле</param>
        /// <param name="sourceType">Тип источника</param>
        /// <returns>Возвращает исходное значение частоты</returns>
        public static double GetFrequency(List<Source> sources, SourceTypes sourceType)
        {
            double lambda = sources.Single(s => s.Type == sourceType).Lambda;

            //freq *= 10E6; // переводим из МегаГерц и Герцы

            double freq = 2 * System.Math.PI * LightSpeed / lambda;
            logger.Debug("Frequency = {0} Hz", freq);

            return freq;
        }

        /// <summary>
        /// Получение значения круговой частоты по длине волны
        /// </summary>
        /// <param name="lambda">Длина волны (метры)</param>
        /// <returns>Возвращает значение Omega</returns>
        public static double GetFrequency(double lambda)
        {
            double freq = 2 * System.Math.PI * LightSpeed / lambda;
            logger.Debug("Frequency = {0} Hz", freq);

            return freq;
        }

        /// <summary>
        /// Получение значения амплитуды для источника
        /// </summary>
        /// <param name="sources">Список источников из входного файла</param>
        /// <param name="simple">Тип источника</param>
        /// <returns>Возвращает значение амплитуды</returns>
        public static double GetAmplitude(List<Source> sources, SourceTypes simple)
        {
            double amp = sources.Single(s => s.Type == SourceTypes.Simple).Amplitude;

            logger.Debug("Amplitude = {0}", amp);

            return amp;
        }

        /// <summary>
        /// Вычисление электрической постоянной на основе закона дисперсии для плазмона
        /// </summary>
        /// <param name="freq">Круговая частота источника (омега, рад/с)</param>
        /// <param name="data">Параметры плазмона</param>
        /// <returns>Возвращает значение Epsilon</returns>
        /// <remarks>Книга Климов Наноплазмоника, формула (3.32), действительная часть (3.33.1)</remarks>
        public static double CalculateEpsilonForPlasmon(double freq, PlasmonData data)
        {
            // NOTE: метод для вычисления дисперсного значения электрической постоянной
            double eps = 1 - ((data.Plasmow * data.Plasmow) * (data.Relax * data.Relax)) / (1 + (freq * freq) * (data.Relax * data.Relax));

            return eps;
        }
    }
}
