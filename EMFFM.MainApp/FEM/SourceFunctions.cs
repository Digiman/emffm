﻿using EMFFM.Common.FEM.Scalar.Elements;

namespace EMFFM.MainApp.FEM
{
    /// <summary>
    /// Класс для описания функций используемых источников
    /// </summary>
    class SourceFunctions
    {
        /// <summary>
        /// Функция для монохроматической волны
        /// E=Amp*cos(w*t)
        /// </summary>
        /// <param name="amplitude">Амплитуда волны</param>
        /// <param name="currentValue">Текущее значение (частота, омега)</param>
        /// <param name="time">Текущее значение времени</param>
        /// <returns>Возвращает значение волны</returns>
        /// <remarks>Для только вещественного значения волны!</remarks>
        public static double MonochromeWaveRe(double amplitude, double currentValue, double time)
        {
            return amplitude * System.Math.Cos(currentValue * time);
        }

        /// <summary>
        /// Вычисление значения функции для монохроматической волны вида E(w)=E0*exp(w*t)
        /// </summary>
        /// <param name="amplitude">Амплитуда волны</param>
        /// <param name="currentValue">Текущее значение (частота, омега)</param>
        /// <returns>Возвращает комплексное число</returns>
        /// <remarks>Полное комплексное значение!</remarks>
        public static System.Numerics.Complex MonochromeWaveFull(double amplitude, double currentValue, double time)
        {
            double real = amplitude * System.Math.Cos(currentValue * time);
            double img = amplitude * System.Math.Cos(currentValue * time);
            return new System.Numerics.Complex(real, img);
        }

        /// <summary>
        /// Вычисление значения поля, создаваемого плоской волной
        /// </summary>
        /// <param name="position">Положение (текущие координаты)</param>
        /// <param name="data">Сведения о волне</param>
        /// <param name="time">Время</param>
        /// <returns>Возвращает скалярное значение поля</returns>
        public static double MonoWave(double position, WaveData data, double time)
        {
            return data.Amplitude*System.Math.Cos(position - data.Omega*time + data.Phase);
        }
    }
}
