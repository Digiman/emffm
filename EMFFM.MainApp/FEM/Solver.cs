﻿using System;
using System.Collections.Generic;
using System.Linq;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Scalar.Elements;
using EMFFM.Common.Math;
using EMFFM.Input;
using EMFFM.Input.Enums;
using EMFFM.MainApp.Helpers;

namespace EMFFM.MainApp.FEM
{
    /// <summary>
    /// Класс для решения итоговой системы уравнений 
    /// </summary>
    class Solver<TType, TType2>
    {
        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        
        InputData _data;
        WaveData _wave;
        string _outputFilePrefix;

        ForceVector _vectorB;
        GlobalMatrix<TType, TType2> _matrix;

        public List<Result> Results { get; set; }

        int[] _boundNums;

        /// <summary>
        /// Инициализация решателя
        /// </summary>
        /// <param name="data">Входные данные</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="matrix">Глобальная матрица</param>
        /// <param name="vector">Объект для работы с вектором воздействий</param>
        /// <param name="boundaryEdges">Номера граничных ребер</param>
        public Solver(InputData data, WaveData wave, GlobalMatrix<TType, TType2> matrix, ForceVector vector, int[] boundaryEdges)
        {
            logger.Info("Инициализация решателя...");
            
            _data = data;
            _wave = wave;
            _outputFilePrefix = String.Format("{0}{1}", data.Output.Path, data.Output.FilePrefix);
            Results = new List<Result>();

            _matrix = matrix;
            _vectorB = vector;

            _boundNums = boundaryEdges;
        }

        /// <summary>
        /// Запуск многоразового решения системы
        /// </summary>
        public void Run()
        {
            if (_data.Options.WithoutIterations)
            {
                logger.Info("Решение для единичного времени...");
                SolveOne(0);
            }
            else
            {
                if (_data.Options.InTimeDomain)
                {
                    logger.Info("Расчет во временной области...");
                    RunInTimeDomain();
                }
                else if (_data.Options.InFrequencyDomain)
                {
                    logger.Info("Расчет в частотной области...");
                    RunInFrequencyDomain();
                }
            }
            logger.Info("Решатель завершил работу!");
        }

        #region Решение по времени
        
        /// <summary>
        /// Решение во временной области (по времени)
        /// </summary>
        private void RunInTimeDomain()
        {
            double initTime = 0;
            // конечная частота работы источника
            double timeEnd = initTime + _data.Variables.Time;

            int step = 1;

            // бегаем по временным шагам
            for (double time = initTime; time < _data.Variables.Time; time += _data.Variables.TimeDelta)
            {
                logger.Info("Solve in time domain at step {0} - time = {1}...", step, time);

                SolveTime(time, step);
                step++;
            }
        } 

        /// <summary>
        /// Вычисления поля для текущего времени
        /// </summary>
        /// <param name="currentTime">Текущее время</param>
        /// <param name="stepNumber">Номер шага (итерация)</param>
        private void SolveTime(double currentTime, int stepNumber)
        {
            // 1. Вычисление вектора B
            double[] vec = _vectorB.Generate(currentTime);

            // 2. Построение глобальной матрицы
            double[,] mat = _matrix.Generate(_wave);

            // NOTE: применение граничных условий Дирихле
            BoundaryBuilder.ApplyDirichletBoundaryCondition2(ref mat, ref vec, _boundNums,
                ArrayHelper.InitValueVector<double>(_boundNums.Length, _data.Boundaries.Single(b => b.Type == BoundaryTypes.Dirichlet).Value));

            // NOTE: или так записать вместео трех предыдущих строк
            double[] result = Math.MathNetHelper.Solve(mat, vec);

            // 4. Запись результата в файл
            string[] resfiles = PrintResultsToFile(vec, result, stepNumber);

            // 5. Добавление результата в список с результатами
            AddResultItem(resfiles[0], resfiles[1], _wave, currentTime);
        }

        #endregion

        #region Решение в частотной области
        
        /// <summary>
        /// Запуск решения в частной области с вариацией частот
        /// </summary>
        private void RunInFrequencyDomain()
        {
            // исходная частота работы источника
            double initFreq = ParametersBuilder.GetFrequency(_data.Sources, SourceTypes.Simple);
            // стартовая частота работы источника
            double freqStart = initFreq - _data.Variables.FrequencyDelta;
            // конечная частота работы источника
            double freqEnd = initFreq + _data.Variables.FrequencyDelta;

            // номер шага
            int step = 1;

            for (double w = freqStart; w < freqEnd; w += _data.Variables.FrequencyStep)
            {
                logger.Info("Solve in frequency domain at step {0} - omega = {1}...", step, w);

                SolveFrequency(w, step);
                step++;
            }
        }

        /// <summary>
        /// Выполнение расчета для текущего шага
        /// </summary>
        /// <param name="freq">Частота источника (MHz)</param>
        /// <param name="iterateNumber">Номер итерации (шага)</param>
        private void SolveFrequency(double freq, int iterateNumber)
        {
            _wave.Omega = ParametersBuilder.CalculateOmega(freq);

            // 1. Вычисление вектора B
            double[] vec = _vectorB.Generate(0);

            // 2. Построение глобальной матрицы
            double[,] mat = _matrix.Generate(_wave);

            // NOTE: применение граничных условий Дирихле
            BoundaryBuilder.ApplyDirichletBoundaryCondition2(ref mat, ref vec, _boundNums,
                ArrayHelper.InitValueVector<double>(_boundNums.Length, _data.Boundaries.Single(b => b.Type == BoundaryTypes.Dirichlet).Value));

            // NOTE: или так записать вместео трех предыдущих строк
            double[] result = Math.MathNetHelper.Solve(mat, vec);

            // 4. Запись результата в файл
            string[] resfiles = PrintResultsToFile(vec, result, iterateNumber);

            double eps = ParametersBuilder.CalculateEpsilonForPlasmon(freq, _data.Plasmons[0]);

            // 5. Добавление результата в список с результатами
            AddResultItem(resfiles[0], resfiles[1], _wave, freq, eps);
        }

        #endregion

        #region Единичное решение

        /// <summary>
        /// Решение в единственный момент времени с заданными начальными параметрами
        /// </summary>
        private void SolveOne(double currentTime)
        {
            // 1. Вычисление вектора B
            double[] vec = _vectorB.Generate(currentTime);

            // 2. Построение глобальной матрицы
            double[,] mat = _matrix.Generate(_wave);

            // NOTE: применение граничных условий Дирихле
            BoundaryBuilder.ApplyDirichletBoundaryCondition2(ref mat, ref vec, _boundNums,
                ArrayHelper.InitValueVector<double>(_boundNums.Length, _data.Boundaries.Single(b => b.Type == BoundaryTypes.Dirichlet).Value));

            // NOTE: или так записать вместео трех предыдущих строк
            double[] result = Math.MathNetHelper.Solve(mat, vec);

            // 4. Запись результата в файл
            string[] resfiles = PrintResultsToFile(vec, result, 0);

            // 5. Добавление результата в список с результатами
            AddResultItem(resfiles[0], resfiles[1], _wave, currentTime);
        }

        #endregion

        #region Вспомогательные функции

        /// <summary>
        /// Вывод результата в текстовый файл
        /// </summary>
        /// <param name="result">Вектор с результатом</param>
        /// <param name="number">Номер итерации</param>
        /// <returns>Возвращает имена записанных файлов с результатами</returns>
        private string[] PrintResultsToFile(double[] vector, double[] result, int number)
        {
            // формируем имя файла в зависимости от итерации
            string fileNameVector = String.Format("{0}{1}-{2}.txt", _outputFilePrefix, "vector", number);
            string fileNameResult = String.Format("{0}{1}-{2}.txt", _outputFilePrefix, "result", number);

            // вывод файла с результатами вычисления вектора воздействий
            OutputHelper.OutputVectorToFile<double>(vector, fileNameVector);

            // вывод файла с результатом решения СЛАУ
            OutputHelper.OutputVectorToFile<double>(result, fileNameResult);

            return new string[] { fileNameVector, fileNameResult };
        }

        /// <summary>
        /// Добавление результата в список с данными о результатах
        /// </summary>
        /// <param name="forceFile">Имя файала с вектором воздействий ({b})</param>
        /// <param name="resultFile">Имя файла с результатами (вектором)</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="time">Значение времени</param>
        private void AddResultItem(string forceFile, string resultFile, WaveData wave, double time)
        {
            Results.Add(new Result()
            {
                FileName = resultFile,
                ForceFileName = forceFile,
                Wave = wave,
                Time = time
            });
        }

        /// <summary>
        /// Добавление результата в список с данными о результатах
        /// </summary>
        /// <param name="forceFile">Имя файала с вектором воздействий ({b})</param>
        /// <param name="resultFile">Имя файла с результатами (вектором)</param>
        /// <param name="wave">Сведения о волне</param>
        /// <param name="time">Значение времени</param>
        /// <param name="frequency">Частота работы источника (Hz)</param>
        /// <param name="epsilon">Диэлектрическая проницаемость</param>
        private void AddResultItem(string forceFile, string resultFile, WaveData wave, double frequency, double epsilon)
        {
            Results.Add(new Result()
            {
                FileName = resultFile,
                ForceFileName = forceFile,
                Wave = wave,
                Frequency = frequency,
                Epsilon = epsilon
            });
        }

        #endregion
    }
}
