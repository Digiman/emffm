﻿using System;
using System.Collections.Generic;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Common.Mesh.Elements;
using EMFFM.Manager.Common;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.MeshParser;
using EMFFM.NetgenUtils;
using EMFFM.NetgenUtils.Enums;
using EMFFM.NetgenUtils.GeomFiles;
using EMFFM.NetgenUtils.Helpers;
using EMFFM.NetgenUtils.Preprocessing;

namespace EMFFM.Manager.Mesh
{
    /// <summary>
    /// Класс для работы с сеткой и ее генерацией с использованием программы Netgen.
    /// </summary>
    public class NetgenWorker : IMeshFacade
    {
        /// <summary>
        /// Переменные окружения, на основании которых работает генератор.
        /// </summary>
        private readonly Variables _vars;
        
        /// <summary>
        /// Файл с геометрией.
        /// </summary>
        private readonly string _geoFile;
        
        /// <summary>
        /// Сетка, считанная из текстового файла и хранящая список элементов и точек.
        /// </summary>
        private MeshData _mesh;
        
        /// <summary>
        /// Сгенерирована ли сетка?
        /// </summary>
        public bool IsGenerated { get; set; }

        /// <summary>
        /// Инициализация класса для работы с сеткой.
        /// </summary>
        /// <param name="vars">Переменные окружения (параметры NetgenUtils).</param>
        /// <param name="geoFile">Файл с геометрией. Уже сгенерированный в *.geo (полный путь).</param>
        public NetgenWorker(Variables vars, string geoFile)
        {
            _vars = vars;
            _geoFile = geoFile;
        }

        #region Реализация методов из интерфейса.

        /// <summary>
        /// Генерация сетки.
        /// </summary>
        public MeshData GenerateMesh()
        {
            var meshGen = new MeshGenerator(_vars);

            // 1. Генерация файла сетки
            meshGen.Generate(_geoFile, String.Format("{0}.neu", FilesHelper.GetFileName(_geoFile)));

            // 2. Использование библиотеки MeshParser для разбора сетки
            var parser =
                new MeshFileParser(String.Format("{0}\\{1}{2}.neu", _vars.OutputPath, _vars.MeshFilePrefix,
                    FilesHelper.GetFileName(_geoFile)));
            _mesh = parser.Parse();

            IsGenerated = true;

            return _mesh;
        }

        /// <summary>
        /// Конвертация сетки в рабочий формат (унифицированная сетка).
        /// </summary>
        /// <returns></returns>
        public TMesh<Tetrahedron, Triangle> Convert()
        {
            var meshData = new TMesh<Tetrahedron, Triangle>(ElementType.Tetrahedron);

            // 1. Обработка данных сетки - составление списка элементов (тетраэдров)
            List<Tetrahedron> elements = GenerateVolumeElementsFromMeshData(_mesh,
                Globals.InputData.Options.IsBuiltEdges);
            meshData.MeshElements = elements;

            // 1.1. Нумерация ребер элементов (глобальная нумерация)
            if (Globals.InputData.Options.IsBuiltEdges)
            {
                meshData.EdgesCount = EdgeMesh.BuiltEdges(ref elements);
            }

            // 2. Обработка списка поверхностных элементов
            List<Triangle> surfs = GenerateSurfaceElementsFromMeshData(_mesh, Globals.InputData.Options.IsBuiltEdges);
            meshData.SurfElements = surfs;

            // 2.1. Нумерация ребер поверхностных элементов (глобальная нумерация)
            if (Globals.InputData.Options.IsBuiltEdges)
            {
                meshData.BoundaryEdgesCount = EdgeMesh.BuiltEdges(ref surfs);
            }

            // 3. Генерация граничных ребер
            if (Globals.InputData.Options.IsBuiltEdges)
            {
                GenerateBoundaryEdges(ref meshData);
            }

            return meshData;
        }

        #endregion

        #region Вспомогательные методы для конвертации сетки в рабочий формат TMesh
        
        /// <summary>
        /// Обработка сетки из программы Netgen из тетраэдров (объемные элементы)
        /// </summary>
        /// <param name="data">Данные сетки Netgen</param>
        /// <param name="isBuiltEdges">Генерировать ли ребра для элементов</param>
        /// <returns>Возвращает список элементов (тетраэдров)</returns>
        private static List<Tetrahedron> GenerateVolumeElementsFromMeshData(MeshData data, bool isBuiltEdges = false)
        {
            var elements = new List<Tetrahedron>(data.VolumeElements.Count);

            int elementNumber = 1;

            foreach (var vel in data.VolumeElements)
            {
                var nodes = new List<Node>(vel.Nodes.Length);
                for (int i = 0; i < vel.Nodes.Length; i++)
                {
                    int nodeNumber = vel.Nodes[i];
                    var p = new Point(data.Points[nodeNumber - 1].X, data.Points[nodeNumber - 1].Y, data.Points[nodeNumber - 1].Z);
                    nodes.Add(new Node(p, nodeNumber, i + 1));
                }

                var tet = new Tetrahedron(nodes, elementNumber, vel.SubDomain);
                if (isBuiltEdges)
                {
                    tet.AssociateEdges();
                }
                elements.Add(tet);

                elementNumber++;
            }

            return elements;
        }

        /// <summary>
        /// Генерация поверхностной сетки (треугольние элементы)
        /// </summary>
        /// <param name="data">Данные сетки Netgen</param>
        /// <param name="isBuiltEdges">Генерировать ли ребра для элементов</param>
        /// <returns>Возвращает список поверхностных элементов</returns>
        private static List<Triangle> GenerateSurfaceElementsFromMeshData(MeshData data, bool isBuiltEdges = false)
        {
            var elements = new List<Triangle>(data.SurfaceElements.Count);

            int elementNumber = 1;

            foreach (var sel in data.SurfaceElements)
            {
                var nodes = new List<Node>(sel.Nodes.Length);
                for (int i = 0; i < sel.Nodes.Length; i++)
                {
                    int nodeNumber = sel.Nodes[i];
                    var p = new Point(data.Points[nodeNumber - 1].X, data.Points[nodeNumber - 1].Y, data.Points[nodeNumber - 1].Z);
                    nodes.Add(new Node(p, nodeNumber, i + 1));
                }

                var tet = new Triangle(nodes, elementNumber, sel.BoundaryCond);
                if (isBuiltEdges)
                {
                    // сразу выполнии ассоциацию для ребер на границах (на поверхностях)
                    tet.AssociateBoundaryEdges(sel.BoundaryCond);
                }
                elements.Add(tet);

                elementNumber++;
            }

            return elements;
        }

        /// <summary>
        /// Обработка ребер для граничных условий
        /// </summary>
        /// <param name="mesh">Сетка со списками элементов</param>
        private static void GenerateBoundaryEdges(ref TMesh<Tetrahedron, Triangle> mesh)
        {
            var surfEdges = new List<Edge>(mesh.BoundaryEdgesCount);

            foreach (var surfElem in mesh.SurfElements)
            {
                surfEdges.AddRange(surfElem.Edges);
            }

            foreach (var surfEdge in surfEdges)
            {
                foreach (var element in mesh.MeshElements)
                {
                    foreach (var edge in element.Edges)
                    {
                        if (edge == surfEdge)
                        {
                            edge.IsBoundary = true;
                            edge.BoundaryNumber = surfEdge.BoundaryNumber;
                        }
                    }
                }
            }
        } 

        #endregion

        #region Реализация вспомогательных статических методов в классе.

        /// <summary>
        /// Конвертация XML файла описания геометриив Geo файл, необходимы для Netgen.
        /// </summary>
        /// <param name="filename">Имя файла с данными в XML формате (полный путь).</param>
        /// <returns>Возвращает имя и путь к сгенерированному файлу.</returns>
        /// <remarks>Работает только для файлов 3D геометрией!</remarks>
        public static string GenarateGeoFile(string filename)
        {
            // создание читателя для файла определения и чтение его
            var reader = FactoryHelper.GetFileReader(GeometryTypes.The3D, filename);
            reader.ReadFile();

            string geoFile = FilesHelper.ChangeExtension(filename, "geo");

            // создание генератора и генерация файла геометрии
            var generator = FactoryHelper.GetFileGenerator(GeometryTypes.The3D, geoFile,
                (reader as The3DGeometryFileReader).GetGemetryData);
            generator.CreateFile();

            return geoFile;
        }

        #endregion
    }
}
