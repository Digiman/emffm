using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.MeshParser;

namespace EMFFM.Manager.Mesh
{
    /// <summary>
    /// ��������� ��� ������, ����������� ������ � ������ ��� ������������� �������, ��������������� � ������.
    /// </summary>
    public interface IMeshFacade
    {
        /// <summary>
        /// ������������� �� �����?
        /// </summary>
        bool IsGenerated { get; set; }

        /// <summary>
        /// ��������� �����.
        /// </summary>
        MeshData GenerateMesh();

        /// <summary>
        /// ����������� ����� � ������� ������ (��������������� �����), ������� ������������ � ����������.
        /// </summary>
        /// <returns>���������� ������ � ����� � ���� ������� ������ TMesh, ������� ������������ �� ���� ����������.</returns>
        TMesh<Tetrahedron, Triangle> Convert();
    }
}