﻿using EMFFM.Analyzer.Elements;
using EMFFM.Common.Enums;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;

namespace EMFFM.Manager.Common
{
    /// <summary>
    /// Класс для хранения полных результатов по решению и анализу.
    /// </summary>
    /// <typeparam name="TValue">Тип значений, в которых производилось решение и анализ задачи.</typeparam>
    /// <typeparam name="TResultValue">Тип данных, используемый для хранения данных в матрице анализа.</typeparam>
    public class FullResults <TValue, TResultValue>
    {
        /// <summary>
        /// Результаты решения.
        /// </summary>
        public SolverResult<TValue> SolverResult { get; set; }

        /// <summary>
        /// Сетка с данными после анализа задачи.
        /// </summary>
        public ResultsMesh<TResultValue> AnalyzeMesh { get; set; }

        /// <summary>
        /// Инициализация "пустого" объекта
        /// </summary>
        public FullResults()
        { }

        /// <summary>
        /// Экспорт результатов решения в текстовый файл.
        /// </summary>
        /// <param name="filename">Имя файла для экспорта (полный путь).</param>
        /// <param name="fileType">Формат файла для экспорта.</param>
        /// <remarks>Результаты пишутся в текстовый файл!</remarks>
        public void ExportSolveResult(string filename, ExportFileType fileType)
        {
            switch (fileType)
            {
                case ExportFileType.XML:
                    OutputHelper.OutputSolverResultToXmlFile(filename, SolverResult);
                    break;
                case ExportFileType.TXT:
                    OutputHelper.OutputSolverResultToTextFile(filename, SolverResult);
                    break;
            }
        }

        /// <summary>
        /// Экспорт сетки анализа в файл.
        /// </summary>
        /// <param name="filename">Имя файла и путь к нему для экскорта.</param>
        /// <param name="fileType">Формат файла для экспорта.</param>
        public void ExportAnalyzeMesh(string filename, ExportFileType fileType)
        {
            // TODO: придумать как реализовать экспорт сетки анализа с результатами в файл и написать нужный для этого код!
            switch (fileType)
            {
                case ExportFileType.XML:
                    break;
                case ExportFileType.TXT:
                    break;
                case ExportFileType.HDF5:
                    break;
            }
        }
    }
}
