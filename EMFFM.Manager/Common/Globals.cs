﻿using EMFFM.Input;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.NetgenUtils;

namespace EMFFM.Manager.Common
{
    /// <summary>
    /// Глобальные переменные, используемые во всем приложении.
    /// </summary>
    public struct Globals
    {
        /// <summary>
        /// Переменные окружения для библиотеки NetgenUtils.
        /// </summary>
        /// <remarks>Используются при старте приложения и постоянно при генерации сетки!</remarks>
        public static Variables Variables;

        /// <summary>
        /// Входные параметры (для всего приложения, при запуске определяются и устанавливаются).
        /// </summary>
        public static InputArgs Args;

        /// <summary>
        /// Входные данные со всей задачей, ее условиями и парметрами.
        /// </summary>
        /// <remarks>Описание задачи, которая решается.</remarks>
        public static InputData InputData;

        /// <summary>
        /// Сетка из элементов и ребер.
        /// </summary>
        /// <remarks>Собственно данные о построенной сетке, которая используется для решения задачи.</remarks>
        public static TMesh<Tetrahedron, Triangle> Mesh;
    }
}
