﻿namespace EMFFM.Manager.Common
{
    /// <summary>
    /// Структура с общими данными для менеджера.
    /// </summary>
    public struct Resources
    {
        private const string InputSchemaFile = @"Schemas\InputFile.xsd";
    }
}
