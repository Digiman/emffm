﻿namespace EMFFM.Manager
{
    // TODO: подумать, какие параметры можно будет поместить сюда. Это надо будет сделать для реализации запуска через консоль.

    /// <summary>
    /// Параметры, описанные пользователем для работы программы в командной строке.
    /// </summary>
    public class InputArgs
    {
        /// <summary>
        /// Файл с параметрами среды для NetgenUtils
        /// </summary>
        public string NetgenParams { get; set; }
    }
}