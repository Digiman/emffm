﻿using System.Diagnostics;
using EMFFM.Common.Enums;
using EMFFM.FEM2.Elements;
using EMFFM.FEM2.Helpers;
using EMFFM.FEM2.Solvers;
using EMFFM.Manager.Common;

namespace EMFFM.Manager.Fem
{
    /// <summary>
    /// Специальный класс (фасад) для реализации обработки всех действий, необходимых для решения задачи.
    /// То есть речь идет о использовании библиотеки, которая реализует МКЭ в приложении.
    /// </summary>
    /// <typeparam name="TValue">Тип значений, в которых производится расчет результатов (значения полей).</typeparam>
    public class FemFacade <TValue> : IFemFacade<TValue>
    {
        /// <summary>
        /// Таймер для замера времени выполнения решения задачи.
        /// </summary>
        private readonly Stopwatch _timer;

        /// <summary>
        /// Выполнено ли решение или нет.
        /// </summary>
        public bool IsSolved { get; set; }

        /// <summary>
        /// Результаты решения в вещественных (скалярных) величинах.
        /// </summary>
        public SolverResult<TValue> Results { get; set; }
        
        /// <summary>
        /// Инициализация класса для работы с библиотекой EMFFM.FEM для решения задачи с помощью ВМКЭ.
        /// </summary>
        public FemFacade()
        {
            _timer = new Stopwatch();
            IsSolved = false;
        }

        /// <summary>
        /// Выполнение решения с отсчетом времени, которое затрачивается на него.
        /// </summary>
        /// <returns>Возвращает время, затраченное на решение.</returns>
        public void Solve()
        {
            // TODO: (в будущем) здесь надо предусмотреть выбор между векторным МКЭ и узловым!

            _timer.Start();

            // TODO: последовательность команд, которые выполняют решение (вызов из фасада операций, которые необходимы в определенном порядке)!!

            var factory = new SolverFactory(Globals.InputData, Globals.Mesh);
            var solver = factory.GetSolver<TValue>();
            var result = solver.Solve();

            _timer.Stop();

            result.SolveTime = _timer.ElapsedMilliseconds;

            Results = result;

            IsSolved = true;
        }

        /// <summary>
        /// Сохранение результатов в тектовый файл.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        /// <param name="type">Тип файла с данными, куда будут экспортированы результаты.</param>
        public void ExportResults(string filename, ExportFileType type)
        {
            switch (type)
            {
                case ExportFileType.XML:
                    OutputHelper.OutputSolverResultToXmlFile(filename, Results);
                    break;
                case ExportFileType.TXT:
                    OutputHelper.OutputSolverResultToTextFile(filename, Results);
                    break;
            }
        }
    }
}
