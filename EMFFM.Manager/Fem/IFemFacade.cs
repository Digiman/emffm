using EMFFM.Common.Enums;
using EMFFM.FEM2.Elements;

namespace EMFFM.Manager.Fem
{
    /// <summary>
    /// ��������� ��� ���������� ������ � �����������, ����������� ��� � ������ ����������.
    /// </summary>
    /// <typeparam name="TValue">��� ������, � ������� ���������� ��������� ������� (������������ ��� ����������� ��������).</typeparam>
    public interface IFemFacade <TValue>
    {
        /// <summary>
        /// ��������� �� ������� ��� ���.
        /// </summary>
        bool IsSolved { get; set; }

        /// <summary>
        /// ���������� �������.
        /// </summary>
        /// <remarks>���������� � ��� ���� ������, ��� ������ � ���������� ���������� � ��� ����������.</remarks>
        SolverResult<TValue> Results { get; set; }

        /// <summary>
        /// ���������� ������� � �������� �������, ������� ������������� �� ����.
        /// </summary>
        /// <returns>���������� �����, ����������� �� �������.</returns>
        void Solve();

        /// <summary>
        /// ���������� ����������� � �������� ����.
        /// </summary>
        /// <param name="filename">��� ����� (������ ����).</param>
        /// <param name="type">��� ����� � �������, ���� ����� �������������� ����������.</param>
        void ExportResults(string filename, ExportFileType type);
    }
}