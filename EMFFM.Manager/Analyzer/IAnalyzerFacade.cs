﻿using EMFFM.Analyzer.Elements;
using EMFFM.Common.Enums;
using EMFFM.FEM2.Elements;

namespace EMFFM.Manager.Analyzer
{
    /// <summary>
    /// Интерфейс для анализатора решения.
    /// </summary>
    /// <typeparam name="TValue">Тип данных для величин, в которых производится расчет (Complex).</typeparam>
    /// <typeparam name="TResultValue">Тип данных, в которых хранится результат анализа полей (фактически это поле Complex.Field).</typeparam>
    public interface IAnalyzerFacade <TValue, TResultValue>
    {
        /// <summary>
        /// Показывает состояние анализа.
        /// </summary>
        bool IsAnalyzed { get; set; }

        /// <summary>
        /// Сетка анализа с результатами.
        /// </summary>
        ResultsMesh<TResultValue> ResultsMesh { get; set; }

        /// <summary>
        /// Выполнение анализа.
        /// </summary>
        /// <param name="results">Результаты решения (в величинах того типа, который использовался при инициализации обобщенного класса и интерфейса).</param>
        ResultsMesh<TResultValue> PerformAnalyze(SolverResult<TValue> results);

        /// <summary>
        /// Экспорт результатов анализа в файл.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        /// <param name="fileType">Тип файла для экспорта.</param>
        void ExportResults(string filename, ExportFileType fileType);
    }
}