﻿using EMFFM.Analyzer.Elements;
using EMFFM.Analyzer.Helpers;
using EMFFM.Common.Enums;
using EMFFM.FEM2.Elements;
using EMFFM.Manager.Common;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;

namespace EMFFM.Manager.Analyzer
{
    /// <summary>
    /// Класс для реализации работы с анализатором полей полсе рещения СЛАУ и использования МКЭ.
    /// </summary>
    public class AnalyzerFacade <TValue, TResultValue> : IAnalyzerFacade<TValue, TResultValue>
    {
        /// <summary>
        /// Сетка из элементов.
        /// </summary>
        private readonly TMesh<Tetrahedron, Triangle> _mesh;

        /// <summary>
        /// Трехмерная матрица с результатами анализа.
        /// </summary>
        public ResultsMesh<TResultValue> ResultsMesh { get; set; }

        /// <summary>
        /// Показывает состояние анализа.
        /// </summary>
        public bool IsAnalyzed { get; set; }

        /// <summary>
        /// Инициализация класса для работы с анализом с параметрами.
        /// </summary>
        /// <param name="mesh">Сетка из набора элементов.</param>
        public AnalyzerFacade(TMesh<Tetrahedron, Triangle> mesh)
        {
            _mesh = mesh;
        }

        /// <summary>
        /// Выполнение анализа.
        /// </summary>
        /// <param name="results">Результаты решения (в величинах того типа, который использовался при инициализации обобщенного класса и интерфейса).</param>
        public ResultsMesh<TResultValue> PerformAnalyze(SolverResult<TValue> results)
        {
            var analyzer = new EMFFM.Analyzer.FieldAnalyzer<TValue, TResultValue>(Globals.Mesh, results);

            ResultsMesh = AnalyzerMeshHelper.CreateMesh<TResultValue>(Globals.InputData.AnalyzeBox);
            ResultsMesh = analyzer.PerformAnalyze(ResultsMesh, Globals.InputData.Geometry.CoFactor);

            IsAnalyzed = true;

            return ResultsMesh;
        }

        /// <summary>
        /// Экспорт результатов анализа в файл.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        /// <param name="fileType">Тип файла для экспорта.</param>
        public void ExportResults(string filename, ExportFileType fileType)
        {
            // TODO: придумать как экспортировать результаты анализа (из фасада для анализатора) в файлы! (в три формате экспорта в идеале)

            switch (fileType)
            {
                case ExportFileType.TXT:
                    break;
                case ExportFileType.XML:
                    break;
                case ExportFileType.HDF5:
                    break;
            }
        }
    }
}
