using EMFFM.Common.Enums;
using EMFFM.Input;
using EMFFM.Manager.Common;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.MeshParser;

namespace EMFFM.Manager
{
    // TODO: ������������ ��� ������, ��� ����� � ������� ������ ��� ������� ������!

    /// <summary>
    /// ��������� ��� �������� ������ � ���������, ������������ ������ �� ����� ������������ ����������.
    /// </summary>
    public interface IMainFacade<TValue, TResultValue>
    {
        /// <summary>
        /// ������ � ����� ��� ���������.
        /// </summary>
        MeshData GetMeshData { get; }

        /// <summary>
        /// ������ �������� ����� � ������� � ������ � �� �����������.
        /// </summary>
        /// <param name="filename">��� ����� � ������� (������ ����).</param>
        /// <returns>���������� ����������� ������ �� �������� �����.</returns>
        InputData LoadTask(string filename);

        /// <summary>
        /// �������� ������ �� ������, ��������� � ������ ���������� ��� ����������.
        /// </summary>
        /// <param name="data">������ ��� ��������.</param>
        /// <returns>���������� �� �� ������, ��� � ���� ���������.</returns>
        InputData LoadTask(InputData data);

        /// <summary>
        /// ��������� ����� � �������������� ���������� Netgen.
        /// </summary>
        /// <param name="geoFile">���� � ���������� �������, ������� ����� ������� �� ��������.</param>
        /// <returns>���������� ��������������� �����, ����������� ��� ������� ������.</returns>
        TMesh<Tetrahedron, Triangle> GenerateMesh(string geoFile);

        /// <summary>
        /// ��������� ������� ��� ����������� ������.
        /// </summary>
        /// <remarks>�� ���������� ������. ��� ���������� ������� �������� � ������� ������!</remarks>
        void SolveTask();

        /// <summary>
        /// ���������� ������� �����������.
        /// </summary>
        /// <remarks>�� ���������� ������. ��� ���������� ������� �������� � ������� ������!</remarks>
        void AnalyzeTask();

        /// <summary>
        /// ��������� ������� ������ � ������ �����������.
        /// </summary>
        void SolveAndAnalyze();

        /// <summary>
        /// ��������� ������ �����������.
        /// </summary>
        /// <returns>���������� ����� ����������� �� ������� � �������.</returns>
        FullResults<TValue, TResultValue> GetFullResults();

        /// <summary>
        /// ������� ������ � ������ XML-����.
        /// </summary>
        /// <param name="filename">��� ����� (������ ����).</param>
        void ExportTask(string filename);

        /// <summary>
        /// ������� ����� � XML ����.
        /// </summary>
        /// <param name="filename">��� ����� ��� ��������.</param>
        /// <param name="type">��� �����, � ������� �������������� ������ � �����.</param>
        /// <param name="withEdges">����� ����� � ������� � ������ ��� ��� ���?</param>
        void ExportMesh(string filename, ExportFileType type, bool withEdges = false);

        /// <summary>
        /// ������� ����������� ������� � ��������� ����.
        /// </summary>
        /// <param name="filename">��� ����� ��� �������� (������ ����).</param>
        /// <param name="type">��� �����, ���� ����� ��������� ������.</param>
        /// <remarks>���������� ������� � ��������� ����!</remarks>
        void ExportSolveResult(string filename, ExportFileType type);

        /// <summary>
        /// ������� ����������� ������� � �����.
        /// </summary>
        /// <param name="filename">��� ����� (������ ����).</param>
        /// <param name="type">��� �����, ���� ����� ��������� ������.</param>
        void ExportAnalyzeResult(string filename, ExportFileType type);

        /// <summary>
        /// ����� ������ � ����� �������.
        /// </summary>
        void ResetTask();
    }
}