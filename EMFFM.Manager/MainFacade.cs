﻿using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Input;
using EMFFM.Manager.Analyzer;
using EMFFM.Manager.Common;
using EMFFM.Manager.Fem;
using EMFFM.Manager.Input;
using EMFFM.Manager.Mesh;
using EMFFM.Mesh;
using EMFFM.Mesh.Elements;
using EMFFM.MeshParser;

namespace EMFFM.Manager
{
    /// <summary>
    /// Класс для реализации основных действий с приложением и обеспечения связи всех модулей.
    /// </summary>
    /// <typeparam name="TValue">Тип данных, в которых производится расчет поля (вещественные или комплексные).</typeparam>
    /// <typeparam name="TResultValue">Тип данных, в которых хранится результат решения после анализа поля.</typeparam>
    /// <remarks>Все оснонвные операции и действия собраны здесь. Через этот класс вызываются остальные классы-прослойки.</remarks>
    public class MainFacade<TValue, TResultValue> : IMainFacade<TValue, TResultValue>
    {
        /// <summary>
        /// Объект для реализации работы с генератором сетки.
        /// </summary>
        /// <remarks>Пока мы говорим о NetgenWorker, ведь именно он используется для построения сетки.</remarks>
        private IMeshFacade _meshFacade;
        
        /// <summary>
        /// Объект для реализации работы с входным файлом.
        /// </summary>
        private IInputFacade _inputFacade;
        
        /// <summary>
        /// Объект для реализации работы с решателем.
        /// </summary>
        private IFemFacade<TValue> _femFacade;

        /// <summary>
        /// Объект для реализации работы с анализатором.
        /// </summary>
        private IAnalyzerFacade<TValue, TResultValue> _analyzerFacade;

        /// <summary>
        /// Данные о сетке.
        /// </summary>
        private MeshData _meshData;

        /// <summary>
        /// Данные о сетке для просмотра.
        /// </summary>
        public MeshData GetMeshData
        {
            get { return _meshData; }
        }

        /// <summary>
        /// Инициализация "пустого" объекта для реализации основных действий в библиотеке.
        /// </summary>
        public MainFacade()
        {
            Init();
        }

        /// <summary>
        /// Инициализация объектов класса.
        /// </summary>
        private void Init()
        {
            _inputFacade = new InputFacade();
            _femFacade = new FemFacade<TValue>();

            _meshFacade = null;
            _analyzerFacade = null;
            _meshData = null;
        }

        #region Загрузка задачи для обработки

        /// <summary>
        /// Чтение входного файла с данными о задаче и ее параметрами.
        /// </summary>
        /// <param name="filename">Имя файла с задачей (полный путь).</param>
        /// <returns>Возвращает загруженные данные из входного файла.</returns>
        public InputData LoadTask(string filename)
        {
            return _inputFacade.Load(filename);
        }

        /// <summary>
        /// Загрузка задачи из данных, созданных в другом приложении или библиотеке.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        /// <returns>Возвращает те же данные, что и были загружены.</returns>
        public InputData LoadTask(InputData data)
        {
            _inputFacade = new InputFacade(data);

            return data;
        }

        #endregion

        #region Работа с сеткой

        /// <summary>
        /// Генерация сетки с использованием генератора Netgen.
        /// </summary>
        /// <param name="geoFile">Файл с геометрией области, которую нужно разбить на элементы.</param>
        /// <returns>Возвращает сгенерированную сетку, необходимую для решения задачи.</returns>
        public TMesh<Tetrahedron, Triangle> GenerateMesh(string geoFile)
        {
            if (FilesHelper.GetFileExtension(geoFile) == "xml")
            {
                geoFile = NetgenWorker.GenarateGeoFile(geoFile);
            }

            _meshFacade = new NetgenWorker(Globals.Variables, geoFile);
            _meshData = _meshFacade.GenerateMesh();
            return _meshFacade.Convert();
        }

        #endregion

        #region Решение и анализ задачи

        /// <summary>
        /// Выполнить решение для загруженной задачи.
        /// </summary>
        /// <remarks>Не возвращает ничего. Все результаты решения хранятся в рабочем классе!</remarks>
        public void SolveTask()
        {
            // 1. Построение сетки
            if (Globals.Mesh == null)
            {
                Globals.Mesh = GenerateMesh(Globals.InputData.Geometry.GometryFile);
            }

            // 2. Запуск решателя
            _femFacade.Solve();
        }

        /// <summary>
        /// Выполнение анализа результатов.
        /// </summary>
        /// <remarks>Не возвращает ничего. Все результаты решения хранятся в рабочем классе!</remarks>
        public void AnalyzeTask()
        {
            // TODO: протестировать код для анализа задачи и придумать как сделать его более универсальным!

            if (_femFacade.IsSolved)
            {
                _analyzerFacade = new AnalyzerFacade<TValue, TResultValue>(Globals.Mesh);
                _analyzerFacade.PerformAnalyze(_femFacade.Results);
            }
        }

        /// <summary>
        /// Выполнить решение задачи и анализ результатов.
        /// </summary>
        public void SolveAndAnalyze()
        {
            SolveTask();
            AnalyzeTask();
        }

        #endregion

        #region Методы сбора данных. Это данные от решателя и анализатора, которые дают ответ на решаемую задачу.

        /// <summary>
        /// Получение полных результатов.
        /// </summary>
        /// <returns>Возвращает набор результатов от решения и анализа.</returns>
        public FullResults<TValue, TResultValue> GetFullResults()
        {
            var results = new FullResults<TValue, TResultValue>();
            results.SolverResult = _femFacade.Results;
            //results.AnalyzeResult = _analyzerWorker.ScalarResult;
            results.AnalyzeMesh = _analyzerFacade.ResultsMesh;

            return results;
        }

        #endregion

        #region Методы для экспорта данных в файлы

        /// <summary>
        /// Экспорт задачи в другой XML-файл.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        public void ExportTask(string filename)
        {
            _inputFacade.Save(filename, Globals.InputData);
        }

        /// <summary>
        /// Экспорт сетки в XML файл.
        /// </summary>
        /// <param name="filename">Имя файла для экспорта.</param>
        /// <param name="type">Тип файла, в которой экспортировать данные о сетке.</param>
        /// <param name="withEdges">Вывод сетки с данными о ребрах или без них?</param>
        public void ExportMesh(string filename, ExportFileType type, bool withEdges = false)
        {
            if (Globals.Mesh != null)
            {
                Globals.Mesh.Export(filename, type, withEdges);
            }
            else
            {
                var mesh = _meshFacade.Convert();
                mesh.Export(filename, type, withEdges);
            }
        }

        /// <summary>
        /// Экспорт результатов решения в текстовый файл.
        /// </summary>
        /// <param name="filename">Имя файла для экспорта (полный путь).</param>
        /// <param name="type">Тип файла, куда будут сохранены данные.</param>
        /// <remarks>Результаты пишутся в текстовый файл!</remarks>
        public void ExportSolveResult(string filename, ExportFileType type)
        {
            _femFacade.ExportResults(filename, type);
        }

        /// <summary>
        /// Экспорт результатов анализа в файлы.
        /// </summary>
        /// <param name="filename">Имя файла (полный путь).</param>
        /// <param name="type">Тип файла.</param>
        public void ExportAnalyzeResult(string filename, ExportFileType type)
        {
            _analyzerFacade.ExportResults(filename, type);
        }

        #endregion

        /// <summary>
        /// Сброс задачи и всего решения.
        /// </summary>
        public void ResetTask()
        {
            // по сути переинициализация задачи (нужно и чистить память здесь надо бы)
            Init();
        }
    }
}
