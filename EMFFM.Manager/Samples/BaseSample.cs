﻿using EMFFM.Input;
using EMFFM.Manager.Common;
using EMFFM.NetgenUtils.Helpers;

namespace EMFFM.Manager.Samples
{
    /// <summary>
    /// Базовый класс для реализации примеров использования менеджера.
    /// </summary>
    public abstract class BaseSample
    {
        /// <summary>
        /// Инициализация глобальных параметров.
        /// </summary>
        protected static void InitGlobals()
        {
            var file = @"DataFiles\Params.xml";
            Globals.Variables = VariablesHelper.Load(file);

            Globals.InputData = new InputData();
        }
    }
}
