﻿using System;
using EMFFM.Common.Enums;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Manager.Common;

namespace EMFFM.Manager.Samples
{
    /// <summary>
    /// Класс для описания примеров использования основного рабочего класса менеджера.
    /// </summary>
    public class MainSample : BaseSample
    {
        /// <summary>
        /// Тестирование чтения входного файла и его обработка:
        ///     - чтение входного файла.
        /// </summary>
        /// <remarks>Проверено и работает!</remarks>
        public static void TestInput()
        {
            var worker = new MainFacade<System.Numerics.Complex, Field>();

            var file1 = @"DataFiles\FreqSolveTest5.xml";
            worker.LoadTask(file1);
        }

        /// <summary>
        /// Тестирование вызовов для генерации сетки и работы с ней:
        ///     - чтение входного файла,
        ///     - конвертация файла XML в GEO,
        ///     - генерация сетки из файла,
        ///     - конвертация сетки в нужный формат,
        ///     - экпорт сетки в XML файл.
        /// </summary>
        /// <remarks>Проверено и работает!</remarks>
        public static void TestMesh()
        {
            InitGlobals();
            Globals.InputData.Options.IsBuiltEdges = true;

            var worker = new MainFacade<System.Numerics.Complex, Field>();

            // чтение входного файла с данными
            var file1 = @"DataFiles\FreqSolveTest5.xml";
            worker.LoadTask(file1);

            // генерация сетки
            var file2 = String.Format(@"DataFiles\{0}", Globals.InputData.Geometry.GometryFile);
            var mesh = worker.GenerateMesh(file2);

            // экспорт сгенерированной и сконвертированнной сетки
            var file3 = @"output\exportMesh.xml";
            //mesh.Export(file3, Globals.InputData.Options.IsBuiltEdges);
            worker.ExportMesh(file3, ExportFileType.XML, Globals.InputData.Options.IsBuiltEdges);
        }

        /// <summary>
        /// Тестиование работы решателя и правильность выхова методов, сохранения объектов и т.д.:
        ///     - чтение входного файла,
        ///     - конвертация файла XML в GEO,
        ///     - генерация сетки из файла,
        ///     - выполнение решения,
        ///     - сохранение результатов.
        /// </summary>
        public static void TestSolve()
        {
            // TODO: протестировать метод для работы с решателем!

            InitGlobals();

            var worker = new MainFacade<System.Numerics.Complex, Field>();

            // чтение входного файла с данными
            var file1 = @"DataFiles\FreqSolveTest5.xml";
            worker.LoadTask(file1);

            // выполнение решения задачи
            worker.SolveTask();

            // экспорт результатов решения в файл
            worker.ExportSolveResult(@"output\solveresults.txt", ExportFileType.TXT);
        }

        /// <summary>
        /// Тестирование работы анализатора решений:
        ///     - чтение входного файла,
        ///     - построение сетки,
        ///     - выполнение решения задачи,
        ///     - анализ полученных результатов,
        ///     - экспорт результатов решения в файл,
        ///     - экспорт результатов анализа в файл:
        ///         - в текстовые файлы,
        ///         - в файл XML.
        /// </summary>
        public static void TestAnalyze()
        {
            // TODO: протестировать метод для работы с решателем и анализатором!

            InitGlobals();

            var worker = new MainFacade<System.Numerics.Complex, Field>();

            // чтение входного файла с данными
            var file1 = @"DataFiles\FreqSolveTest5.xml";
            worker.LoadTask(file1);

            // выполнение решения задачи
            worker.SolveAndAnalyze();

            // экспорт результатов решения в файл
            worker.ExportSolveResult(@"output\solveresults.txt", ExportFileType.TXT);

            // экспорт результатов анализа
            worker.ExportAnalyzeResult(@"output\AnalyzeResults.txt", ExportFileType.TXT);
            worker.ExportAnalyzeResult(@"output\AnalyzeResults2.xml", ExportFileType.XML);
        }
    }
}
