﻿using EMFFM.Common.Enums;
using EMFFM.Manager.Common;
using EMFFM.Manager.Mesh;

namespace EMFFM.Manager.Samples
{
    /// <summary>
    /// Класс с примерами для работы с сеткой.
    /// </summary>
    public class MeshSamples : BaseSample
    {
        /// <summary>
        /// Новый вариант генерации сетки с новыми оптимизированными классами.
        ///     - генерация сетки по файлу геометрии,
        ///     - экспорт сетки в XML-файл,
        ///     - конвертация в рабочий формат сетки,
        ///     - построение ребер,
        ///     - экспорт рабочей сетки в XML-файл,
        ///     - экспорт рабочей сетки в текстовый файл.
        /// </summary>
        public static void GenerateNetgenMesh()
        {
            // Инициализация глобальных параметров
            InitGlobals();
            Globals.InputData.Options.IsBuiltEdges = true;
            
            var xmlGeoFile = @"DataFiles\geom5.xml";
            var geoFile = NetgenWorker.GenarateGeoFile(xmlGeoFile);

            // 1. Подготовка сетки и ее генераиция
            var worker = new NetgenWorker(Globals.Variables, geoFile);
            var meshData = worker.GenerateMesh();
            var file1 = @"output\meshdata.xml";
            meshData.ExportMesh(file1, ExportFileType.XML);

            // 2. Конвертация сетки в рабочий формат
            var mesh = worker.Convert();

            // 3. Экспорт сетки из рабочего формата в XMl-файл
            var file2 = @"output\mesh.xml";
            mesh.Export(file2, ExportFileType.XML, Globals.InputData.Options.IsBuiltEdges);
            mesh.Export(file2, ExportFileType.TXT, Globals.InputData.Options.IsBuiltEdges);
        }

        /// <summary>
        /// Новый вариант генерации сетки с новыми оптимизированными классами.
        ///     - генерация сетки по файлу геометрии,
        ///     - экспорт сетки в XML-файл,
        ///     - конвертация в рабочий формат сетки,
        ///     - без построения ребер,
        ///     - экспорт рабочей сетки в XML-файл,
        ///     - экспорт рабочей сетки в текстовый файл.
        /// </summary>
        public static void GenerateNetgenMesh2()
        {
            // Инициализация глобальных параметров
            InitGlobals();
            Globals.InputData.Options.IsBuiltEdges = false;

            var xmlGeoFile = @"DataFiles\geom5.xml";
            var geoFile = NetgenWorker.GenarateGeoFile(xmlGeoFile);

            // 1. Подготовка сетки и ее генераиция
            var worker = new NetgenWorker(Globals.Variables, geoFile);
            var meshData = worker.GenerateMesh();
            var file1 = @"output\meshdata2.xml";
            meshData.ExportMesh(file1, ExportFileType.XML);

            // 2. Конвертация сетки в рабочий формат
            var mesh = worker.Convert();

            // 3. Экспорт сетки из рабочего формата в XMl-файл
            var file2 = @"output\mesh2.xml";
            mesh.Export(file2, ExportFileType.XML, Globals.InputData.Options.IsBuiltEdges);
            mesh.Export(file2, ExportFileType.TXT, Globals.InputData.Options.IsBuiltEdges);
        }

        /// <summary>
        /// Чтение файла и проверка его схемы:
        ///     - чтение XML файла с описанием геометрии,
        ///     - проверка его схемы.
        /// </summary>
        public static void ReadGeoFileAndChechSchema()
        {
            var xmlGeoFile = @"DataFiles\geom5.xml";
            var geoFile = NetgenWorker.GenarateGeoFile(xmlGeoFile);
        }
    }
}
