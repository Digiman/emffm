﻿using EMFFM.Manager.Input;

namespace EMFFM.Manager.Samples
{
    /// <summary>
    /// Класс с примерами работы со входными данными.
    /// </summary>
    public class InputSamples : BaseSample
    {
        /// <summary>
        /// Загрузка входных данных из файла:
        ///     - чтение тестового входного файла,
        ///     - сохранение его в новый файл (проверка записи).
        /// </summary>
        public static void LoadInputDataAndSave()
        {
            var file = @"DataFiles\input1.xml";

            var worker = new InputFacade(file);
            var inputData = worker.Load(file);

            var file2 = @"output\input1_out.xml";
            worker.Save(file2, inputData);
        }

        /// <summary>
        /// Загрузка входных данных и файла и его провыерка по схеме:
        ///     - чтение тестового входного файла,
        ///     - проверка файла на соответствие схеме,
        ///     - сохранение его в новый файл (проверка записи).
        /// </summary>
        public static void LoadInputDataAndCheck()
        {
            var file = @"DataFiles\input1.xml";

            var worker = new InputFacade(file);
            var result = worker.CheckInputFileSchema(file);
            if (result == false) // нету ошибок при валидации
            {
                var inputData = worker.Load(file);

                // сохранение прочитанных данных в новый файл
                var file2 = @"output\input1_out.xml";
                worker.Save(file2, inputData);
            }
        }
    }
}
