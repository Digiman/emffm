﻿using EMFFM.Input;

namespace EMFFM.Manager.Input
{
    /// <summary>
    /// Класс для работы с входными данными InputAPI.
    /// </summary>
    public class InputFacade : IInputFacade
    {
        /// <summary>
        /// Входные данные с описанием задачи и действий.
        /// </summary>
        private InputData _data;

        // TODO: проверить путь к схеме для проверки валидности входного файла
        /// <summary>
        /// Файл со схемой XML Schema для проверки валидности файла с данными в XML-формате.
        /// </summary>
        private const string SchemaFile = @"Schemas\InputFile.xsd";

        /// <summary>
        /// Инициализация "пустого" объекта для работы с входными данными.
        /// </summary>
        public InputFacade()
        {
            _data = new InputData();
        }

        /// <summary>
        /// Инициализация объекта для работы с входными данными.
        /// </summary>
        /// <param name="data">Входные данные.</param>
        public InputFacade(InputData data)
        {
            _data = data;
        }

        /// <summary>
        /// Инициализация объекта для работы с входными данными путем чтения данных из файла.
        /// </summary>
        /// <param name="filename">Файл с данными для инициализации (полный путь).</param>
        public InputFacade(string filename)
        {
            Load(filename);
        }

        /// <summary>
        /// Загрузка данных из файла.
        /// </summary>
        /// <param name="filename">Файл с входными данными и описанием задачи (полнй путь).</param>
        /// <returns>Возвращает объект, заполненный считанными данными из входного файла.</returns>
        public InputData Load(string filename)
        {
            if (CheckInputFileSchema(filename) == false)
            {
                _data = InputFileReader.ReadInputFile(filename);
            }

            return _data;
        }

        /// <summary>
        /// Сохранение входных данных и всей задачи в файле.
        /// </summary>
        /// <param name="filename">Файл для сохранения (полный путь).</param>
        /// <param name="data">Входные данные (описание задачи) для сохранения в файл.</param>
        public void Save(string filename, InputData data)
        {
            if (_data != null)
            {
                InputFileWriter.SaveInputData(data, filename);
            }
        }

        /// <summary>
        /// Выполнение проверки файла с помощью XML Schema на его корректность.
        /// </summary>
        /// <param name="filename">Имя файла с данными для проверки (полный путь).</param>
        /// <returns>Возвращает False, если файл проверен успешно и соответствует схеме (не найдено ошибок), иначе - True (есть ошибки).</returns>
        public bool CheckInputFileSchema(string filename)
        {
            //var result = XmlHelper.ValidateFile(SchemaFile, filename);

            //return result;
            
            // NOTE: пока проверка сделана как заглушка, потому что нету файла схемы и конечной спецификации входного файла!
            return false;
        }
    }
}
