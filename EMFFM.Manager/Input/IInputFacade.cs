﻿using EMFFM.Input;

namespace EMFFM.Manager.Input
{
    /// <summary>
    /// Интерфейс для описания того, как работать с входными данными (описанием задачи).
    /// </summary>
    public interface IInputFacade
    {
        /// <summary>
        /// Загрузка данных из файла.
        /// </summary>
        /// <param name="filename">Файл с входными данными и описанием задачи (полный путь).</param>
        /// <returns>Возвращает объект, заполненный считанными данными из входного файла.</returns>
        InputData Load(string filename);

        /// <summary>
        /// Сохранение входных данных и всей задачи в файле.
        /// </summary>
        /// <param name="filename">Файл для сохранения (полный путь).</param>
        /// <param name="data">Входные данные (описание задачи) для сохранения в файл.</param>
        void Save(string filename, InputData data);

        /// <summary>
        /// Выполнение проверки файла с помощью XML Schema на его корректность.
        /// </summary>
        /// <param name="filename">Имя файла с данными для проверки (полный путь).</param>
        /// <returns>Возвращает False, если файл проверен успешно и соответствует схеме (не найдено ошибок), иначе - True (есть ошибки).</returns>
        bool CheckInputFileSchema(string filename);
    }
}
