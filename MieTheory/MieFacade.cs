﻿using System;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Common.Materials;
using MieTheory.Common;
using MieTheory.Functions;
using MieTheory.Input;

namespace MieTheory
{
    /// <summary>
    /// Реализация фасада для работы с методами библиотеки по теории Ми.
    /// </summary>
    public class MieFacade : IMieFacade
    {
        /// <summary>
        /// Параметры задачи и работ приложения.
        /// </summary>
        public InputMieData Data { get; set; }

        /// <summary>
        /// Результаты решения задачи.
        /// </summary>
        public MieFullResults Results { get; set; }

        /// <summary>
        /// Инициализация "пустого" фасада.
        /// </summary>
        public MieFacade()
        {
            Data = new InputMieData();
        }

        /// <summary>
        /// Инициализация фасада с параметрами.
        /// </summary>
        /// <param name="data">ПАраметры задачи и приложения.</param>
        public MieFacade(InputMieData data)
        {
            Data = data;
        }

        /// <summary>
        /// Загрузка параметров работы приложения и расчетов.
        /// </summary>
        /// <param name="filename">Имя файла, откуда загружать данные.</param>
        /// <returns>Возвращает загруженные из файла данные.</returns>
        public InputMieData LoadInputData(string filename)
        {
            return XmlHelper.DeserializeData<InputMieData>(filename);
        }

        /// <summary>
        /// Сохранение параметров приложения.
        /// </summary>
        /// <param name="filename">Имя файла, в который нужно произвести запись данных.</param>
        /// <param name="data">Данные приложения для сохранения в файл.</param>
        public void SaveInputData(string filename, InputMieData data)
        {
            XmlHelper.SerializeData(filename, data);
        }

        /// <summary>
        /// Выполнение расчета.
        /// </summary>
        public void Calculate()
        {
            // определение параметров частицы
            var model = new LorenzDrude(Data.Lambda);
            Complex epsilon = model.Calculate(Data.Material, Data.MaterialModel);

            LogHelper.Log(LogMessageType.Debug, "Particle in MieTheory have Epsilon = {0} from LD model",
                epsilon.ToString());

            Complex ns = Complex.Sqrt(epsilon);
            
            double[] x = new double[Data.Count];
            double[] y = new double[Data.Count];
            double[,] z = new double[Data.Count, Data.Count];

            double temp = Math.Max(3*Data.Radius, Data.Lambda/8);

            double xc_min = -temp;
            double xc_max = temp;
            double yc_min = -temp;
            double yc_max = temp;

            double zc = 0;
            double dx = (xc_max - xc_min)/(Data.Count - 1);
            double dy = (yc_max - yc_min)/(Data.Count - 1);

            double xc, yc;
            for (int i = 0; i < Data.Count; i++)
            {
                xc = xc_min + i*dx;
                x[i] = xc;
                for (int j = 0; j < Data.Count; j++)
                {
                    yc = yc_min + j*dy;
                    y[j] = yc;
                    z[i, j] = 0;
                    if (Math.Sqrt(xc*xc + yc*yc + zc*zc) > Data.Radius)
                    {
                        var fields = MieCalculation.CalculateMieNearField(Data.Lambda, Data.Radius, ns, Data.Nm, xc,
                            yc, zc);
                        z[i, j] = Math.Sqrt(Sum(fields[0]));
                    }
                }
            }

            // формирование объекта с результатами решения задачи.
            Results = new MieFullResults {X = x, Y = y, Z = z};
        }

        /// <summary>
        /// Экспорт результатов решения в файлы.
        /// </summary>
        /// <param name="applicationPath">Полный путь к приложению, откуда оно запущено.</param>
        public void ExportResults(string applicationPath)
        {
            Results.Export(String.Format("{0}\\{1}", applicationPath, Data.OutputPath));
        }

        /// <summary>
        /// Выполнение решения (расчет и экспорт результатов).
        /// </summary>
        /// <param name="applicationPath">Полный путь к приложению, откуда оно запущено.</param>
        /// <param name="withExport">Экспортировтаь ли результаты сразу после решения?</param>
        public void Solve(string applicationPath, bool withExport = false)
        {
            // 1. Выроление расчетов.
            Calculate();

            // 2. Экспорт результатов решения.
            if (withExport) ExportResults(applicationPath);
        }

        /// <summary>
        /// Вычисление суммы.
        /// </summary>
        /// <param name="array">Массив с значениями поля.</param>
        /// <returns>Возвращает значение суммы полей.</returns>
        private double Sum(Complex[] array)
        {
            double rez = 0;
            for (int i = 0; i < array.Length; i++)
            {
                rez += array[i].Real * array[i].Real + array[i].Imaginary * array[i].Imaginary;
            }
            return rez;
        }
    }
}
