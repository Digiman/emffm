﻿using System;
using System.Collections.Generic;
using System.Numerics;
using EMFFM.Math.Elements;
using MieTheory.Helpers;

namespace MieTheory.Functions
{
    // TODO: если все в новом классе работает, то этот класс будет полностью удален!

    /// <summary>
    /// Класс для расчета различных функций из теории Ми
    /// </summary>
    /// <remarks>Используются методы из матлаба примера!</remarks>
    public static class MieFunctions
    {
        /// <summary>
        /// Вычисление коэффициентов An, Bn, Cn, Dn
        /// </summary>
        /// <param name="m"></param>
        /// <param name="x"></param>
        /// <returns>Возвращает список вычисленных кэффициентов</returns>
        public static List<Complex[]> CalculateABCDCoefficients(Complex m, double x)
        {
            /*int nmax = (int)Math.Round(2 + x + 4*Math.Pow(x, 1/3));
            
            int[] n = ArrayHelper.InitArray(1, nmax, 1);
            double[] nu = ArrayHelper.InitArray(1.5, nmax + 0.5, 1);

            var z = ArrayHelper.InitArray(nmax, ComplexHelper.RealMultiply(x, m));
            var m2 = ArrayHelper.InitArray(nmax, m*m);

            Complex[] sqx = ComplexHelper.Convert(ArrayHelper.InitArray(nmax, Math.Sqrt(0.5*Math.PI/x)));
            Complex[] sqz = ArrayHelper.Devide(Complex.Sqrt, 0.5*Math.PI, z);

            var bx = MathHelper.Besseljn(nu, x)*sqx;
            var bz = MathHelper.Besseljn(nu, z)*sqz;
            var yx = MathHelper.Besseljn(nu, x)*sqx;
            */
            /*
            nmax=round(2+x+4*x^(1/3));
            n=(1:nmax); nu = (n+0.5); z=m.*x; m2=m.*m;
            sqx= sqrt(0.5*pi./x); sqz= sqrt(0.5*pi./z);
            bx = besselj(nu, x).*sqx;
            bz = besselj(nu, z).*sqz;
            yx = bessely(nu, x).*sqx;
            hx = bx+i*yx;
            b1x=[sin(x)/x, bx(1:nmax-1)];
            b1z=[sin(z)/z, bz(1:nmax-1)];
            y1x=[-cos(x)/x, yx(1:nmax-1)];
            h1x= b1x+i*y1x;
            ax = x.*b1x-n.*bx;
            az = z.*b1z-n.*bz;
            ahx= x.*h1x-n.*hx;
            an = (m2.*bz.*ax-bx.*az)./(m2.*bz.*ahx-hx.*az);
            bn = (bz.*ax-bx.*az)./(bz.*ahx-hx.*az);
            cn = (bx.*ahx-hx.*ax)./(bz.*ahx-hx.*az);
            dn = m.*(bx.*ahx-hx.*ax)./(m2.*bz.*ahx-hx.*az);
            result=[an; bn; cn; dn];*/

            var list = new List<Complex[]>(4);

            return list;
        }

        /// <summary>
        /// Вычисление коэффициентов угловых функций
        /// </summary>
        /// <param name="theta">Угол поляризации</param>
        /// <param name="nmax">Число параметров в ряду</param>
        /// <returns>Возвращает список значений для функций</returns>
        public static List<AngularFunction> CalculatePiAndTau(double theta, int nmax)
        {
            var funcs = new List<AngularFunction>(nmax);
            double u = Math.Cos(theta);

            funcs.Add(new AngularFunction() {Pi = 0, Tau = 0});
            funcs.Add(new AngularFunction() {Pi = 1, Tau = u});
            funcs.Add(new AngularFunction() {Pi = 3*u, Tau = 3*Math.Cos(2*Math.Acos(u))});
            for (int n1 = 3; n1 < nmax; n1++)
            {
                double p1 = (2*n1 - 1)/(n1 - 1)*funcs[n1 - 1].Pi*u;
                double p2 = n1/(n1 - 1)*funcs[n1 - 2].Pi;
                double t1 = n1*u*(p2 - p1);
                double t2 = (n1 + 1)*funcs[n1 - 1].Pi;
                funcs.Add(new AngularFunction() {Pi = p2 - p1, Tau = t1 - t2});
            }

            return funcs;
        }

        /// <summary>
        /// Расчет полей в ближней зоне по теории Ми
        /// </summary>
        /// <param name="lambda">Длина волны (нм)</param>
        /// <param name="r">Радиус частицы (сферы)</param>
        /// <param name="nm">Показатель преломления среды</param>
        /// <param name="ns">Показатель преломления частицы</param>
        /// <param name="xc">Координата исследуемой точки</param>
        /// <param name="yx">Координата исследуемой точки</param>
        /// <param name="zc">Координата исследуемой точки</param>
        /// <returns>Возвращает массив значений полей вокруг частицы</returns>
        public static List<Complex[]> CalculateMieNearField(double lambda, double r, double nm, Complex ns, double xc, double yx, double zc)
        {
            // 1. Подготовка параметров для расчета
            var k = (2*Math.PI)/(lambda*nm);
            var x = k*r;
            var m = ns/nm;

            // 2. Вычисление коэффициентов An и Bn
            var coef = CalculateCoefficientAB(x, m);

            // 3. Расчет поля
            var fields = CalculateMieField(lambda, r, nm, coef[0], coef[1], xc, yx, zc);

            return fields;
        }

        /// <summary>
        /// Вычисление коэффициентов Ми
        /// </summary>
        /// <param name="x"></param>
        /// <param name="m">Комплексное значение преломляющего коэффициента</param>
        /// <returns>Возвращает список значений коэффициентов</returns>
        private static List<Complex[]> CalculateCoefficientAB(double x, Complex m)
        {
            int M = (int)Math.Ceiling(x + 4*(Math.Pow(x, 1/3)) + 2);
            int[] n = ArrayHelper.InitArray(1, M, 1);

            /*var Sx = MathHelper.Ricbesj(n, x);
            var dSx = MathHelper.Dricbesj(n, x);
            var Smx = MathHelper.Ricbesj(n, m*x);
            var dSmx = MathHelper.Dricbesj(n, m * x);
            var xix = MathHelper.Ricbesh(n, x);
            var dxix = MathHelper.Dricbesh(n, x);
            */
            //an=(m*Smx.*dSx-Sx.*dSmx)./(m*Smx.*dxix-xix.*dSmx);
            //bn=(Smx.*dSx-m*Sx.*dSmx)./(Smx.*dxix-m*xix.*dSmx);

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lambda">Доина волны</param>
        /// <param name="r">Радиус частицы (сферы)</param>
        /// <param name="nm"></param>
        /// <param name="an"></param>
        /// <param name="bn"></param>
        /// <param name="xc"></param>
        /// <param name="yx"></param>
        /// <param name="zc"></param>
        /// <returns></returns>
        private static List<Complex[]> CalculateMieField(double lambda, double r, double nm, Complex[] an, Complex[] bn, double xc, double yx, double zc)
        {


            return new List<Complex[]>();
        }
    }
}
