﻿using System;
using System.Collections.Generic;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;
using EMFFM.Common.Math;
using EMFFM.Common.Structs;
using MieTheory.Bessel;

namespace MieTheory.Functions
{
    /// <summary>
    /// Класс для выполнения рассчетов для функций из теории Ми.
    /// </summary>
    public static class MieCalculation
    {
        /// <summary>
        /// Расчет полей в ближней зоне по теории Ми.
        /// </summary>
        /// <param name="lambda">Длина волны (нм)</param>
        /// <param name="r">Радиус частицы (сферы)</param>
        /// <param name="nm">Показатель преломления среды</param>
        /// <param name="ns">Показатель преломления частицы</param>
        /// <param name="xc">Координата исследуемой точки</param>
        /// <param name="yc">Координата исследуемой точки</param>
        /// <param name="zc">Координата исследуемой точки</param>
        /// <returns>Возвращает массив значений полей вокруг частицы</returns>
        public static List<Complex[]> CalculateMieNearField(double lambda, double r, Complex ns, double nm, double xc,
            double yc, double zc)
        {
            // 1. Подготовка параметров для расчета
            var k = (2*Math.PI)/(lambda*nm);

            //LogHelper.Log(LogMessageType.Debug, "Wave Number(k) = {0} in MieTheory", k);

            var x = k*r;
            var m = ns/nm;

            // 2. Вычисление коэффициентов An и Bn
            var coef = CalculateCoefficientAB(x, m);

            // 3. Расчет поля
            var fields = CalculateMieField(lambda, r, nm, coef[0], coef[1], xc, yc, zc);

            // 4. Возвращение значений для расчитанного поля
            return fields;
        }

        /// <summary>
        /// Вычисление поля по теории Ми.
        /// </summary>
        /// <param name="lambda">Длина волны.</param>
        /// <param name="r">Радиус частицы.</param>
        /// <param name="nm"></param>
        /// <param name="an"></param>
        /// <param name="bn"></param>
        /// <param name="xc"></param>
        /// <param name="yc"></param>
        /// <param name="zc"></param>
        /// <returns></returns>
        private static List<Complex[]> CalculateMieField(double lambda, double r, double nm, Complex[] an, Complex[] bn,
            double xc, double yc, double zc)
        {
            Complex[] e1 = ComplexArrayHelper.InitValueVector(3, 0);
            Complex[] h1 = ComplexArrayHelper.InitValueVector(3, 0);

            double k = 2*Math.PI/lambda*nm;
            int M = an.Length;
            double[] n = new double[M];

            for (int i = 1; i <= M; i++)
            {
                n[i - 1] = i;
            }
            double E0 = 1;
            Complex[] En = new Complex[M];
            for (int i = 0; i < M; i++)
            {
                En[i] = Complex.Pow(Complex.One, n[i])*E0*(2*n[i] + 1)/(n[i]*(n[i] + 1));
            }

            double c0 = ConstantParameters.LightSpeed;
            double mue0 = ConstantParameters.Mue0;
            double omega = 2*Math.PI/lambda*c0;

            //LogHelper.Log(LogMessageType.Debug, "Omega = {0} in MieTheory", omega);

            double[,] R = new double[3, 3];

            double theta;
            double phi;
            double rad;
            double rho;

            double[] pin;
            double[] taun;

            Complex[,] Mo1n = new Complex[M, 3];
            Complex[,] Me1n = new Complex[M, 3];
            Complex[,] No1n = new Complex[M, 3];
            Complex[,] Ne1n = new Complex[M, 3];

            Complex[,] Edum = new Complex[M, 3];
            Complex[,] Hdum = new Complex[M, 3];

            Xcart2sph(xc, yc, zc, out theta, out phi, out rad);
            rho = k*rad;
            Angdepfunmie(theta, n, out pin, out taun);

            Complex[] h = new Complex[M];
            Complex[] dxi = new Complex[M];
            for (int i = 0; i < M; i++)
            {
                h[i] = SphericalBessel.GetSbesselH(n[i], 1, rho);
                dxi[i] = DerivativeRiccattiBessel.GetDricbesH(n[i], 1, rho);
            }

            R[0, 0] = Math.Sin(theta)*Math.Cos(phi);
            R[0, 1] = Math.Cos(theta)*Math.Cos(phi);
            R[0, 2] = -Math.Sin(phi);
            R[1, 0] = Math.Sin(theta)*Math.Sin(phi);
            R[1, 1] = Math.Cos(theta)*Math.Sin(phi);
            R[1, 2] = Math.Cos(phi);
            R[2, 0] = Math.Cos(theta);
            R[2, 1] = -Math.Sin(theta);
            R[2, 2] = 0;
            if (rad > r)
            {
                for (int i = 0; i < M; i++)
                {
                    Mo1n[i, 0] = 0;
                    Mo1n[i, 1] = Math.Cos(phi)*pin[i]*h[i];
                    Mo1n[i, 2] = -Math.Sin(phi)*taun[i]*h[i];
                    Me1n[i, 0] = 0;
                    Me1n[i, 1] = -Math.Sin(phi)*pin[i]*h[i];
                    Me1n[i, 2] = -Math.Cos(phi)*taun[i]*h[i];
                    No1n[i, 0] = Math.Sin(phi)*n[i]*(n[i] + 1)*Math.Sin(theta)*pin[i]*h[i]/rho;
                    No1n[i, 1] = Math.Sin(phi)*taun[i]*dxi[i]/rho;
                    No1n[i, 2] = Math.Cos(phi)*pin[i]*dxi[i]/rho;
                    Ne1n[i, 0] = Math.Cos(phi)*n[i]*(n[i] + 1)*Math.Sin(theta)*pin[i]*h[i]/rho;
                    Ne1n[i, 1] = Math.Cos(phi)*taun[i]*dxi[i]/rho;
                    Ne1n[i, 2] = -Math.Sin(phi)*pin[i]*dxi[i]/rho;
                }

                for (int d = 0; d < 3; d++)
                {
                    for (int i = 0; i < M; i++)
                    {
                        Edum[i, d] = En[i]*(Complex.One*an[i]*Ne1n[i, d] - bn[i]*Mo1n[i, d]);
                        Hdum[i, d] = En[i]*(Complex.One*bn[i]*No1n[i, d] + an[i]*Me1n[i, d]);
                    }
                }

                Complex[] sumEdum = CalculateSum(Edum);
                Complex[] sumHdum = CalculateSum(Hdum);

                for (int j = 0; j < 3; j++)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        e1[j] += R[j, i]*sumEdum[i];
                        h1[j] += k/omega/mue0*R[j, i]*sumHdum[i];
                    }
                }
            }

            return new List<Complex[]> {e1, h1};
        }

        /// <summary>
        /// Вычисление коэффициентов Ми.
        /// EXPCOEFFMIE Calculates the expansion coefficients for the Mie theory.
        /// 
        /// [AN,BN] = EXPCOEFFMIE(x,m) calculates the expansion coefficients
        /// an, bn (AN,BN) for a sphere with size parameter x and relative refractive index m.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="m">Комплексное значение преломляющего коэффициента</param>
        /// <returns>Возвращает список значений коэффициентов</returns>
        private static List<Complex[]> CalculateCoefficientAB(double x, Complex m)
        {
            int M = (int) Math.Ceiling(x + 4.0*(Math.Pow(x, (1.0/3.0))) + 2.0);
            Complex Sx, dSx, Smx, dSmx, xix, dxix;

            var an = new Complex[M];
            var bn = new Complex[M];

            for (int i = 1; i <= M; i++)
            {
                Sx = RiccattiBessel.GetRicbesJ(i, new Complex(x, 0));
                dSx = DerivativeRiccattiBessel.GetDricbesJ(i, new Complex(x, 0));
                Smx = RiccattiBessel.GetRicbesJ(i, m*(new Complex(x, 0)));
                dSmx = DerivativeRiccattiBessel.GetDricbesJ(i, m*(new Complex(x, 0)));
                xix = RiccattiBessel.GetRicbesH(i, 1, new Complex(x, 0));
                dxix = DerivativeRiccattiBessel.GetDricbesH(i, 1, new Complex(x, 0));

                an[i - 1] = (m*Smx*dSx - Sx*dSmx)/(m*Smx*dxix - xix*dSmx);
                bn[i - 1] = (Smx*dSx - m*Sx*dSmx)/(Smx*dxix - m*xix*dSmx);
            }

            return new List<Complex[]> {an, bn};
        }

        /// <summary>
        /// Вычисление суммы для всех значений в списке массив из комплексных числел.
        /// </summary>
        /// <param name="data">Данные для расчета суммы.</param>
        /// <returns>Возвращет значение суммы всех чисел в переданных в функцию данных.</returns>
        public static Complex[] CalculateSum(Complex[,] data)
        {
            Complex[] rez = ComplexArrayHelper.InitValueVector(data.GetLength(0), 0);
            int n = data.GetLength(0);
            int m = data.GetLength(1);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    rez[j] += data[i, j];
                }
            }
            return rez;
        }

        /// <summary>
        /// Преобразование коррдинат из декартовой системы в сферическую.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="theta"></param>
        /// <param name="phi"></param>
        /// <param name="r"></param>
        private static void Xcart2sph(double x, double y, double z, out double theta, out double phi, out double r)
        {
            theta = Math.PI/2 - Math.Atan(z/Math.Sqrt(x*x + y*y));
            phi = Math.Atan2(y, x);
            r = Math.Sqrt(x*x + y*y + z*z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theta"></param>
        /// <param name="n"></param>
        /// <param name="pin"></param>
        /// <param name="taun"></param>
        private static void Angdepfunmie(double theta, double[] n, out double[] pin, out double[] taun)
        {
            pin = new double[n.Length];
            taun = new double[n.Length];
            double mu = Math.Cos(theta);
            pin[0] = 1.0;
            pin[1] = 3.0*mu;
            taun[0] = mu;
            taun[1] = 6.0*mu*mu - 3.0;
            for (int i = 3; i <= n.Length; i++)
            {
                pin[i - 1] = (2*i - 1)/(double) (i - 1)*mu*pin[i - 1 - 1] - i/(double) (i - 1)*pin[i - 2 - 1];
                taun[i - 1] = i*mu*pin[i - 1] - (i + 1)*pin[i - 1 - 1];
            }
        }
    }
}
