﻿using EMFFM.Common.Enums;

namespace MieTheory.Input
{
    /// <summary>
    /// Входные данные и параметры для задачи.
    /// </summary>
    public class InputMieData
    {
        /// <summary>
        /// Радиуч частицы.
        /// </summary>
        public double Radius { get; set; }

        /// <summary>
        /// Длина волны источника (плоская волна от источника ЭМ излучения).
        /// </summary>
        public double Lambda { get; set; }

        /// <summary>
        /// Материал частицы.
        /// </summary>
        public MaterialTypes Material { get; set; }

        /// <summary>
        /// Размерность.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Модель для материала (расчет его дисперсионных параметров).
        /// </summary>
        public ModelTypes MaterialModel { get; set; }

        /// <summary>
        /// Порядок или что-то там еще.
        /// </summary>
        public int Nm { get; set; }

        /// <summary>
        /// Каталог для выходных файлов.
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        /// Инициализация параметров по умолчанию.
        /// </summary>
        /// <remarks>Нужен для работы сериализации!</remarks>
        public InputMieData()
        {
            Nm = 1;
            Radius = 4e-8;
            Lambda = 4.05e-7;
            Material = MaterialTypes.Ag;
            MaterialModel = ModelTypes.LD; // Лоренца-Друде
            Count = 200;

            OutputPath = @"output\";
        }
    }
}
