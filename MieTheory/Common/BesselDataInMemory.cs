﻿using BesselData;

namespace MieTheory.Common
{
    /// <summary>
    /// Класс, инициализирующий данные для функций Бесселя.
    /// </summary>
    public struct BesselDataInMemory
    {
        /// <summary>
        /// Функция BesselH.
        /// </summary>
        public static readonly BesselFunction BesselH = new BesselFunction(@"Data\besselh.json", @"с:\besselh-clean.txt", new []{25, 151, 151});

        /// <summary>
        /// Функция BesselH1.
        /// </summary>
        public static readonly BesselFunction BesselH1 = new BesselFunction(@"Data\besselh1.json", @"с:\besselh1-clean.txt", new[] { 25, 151, 151 });

        /// <summary>
        /// Функция BesselJ.
        /// </summary>
        public static readonly BesselFunction BesselJ = new BesselFunction(@"Data\besselj.json", @"с:\besselj-clean.txt", new[] { 25, 151, 151 });

        /// <summary>
        /// Функция BesselY.
        /// </summary>
        public static readonly BesselFunction BesselY = new BesselFunction(@"Data\bessely.json", @"с:\bessely-clean.txt", new[] { 31, 101, 101 });
    }
}
