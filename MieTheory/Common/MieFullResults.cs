﻿using System;
using System.Collections.Generic;
using EMFFM.Common.Helpers;
using MieTheory.Helpers;

namespace MieTheory.Common
{
    /// <summary>
    /// Класс для хранерия полных результатов, полученных в процссе решения задачи по теории Ми.
    /// </summary>
    public class MieFullResults
    {
        /// <summary>
        /// Координаты по оси OX.
        /// </summary>
        public double[] X { get; set; }

        /// <summary>
        /// Координаты по оси OY.
        /// </summary>
        public double[] Y { get; set; }

        /// <summary>
        /// Матрица с данными о рапределении поля в плоскости.
        /// </summary>
        public double[,] Z { get; set; }

        /// <summary>
        /// Инициализайция "пустого" объекта с параметрами по умолчанию.
        /// </summary>
        public MieFullResults()
        {
            X = new double[0];
            Y = new double[0];
            Z = new double[0, 0];
        }

        /// <summary>
        /// Экспорт результатов в файлы.
        /// </summary>
        /// <param name="outputPath">Выходной каталог для размещения результатов.</param>
        public void Export(string outputPath)
        {
            IoHelper.CheckAndCreateDirectory(outputPath);
            IoHelper.CopyFileToDirectory(@"Matlab/PlotToFile.m", outputPath);

            OutputHelper.SaveArrayToFile(outputPath + "\\x.txt", X);
            OutputHelper.SaveArrayToFile(outputPath + "\\y.txt", Y);
            OutputHelper.SaveMatrixToFile(outputPath + "\\matrix.txt", Z);

            OutputGraphData(outputPath);
        }

        /// <summary>
        /// Вывод данных для среза (те что используются для построения графика).
        /// </summary>
        /// <param name="outputPath">Выходной каталог.</param>
        private void OutputGraphData(string outputPath)
        {
            var data = CutLines();

            OutputHelper.SaveArrayToFile(outputPath + "\\datay1.txt", data[0]);
            OutputHelper.SaveArrayToFile(outputPath + "\\datay2.txt", data[1]);
        }

        /// <summary>
        /// Формирование срезов плоских
        /// </summary>
        /// <returns>Возвращает список массивов с данными для плоского графика.</returns>
        private List<double[]> CutLines()
        {
            int sizeX = X.Length;
            int sizeY = Y.Length;

            // построение вектора (массива) для заданного положения по матрице среза
            var data1 = new double[sizeX];
            var data2 = new double[sizeX];

            var nx = (int)Math.Floor(sizeX / 2.0);
            var ny = (int)Math.Floor(sizeY / 2.0);

            for (int i = 0; i < sizeY; i++)
            {
                data1[i] = Z[i, nx];
            }
            for (int i = 0; i < sizeX; i++)
            {
                data2[i] = Z[ny, i];
            }

            return new List<double[]> {data1, data2};
        }
    }
}
