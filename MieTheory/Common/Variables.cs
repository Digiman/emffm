﻿namespace MieTheory.Common
{
    /// <summary>
    /// Глобальные паременные для расчета функций Бесселя.
    /// </summary>
    public struct Variables
    {
        public const double Inf = 1E+37;
        public const int NaN = 0;

        public const int NU_MAX = 12;
        public const int NU_MIN = 0;
        public const double NU_E = 0.5;

        public const int Z_MIN = 0;
        public const int Z_MAX = 15;
        public const double Z_E = 0.1;

        public const int Zi_MIN = 0;
        public const int Zi_MAX = 15;
        public const double Zi_E = 0.1;

        //NU=0:0.5:12
        //Z=0:0.1:15
        //Zi=0:0.1:15
        public const int N_NU = 25;
        public const int N_Z = 151;
        public const int N_Zi = 151;

        public const int N_NU_Y = 31;
        public const int N_Z_Y = 101;
        public const int N_Zi_Y = 101;
    }
}