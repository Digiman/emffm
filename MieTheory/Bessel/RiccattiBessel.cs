﻿using System;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;

namespace MieTheory.Bessel
{
    /// <summary>
    ///     Функции Риккатти-Бесселя.
    /// </summary>
    public static class RiccattiBessel
    {
        /// <summary>
        ///     Riccati-Bessel function of the first kind.
        ///     S=RICBESJ(NU,Z)is the Riccati-Bessel function of the first kind for Z
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetRicbesJ(double nu, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = Complex.Sqrt(Math.PI * z / 2.0) * Bessel.GetBesselJ(nu + 0.5, z);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }

        /// <summary>
        ///     Riccati-Besselfunction of the second kind.
        ///     C=RICBESY(NU,Z)is the Riccati-Bessel function of the second kind for Z
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetRicbesY(double nu, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = -Complex.Sqrt(Math.PI*z/2.0)*Bessel.GetBesselY(nu + 0.5, z);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }

        /// <summary>
        ///     Riccati-Bessel function of the third kind
        ///     XI=RICBESH(NU,K,Z),for K=1 or 2,computes the Riccati-Bessel
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="k"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetRicbesH(double nu, int k, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = Complex.Sqrt(Math.PI * z / 2.0) * Bessel.GetBesselH(nu + 0.5, k, z);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }
    }
}