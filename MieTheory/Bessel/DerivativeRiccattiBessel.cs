﻿using System;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;

namespace MieTheory.Bessel
{
    /// <summary>
    /// Класс для опиания приведенных функций Риккатти-Бесселя.
    /// </summary>
    public static class DerivativeRiccattiBessel
    {
        /// <summary>
        ///     DRICBESH First derivative of the Riccati-Bessel function of the third kind.
        ///     XI = DRICBESH(NU,K,Z),for K=1 or 2,computes the first derivative
        ///     of the Riccati-Bessel function of the third kind for each element of
        ///     the complex array Z.
        ///     XI=DRICBESH(NU,Z) uses K=1.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="k"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetDricbesH(double nu, int k, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = 0.5 * Complex.Sqrt(Math.PI / 2.0 / z) * Bessel.GetBesselH(nu + 0.5, k, z) +
                    Complex.Sqrt(Math.PI * z / 2.0) * DerivativeBessel.GetDbesselH(nu + 0.5, k, z);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }

        /// <summary>
        ///     DRICBESY First derivative of the Ricatti-Bessel function of the second kind.
        ///     C = DRICBESY(NU,Z) is thefirstderivativeoftheRiccati-Bessel
        ///     function of the second kind foreach element of the complex array Z.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetDricbes(double nu, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = -0.5 * Complex.Sqrt(Math.PI / 2.0 / z) * Bessel.GetBesselY(nu + 0.5, z) -
                    Complex.Sqrt(Math.PI * z / 2.0) * DerivativeBessel.GetDbesselY(nu + 0.5, z);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }

        /// <summary>
        ///     DRICBESJ First derivative of the Riccati-Bessel function of the first kind.
        ///     S=DRICBESJ(NU,Z)is the first derivative of the Riccati-Bessel
        ///     function of the first kind for each element of the complex array Z.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetDricbesJ(double nu, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = 0.5 * Complex.Sqrt(Math.PI / 2.0 / z) * Bessel.GetBesselJ(nu + 0.5, z) +
                    Complex.Sqrt(Math.PI * z / 2.0) * DerivativeBessel.GetDbesselJ(nu + 0.5, z);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }
    }
}