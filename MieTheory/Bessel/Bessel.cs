﻿using System;
using System.Numerics;
using MieTheory.Common;
using MieTheory.Helpers;

namespace MieTheory.Bessel
{
    /// <summary>
    /// Класс для расчета функций Бесселя.
    /// </summary>
    public static class Bessel
    {
        public static Complex GetBesselJ(double nu, Complex z)
        {
            int NU, Z, Zi;
            CalculateIndexes(nu, z, out NU, out Z, out Zi);

            if (NU < Variables.N_NU && Z < Variables.N_Z && Zi < Variables.N_Zi)
                return BesselDataInMemory.BesselJ[NU, Z, Zi];

            throw new Exception(String.Format("BESSELJ::input data is not handling for current nu({0}) and z {1}", nu, z));
        }

        public static Complex GetBesselY(double nu, Complex z)
        {
            int NU, Z, Zi;
            CalculateIndexes(nu, z, out NU, out Z, out Zi);

            if (NU < Variables.N_NU_Y && Z < Variables.N_Z_Y && Zi < Variables.N_Zi_Y)
                return BesselDataInMemory.BesselY[NU, Z, Zi];

            throw new Exception(String.Format("BESSELY::input data is not handling for current nu({0}) and z {1}", nu, z));
        }

        public static Complex GetBesselH(double nu, Complex z)
        {
            int NU, Z, Zi;
            CalculateIndexes(nu, z, out NU, out Z, out Zi);

            if (NU < Variables.N_NU && Z < Variables.N_Z && Zi < Variables.N_Zi)
                return BesselDataInMemory.BesselH[NU, Z, Zi];

            throw new Exception(String.Format("BESSELH::input data is not handling for current nu({0}) and z {1}", nu, z));
        }

        public static Complex GetBesselH(double nu, int k, Complex z)
        {
            int NU, Z, Zi;
            CalculateIndexes(nu, z, out NU, out Z, out Zi);

            if (NU < Variables.N_NU && Z < Variables.N_Z && Zi < Variables.N_Zi)
            {
                if (k == 1)
                {
                    return BesselDataInMemory.BesselH[NU, Z, Zi];
                }
                else
                {
                    return BesselDataInMemory.BesselH1[NU, Z, Zi];
                }
            }

            throw new Exception(String.Format("BESSELH::input data is not handling for current nu({0}) and z {1}", nu, z));
        }

        private static void CalculateIndexes(double nu, Complex z, out int NU, out int Z, out int Zi)
        {
            NU = ComplexHelper.Round((nu - Variables.NU_MIN)/Variables.NU_E);
            Z = ComplexHelper.Round((z.Real - Variables.Z_MIN)/Variables.Z_E);
            Zi = ComplexHelper.Round((z.Imaginary - Variables.Zi_MIN)/Variables.Zi_E);
        }
    }
}
