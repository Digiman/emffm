﻿using System;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;

namespace MieTheory.Bessel
{
    public static class DerivativeBessel
    {
        /// <summary>
        /// First derivative of the Bessel function of the third kind.
        /// H = DBESSELH(NU,K,Z), for K=1 or 2, computes the derivative of the
        /// Hankel function, dH?(K)NU(Z)/dZ, for each element of the complex array Z.
        /// 
        /// H = DBESSELH(NU,Z)uses K=1.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="k"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetDbesselH(double nu, int k, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = 0.5*(Bessel.GetBesselH(nu - 1, k, z) - Bessel.GetBesselH(nu + 1, k, z));
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }

        /// <summary>
        /// DBESSELJ First derivative of the Bessel function of the first kind.
        /// J=DBESSELJ(NU,Z)is First derivative of the Bessel function of the first kind,
        /// dJNU(Z)/dZ.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetDbesselJ(double nu, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = 0.5*(Bessel.GetBesselJ(nu - 1, z) - Bessel.GetBesselJ(nu + 1, z));
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }

        /// <summary>
        /// DBESSELJ First derivative of the Bessel function of the second kind.
        /// J=DBESSELJ(NU,Z)is First derivative of the Bessel function of the second kind,
        /// 
        /// YNU(Z)/dZ.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex GetDbesselY(double nu, Complex z)
        {
            Complex S = Complex.Zero;
            try
            {
                S = 0.5*(Bessel.GetBesselY(nu - 1, z) - Bessel.GetBesselY(nu + 1, z));
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return S;
        }
    }
}
