﻿using System;
using System.Numerics;
using EMFFM.Common.Enums;
using EMFFM.Common.Helpers;

namespace MieTheory.Bessel
{
    /// <summary>
    ///     Сферическая функция Бесселя.
    /// </summary>
    public static class SphericalBessel
    {
        /// <summary>
        ///     SBESSELH Spherical Bessel function of the third kind.
        ///     H=SBESSELH(NU,K,Z),for K=1or 2, computes the spherical Bessel
        ///     function of the third kind,h?(K)NU(Z)/dZ.
        ///     H=SBESSELH(NU,Z)usesK=1.
        /// </summary>
        /// <param name="nu"></param>
        /// <param name="k"></param>
        /// <param name="z"></param>
        /// <returns>Возвращает значение сферической функции Бесселя.</returns>
        public static Complex GetSbesselH(double nu, int k, Complex z)
        {
            Complex s = Complex.Zero;
            try
            {
                s = RiccattiBessel.GetRicbesH(nu, k, z)/z;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogMessageType.Error, e.Message);
            }
            return s;
        }
    }
}