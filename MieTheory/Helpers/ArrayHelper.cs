﻿using System;
using System.Numerics;

namespace MieTheory.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с векторами (одномерными массивами)
    /// </summary>
    static class ArrayHelper
    {
        /// <summary>
        /// Инициализация вектора
        /// </summary>
        /// <typeparam name="TType">Тип значений для элементов вектора</typeparam>
        /// <param name="size">Размерность вектора</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает значение вектора</returns>
        public static TType[] InitArray<TType>(int size, TType value)
        {
            var array = new TType[size];

            for (int i = 0; i < size; i++)
            {
                array[i] = value;
            }

            return array;
        }

        /// <summary>
        /// Инициализация массива значениями в диапазоне
        /// </summary>
        /// <param name="startValue">Начальное значение</param>
        /// <param name="endValue">Конечное значение</param>
        /// <param name="stepValue">Шаг</param>
        /// <returns>Возвращает иициализированный массив</returns>
        public static int[] InitArray(int startValue, int endValue, int stepValue)
        {
            int size = (endValue - startValue)/stepValue;
            var array = new int[size];

            int value = startValue;
            for (int i = 0; i < size; i++)
            {
                array[i] = value;
                value += stepValue;
            }

            return array;
        }

        /// <summary>
        /// Инициализация массива значениями в диапазоне
        /// </summary>
        /// <param name="startValue">Начальное значение</param>
        /// <param name="endValue">Конечное значение</param>
        /// <param name="stepValue">Шаг</param>
        /// <returns>Возвращает иициализированный массив</returns>
        public static double[] InitArray(double startValue, double endValue, double stepValue)
        {
            int size = Convert.ToInt32((endValue - startValue)/stepValue);
            var array = new double[size];

            double value = startValue;
            for (int i = 0; i < size; i++)
            {
                array[i] = value;
                value += stepValue;
            }

            return array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static double[] Devide(double value, double[] array)
        {
            for (int i=0; i<array.Length; i++)
            {
                array[i] = value/array[i];
            }

            return array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="value"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static double[] Devide(Func<double, double> operation, double value, double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = operation(value/array[i]);
            }

            return array;
        }

        /// <summary>
        /// Выполненение операции деления в аргументах функции
        /// </summary>
        /// <param name="operation">Функция</param>
        /// <param name="value">Значение для функции (числитель)</param>
        /// <param name="array">Массив (значения в знаменателе)</param>
        /// <returns>Возвращает новый массив со знаяениями, вычисленными через функцую</returns>
        public static Complex[] Devide(Func<Complex, Complex> operation, double value, Complex[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = operation(value / array[i]);
            }

            return array;
        }

        /// <summary>
        /// Перемножение почленное элементов массива на значение
        /// </summary>
        /// <param name="array">Массив</param>
        /// <param name="value">Значение</param>
        /// <returns>Возвращает обработанный массив</returns>
        public static double[] Multiply(double[] array, double value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = value*array[i];
            }

            return array;
        }
    }
}
