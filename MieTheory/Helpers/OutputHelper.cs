﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Numerics;
using EMFFM.Common.Extensions;
using EMFFM.Common.FEM.Complex.Elements;
using EMFFM.Math.Elements;

namespace MieTheory.Helpers
{
    /// <summary>
    /// Вспомогательный класс для вывода в файлы рассчитанных данных
    /// </summary>
    static class OutputHelper
    {
        /// <summary>
        /// Вывод значений угловых функций в файл
        /// </summary>
        /// <param name="functions">Список значений</param>
        /// <param name="fname">Имя файла для вывода</param>
        public static void OutputAngularFunctions(List<AngularFunction> functions, string fname)
        {
            var file = new StreamWriter(fname);

            foreach (var function in functions)
            {
                file.WriteLine("p = {0}, t = {1}", function.Pi, function.Tau);
            }

            file.Close();
        }

        /// <summary>
        /// Вывод комплексного значения 
        /// </summary>
        /// <param name="value">Комплексное значение</param>
        /// <param name="fname">Имя и путь к файлу для вывода</param>
        public static void OutputComplexValue(Complex value, string fname)
        {
            var file = new StreamWriter(fname);
            
            file.WriteLine(value.ToString());

            file.Close();
        }

        /// <summary>
        /// Вывод массива комплексных значений в текстовый файл
        /// </summary>
        /// <param name="values">Массив комплексных значений</param>
        /// <param name="fname">Имя и путь к файлу для вывода</param>
        public static void OutputComplexValues(Complex[] values, string fname)
        {
            var file = new StreamWriter(fname);

            for (int i = 0; i < values.Length; i++)
            {
                values[i].ToString();
            }

            file.Close();
        }

        /// <summary>
        /// Вывод матрицы комплексных чисел в текстовый файл
        /// </summary>
        /// <param name="values">Матрица комплексных чисел</param>
        /// <param name="fname">Имя и путь к файлу для вывода</param>
        public static void OutputComplexValues(Complex[,] values, string fname)
        {
            var file = new StreamWriter(fname);

            for (int i = 0; i < values.GetLength(0); i++)
            {
                for (int j = 0; j < values.GetLength(1); j++)
                {
                    values[i, j].ToString();
                }
            }

            file.Close();
        }

        /// <summary>
        /// Сохранение матрицы с данными в текстовый файл.
        /// </summary>
        /// <typeparam name="TResultsValue">Тип данных, в которых хранятся результаты анализа.</typeparam>
        /// <param name="filename">Имя файла, куда сохраняется результат.</param>
        /// <param name="matrix">Сама матрица с данными.</param>
        public static void SaveMatrixToFile<TResultsValue>(string filename, TResultsValue[,] matrix)
        {
            var file = new StreamWriter(filename);

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    file.Write("{0:e} ", (matrix[i, j] as double?).Value.ToString("E", CultureInfo.InvariantCulture));

                    //var value = matrix[i, j].ToString();
                    //file.Write("{0}", value.CorrectDoubleStringValueForMatlab());
                }

                file.WriteLine();
            }

            file.Close();
        }

        /// <summary>
        /// Сохранение данных в виде вектора (одномерного массива) в файл.
        /// </summary>
        /// <typeparam name="TValue">Тип данных элементов массива.</typeparam>
        /// <param name="filename">Имя файла для сохранения данных.</param>
        /// <param name="array">Массив с данными для записи в файл.</param>
        public static void SaveArrayToFile<TValue>(string filename, TValue[] array)
        {
            var file = new StreamWriter(filename);

            for (int i = 0; i < array.Length; i++)
            {
                file.WriteLine("{0}", (array[i] as double?).Value.ToString("E", CultureInfo.InvariantCulture));
            }

            file.Close();
        }
    }
}
