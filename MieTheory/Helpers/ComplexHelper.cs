﻿using System;
using System.Numerics;

namespace MieTheory.Helpers
{
    /// <summary>
    /// Вспомогательный класс для работы с System.Numerics.Complex
    /// </summary>
    static class ComplexHelper
    {
        public static Complex RealMultiply(double value, Complex complex)
        {
            return new Complex(value*complex.Real, value*complex.Imaginary);
        }

        /// <summary>
        /// Конвертация массива в массив
        /// </summary>
        /// <param name="vector">Исходный массив</param>
        /// <returns>Возвращает сконвертированный массив</returns>
        public static Complex[] Convert(double[] vector)
        {
            var result = new Complex[vector.Length];

            for (int i = 0; i < vector.Length; i++)
            {
                result[i] = new Complex(vector[i], 0);
            }

            return result;
        }

        /// <summary>
        /// Округление.
        /// </summary>
        /// <param name="value">Значение для округления.</param>
        /// <returns>Возвращает округленное значение.</returns>
        public static int Round(double value)
        {
            return (int) Math.Floor(value + 0.5);
        }
    }
}
