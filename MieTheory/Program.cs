﻿using System;
using MieTheory.Samples;

namespace MieTheory
{
    /// <summary>
    /// Главный класс консольного приложения
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Метод для запуска тестовыз функций
        /// </summary>
        private static void Test()
        {
            TestAngularFunctions.Test();

            MaterialsSamples.Test2();

            MaterialsSamples.Test3();
        }

        /// <summary>
        /// Главный метод консольного приложения
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            Test();

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
