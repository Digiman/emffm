﻿using MieTheory.Functions;
using MieTheory.Helpers;

namespace MieTheory.Samples
{
    /// <summary>
    /// Класс для тестирования угловых функций расчета
    /// </summary>
    public static class TestAngularFunctions
    {
        /// <summary>
        /// Темтирование вычисления знакчения круговой функции.
        /// </summary>
        public static void Test()
        {
            double theta = 10;
            int n = 4;

            var res = MieFunctions.CalculatePiAndTau(theta, n);

            OutputHelper.OutputAngularFunctions(res, @"output\angular.txt");
        }
    }
}
