﻿using System;
using EMFFM.Common.Enums;
using EMFFM.Common.Materials;

namespace MieTheory.Samples
{
    /// <summary>
    /// Класс для тестирования расчетов параметров материала по законам Друде Лоренца.
    /// </summary>
    static class MaterialsSamples
    {
        /// <summary>
        /// Выполнение расчета диэлектрической проницаемости по закону Друде
        /// Входные данные:
        ///     - длина волны (Lambda, нм),
        ///     - тип материала (Ag),
        ///     - тип модели (Друде).
        /// </summary>
        public static void Test1()
        {
            var lambda = 405*1e-9;
            var ld = new LorenzDrude(lambda);
            var eps = ld.Calculate(MaterialTypes.Ag, ModelTypes.D);

            Console.WriteLine("Epsilon: {0}", eps);
        }

        /// <summary>
        /// Выполнение расчета диэлектрической проницаемости по закону Друде
        /// Входные данные:
        ///     - длина волны (Lambda, нм),
        ///     - тип материала (Ag),
        ///     - тип модели (Друде),
        ///     - комплексное преломление частицы.
        /// </summary>
        public static void Test2()
        {
            var lambda = 405 * 1e-9;
            var ld = new LorenzDrude(lambda);
            var eps = ld.Calculate(MaterialTypes.Ag, ModelTypes.D);

            Console.WriteLine("Epsilon: {0}", eps);
            Console.WriteLine("Reflective index: {0}", ld.CalculateRefractiveIndex(eps));
        }

        /// <summary>
        /// Выполнение расчета диэлектрической проницаемости по закону Лоренца Друде
        /// Входные данные:
        ///     - длина волны (Lambda, нм),
        ///     - тип материала (Ag),
        ///     - тип модели (Лоренц - Друде),
        ///     - комплексное преломление частицы.
        /// </summary>
        public static void Test3()
        {
            var lambda = 405 * 1e-9;
            var ld = new LorenzDrude(lambda);
            var eps = ld.Calculate(MaterialTypes.Ag, ModelTypes.LD);

            Console.WriteLine("Epsilon: {0}", eps);
            Console.WriteLine("Reflective index: {0}", ld.CalculateRefractiveIndex(eps));
        }
    }
}
