﻿using System.Net.Configuration;
using MieTheory.Common;
using MieTheory.Input;

namespace MieTheory
{
    /// <summary>
    /// Интерфейс для реализация фасада для работы с методами библиотеки по теории Ми.
    /// </summary>
    public interface IMieFacade
    {
        /// <summary>
        /// Параметры задачи и работ приложения.
        /// </summary>
        InputMieData Data { get; set; }

        /// <summary>
        /// Результаты решения задачи.
        /// </summary>
        MieFullResults Results { get; set; }

        /// <summary>
        /// Загрузка параметров работы приложения и расчетов.
        /// </summary>
        /// <param name="filename">Имя файла, откуда загружать данные.</param>
        /// <returns>Возвращает загруженные из файла данные.</returns>
        InputMieData LoadInputData(string filename);

        /// <summary>
        /// Сохранение параметров приложения.
        /// </summary>
        /// <param name="filename">Имя файла, в который нужно произвести запись данных.</param>
        /// <param name="data">Данные приложения для сохранения в файл.</param>
        void SaveInputData(string filename, InputMieData data);

        /// <summary>
        /// Выполнение расчета.
        /// </summary>
        void Calculate();

        /// <summary>
        /// Экспорт результатов решения в файлы.
        /// </summary>
        /// <param name="applicationPath">Полный путь к приложению, откуда оно запущено.</param>
        void ExportResults(string applicationPath);

        /// <summary>
        /// Выполнение решения (расчет и экспорт результатов).
        /// </summary>
        /// <param name="applicationPath">Полный путь к приложению, откуда оно запущено.</param>
        /// <param name="withExport">Экспортировтаь ли результаты сразу после решения?</param>
        void Solve(string applicationPath, bool withExport = false);
    }
}